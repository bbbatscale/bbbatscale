.. role:: python(code)
   :language: python


######################
Coding style guideline
######################

This document describes guidelines regarding our coding style
by presenting our understanding of the application structure.
It also serves as a collection of good and bad solutions,
which we have recognized during the development.

This document is split into four section which correspond to our architectural
layers. They consist of the three main layers model, view, template (MVT) as
they are predetermined by Django plus the additional service layer, which holds
business logic.

Each layer is explained by pointing out its purpose and used technologies as
well as dos and don'ts.

.. note::
   These guidelines do not claim to be complete or absolute, but they are based
   on a common agreement of the core development team. Therefore, they should be
   considered by everyone while introducing changes to the code base.

   If you have to break with or miss a guideline, consider to add your solution
   here so that everyone can profit from your finding.

********
Template
********

Purpose
=======

The template layer serves the purpose of determining the look and feel of the
graphical user interface and displaying data.
It takes care of concerns like "Where is data placed in the web page?"
and "How is data displayed regarding styles?".

Implementation technologies
===========================

* HTML 5
* Bootstrap 4
* Vanilla JavaScript ES6
* `Django Templates`_

Dos
===

Include tag
-----------

You can define HTML blocks which can be reused in repetitive cases.This is
realized with the Django template `include tag`_.

A HTML component is a single file with only one HTML fragment, not a complete
HTML file with header and body.

.. code-block:: htmldjango

   <!-- BBBatScale/core/templates/meeting_detail_entry.html-->
   <a href="{{ entry.url }}"
      class="{% if single_entry %}form-control col-lg-8 {% endif %}text-primary{% if entry.value is None %} font-italic{% endif %}">
       {% if entry.value is not None %}
         {{ entry.value }}
       {% else %}
         {% translate "Unknown" %}
       {% endif %}
   </a>

This HTML component can be included via the `include tag`_. You can pass data to
the HTML component using the keyword `with`:code:.

.. code-block:: htmldjango

   <!--BBBatScale/core/templates/meeting_detail.html-->
   {% include "meeting_detail_entry.html" with entry=detail single_entry=True %}

Forms
-----

Forms are a common way to pass data from the user to the application. We use
`Django Forms`_ to declare and handle form data and `Crispy Forms`_ to layout
forms. This approach allows to work with forms easily, because boilerplate code
is reduced.

.. code-block:: python

   # BBBatScale/core/forms.py
   class HomeRoomForm(forms.ModelForm):
       ...

An instance of a form can be passed to the `render`_ function via the context
argument. It holds all data necessary to display the form.

.. code-block:: python

   # BBBatScale/core/views/home_room.py
   def home_room_update(request, home_room):
       ...
       return render(request, "home_rooms_update.html", {"form": form})

This form instance can then be used by `Crispy Forms`_ to render the HTML form.

.. code-block:: htmldjango

   <!--BBBatScale/core/templates/home_rooms_update.html-->
   {% load crispy_forms_tags %}
   ...
   {% crispy form %}

Permission based visibility
---------------------------

We have defined the template tag `if_has_tenant_based_perms`:code: to
enable/disable the rendering of HTML blocks based on a user's permissions.

.. code-block:: htmldjango

   <!--BBBatScale/templates/base.html-->
   {% if_has_tenant_based_perms "core.view_site" elevate_staff=False %}
   <li class="nav-item">
       <a href="{% url 'tenant_overview' %}" class="nav-link">
           <em class="nav-icon fas fa-users-cog"></em>
           <p>{% translate "Tenant" %}</p>
       </a>
   </li>
   {% endif_has_tenant_based_perms %}

Don'ts
======

* Within the templates there should be only a bare minimum of data
  transformation. Data which is passed to the template should only include
  needed, prepared and easily accessible data.
* It is necessary to avoid complex template structures. This can happen by
  extensive usage of nested tags.

****
View
****

Purpose
=======

The view layer serves the purpose of a web interface to the application. It
takes care of incoming HTTP requests by deserializing, validating, authorizing,
... them. The view executes business logic and the result will be serializes as
an HTTP response.

Implementation technologies
===========================

* `Django Views`_
* `Django Forms`_
* `Crispy Forms`_

Dos
===

Authorization
-------------

Authorization is mainly realized through the decorator
`tenant_based_permission_required`:code:.
It evaluates the user's role, permissions and tenancy to grant access.

.. code-block:: python

    # BBBatScale/core/decorators.py
    def tenant_based_permission_required(
        perm: Union[str, Iterable[str]],
        login_url=None,
        redirect_field_name=REDIRECT_FIELD_NAME,
        raise_exception=False,
        *,
        elevate_staff=True,
    ) -> Callable:
        ...

.. code-block:: python

   # BBBatScale/core/views/meetings.py
   @login_required
   @tenant_based_permission_required("core.view_meeting", raise_exception=True)
   def meetings_overview(request: HttpRequest) -> HttpResponse:
       ...

Request handling
----------------

There are various ways to define the handling of request data. In this sections
we present you our way.

Path parameters
^^^^^^^^^^^^^^^

Data can be put into the request URL path. This is especially useful to address
a specific entity by an identifier. These parameters are required and can not be
left out.

.. code-block:: python

   # BBBatScale/core/urls.py
   path("user/delete/<int:user>", users.user_delete, name="user_delete"),

The identifier is passed to the according view function as additional argument.

.. code-block:: python

   # BBBatScale/core/views/users.py
   def user_delete(request, user):
       ...

Query parameters
^^^^^^^^^^^^^^^^

Data can also be put into the query part of an URL. They behave like key value
pairs which are accessible by the request object. Since these are optional by
design, ensure that the underlying functionality works even if these query
parameters are not present.

.. code-block:: python

   # BBBatScale/core/views/home.py
   def get_rooms(request: HttpRequest) -> HttpResponse:
       ...
       search: str = request.GET.get("filter")
       ...

Form data
^^^^^^^^^

Data can be transferred as form data. This is realized and handled by
`Django forms`_.

.. code-block:: python

    # BBBatScale/core/forms.py
    class SchedulingStrategyForm(forms.ModelForm):
        class Meta:
            model = SchedulingStrategy
            fields = [
                "name",
                "scheduling_strategy",
                "description",
                "notifications_emails",
                "tenants",
            ]
        ...

An instance of a Django form is easily populated by passing the request data.

.. code-block:: python

    # BBBatScale/core/views/scheduling_strategies.py
    def scheduling_strategy_create(request):
        ...
        form = SchedulingStrategyForm(request.POST or None)
        ...

JSON body
^^^^^^^^^

There are also cases where the data is put into the body of the request and
serialized as JSON. The body can be parsed into a plain dictionary with python's
`json`:code: library.

.. code-block:: python

   # BBBatScale/core/views/api.py
   def api_server_registration(request):
       ...
       data = json.loads(request.body)


Execution of business logic
---------------------------

Within the view function business logic is executed by calling according service
functions.

In the following example you see a view function which executes two separate
service functions. Both functions are needed to fulfill the request, but
conceptually they are two different business requirements.

.. code-block:: python

   # BBBatScale/core/services.py
   def register_server(registration_details: RegistrationDetails) -> bool:
       ...

.. code-block:: python

   # BBBatScale/core/services.py
   def get_interval(
       agent_config_selector: AgentConfigurationSelector, interval_select: AgentIntervalSelector
   ) -> Optional[timedelta]:
       ...

.. code-block:: python

   # BBBatScale/core/views/api.py
   def api_server_registration(request):
       ...
       server_created = register_server(RegistrationDetails.from_api(communication_token, data))
       ...
       interval = get_interval(apitoken_agent_config_selector(communication_token), select_registration_interval)
       ...

Response serialization
----------------------

Rendering of templates
^^^^^^^^^^^^^^^^^^^^^^

A response can be a fully rendered HTML. This is achieved by using the
`render`_ function.

.. code-block:: python

   # BBBatScale/core/views/api_token.py
   def token_overview(request):
       tokens = ApiToken.objects.all()
       return render(
           request,
           "api_token_overview.html",
           {"tokens": tokens},
       )

Redirect
^^^^^^^^

You can redirect a request by simply using the `redirect`_ function.

.. code-block:: python

   # BBBatScale/core/views/users.py
   def user_create(request):
       ...
       return redirect("users_overview")

JsonResponse
^^^^^^^^^^^^

A response can contain JSON in the body which then can be consumed by the
requesting client. In this case we make use of the `JsonResponse`:code:. It is
constructed by passing a plain python dictionary as `data`:code: and a
meaningful HTTP status via the `status`:code: keyword argument.

.. code-block:: python

   # BBBatScale/core/views/api.py
   def api_server_registration(request):
       ...
       return JsonResponse(
           data={"interval": interval.total_seconds() if interval else None, "created": server_created},
           status=200,
       )

Don'ts
======

* Do not create database queries directly within the view function.

*******
Service
*******

Purpose
=======

The service layer encapsulates the business processes and rules of the
application. While the other layers deal with requirements on a very technical
level, this layer implements business requirements on a more abstract and less
technical level. E.g. this layer does not care whether it was invoked by an web
request or from the command line. In the same sense, it is irrelevant for the
layer in which form or how data is stored.

In a way the service layer acts as a executable documentation of all business
processes that distinguishes BBB@Scale from other solutions.

Implementation technologies
===========================

* mostly plain python
* `Dataclasses`_

  * They represent ephemeral data objects.
  * They can help with typing in form of specialized argument types and return
    types.


Dos
===

Function declaration
--------------------

Functions should be at best self documenting and should reflect the common
language of the domain.

.. code-block:: python

   # BBBatScale/core/services.py
   def register_server(registration_details: RegistrationDetails) -> bool:
       ...

Dataclasses
-----------

`Dataclasses`_ help to provide a better context to functions and enhance the
function signature as they encapsulate data that is used within a service
function.

.. code-block:: python

   # BBBatScale/core/services.py
   @dataclass
   class RegistrationDetails:
       scheduling_strategy: SchedulingStrategy

       dns: str
       machine_id: str
       shared_secret: str

       @classmethod
       def from_api(cls, communication_token: ApiToken, data: dict) -> "RegistrationDetails":
           return cls(
               dns=data["hostname"],
               machine_id=data["machine_id"],
               shared_secret=data["shared_secret"],
               scheduling_strategy=communication_token.scheduling_strategy,
           )
       ...

Dependency injection
--------------------

Service functions can depend on external services. The interface of those
services should be wrapped in a class. This class can then later be used to
communicate with the service in a, for BBB@Scale, standardized way so that we
can easily exchange one service with another similar one by just switching out
the class instance without further code changes. This comes in handy for
example when supporting multiple BigBlueButton API versions. For every API
version there should exist a class with the same method set as the other ones
but with potentially different implementation. These classes will then be
instantiated and passed to a service function which then only call the methods
without worrying about wich API version it communicates with. In some cases the
service function needs to pass initial parameters to the wrapper class in this
case a factory can be passed to the service factory or the class itself without
initiating it.

.. code-block:: python

   # BBBatScale/core/utils.py
   class BigBlueButton(ABC):
       def __init__(self, bbb_server_url: str, bbb_server_salt: str) -> None:
           ...

   class BigBlueButton24(BigBlueButton):
       ...

A `factory`_ function (or the class itself) or an already created object can
be passed to a service function.

.. code-block:: python

   # BBBatScale/core/services.py
   def collect_server_stats(bbb_api_factory: Callable[[str, str], BigBlueButton]) -> None:
       ...
       bbb_api = bbb_api_factory(server.dns, server.shared_secret)
       ...

.. code-block:: python

   # BBBatScale/core/management/commands/collect_server_stats.py
   class Command(BaseCommand):
       def handle(self, *args: Any, **options: Any) -> None:
           ...
           collect_server_stats(BigBlueButton24)
           ...

Single level of abstraction
---------------------------

The `Single Level of Abstraction`_ principle helps to split functions. A
function handles always only one detail and calls further functions for further
details.

.. code-block:: python

  # BBBatScale/core/services.py
  def sync_meeting_stats(server: Server, m: MeetingStats) -> None:
      ...

.. code-block:: python

   # BBBatScale/core/services.py
   def sync_server_stats(server: Server, meeting_stats: List[MeetingStats]):
       for m_stats in meeting_stats:
           sync_meeting_stats(server, m_stats)

       if server.state != SERVER_STATE_DISABLED:
           server.state = SERVER_STATE_UP
       server.last_health_check = datetime.datetime.now()
       server.save()

The `sync_server_stats`:code: function takes care of a server, while the
`sync_meeting_stats`:code: takes care of a meeting.

Django model manager
--------------------

With the help of a custom Django manager you can define methods which
encapsulates complex or often used logic like creating an object or query sets.
Those methods can then be accessed by the manager for example
`User.objects.create_superuser("johndoe", "johndoe@example.org",
"super-password!", tenant, ...)`:python:

Don'ts
======

* The service layer should not deal with technical aspects like request
  objects. The calling function should rather provide the unpacked and prepared
  data to the service layer.
* Avoid working with generic data structs like plain dictionaries. Better use
  representative data structures that provide a meaningful context to the
  encapsulated data.

*****
Model
*****

Purpose
=======

A model defines database entities which are needed in the application. Models
are the base upon which the whole application is build. Therefore this layer
takes care of several aspects about a specific entity.

* They define fields/attributes which should be persisted in the database.
* Relations between entities like one to one, one to many and so on are
  defined by the model.
* Derived data can be provided by adding model specific methods or property
  methods.
* Custom model managers can be specified to get access to prepared query sets
  and abstracted/higher level functions.


Implementation technologies
===========================

* Django models
* Django queryset
* Django manager

Dos
===

Normalization
-------------

The database schema and therefore the models should conform the `normalized`_
form of the relational model. De-normalization should only be used in specific
cases, where e.g. it improves performance.

Allowing empty `CharField`:code: and `TextField`:code:
------------------------------------------------------

|CharField s|_ and |TextField s|_ declare the persistence of a string. In the
case that the field should be able to be empty `null=True`:python: should
**not** be specified, but instead `blank=True`:python:. The only exception
where both should be `True`:python: is when also `unique=True`:python: has been
set, since otherwise there could only be one empty entry. See also the Django
docs regarding the `null option`_.

Model methods
-------------

Methods for models are useful where derived data or additional computations
based on attributes of the entity are required. In some cases a method might be
superior to an additional persisted field, especial to prevent de-normalization.

.. code-block:: python

   # BBBatScale/core/models.py
   def get_utilization(self):
       audio_video_factor = self.participant_count_max / self.videostream_count_max
       audio_usage = self.get_participant_count() / audio_video_factor
       video_usage = self.get_videostream_count()
       total_usage = audio_usage + video_usage

       return 0 if total_usage <= 0 else total_usage or True

A model method can also be used to declare an interaction. This interaction
is an abstraction on the change of internal data. Within this method you
validate passed arguments and may change the values of one or more attributes.

Don'ts
======

* Even though a model has relations to another model, it should be avoided to
  extensively use these relations to access related models. E.g. you should not
  chain models like `model_a.model_b.model_c.model_d.attribute`:code:. It is better to
  make use of a Django model manager / query sets like
  `ModelC.objects.get(model_d__model_b__model_a=model_a).attribute`:code:. This
  reduces database access and therefore speeds up the process. The first would
  query the database three times, the later one only one time.



.. |CharField s| replace:: `CharField`:code:\ s
.. |TextField s| replace:: `TextField`:code:\ s

.. _`include tag`: https://docs.djangoproject.com/en/4.0/ref/templates/builtins/#include
.. _`Crispy Forms`: https://django-crispy-forms.readthedocs.io/en/latest/
.. _JsonResponse: https://docs.djangoproject.com/en/4.0/ref/request-response/#jsonresponse-objects
.. _render: https://docs.djangoproject.com/en/4.0/topics/http/shortcuts/#render
.. _`Django Forms`: https://docs.djangoproject.com/en/4.0/topics/forms/
.. _normalized: https://en.wikipedia.org/wiki/Database_normalization
.. _`CharField s`: https://docs.djangoproject.com/en/4.0/ref/models/fields/#charfield
.. _`TextField s`: https://docs.djangoproject.com/en/4.0/ref/models/fields/#textfield
.. _`null option`: https://docs.djangoproject.com/en/4.0/ref/models/fields/#null
.. _Dataclasses: https://realpython.com/python-data-classes/
.. _`Django Templates`: https://docs.djangoproject.com/en/4.0/topics/templates/
.. _factory: https://refactoring.guru/design-patterns/factory-method/python/example
.. _`Django Views`: https://docs.djangoproject.com/en/4.0/topics/http/views/