ARG VERSION=stable
ARG BBBATSCALE_IMAGE

FROM $BBBATSCALE_IMAGE AS bbbatscale

FROM nginx:${VERSION}-alpine
COPY --from=bbbatscale /bbbatscale/staticfiles/ /var/www/bbbatscale/static/

COPY 10-enable-media.sh 10-select-config.sh 30-envsubst-on-include-templates.sh /docker-entrypoint.d/
RUN rm /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh

COPY nginx.conf /etc/nginx/
COPY conf.d/ /etc/nginx/conf.d-available/

COPY include/default_options.nginx /etc/nginx/include/default_options
COPY include/http_options.nginx.template /etc/nginx/templates-include/http_options.template
COPY include/https_options.nginx.template /etc/nginx/templates-include/https_options.template
COPY include/https_redirect.nginx /etc/nginx/include/https_redirect
COPY include/locations_media.nginx.template /etc/nginx/templates-include/locations_media.template.disabled
COPY include/locations_static.nginx /etc/nginx/include/locations_static
COPY include/locations_unprotected.nginx /etc/nginx/include/locations_unprotected
COPY include/map_connection_upgrade.nginx /etc/nginx/include/map_connection_upgrade
COPY include/proxy.nginx.template /etc/nginx/templates-include/proxy.template

RUN rm /etc/nginx/conf.d/* \
    && chmod 0777 /etc/nginx/conf.d /etc/nginx/include /etc/nginx/templates-include \
    && touch /etc/nginx/include/additional_server_config \
    && chmod 0666 /etc/nginx/include/additional_server_config

USER nginx:nginx

ENV NGINX_X_FORWARDED_PROTO=\$scheme

EXPOSE 8080
EXPOSE 8443

STOPSIGNAL TERM
