FROM python:3.9-slim

COPY Pipfile Pipfile.lock /

RUN apt-get update \
    && apt-get install -y gettext \
    && pip install pipenv && pipenv install --system --deploy \
    && rm -rf /var/lib/apt/lists/*


WORKDIR /bbbatscale
COPY Licenses/ /bbbatscale/Licenses/
COPY BBBatScale/ /bbbatscale/

ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH=/bbbatscale
ENV DJANGO_SETTINGS_MODULE=BBBatScale.settings.production

COPY docker/bbbatscale/bbbatscale.py /usr/local/bin/bbbatscale

VOLUME /bbbatscale/media

RUN DJANGO_SETTINGS_MODULE=BBBatScale.settings.testing sh -c "bbbatscale compile-messages && bbbatscale collect-statics" \
    && addgroup --system --gid 101 bbbatscale \
    && adduser --system --disabled-login --ingroup bbbatscale --home /var/bbbatscale --gecos "bbbatscale user" --shell /bin/false --uid 101 bbbatscale \
    && chown -R bbbatscale:bbbatscale /bbbatscale/

USER bbbatscale:bbbatscale

CMD ["bbbatscale", "run"]
