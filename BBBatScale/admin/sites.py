from django.contrib.admin import AdminSite as DjangoAdminSite
from django.contrib.admin.forms import AdminAuthenticationForm as DjangoAdminAuthenticationForm
from django.core.exceptions import ValidationError
from django.http import HttpRequest


class AdminAuthenticationForm(DjangoAdminAuthenticationForm):
    def confirm_login_allowed(self, user):
        super(DjangoAdminAuthenticationForm, self).confirm_login_allowed(user)
        if not user.is_superuser:
            raise ValidationError(
                self.error_messages["invalid_login"],
                code="invalid_login",
                params={"username": self.username_field.verbose_name},
            )


class AdminSite(DjangoAdminSite):
    login_form = AdminAuthenticationForm

    def has_permission(self, request: HttpRequest) -> bool:
        return request.user.is_active and request.user.is_superuser
