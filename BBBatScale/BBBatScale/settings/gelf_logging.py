import dataclasses
import enum
import logging
import os
import re
from abc import ABC, abstractmethod
from logging import LogRecord
from typing import Any, Callable, ClassVar, Dict, Literal, Optional, Tuple, Union, overload

import graypy
from utils import join_strings

from BBBatScale.settings import check_environ_contains, check_represents_true, get_import_path

logger = logging.getLogger("bbbatscale.config")


@enum.unique
class SyslogLevel(enum.IntEnum):
    EMERGENCY = 0
    ALERT = 1
    CRITICAL = 2
    ERROR = 3
    WARNING = 4
    NOTICE = 5
    INFORMATIONAL = 6
    DEBUG = 7

    @classmethod
    def get_by_log_level(cls, log_level: int) -> "SyslogLevel":
        if log_level >= logging.CRITICAL:
            return cls.CRITICAL
        elif log_level >= logging.ERROR:
            return cls.ERROR
        elif log_level >= logging.WARNING:
            return cls.WARNING
        elif log_level >= logging.INFO:
            return cls.INFORMATIONAL
        else:
            return cls.DEBUG


@dataclasses.dataclass
class GELFPayload:
    host: str
    short_message: str
    timestamp: float
    level: SyslogLevel

    version: str = "1.1"
    full_message: Optional[str] = None

    additional_fields: Dict[str, Union[str, int, float]] = dataclasses.field(default_factory=dict, init=False)

    def __getitem__(self, item: str) -> Union[str, int, float]:
        return self.additional_fields[item]

    def __setitem__(self, item: str, value: Union[str, int, float]) -> None:
        self.additional_fields[item] = value

    @staticmethod
    def _clean_additional_field_name(name: str) -> str:
        """
        Replace disallowed characters with underscores and replace repeating underscores with a single one.
        Also add the required leading underscore.
        """
        name = f"_{name}"
        name = re.sub(r"[^\w.\-]", "_", name)
        return re.sub(r"_{2,}", "_", name)

    @staticmethod
    def _clean_value(value: Any) -> Union[str, int, float]:
        if isinstance(value, bool):
            # Since bools inherit from int and GELF does not support booleans, we must handle them first.
            return "true" if value else "false"
        # Convert the value to its tested type since it may be a subtype.
        elif isinstance(value, str):
            return str(value)
        elif isinstance(value, int):
            return int(value)
        elif isinstance(value, float):
            return float(value)
        elif value is None:
            return "N/A"

        return repr(value)

    def to_dict(self) -> Dict[str, Union[str, int, float]]:
        payload = {
            "version": self._clean_value(self.version),
            "host": self._clean_value(self.host),
            "short_message": self._clean_value(self.short_message),
            "timestamp": self._clean_value(self.timestamp),
            "level": self._clean_value(self.level),
        }

        if self.full_message is not None:
            payload["full_message"] = self._clean_value(self.full_message)

        for key, value in self.additional_fields.items():
            payload[self._clean_additional_field_name(key)] = self._clean_value(value)

        return payload


class GELFHandlerMixin(ABC):
    _supports_compression: ClassVar[bool] = True
    _fixed_common_init_kwargs: ClassVar[Dict[str, Any]] = dict(
        debugging_fields=True,
        extra_fields=True,
        fqdn=False,
        localname=None,
        level_names=True,
    )

    def _render_message(self: graypy.handler.BaseGELFHandler, record: LogRecord) -> Tuple[str, Optional[str]]:
        short_message = record.getMessage().strip()
        full_message = self.format(record).strip()

        if short_message == full_message:
            return short_message, None
        else:
            return short_message, full_message

    @staticmethod
    def _add_additional_fields(payload: GELFPayload, record: LogRecord) -> None:
        skip_list = {
            "args",
            "asctime",
            "created",
            "exc_info",
            "exc_text",
            "filename",
            "funcName",
            "id",
            "levelname",
            "levelno",
            "lineno",
            "message",
            "module",
            "msecs",
            "msg",
            "name",
            "pathname",
            "process",
            "processName",
            "relativeCreated",
            "stack_info",
            "thread",
            "threadName",
        }

        for key, value in vars(record).items():
            if key not in skip_list and not key.startswith("_"):
                payload[key] = value

    def _make_gelf_dict(self: graypy.handler.BaseGELFHandler, record: LogRecord) -> Dict[str, Any]:
        short_message, full_message = self._render_message(record)

        payload = GELFPayload(
            host=self._resolve_host(self.fqdn, self.localname),
            short_message=short_message,
            full_message=full_message,
            timestamp=record.created,
            level=SyslogLevel.get_by_log_level(record.levelno),
        )

        payload["logger"] = record.name

        if self.level_names:
            payload["python_level"] = record.levelno
            payload["python_level_name"] = record.levelname

        if self.facility is not None:
            payload["facility"] = self.facility

        if self.debugging_fields:
            payload["file"] = record.pathname
            payload["line"] = record.lineno
            payload["function"] = record.funcName
            payload["module"] = record.module
            payload["process_id"] = record.process

            if record.process is not None:
                payload["process_id"] = record.process
            if record.processName is not None:
                payload["process_name"] = record.processName

            if record.thread is not None:
                payload["thread_id"] = record.thread
            if record.threadName is not None:
                payload["thread_name"] = record.threadName

        if self.extra_fields:
            self._add_additional_fields(payload, record)

        return payload.to_dict()

    @staticmethod
    def _set_from_env_if_present(
        handler_config: Dict[str, Any],
        env_key: str,
        config_key: str,
        mapper: Callable[[str], Any] = lambda value: value,
    ) -> None:
        if env_key in os.environ:
            handler_config[config_key] = mapper(os.environ[env_key])

    @classmethod
    @abstractmethod
    def configure_handler_from_env(cls, host: str, port: int) -> Dict[str, Any]:
        handler_config = {
            "class": get_import_path(cls),
            "host": host,
            "port": port,
        }

        cls._set_from_env_if_present(handler_config, "BBBATSCALE_LOGGING_GELF_FACILITY", "facility")

        if cls._supports_compression:
            cls._set_from_env_if_present(
                handler_config, "BBBATSCALE_LOGGING_GELF_COMPRESS", "compress", check_represents_true
            )
        elif "BBBATSCALE_LOGGING_GELF_COMPRESS" in os.environ:
            logger.warning(
                f"Ignoring 'BBBATSCALE_LOGGING_GELF_COMPRESS' since {get_import_path(cls)} does not support"
                " compression."
            )

        return handler_config


class GELFUDPHandler(GELFHandlerMixin, graypy.GELFUDPHandler):
    def __init__(self, host: str, port: int, *, facility: Optional[str] = None, compress: bool = True) -> None:
        super().__init__(host, port, facility=facility, compress=compress, **self._fixed_common_init_kwargs)

    @classmethod
    def configure_handler_from_env(cls, host: str, port: int) -> Dict[str, Any]:
        return super().configure_handler_from_env(host, port)


class GELFTCPHandler(GELFHandlerMixin, graypy.GELFTCPHandler):
    _supports_compression: ClassVar[bool] = False

    def __init__(self, host: str, port: int, *, facility: Optional[str] = None) -> None:
        super().__init__(host, port, facility=facility, **self._fixed_common_init_kwargs)

    @classmethod
    def configure_handler_from_env(cls, host: str, port: int) -> Dict[str, Any]:
        return super().configure_handler_from_env(host, port)


class GELFTLSHandler(GELFHandlerMixin, graypy.GELFTLSHandler):
    _supports_compression: ClassVar[bool] = False

    @overload
    def __init__(
        self,
        host: str,
        port: int,
        client_cert_file: str,
        *,
        facility: Optional[str] = None,
        client_key_file: Optional[str] = None,
        validate_server_cert: Literal[False] = False,
        ca_certs_file: Optional[str] = None,
    ) -> None:
        ...

    @overload
    def __init__(
        self,
        host: str,
        port: int,
        client_cert_file: str,
        *,
        facility: Optional[str] = None,
        client_key_file: Optional[str] = None,
        validate_server_cert: Literal[True],
        ca_certs_file: str,
    ) -> None:
        ...

    def __init__(
        self,
        host: str,
        port: int,
        client_cert_file: str,
        *,
        facility: Optional[str] = None,
        client_key_file: Optional[str] = None,
        validate_server_cert: bool = False,
        ca_certs_file: Optional[str] = None,
    ) -> None:
        if validate_server_cert and ca_certs_file is None:
            raise ValueError("The CA certificates file must be passed in order to validate the server certificate.")

        super().__init__(
            host,
            port,
            facility=facility,
            certfile=client_cert_file,
            keyfile=client_key_file,
            validate=validate_server_cert,
            ca_certs=ca_certs_file,
            **self._fixed_common_init_kwargs,
        )

    @classmethod
    def configure_handler_from_env(cls, host: str, port: int) -> Dict[str, Any]:
        handler_config = super().configure_handler_from_env(host, port)

        if "BBBATSCALE_LOGGING_GELF_TLS_CLIENT_CERT_FILE" not in os.environ:
            raise ValueError(
                "The required environment variable 'BBBATSCALE_LOGGING_GELF_TLS_CLIENT_CERT_FILE' is missing."
            )

        handler_config["client_cert_file"] = os.environ["BBBATSCALE_LOGGING_GELF_TLS_CLIENT_CERT_FILE"]
        cls._set_from_env_if_present(handler_config, "BBBATSCALE_LOGGING_GELF_TLS_CLIENT_KEY_FILE", "client_key_file")

        cls._set_from_env_if_present(
            handler_config,
            "BBBATSCALE_LOGGING_GELF_TLS_VALIDATE_SERVER_CERT",
            "validate_server_cert",
            check_represents_true,
        )
        cls._set_from_env_if_present(handler_config, "BBBATSCALE_LOGGING_GELF_TLS_CA_CERTS_FILE", "ca_certs_file")

        if handler_config.get("validate_server_cert", False) and "ca_certs_file" not in handler_config:
            raise ValueError(
                "The required environment variable 'BBBATSCALE_LOGGING_GELF_TLS_CA_CERTS_FILE' is missing."
            )

        return handler_config


class GELFHTTPHandler(GELFHandlerMixin, graypy.GELFHTTPHandler):
    def __init__(
        self,
        host: str,
        port: int,
        *,
        facility: Optional[str] = None,
        compress: bool = True,
        path: str = "/gelf",
        timeout: int = 5,
    ) -> None:
        super().__init__(
            host,
            port,
            facility=facility,
            compress=compress,
            path=path,
            timeout=timeout,
            **self._fixed_common_init_kwargs,
        )

    @classmethod
    def configure_handler_from_env(cls, host: str, port: int) -> Dict[str, Any]:
        handler_config = super().configure_handler_from_env(host, port)
        cls._set_from_env_if_present(handler_config, "BBBATSCALE_LOGGING_GELF_HTTP_PATH", "path")
        cls._set_from_env_if_present(handler_config, "BBBATSCALE_LOGGING_GELF_HTTP_TIMEOUT", "timeout", int)
        return handler_config

    def emit(self, record: LogRecord) -> None:
        try:
            super().emit(record)
        except Exception:
            self.handleError(record)


_supported_gelf_handlers: Dict[str, GELFHandlerMixin] = {
    "udp".casefold(): GELFUDPHandler,
    "tcp".casefold(): GELFTCPHandler,
    "tls".casefold(): GELFTLSHandler,
    "http".casefold(): GELFHTTPHandler,
}

REQUIRED_ENV_KEYS = ("BBBATSCALE_LOGGING_GELF_HOST", "BBBATSCALE_LOGGING_GELF_PROTOCOL", "BBBATSCALE_LOGGING_GELF_PORT")


def configure_gelf_logging(logging_config: Dict[str, Any]) -> None:
    required_env_keys = check_environ_contains(*REQUIRED_ENV_KEYS)

    if required_env_keys.present_keys and required_env_keys.absent_keys:
        raise ValueError(
            "To configure gelf logging the environment variables"
            f" {join_strings(sorted(required_env_keys.expected_keys))} must be present"
            " (or absent in order to deactivate)."
        )
    if not required_env_keys.present_keys:
        return

    host = os.environ["BBBATSCALE_LOGGING_GELF_HOST"]
    port = int(os.environ["BBBATSCALE_LOGGING_GELF_PORT"])
    protocol = os.environ["BBBATSCALE_LOGGING_GELF_PROTOCOL"]

    GELFHandler = _supported_gelf_handlers.get(protocol.casefold(), None)
    if GELFHandler is not None:
        handler_config = GELFHandler.configure_handler_from_env(host, port)
    else:
        raise ValueError(
            "Unsupported GELF protocol '%s'. Supported protocols are: %s."
            % (protocol, ", ".join(protocol for protocol in _supported_gelf_handlers))
        )

    logging_config.setdefault("handlers", dict())["gelf"] = handler_config
    logging_config.setdefault("root", dict()).setdefault("handlers", list()).append("gelf")
