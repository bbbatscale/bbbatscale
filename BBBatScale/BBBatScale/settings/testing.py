import os

os.environ.setdefault("WEBHOOKS", "enabled")
os.environ.setdefault("BBBATSCALE_MEDIA", "enabled")

from BBBatScale.settings.development import *  # noqa: F401,F403,E402

EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"
