import os
from typing import NamedTuple, Set, Union

default_settings_module = "BBBatScale.settings.production"


class CheckEnvironmentResult(NamedTuple):
    present_keys: Set[str]
    absent_keys: Set[str]

    @property
    def expected_keys(self) -> Set[str]:
        return self.present_keys | self.absent_keys


def check_environ_contains(*keys: str) -> CheckEnvironmentResult:
    expected_keys = set(keys)
    missing_keys = expected_keys.difference(os.environ)

    return CheckEnvironmentResult(expected_keys.difference(missing_keys), missing_keys)


TRUE_VALUES = {
    "1".casefold(),
    "on".casefold(),
    "t".casefold(),
    "true".casefold(),
    "enable".casefold(),
    "enabled".casefold(),
}


def check_represents_true(value: Union[str, bool]) -> bool:
    if isinstance(value, bool):
        return value

    return value.strip().casefold() in TRUE_VALUES


def get_import_path(cls: type) -> str:
    path = f"{cls.__module__}.{cls.__qualname__}"

    if "<locals>" in path:
        raise ValueError(f"Class {cls} is not globally accessible")

    return path
