from importlib import reload

from django.conf import settings
from pytest_mock import MockerFixture

from BBBatScale.settings import default_logging


def test_contains_root_log_level_debug(mocker: MockerFixture) -> None:
    mocker.patch.object(settings, "DEBUG", True)
    reload(default_logging)

    assert default_logging.LOGGING["root"]["level"] == "DEBUG"
    assert "level" not in default_logging.LOGGING_WITHOUT_ROOT_LOG_LEVEL["root"]


def test_contains_root_log_level_info(mocker: MockerFixture) -> None:
    mocker.patch.object(settings, "DEBUG", False)
    reload(default_logging)

    assert default_logging.LOGGING["root"]["level"] == "INFO"
    assert "level" not in default_logging.LOGGING_WITHOUT_ROOT_LOG_LEVEL["root"]
