import itertools
import logging
import multiprocessing
import os
import re
import socket
import threading
from inspect import currentframe, getframeinfo
from typing import Any, Dict, FrozenSet, Optional, Type

import pytest
from conftest import DictAlsoCheckingType, product_kwargs
from freezegun import freeze_time
from graypy.handler import BaseGELFHandler
from pytest_mock import MockerFixture

from BBBatScale.settings.gelf_logging import (
    REQUIRED_ENV_KEYS,
    GELFHTTPHandler,
    GELFTCPHandler,
    GELFTLSHandler,
    GELFUDPHandler,
    SyslogLevel,
    configure_gelf_logging,
)


@pytest.mark.parametrize(
    ("python_log_level", "expected_syslog_level", "expected_syslog_level_int"),
    [
        (logging.CRITICAL, SyslogLevel.CRITICAL, 2),
        (logging.FATAL, SyslogLevel.CRITICAL, 2),
        (51, SyslogLevel.CRITICAL, 2),
        (50, SyslogLevel.CRITICAL, 2),
        #
        (logging.ERROR, SyslogLevel.ERROR, 3),
        (49, SyslogLevel.ERROR, 3),
        (41, SyslogLevel.ERROR, 3),
        (40, SyslogLevel.ERROR, 3),
        #
        (logging.WARNING, SyslogLevel.WARNING, 4),
        (logging.WARN, SyslogLevel.WARNING, 4),
        (39, SyslogLevel.WARNING, 4),
        (31, SyslogLevel.WARNING, 4),
        (30, SyslogLevel.WARNING, 4),
        #
        (logging.INFO, SyslogLevel.INFORMATIONAL, 6),
        (29, SyslogLevel.INFORMATIONAL, 6),
        (21, SyslogLevel.INFORMATIONAL, 6),
        (20, SyslogLevel.INFORMATIONAL, 6),
        #
        (logging.DEBUG, SyslogLevel.DEBUG, 7),
        (19, SyslogLevel.DEBUG, 7),
        (11, SyslogLevel.DEBUG, 7),
        (10, SyslogLevel.DEBUG, 7),
        (9, SyslogLevel.DEBUG, 7),
        (0, SyslogLevel.DEBUG, 7),
    ],
)
def test_syslog_level(
    python_log_level: int, expected_syslog_level: SyslogLevel, expected_syslog_level_int: int
) -> None:
    syslog_level = SyslogLevel.get_by_log_level(python_log_level)
    assert syslog_level is expected_syslog_level
    assert syslog_level == expected_syslog_level_int
    assert int(syslog_level) == expected_syslog_level_int


def test_configure_gelf_logging__no_env(mocker: MockerFixture) -> None:
    mocker.patch.dict(os.environ, {}, clear=True)
    logging_config = dict()
    configure_gelf_logging(logging_config)
    assert logging_config == dict()


@pytest.mark.parametrize(
    "env_keys",
    frozenset(
        frozenset(entry)
        for entry in itertools.product(*itertools.repeat(REQUIRED_ENV_KEYS, len(REQUIRED_ENV_KEYS) - 1))
    ),
)
def test_configure_gelf_logging__insufficient_env(mocker: MockerFixture, env_keys: FrozenSet[str]) -> None:
    mocker.patch.dict(os.environ, dict((env_key, "some-value") for env_key in env_keys), clear=True)

    with pytest.raises(
        ValueError,
        match=re.escape(
            "To configure gelf logging the environment variables BBBATSCALE_LOGGING_GELF_HOST,"
            " BBBATSCALE_LOGGING_GELF_PORT and BBBATSCALE_LOGGING_GELF_PROTOCOL must be present"
            " (or absent in order to deactivate)."
        ),
    ):
        configure_gelf_logging(dict())


def test_configure_gelf_logging__unsupported_protocol(mocker: MockerFixture) -> None:
    mocker.patch.dict(
        os.environ,
        {
            "BBBATSCALE_LOGGING_GELF_HOST": "bbbatscale.test",
            "BBBATSCALE_LOGGING_GELF_PORT": "12201",
            "BBBATSCALE_LOGGING_GELF_PROTOCOL": "unsupported-protocol",
        },
        clear=True,
    )

    with pytest.raises(
        ValueError,
        match=re.compile(
            r"^Unsupported GELF protocol 'unsupported-protocol'\. Supported protocols are: \w+(, \w+)*\.$"
        ),
    ):
        configure_gelf_logging(dict())


@pytest.mark.parametrize("facility", (None, "some-facility"))
@pytest.mark.parametrize("compress", (None, False, True))
def test_configure_gelf_logging__udp(mocker: MockerFixture, facility: Optional[str], compress: Optional[bool]) -> None:
    mocker.patch.dict(
        os.environ,
        {
            "BBBATSCALE_LOGGING_GELF_HOST": "bbbatscale.test",
            "BBBATSCALE_LOGGING_GELF_PORT": "12201",
            "BBBATSCALE_LOGGING_GELF_PROTOCOL": "udp",
        },
        clear=True,
    )

    expected_handler_config = {
        "class": "BBBatScale.settings.gelf_logging.GELFUDPHandler",
        "host": "bbbatscale.test",
        "port": 12201,
    }

    if facility is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_FACILITY": facility})
        expected_handler_config["facility"] = facility

    if compress is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_COMPRESS": str(compress)})
        expected_handler_config["compress"] = compress

    logging_config = dict()
    configure_gelf_logging(logging_config)
    assert logging_config == {
        "handlers": {"gelf": expected_handler_config},
        "root": {"handlers": ["gelf"]},
    }

    handler_kwargs = logging_config["handlers"]["gelf"].copy()
    del handler_kwargs["class"]
    assert isinstance(GELFUDPHandler(**handler_kwargs), logging.Handler)


@pytest.mark.parametrize("facility", (None, "some-facility"))
@pytest.mark.parametrize("compress", (None, False, True))
def test_configure_gelf_logging__tcp(mocker: MockerFixture, facility: Optional[str], compress: Optional[bool]) -> None:
    mocker.patch.dict(
        os.environ,
        {
            "BBBATSCALE_LOGGING_GELF_HOST": "bbbatscale.test",
            "BBBATSCALE_LOGGING_GELF_PORT": "12201",
            "BBBATSCALE_LOGGING_GELF_PROTOCOL": "tcp",
        },
        clear=True,
    )

    expected_handler_config = {
        "class": "BBBatScale.settings.gelf_logging.GELFTCPHandler",
        "host": "bbbatscale.test",
        "port": 12201,
    }

    if facility is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_FACILITY": facility})
        expected_handler_config["facility"] = facility

    if compress is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_COMPRESS": str(compress)})

    handle_spy = mocker.spy(logging.root.handlers[0], "handle")

    logging_config = dict()
    configure_gelf_logging(logging_config)
    assert logging_config == {
        "handlers": {"gelf": expected_handler_config},
        "root": {"handlers": ["gelf"]},
    }

    if compress is not None:
        handle_spy.assert_called_once()
        assert (
            handle_spy.call_args.args[0].msg
            == "Ignoring 'BBBATSCALE_LOGGING_GELF_COMPRESS' since BBBatScale.settings.gelf_logging.GELFTCPHandler"
            " does not support compression."
        )
    else:
        handle_spy.assert_not_called()

    handler_kwargs = logging_config["handlers"]["gelf"].copy()
    del handler_kwargs["class"]
    assert isinstance(GELFTCPHandler(**handler_kwargs), logging.Handler)


@pytest.mark.parametrize("facility", (None, "some-facility"))
@pytest.mark.parametrize("compress", (None, False, True))
def test_configure_gelf_logging__tls__missing_client_cert(
    mocker: MockerFixture, facility: Optional[str], compress: Optional[bool]
) -> None:
    mocker.patch.dict(
        os.environ,
        {
            "BBBATSCALE_LOGGING_GELF_HOST": "bbbatscale.test",
            "BBBATSCALE_LOGGING_GELF_PORT": "12201",
            "BBBATSCALE_LOGGING_GELF_PROTOCOL": "tls",
        },
        clear=True,
    )

    if facility is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_FACILITY": facility})

    if compress is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_COMPRESS": str(compress)})

    with pytest.raises(
        ValueError,
        match=re.escape("The required environment variable 'BBBATSCALE_LOGGING_GELF_TLS_CLIENT_CERT_FILE' is missing."),
    ):
        configure_gelf_logging(dict())


@pytest.mark.parametrize("facility", (None, "some-facility"))
@pytest.mark.parametrize("compress", (None, False, True))
@pytest.mark.parametrize("client_key_file", (None, "/path/to/client.key"))
@pytest.mark.parametrize(
    ("ca_certs_file", "validate_server_cert"),
    itertools.chain(
        itertools.product(
            (None,),
            (None, False),
        ),
        itertools.product(
            ("/path/to/ca.crt",),
            (None, False, True),
        ),
    ),
)
def test_configure_gelf_logging__tls(
    mocker: MockerFixture,
    facility: Optional[str],
    compress: Optional[bool],
    client_key_file: Optional[str],
    ca_certs_file: Optional[str],
    validate_server_cert: Optional[bool],
) -> None:
    mocker.patch.dict(
        os.environ,
        {
            "BBBATSCALE_LOGGING_GELF_HOST": "bbbatscale.test",
            "BBBATSCALE_LOGGING_GELF_PORT": "12201",
            "BBBATSCALE_LOGGING_GELF_PROTOCOL": "tls",
            "BBBATSCALE_LOGGING_GELF_TLS_CLIENT_CERT_FILE": "/path/to/client.crt",
        },
        clear=True,
    )

    expected_handler_config = {
        "class": "BBBatScale.settings.gelf_logging.GELFTLSHandler",
        "host": "bbbatscale.test",
        "port": 12201,
        "client_cert_file": "/path/to/client.crt",
    }

    if facility is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_FACILITY": facility})
        expected_handler_config["facility"] = facility

    if compress is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_COMPRESS": str(compress)})

    if client_key_file is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_TLS_CLIENT_KEY_FILE": client_key_file})
        expected_handler_config["client_key_file"] = client_key_file

    if ca_certs_file is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_TLS_CA_CERTS_FILE": ca_certs_file})
        expected_handler_config["ca_certs_file"] = ca_certs_file

    if validate_server_cert is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_TLS_VALIDATE_SERVER_CERT": str(validate_server_cert)})
        expected_handler_config["validate_server_cert"] = validate_server_cert

    handle_spy = mocker.spy(logging.root.handlers[0], "handle")

    logging_config = dict()
    configure_gelf_logging(logging_config)
    assert logging_config == {
        "handlers": {"gelf": expected_handler_config},
        "root": {"handlers": ["gelf"]},
    }

    if compress is not None:
        handle_spy.assert_called_once()
        assert (
            handle_spy.call_args.args[0].msg
            == "Ignoring 'BBBATSCALE_LOGGING_GELF_COMPRESS' since BBBatScale.settings.gelf_logging.GELFTLSHandler"
            " does not support compression."
        )
    else:
        handle_spy.assert_not_called()

    handler_kwargs = logging_config["handlers"]["gelf"].copy()
    del handler_kwargs["class"]
    assert isinstance(GELFTLSHandler(**handler_kwargs), logging.Handler)


@pytest.mark.parametrize("facility", (None, "some-facility"))
@pytest.mark.parametrize("compress", (None, False, True))
@pytest.mark.parametrize("client_key_file", (None, "/path/to/client.key"))
def test_configure_gelf_logging__tls__validate_but_missing_ca_cert(
    mocker: MockerFixture,
    facility: Optional[str],
    compress: Optional[bool],
    client_key_file: Optional[str],
) -> None:
    mocker.patch.dict(
        os.environ,
        {
            "BBBATSCALE_LOGGING_GELF_HOST": "bbbatscale.test",
            "BBBATSCALE_LOGGING_GELF_PORT": "12201",
            "BBBATSCALE_LOGGING_GELF_PROTOCOL": "tls",
            "BBBATSCALE_LOGGING_GELF_TLS_CLIENT_CERT_FILE": "/path/to/client.crt",
            "BBBATSCALE_LOGGING_GELF_TLS_VALIDATE_SERVER_CERT": "true",
        },
        clear=True,
    )

    handler_kwargs = {
        "host": "bbbatscale.test",
        "port": 12201,
        "client_cert_file": "/path/to/client.crt",
        "validate_server_cert": True,
    }

    if facility is not None:
        handler_kwargs["facility"] = facility

    if compress is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_COMPRESS": str(compress)})

    if client_key_file is not None:
        handler_kwargs["client_key_file"] = client_key_file

    with pytest.raises(
        ValueError,
        match=re.escape("The required environment variable 'BBBATSCALE_LOGGING_GELF_TLS_CA_CERTS_FILE' is missing."),
    ):
        configure_gelf_logging(dict())

    with pytest.raises(
        ValueError,
        match=re.escape("The CA certificates file must be passed in order to validate the server certificate."),
    ):
        GELFTLSHandler(**handler_kwargs)


@pytest.mark.parametrize("facility", (None, "some-facility"))
@pytest.mark.parametrize("compress", (None, False, True))
@pytest.mark.parametrize("path", (None, "/some/path"))
@pytest.mark.parametrize("timeout", (None, 123))
def test_configure_gelf_logging__http(
    mocker: MockerFixture,
    facility: Optional[str],
    compress: Optional[bool],
    path: Optional[str],
    timeout: Optional[int],
) -> None:
    mocker.patch.dict(
        os.environ,
        {
            "BBBATSCALE_LOGGING_GELF_HOST": "bbbatscale.test",
            "BBBATSCALE_LOGGING_GELF_PORT": "12201",
            "BBBATSCALE_LOGGING_GELF_PROTOCOL": "http",
        },
        clear=True,
    )

    expected_handler_config = {
        "class": "BBBatScale.settings.gelf_logging.GELFHTTPHandler",
        "host": "bbbatscale.test",
        "port": 12201,
    }

    if facility is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_FACILITY": facility})
        expected_handler_config["facility"] = facility

    if compress is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_COMPRESS": str(compress)})
        expected_handler_config["compress"] = compress

    if path is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_HTTP_PATH": path})
        expected_handler_config["path"] = path

    if timeout is not None:
        mocker.patch.dict(os.environ, {"BBBATSCALE_LOGGING_GELF_HTTP_TIMEOUT": str(timeout)})
        expected_handler_config["timeout"] = timeout

    logging_config = dict()
    configure_gelf_logging(logging_config)
    assert logging_config == {
        "handlers": {"gelf": expected_handler_config},
        "root": {"handlers": ["gelf"]},
    }

    handler_kwargs = logging_config["handlers"]["gelf"].copy()
    del handler_kwargs["class"]
    assert isinstance(GELFHTTPHandler(**handler_kwargs), logging.Handler)


@pytest.mark.parametrize(
    ("handler_class", "handler_kwargs", "handler_check_send"),
    itertools.chain(
        (
            (GELFUDPHandler, kwargs, True)
            for kwargs in product_kwargs(
                host=("bbbatscale.test",),
                port=(12201,),
                facility=(None, "some-facility"),
                compress=(None, False, True),
            )
        ),
        (
            (GELFTCPHandler, kwargs, True)
            for kwargs in product_kwargs(
                host=("bbbatscale.test",),
                port=(12201,),
                facility=(None, "some-facility"),
            )
        ),
        (
            (GELFTLSHandler, kwargs, True)
            for kwargs in itertools.chain.from_iterable(
                product_kwargs(
                    host=("bbbatscale.test",),
                    port=(12201,),
                    facility=(None, "some-facility"),
                    client_cert_file=("/path/to/client.crt",),
                    client_key_file=(None, "/path/to/client.key"),
                    ca_certs_file=(ca_certs_file,),
                    validate_server_cert=(None, False) if ca_certs_file is None else (None, False, True),
                )
                for ca_certs_file in (None, "/path/to/ca.crt")
            )
        ),
        (
            (GELFHTTPHandler, kwargs, False)
            for kwargs in product_kwargs(
                host=("bbbatscale.test",),
                port=(12201,),
                facility=(None, "some-facility"),
                compress=(None, False, True),
                path=(None, "/some/path"),
                timeout=(None, 123),
            )
        ),
    ),
)
@freeze_time("1970-01-02T01:00:00.100+00:00")
def test_gelf_handler(
    mocker: MockerFixture,
    handler_class: Type[BaseGELFHandler],
    handler_kwargs: Dict[str, Any],
    handler_check_send: bool,
) -> None:
    """
    This test also implicitly tests that logging to a gelf handler with an unreachable host
    does not result in a crash of the program (results in unhandled exceptions).
    """
    handler = handler_class(**handler_kwargs)
    handler_make_gelf_dict_spy = mocker.spy(handler, "_make_gelf_dict")

    handler_send_spy = mocker.spy(handler, "send") if handler_check_send else None

    mocker.patch.object(logging.root, "handlers", [handler])

    traceback = getframeinfo(currentframe())
    logging.getLogger("some.logger").warning("Some important message")

    handler_make_gelf_dict_spy.assert_called_once()
    handler_return_value = handler_make_gelf_dict_spy.spy_return.copy()

    if handler_check_send:
        handler_send_spy.assert_called()

    if handler.facility is not None:
        assert handler_return_value.pop("_facility") == handler.facility

    assert DictAlsoCheckingType(handler_return_value) == {
        "version": "1.1",
        "host": socket.gethostname(),
        "short_message": "Some important message",
        "timestamp": 90000.1,
        "level": 4,
        "_logger": "some.logger",
        "_python_level": 30,
        "_python_level_name": "WARNING",
        "_process_id": multiprocessing.current_process().ident,
        "_process_name": multiprocessing.current_process().name,
        "_thread_id": threading.current_thread().ident,
        "_thread_name": threading.current_thread().name,
        "_file": traceback.filename,
        "_module": "test_gelf_logging",
        "_function": "test_gelf_handler",
        "_line": traceback.lineno + 1,
    }
