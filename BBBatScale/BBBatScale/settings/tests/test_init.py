import os

import pytest
from pytest_mock import MockerFixture

from BBBatScale.settings import check_environ_contains, check_represents_true, get_import_path


def test_check_environ_contains(mocker: MockerFixture) -> None:
    mocker.patch.dict(os.environ, {"TEST_VAR_1": "something", "TEST_VAR_2": "something", "TEST_VAR_3": ""}, clear=True)
    result = check_environ_contains("TEST_VAR_1", "TEST_VAR_3", "TEST_VAR_5")
    assert result.present_keys == {"TEST_VAR_1", "TEST_VAR_3"}
    assert result.absent_keys == {"TEST_VAR_5"}


def test_check_represents_true() -> None:
    assert check_represents_true("") is False
    assert check_represents_true("some-text") is False
    assert check_represents_true("false") is False
    assert check_represents_true("disabled") is False

    assert check_represents_true("1") is True
    assert check_represents_true("  \n1\n    ") is True

    assert check_represents_true("oN") is True
    assert check_represents_true("On") is True
    assert check_represents_true("    \nON    ") is True

    assert check_represents_true("t") is True
    assert check_represents_true("T") is True
    assert check_represents_true(" t ") is True

    assert check_represents_true("TrUe") is True
    assert check_represents_true("tRuE") is True
    assert check_represents_true("    TRUE\n") is True

    assert check_represents_true("EnAbLe") is True
    assert check_represents_true("eNaBlE") is True
    assert check_represents_true("  \n  ENABLE  ") is True

    assert check_represents_true("EnAbLeD") is True
    assert check_represents_true("eNaBlEd") is True
    assert check_represents_true("  ENABLED    \n  ") is True

    assert check_represents_true(True) is True
    assert check_represents_true(False) is False


class GlobalTestClass:
    pass


def test_get_import_path() -> None:
    class LocalTestClass:
        pass

    with pytest.raises(ValueError):
        get_import_path(LocalTestClass)

    assert get_import_path(GlobalTestClass) == "BBBatScale.settings.tests.test_init.GlobalTestClass"
    assert get_import_path(os.PathLike) == "os.PathLike"
    assert get_import_path(MockerFixture) == "pytest_mock.plugin.MockerFixture"
