import logging
from copy import deepcopy
from logging import LogRecord, StreamHandler
from typing import Any, Dict

from django.utils.functional import SimpleLazyObject
from django_guid.log_filters import CorrelationId

from BBBatScale.settings import get_import_path
from BBBatScale.settings.gelf_logging import configure_gelf_logging


class ConsoleFormatter(logging.Formatter):
    def __init__(self) -> None:
        super().__init__(
            fmt="%(levelname)s [%(asctime)s] - correlation id: %(correlation_id)s - %(filename)s:%(lineno)d"
            " in %(funcName)s - %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
            style="%",
        )

    def format(self, record: LogRecord) -> str:
        record.correlation_id = str(getattr(record, "correlation_id", None) or "N/A")

        return super().format(record)


LOGGING_WITHOUT_ROOT_LOG_LEVEL: Dict[str, Any] = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "correlation_id": {
            "()": get_import_path(CorrelationId),
        }
    },
    "formatters": {
        "console": {
            "()": get_import_path(ConsoleFormatter),
        }
    },
    "handlers": {
        "console": {
            "class": get_import_path(StreamHandler),
            "formatter": "console",
            "filters": ["correlation_id"],
        },
    },
    "root": {
        "handlers": ["console"],
    },
    "loggers": {
        "uvicorn.access": {
            "handlers": [],
            "propagate": False,
        },
    },
}

configure_gelf_logging(LOGGING_WITHOUT_ROOT_LOG_LEVEL)


def logging_config_with_root_log_level() -> Dict[str, Any]:
    from django.conf import settings

    logging_config = deepcopy(LOGGING_WITHOUT_ROOT_LOG_LEVEL)
    logging_config.setdefault("root", dict())["level"] = "DEBUG" if settings.DEBUG else "INFO"

    return logging_config


LOGGING = SimpleLazyObject(logging_config_with_root_log_level)
