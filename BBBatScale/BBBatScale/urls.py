from typing import Callable, Optional, Type

from core.exceptions import BaseHttpException, Http400Exception, Http403Exception, Http404Exception, Http500Exception
from core.utils import get_tenant
from core.views import join_or_create, reset_password
from core.views.home import home
from core.views.recordings import recording_redirect
from crispy_forms.bootstrap import AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Layout, Submit
from django.conf import settings
from django.conf.urls import include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.forms import Form
from django.http import HttpRequest, HttpResponse
from django.urls import path, reverse
from django.utils.translation import gettext_lazy as _
from oidc_authentication.models import AuthParameter
from oidc_authentication.views import (
    TenantOIDCAuthenticationCallbackView,
    TenantOIDCAuthenticationRequestView,
    TenantOIDCLogoutView,
    auth_parameter_create,
    auth_parameter_delete,
    auth_parameter_update,
)


class LoginView(auth_views.LoginView):
    template_name = "login.html"
    redirect_authenticated_user = True

    def get_form(self, form_class: Optional[Type[Form]] = None) -> Form:
        form = super().get_form(form_class)

        form.helper = FormHelper()
        form.helper.form_show_labels = False
        form.helper.layout = Layout(
            AppendedText("username", '<i class="fas fa-user"></i>', placeholder=form.fields["username"].label),
            AppendedText("password", '<i class="fas fa-lock"></i>', placeholder=form.fields["password"].label),
            Div(
                Submit("log-in", _("Log in"), css_class="btn-primary"),
                HTML(f'<a href="{reverse("password_reset")}" class="btn btn-link">{_("Forgot Password?")}</a>'),
                css_class="d-flex justify-content-between align-items-center",
            ),
        )
        return form


def login_view(request):
    return (
        TenantOIDCAuthenticationRequestView.as_view()(request)
        if AuthParameter.objects.filter(tenant=get_tenant(request)).exists()
        else LoginView.as_view()(request)
    )


def logout_view(request):
    return (
        TenantOIDCLogoutView.as_view()(request)
        if AuthParameter.objects.filter(tenant=get_tenant(request)).exists()
        else auth_views.LogoutView.as_view()(request)
    )


urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
    path("admin/", admin.site.urls),
    path("core/", include("core.urls")),
    path("login", login_view, name="login"),
    path("logout", logout_view, name="logout"),
    # Since the OIDC middleware expects the logout url name oidc_logout and the view does not provide a naming list
    # this needs to be included
    path("logout", logout_view, name="oidc_logout"),
    path("password_reset/", reset_password.password_reset, name="password_reset"),
    path("reset/<uidb64>/<token>/", reset_password.PasswordResetConfirm.as_view(), name="password_reset_confirm"),
    path("", home, name="home"),
    path("r", join_or_create.JoinOrCreateMeetingView.as_view(), name="join_or_create_meeting"),
    path(
        "r/<path:room_name>", join_or_create.DeprecatedJoinRedirectView.as_view(), name="join_redirect"
    ),  # TODO deprecated
    path("api/join-create/init", join_or_create.JoinOrCreateInitView.as_view()),
    path("api/join-create/next-action", join_or_create.JoinOrCreateNextActionView.as_view()),
    path("api/join-create/create-meeting", join_or_create.CreateMeetingView.as_view()),
    path("api/join-create/opt-out-create-meeting", join_or_create.OptOutCreateMeetingView.as_view()),
    path("api/join-create/opt-in-create-meeting", join_or_create.OptInCreateMeetingView.as_view()),
    path("api/join-create/validate-access-code", join_or_create.ValidateAccessCodeView.as_view()),
    path("api/join-create/set-join-name", join_or_create.SetJoinNameView.as_view()),
    path("p/<str:replay_id>", recording_redirect, name="recording_redirect"),
    path("support/", include("support_chat.urls")),
    path("login/callback", TenantOIDCAuthenticationCallbackView.as_view(), name="oidc_authentication_callback"),
    path("login/authenticate", TenantOIDCAuthenticationRequestView.as_view(), name="oidc_authentication_init"),
    path("tenant/authparameter/create/<int:tenant>", auth_parameter_create, name="auth_parameter_create"),
    path("tenant/authparameter/update/<int:tenant>", auth_parameter_update, name="auth_parameter_update"),
    path("tenant/authparameter/delete/<int:tenant>", auth_parameter_delete, name="auth_parameter_delete"),
]

if settings.WEBHOOKS_ENABLED:
    urlpatterns.append(path("django-rq/", include("django_rq.urls")))


# Django error handler views (must be defined in urls.py)


def _handler_factory(
    exception_factory: Callable[[], BaseHttpException]
) -> Callable[[HttpRequest, Exception], HttpResponse]:
    def handler(request: HttpRequest, exception: Exception) -> HttpResponse:
        _exception = exception_factory()
        # set cause and suppress context to behave like `raise HttpException from exc`
        _exception.__cause__ = exception
        _exception.__suppress_context__ = True
        return _exception.response(request)

    return handler


handler400 = _handler_factory(Http400Exception)
handler403 = _handler_factory(Http403Exception)
handler404 = _handler_factory(Http404Exception)


def handler500(request: HttpRequest) -> HttpResponse:
    return Http500Exception().response(request)
