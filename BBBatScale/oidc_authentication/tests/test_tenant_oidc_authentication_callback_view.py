import pytest
from oidc_authentication.views import TenantOIDCAuthenticationCallbackView


@pytest.mark.django_db
def test_get_settings():
    assert TenantOIDCAuthenticationCallbackView.get_settings("EXAMPLE_SETTING", "test-setting") == "test-setting"


@pytest.mark.django_db
def test_failure_url(testserver_tenant_auth_parameter):
    view = TenantOIDCAuthenticationCallbackView()
    view.auth_parameter = testserver_tenant_auth_parameter
    assert view.failure_url == "/"


@pytest.mark.django_db
def test_success_url(testserver_tenant_auth_parameter):
    view = TenantOIDCAuthenticationCallbackView()

    class Request:
        session = {}

    view.request = Request()

    view.auth_parameter = testserver_tenant_auth_parameter
    assert view.success_url == "/"

    view.request.session.update({"oidc_login_next": "/example/path"})
    assert view.success_url == "/example/path"


@pytest.mark.django_db
def test_login_success(testserver_tenant_auth_parameter, mocker):
    def mock_login_success(self):
        return "/example/url"

    mocker.patch("mozilla_django_oidc.views.OIDCAuthenticationCallbackView.login_success", mock_login_success)

    view = TenantOIDCAuthenticationCallbackView()

    class Request:
        session = {}

    view.request = Request()
    view.auth_parameter = testserver_tenant_auth_parameter
    assert view.login_success() == "/example/url"


@pytest.mark.django_db
def test_get(testserver_tenant_auth_parameter, mocker):
    def mock_get(self, request):
        pass

    mocker.patch("mozilla_django_oidc.views.OIDCAuthenticationCallbackView.get", mock_get)

    view = TenantOIDCAuthenticationCallbackView()

    class Request:
        session = {}
        tenant = testserver_tenant_auth_parameter.tenant

    request = Request()

    view.get(request)

    assert view.auth_parameter == testserver_tenant_auth_parameter
