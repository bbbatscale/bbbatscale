from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field, Layout, Submit
from django import forms
from django.utils.translation import gettext_lazy as _
from oidc_authentication.models import AuthParameter, validate_dict


class AuthParameterForm(forms.ModelForm):
    class Meta:
        model = AuthParameter
        fields = [
            "tenant",
            "client_id",
            "client_secret",
            "authorization_endpoint",
            "token_endpoint",
            "user_endpoint",
            "jwks_endpoint",
            "end_session_endpoint",
            "scopes",
            "verify_ssl",
            "sign_algo",
            "logout_url_method",
            "login_redirect_url_failure",
            "login_redirect_url",
            "idp_sign_key",
            "refresh_enabled",
            "oidc_renew_id_token_expiry_seconds",
            "state_size",
            "use_nonce",
            "nonce_size",
            "allow_unsecured_jwt",
            "timeout",
            "create_user",
            "oidc_username_standard",
            "claim_unique_username_field",
            "proxy",
            "token_use_basic_auth",
        ]

    def __init__(self, *args, **kwargs):
        super(AuthParameterForm, self).__init__(*args, **kwargs)

        self.fields["tenant"].disabled = True

        proxy_prepare_value = self.fields["proxy"].prepare_value
        proxy_to_python = self.fields["proxy"].to_python
        self.fields["proxy"].prepare_value = lambda value: "" if value is None else proxy_prepare_value(value)
        self.fields["proxy"].to_python = (
            lambda value: None if isinstance(value, str) and value.strip() == "" else proxy_to_python(value)
        )
        self.fields["proxy"].empty_values = [None]
        self.fields["proxy"].validators.append(validate_dict)

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("tenant"),
            Field("client_id"),
            Field("client_secret"),
            Field("authorization_endpoint"),
            Field("token_endpoint"),
            Field("user_endpoint"),
            Field("jwks_endpoint"),
            Field("end_session_endpoint"),
            Field("scopes"),
            Field("sign_algo"),
            Field("verify_ssl"),
            Field("logout_url_method"),
            Field("login_redirect_url"),
            Field("login_redirect_url_failure"),
            Field("idp_sign_key"),
            Field("refresh_enabled"),
            Field("oidc_renew_id_token_expiry_seconds"),
            Field("state_size"),
            Field("use_nonce"),
            Field("nonce_size"),
            Field("allow_unsecured_jwt"),
            Field("timeout"),
            Field("create_user"),
            Field("oidc_username_standard"),
            Field("claim_unique_username_field"),
            Field("proxy"),
            Field("token_use_basic_auth"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )
