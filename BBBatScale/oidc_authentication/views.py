import logging
import time
from urllib.parse import urlencode

from core.decorators import tenant_based_permission_required
from core.utils import get_tenant
from core.views import create_view_access_logging_message
from django.contrib import auth, messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.exceptions import RequestAborted
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.crypto import get_random_string
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _
from mozilla_django_oidc.utils import absolutify, add_state_and_nonce_to_session
from mozilla_django_oidc.views import (
    OIDCAuthenticationCallbackView,
    OIDCAuthenticationRequestView,
    OIDCLogoutView,
    get_next_url,
)
from oidc_authentication.forms import AuthParameterForm
from oidc_authentication.models import AuthParameter

logger = logging.getLogger(__name__)


# Create your views here.


class TenantOIDCAuthenticationCallbackView(OIDCAuthenticationCallbackView):
    auth_parameter: AuthParameter

    @staticmethod
    def get_settings(attr, *args):
        logger.debug(
            f"Retrieving the non-tenant-based {attr}.",
        )
        return OIDCAuthenticationCallbackView.get_settings(attr, *args)

    @property
    def failure_url(self):
        return self.auth_parameter.login_redirect_url_failure

    @property
    def success_url(self):
        next_url = self.request.session.get("oidc_login_next", None)
        return next_url or self.auth_parameter.login_redirect_url

    def login_success(self):
        success_url = super().login_success()

        self.request.session["oidc_id_token_expiration"] = (
            time.time() + self.auth_parameter.oidc_renew_id_token_expiry_seconds
        )

        return success_url

    def get(self, request):
        tenant = get_tenant(request)
        try:
            self.auth_parameter = tenant.auth_parameter
        except Site.auth_parameter.RelatedObjectDoesNotExist:
            logger.error(f"No auth_parameter configured for tenant {tenant.domain}")
            raise RequestAborted()
        return super().get(request)


class TenantOIDCAuthenticationRequestView(OIDCAuthenticationRequestView):
    redirect_field_name = REDIRECT_FIELD_NAME

    def __init__(self, *args, **kwargs):
        pass

    def get(self, request):
        """OIDC client authentication initialization HTTP endpoint"""
        auth_parameter = get_tenant(request).auth_parameter
        state = get_random_string(auth_parameter.state_size)

        params = {
            "response_type": "code",
            "scope": auth_parameter.scopes,
            "client_id": auth_parameter.client_id,
            "redirect_uri": absolutify(request, reverse("oidc_authentication_callback")),
            "state": state,
        }

        params.update(self.get_extra_params(request))

        if auth_parameter.use_nonce:
            nonce = get_random_string(auth_parameter.nonce_size)
            params.update({"nonce": nonce})

        add_state_and_nonce_to_session(request, state, params)

        request.session["oidc_login_next"] = get_next_url(request, self.redirect_field_name)

        query = urlencode(params)
        redirect_url = "{url}?{query}".format(url=auth_parameter.authorization_endpoint, query=query)
        return HttpResponseRedirect(redirect_url)


class TenantOIDCLogoutView(OIDCLogoutView):
    def post(self, request):
        """Log out the user."""
        logout_url = self.redirect_url
        auth_parameter = get_tenant(request).auth_parameter
        if request.user.is_authenticated:
            # Check if a method exists to build the URL to log out the user
            # from the OP.
            logout_from_op = auth_parameter.logout_url_method
            if logout_from_op:
                logout_url = import_string(logout_from_op)(request)

            # Log out the Django user if they were logged in.
            auth.logout(request)

        return HttpResponseRedirect(logout_url)


@login_required
@tenant_based_permission_required("core.add_authparameter", elevate_staff=False, raise_exception=True)
def auth_parameter_create(request, tenant):
    logger.info(create_view_access_logging_message(request))
    instance = get_object_or_404(Site, pk=tenant)
    form = AuthParameterForm(request.POST or None, initial={"tenant": instance})
    if request.method == "POST" and form.is_valid():
        form.save()
        messages.success(request, _("AuthParameter for {} was created successfully").format(instance.name))
        # return to URL
        return redirect("tenant_overview")
    return render(request, "auth_parameter_create.html", {"form": form})


@login_required
@tenant_based_permission_required(
    ["core.view_authparameter", "core.change_authparameter"], elevate_staff=False, raise_exception=True
)
def auth_parameter_update(request, tenant):
    logger.info(create_view_access_logging_message(request))
    instance = get_object_or_404(Site, pk=tenant)
    form = AuthParameterForm(request.POST or None, instance=instance.auth_parameter)
    if request.method == "POST" and form.is_valid():
        form.save()
        messages.success(request, _("AuthParameter for {} was updated successfully").format(instance.name))
        # return to URL
        return redirect("tenant_overview")
    return render(request, "auth_parameter_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_authparameter", elevate_staff=False, raise_exception=True)
def auth_parameter_delete(request, tenant):
    logger.info(create_view_access_logging_message(request))
    instance = get_object_or_404(Site, pk=tenant)
    instance.auth_parameter.delete()
    messages.success(request, _("AuthParameter for {} was deleted successfully").format(instance.name))
    return redirect("tenant_overview")
