import logging
import time
from urllib.parse import urlencode

from core.utils import get_tenant
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.utils.crypto import get_random_string
from mozilla_django_oidc.middleware import SessionRefresh
from mozilla_django_oidc.utils import absolutify, add_state_and_nonce_to_session

LOGGER = logging.getLogger(__name__)


class TenantSessionRefresh(SessionRefresh):
    @staticmethod
    def get_settings(attr, *args):
        LOGGER.debug("Retrieving the non-tenant-based %s.", attr)
        return SessionRefresh.get_settings(attr, *args)

    def process_request(self, request):
        if not self.is_refreshable_url(request):
            LOGGER.debug("request is not refreshable")
            return
        auth_parameter = get_tenant(request).auth_parameter

        expiration = request.session.get("oidc_id_token_expiration", 0)
        now = time.time()
        if expiration > now:
            # The id_token is still valid, so we don't have to do anything.
            LOGGER.debug("id token is still valid (%s > %s)", expiration, now)
            return

        LOGGER.debug("id token has expired")
        # The id_token has expired, so we have to re-authenticate silently.

        auth_url = auth_parameter.authorization_endpoint
        client_id = auth_parameter.client_id
        state = get_random_string(auth_parameter.state_size)

        # Build the parameters as if we were doing a real auth handoff, except
        # we also include prompt=none.
        params = {
            "response_type": "code",
            "client_id": client_id,
            "redirect_uri": absolutify(request, reverse("oidc_authentication_callback")),
            "state": state,
            "scope": auth_parameter.scopes,
            "prompt": "none",
        }

        if auth_parameter.use_nonce:
            nonce = get_random_string(auth_parameter.nonce_size)
            params.update({"nonce": nonce})

        add_state_and_nonce_to_session(request, state, params)

        request.session["oidc_login_next"] = request.get_full_path()

        query = urlencode(params)
        redirect_url = "{url}?{query}".format(url=auth_url, query=query)
        if request.is_ajax():
            # Almost all XHR request handling in client-side code struggles
            # with redirects since redirecting to a page where the user
            # is supposed to do something is extremely unlikely to work
            # in an XHR request. Make a special response for these kinds
            # of requests.
            # The use of 403 Forbidden is to match the fact that this
            # middleware doesn't really want the user in if they don't
            # refresh their session.
            response = JsonResponse({"refresh_url": redirect_url}, status=403)
            response["refresh_url"] = redirect_url
            return response
        return HttpResponseRedirect(redirect_url)
