# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-21 08:58+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

# TODO
#: support_chat/static/chat/js/chat.js:509
#: support_chat/static/chat/js/chat.js:636
msgid "Support offline"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:512
msgid "Support online"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:515
msgid "Supporter joined"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:629
msgid "Support"
msgstr ""

#: support_chat/static/chat/js/chat.js:644
#: support_chat/static/chat/js/support.js:258
msgid "Moderator"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:673
msgid "Leave Chat"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:742
msgid "Message..."
msgstr ""

#: support_chat/static/chat/js/chat.js:757
msgid "Prepared Answers"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:781
msgid "Send"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:791
msgid "Join Chat"
msgstr ""

# TODO
#: support_chat/static/chat/js/support.js:620
msgid "Inactivate Support"
msgstr ""

# TODO
#: support_chat/static/chat/js/support.js:623
msgid "Activate Support"
msgstr ""

# TODO
#: support_chat/static/chat/js/support.js:715
#: support_chat/static/chat/js/support.js:716
msgid "Search"
msgstr ""

# TODO
#: support_chat/static/chat/js/support.js:803
msgid "Inactivate all Supporters"
msgstr ""

#: support_chat/static/chat/js/utils.js:14
#, javascript-format
msgid "%s Message"
msgid_plural "%s Messages"
msgstr[0] ""
msgstr[1] ""
