from enum import Enum, auto
from typing import List

import pytest
import support_chat
from channels.db import database_sync_to_async
from core.models import User
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.models import Site
from django.db import transaction
from django.utils import timezone
from django.utils.functional import empty
from freezegun import freeze_time
from support_chat.models import ChatMessage, SupportChatParameter
from support_chat.tests.websocket_consumer.conftest import (
    WebsocketCommunicator,
    assert_connection_refuse,
    assert_error,
    assert_illegal_request,
    chat_consumer,
    create_chat,
    fill_chat,
    join_chat,
    random_message,
    random_string,
    support_consumer,
)
from utils.websockets import Error


def prepare_messages(chat_communicator: WebsocketCommunicator, chat_messages: List[ChatMessage]) -> List[dict]:
    return [
        {
            "id": chat_message.id,
            "message": chat_message.message,
            "isOwnMessage": chat_message.user == chat_communicator.user,
            "userRealName": str(chat_message.user),
            "timestamp": chat_message.timestamp.isoformat(),
        }
        for chat_message in chat_messages
    ]


@pytest.mark.asyncio
async def test_access_chat_without_login(async_example_tenant: Site) -> None:
    chat_communicator_anonymous_user = chat_consumer(AnonymousUser(), async_example_tenant)

    await assert_connection_refuse(chat_communicator_anonymous_user, Error.LOGIN_REQUIRED)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_access_chat_with_non_ascii_username(async_example_tenant: Site) -> None:
    await database_sync_to_async(async_example_tenant.save)()
    user = await database_sync_to_async(User.objects.create)(
        username="Джон_Доу", first_name="Джон", last_name="Доу", tenant=async_example_tenant
    )
    async with chat_consumer(user, user.tenant):
        pass


@pytest.mark.asyncio
async def test_access_chat_with_login(async_user: User) -> None:
    async with chat_consumer(async_user, async_user.tenant):
        pass


@pytest.mark.asyncio
async def test_access_specific_chat_without_permissions(async_user: User) -> None:
    chat_communicator_user = chat_consumer(async_user, async_user.tenant, async_user.username)

    await assert_connection_refuse(chat_communicator_user, Error.SUPPORTER_REQUIRED)


@pytest.mark.asyncio
async def test_access_different_tenant_without_permission(
    async_user_other_tenant: User, async_active_supporter_user: User
) -> None:
    async with create_chat(async_user_other_tenant, async_user_other_tenant.tenant):
        chat_communicator_supporter = chat_consumer(
            async_active_supporter_user, async_user_other_tenant.tenant, async_user_other_tenant.username
        )
        await assert_connection_refuse(chat_communicator_supporter, Error.SUPPORTER_REQUIRED)


@pytest.mark.asyncio
async def test_access_specific_chat_with_unknown_user(async_active_supporter_user: User) -> None:
    chat_communicator_active_supporter_user = chat_consumer(
        async_active_supporter_user, async_active_supporter_user.tenant, "IAmUnknown"
    )
    await assert_connection_refuse(chat_communicator_active_supporter_user, Error.CHAT_NOT_FOUND)


@pytest.mark.asyncio
async def test_access_specific_chat_with_user_without_chat(async_user: User, async_active_supporter_user: User) -> None:
    chat_communicator_active_supporter_user = chat_consumer(
        async_active_supporter_user, async_user.tenant, async_user.username
    )
    await assert_connection_refuse(chat_communicator_active_supporter_user, Error.CHAT_NOT_FOUND)


@pytest.mark.asyncio
async def test_access_specific_chat_wrong_different_tenant(
    async_user_other_tenant: User, async_active_supporter_user: User, async_example_tenant: Site
) -> None:
    async with create_chat(async_user_other_tenant, async_user_other_tenant.tenant):
        chat_communicator_supporter = chat_consumer(
            async_active_supporter_user, async_example_tenant, async_user_other_tenant.username
        )
        await assert_connection_refuse(chat_communicator_supporter, Error.CHAT_NOT_FOUND)


@pytest.mark.asyncio
async def test_access_specific_chat_with_permissions(async_user: User, async_active_supporter_user: User) -> None:
    async with (
        create_chat(async_user, async_user.tenant),
        chat_consumer(async_active_supporter_user, async_user.tenant, async_user.username),
    ):
        pass


@pytest.mark.asyncio
async def test_illegal_requests_user(async_user: User) -> None:
    async with chat_consumer(async_user, async_user.tenant) as chat_communicator_user:
        await assert_illegal_request(chat_communicator_user)


@pytest.mark.asyncio
async def test_illegal_requests_supporter(async_user: User, async_active_supporter_user: User) -> None:
    async with (
        create_chat(async_user, async_user.tenant),
        chat_consumer(
            async_active_supporter_user, async_user.tenant, async_user.username
        ) as chat_communicator_supporter,
    ):
        await assert_illegal_request(chat_communicator_supporter)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_create_chat_via_send(
    async_user: User, async_other_user: User, async_active_supporter_user: User
) -> None:
    async with (
        support_consumer(
            async_active_supporter_user, async_active_supporter_user.tenant
        ) as support_communicator_supporter,
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        chat_consumer(async_other_user, async_other_user.tenant),
    ):
        await support_communicator_supporter.receive_nothing(timeout=1)

        message = random_message()
        await chat_communicator_user.send_json_to({"type": "send", "message": message})

        response = await chat_communicator_user.receive_json_from()
        assert response["type"] == "message"

        assert await support_communicator_supporter.receive_json_from() == {
            "type": "chat",
            "chatOwner": async_user.username,
            "chatOwnerRealName": str(async_user),
            "chatOwnerIsModerator": False,
            "unreadMessages": 1,
            "isSupportActive": False,
            "message": message,
            "timestamp": response["timestamp"],
        }

        assert await support_communicator_supporter.receive_json_from() == {
            "type": "newMessage",
            "chatOwner": async_user.username,
            "message": message,
            "timestamp": response["timestamp"],
        }

        assert await support_communicator_supporter.receive_json_from() == {
            "type": "unreadMessagesCount",
            "chatOwner": async_user.username,
            "unreadMessagesCount": 1,
        }


@freeze_time("2020-06-17T01:03:00+02:00", ignore=["asgiref", "asyncio"])
@pytest.mark.asyncio
@pytest.mark.django_db
async def test_send_message(
    async_user: User, async_other_user: User, async_active_supporter_user: User, async_example_tenant: Site
) -> None:
    async def assert_malformed_send(chat_communicator: WebsocketCommunicator) -> None:
        await chat_communicator.send_json_to({"type": "send", "message": 42})
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

        await chat_communicator.send_json_to({"type": "send"})
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

    unread_user_messages = 0
    unread_supporter_messages = 0

    class Direction(Enum):
        TO_SUPPORTER = auto()
        TO_USER = auto()

    async def assert_send(direction: Direction, message: str) -> None:
        nonlocal unread_user_messages
        nonlocal unread_supporter_messages

        if direction == Direction.TO_SUPPORTER:
            chat_communicator_sender = chat_communicator_user
            unread_user_messages += 1
        else:
            chat_communicator_sender = chat_communicator_supporter
            unread_supporter_messages += 1

        sender_real_name = str(chat_communicator_sender.user)

        await chat_communicator_sender.send_json_to({"type": "send", "message": message})

        response = await chat_communicator_user.receive_json_from()
        assert isinstance(response.pop("id"), int)
        assert response == {
            "type": "message",
            "message": message,
            "isOwnMessage": direction == Direction.TO_SUPPORTER,
            "userRealName": sender_real_name,
            "timestamp": timezone.now().isoformat(),
        }

        response = await chat_communicator_supporter.receive_json_from()
        assert isinstance(response.pop("id"), int)
        assert response == {
            "type": "message",
            "message": message,
            "isOwnMessage": direction == Direction.TO_USER,
            "userRealName": sender_real_name,
            "timestamp": timezone.now().isoformat(),
        }

        if direction == Direction.TO_USER:
            assert await chat_communicator_user.receive_json_from() == {
                "type": "unreadMessagesCount",
                "unreadMessagesCount": unread_supporter_messages,
            }

            assert await chat_communicator_supporter.receive_json_from() == {
                "type": "unreadMessagesCount",
                "unreadMessagesCount": unread_user_messages,
            }

    async with (
        create_chat(async_user, async_user.tenant) as chat,
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        chat_consumer(async_other_user, async_other_user.tenant),
        chat_consumer(
            async_active_supporter_user, async_user.tenant, async_user.username
        ) as chat_communicator_supporter,
    ):
        await chat_communicator_supporter.send_json_to({"type": "send", "message": random_message()})
        await assert_error(chat_communicator_supporter, Error.SUPPORTER_NOT_JOINED_CHAT)

        await assert_send(Direction.TO_SUPPORTER, random_message())

        async with join_chat(async_user, chat_communicator_supporter, chat_communicator_user):
            await assert_malformed_send(chat_communicator_user)
            await assert_malformed_send(chat_communicator_supporter)

            await assert_send(Direction.TO_SUPPORTER, random_message())
            await assert_send(Direction.TO_USER, random_message())

            message_max_length = (
                await database_sync_to_async(SupportChatParameter.objects.load)(async_example_tenant)
            ).message_max_length

            await assert_send(Direction.TO_SUPPORTER, random_string(message_max_length))
            await chat_communicator_user.send_json_to(
                {"type": "send", "message": random_string(message_max_length + 1)}
            )
            await assert_error(chat_communicator_user, Error.MESSAGE_TOO_LONG)

            await database_sync_to_async(chat.refresh_from_db)()
            assert await database_sync_to_async(lambda: chat.owner)() == async_user

            assert await database_sync_to_async(ChatMessage.objects.filter(chat=chat).count)() == 4

            assert chat.unread_owner_messages == 3

            assert chat.is_support_active
            assert chat.support_active == 1
            assert chat.unread_support_messages == 1


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_history(async_user: User, async_other_user: User, async_active_supporter_user: User) -> None:
    async def assert_history(chat_communicator: WebsocketCommunicator) -> None:
        prepared_messages = prepare_messages(chat_communicator, chat_messages)

        await chat_communicator.send_json_to({"type": "getHistory", "beforeMessageId": "I am not an int!"})
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

        await chat_communicator.send_json_to({"type": "getHistory"})
        assert await chat_communicator.receive_json_from() == {"type": "history", "messages": prepared_messages[:4:-1]}

        await chat_communicator.send_json_to({"type": "getHistory", "beforeMessageId": prepared_messages[5]["id"]})
        assert await chat_communicator.receive_json_from() == {"type": "history", "messages": prepared_messages[4::-1]}

        await chat_communicator.send_json_to({"type": "getHistory", "beforeMessageId": prepared_messages[0]["id"]})
        assert await chat_communicator.receive_json_from() == {"type": "historyEnd"}

    async with (
        create_chat(async_user, async_user.tenant) as chat,
        fill_chat(chat, async_user, async_active_supporter_user, 15) as chat_messages,
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        chat_consumer(async_other_user, async_other_user.tenant),
        chat_consumer(
            async_active_supporter_user, async_user.tenant, async_user.username
        ) as chat_communicator_supporter,
    ):
        await assert_history(chat_communicator_user)
        await assert_history(chat_communicator_supporter)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_new_messages(async_user: User, async_other_user: User, async_active_supporter_user: User) -> None:
    async def assert_new_messages(chat_communicator: WebsocketCommunicator) -> None:
        prepared_messages = prepare_messages(chat_communicator, chat_messages)

        await chat_communicator.send_json_to({"type": "getNewMessages"})
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

        await chat_communicator.send_json_to({"type": "getNewMessages", "afterMessageId": "I am not an int!"})
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

        await chat_communicator.send_json_to({"type": "getNewMessages", "afterMessageId": chat_messages[9].id})
        assert await chat_communicator.receive_json_from() == {
            "type": "newMessages",
            "messages": prepared_messages[10:],
        }

        await chat_communicator.send_json_to({"type": "getNewMessages", "afterMessageId": chat_messages[14].id})
        assert await chat_communicator.receive_json_from() == {"type": "newMessages", "messages": []}

        await chat_communicator.send_json_to({"type": "getNewMessages", "afterMessageId": chat_messages[4].id})
        assert await chat_communicator.receive_json_from() == {"type": "newMessages", "messages": prepared_messages[5:]}

        await chat_communicator.send_json_to({"type": "getNewMessages", "afterMessageId": chat_messages[3].id})
        assert await chat_communicator.receive_json_from() == {"type": "tooManyNewMessages"}

        await chat_communicator.send_json_to({"type": "getNewMessages", "afterMessageId": chat_messages[0].id})
        assert await chat_communicator.receive_json_from() == {"type": "tooManyNewMessages"}

    async with (
        create_chat(async_user, async_user.tenant) as chat,
        fill_chat(chat, async_user, async_active_supporter_user, 15) as chat_messages,
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        chat_consumer(async_other_user, async_other_user.tenant),
        chat_consumer(
            async_active_supporter_user, async_user.tenant, async_user.username
        ) as chat_communicator_supporter,
    ):
        await assert_new_messages(chat_communicator_user)
        await assert_new_messages(chat_communicator_supporter)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_messages_read(async_user: User, async_other_user: User, async_active_supporter_user: User) -> None:
    async with (
        create_chat(async_user, async_user.tenant) as chat,
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        chat_consumer(
            async_active_supporter_user, async_user.tenant, async_user.username
        ) as chat_communicator_supporter,
        support_consumer(
            async_active_supporter_user, async_active_supporter_user.tenant
        ) as support_communicator_supporter,
        chat_consumer(async_other_user, async_other_user.tenant),
    ):
        await chat_communicator_supporter.send_json_to({"type": "messagesRead"})
        await assert_error(chat_communicator_supporter, Error.SUPPORTER_NOT_JOINED_CHAT)

        async with (
            join_chat(
                async_user,
                chat_communicator_supporter,
                chat_communicator_user,
                support_communicator_supporter,
            ),
            fill_chat(chat, async_user, async_active_supporter_user, (10, 5)),
        ):
            assert chat.unread_owner_messages == 10
            await chat_communicator_supporter.send_json_to({"type": "messagesRead"})
            assert await support_communicator_supporter.receive_json_from() == {
                "type": "unreadMessagesCount",
                "chatOwner": async_user.username,
                "unreadMessagesCount": 0,
            }
            await database_sync_to_async(chat.refresh_from_db)()
            assert chat.unread_owner_messages == 0

        @transaction.atomic
        def reset_unread_messages() -> None:
            chat.refresh_from_db()
            chat.unread_owner_messages = 0
            chat.unread_support_messages = 0
            chat.save()

        await database_sync_to_async(reset_unread_messages)()

        async with (
            chat_consumer(async_user, async_user.tenant) as other_chat_communicator_user,
            fill_chat(chat, async_user, async_active_supporter_user, (5, 10)),
        ):
            assert chat.unread_support_messages == 10
            await chat_communicator_user.send_json_to({"type": "messagesRead"})
            expected_user_response = {"type": "unreadMessagesCount", "unreadMessagesCount": 0}
            assert await chat_communicator_user.receive_json_from() == expected_user_response
            assert await other_chat_communicator_user.receive_json_from() == expected_user_response
            assert await chat_communicator_supporter.receive_json_from() == {
                "type": "unreadMessagesCount",
                "unreadMessagesCount": 5,
            }
            await database_sync_to_async(chat.refresh_from_db)()
            assert chat.unread_support_messages == 0


@pytest.mark.asyncio
async def test_get_chat_status(async_user: User, async_other_user: User, async_active_supporter_user: User) -> None:
    async def send_get_chat_status(chat_communicator: WebsocketCommunicator) -> bool:
        await chat_communicator.send_json_to({"type": "getChatStatus"})

        response = await chat_communicator.receive_json_from()
        response_type = response.pop("type")
        assert response_type in ["chatGainedSupport", "chatLostSupport"]
        assert len(response) == 0

        return response_type == "chatGainedSupport"

    async with (
        create_chat(async_user, async_user.tenant),
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        support_consumer(
            async_active_supporter_user, async_active_supporter_user.tenant
        ) as support_communicator_supporter,
        chat_consumer(async_other_user, async_other_user.tenant),
    ):
        assert await send_get_chat_status(chat_communicator_user) is False

        async with (
            chat_consumer(
                async_active_supporter_user, async_user.tenant, async_user.username
            ) as chat_communicator_supporter,
            join_chat(
                async_user,
                chat_communicator_supporter,
                chat_communicator_user,
                support_communicator_supporter,
            ),
        ):
            assert await send_get_chat_status(chat_communicator_user) is True
            assert await send_get_chat_status(chat_communicator_supporter) is True

        assert await send_get_chat_status(chat_communicator_user) is False


@pytest.mark.asyncio
async def test_join_leave_chat(
    async_user: User, async_other_user: User, async_inactive_supporter_user: User, async_active_supporter_user: User
) -> None:
    async def assert_gained_support(
        support_communicator: WebsocketCommunicator, *chat_communicators: WebsocketCommunicator
    ) -> None:
        assert await support_communicator.receive_json_from() == {
            "type": "chatGainedSupport",
            "chatOwner": async_user.username,
        }

        for chat_communicator in chat_communicators:
            assert await chat_communicator.receive_json_from() == {"type": "chatGainedSupport"}

    async def assert_lost_support(
        support_communicator: WebsocketCommunicator, *chat_communicators: WebsocketCommunicator
    ) -> None:
        assert await support_communicator.receive_json_from() == {
            "type": "chatLostSupport",
            "chatOwner": async_user.username,
        }

        for chat_communicator in chat_communicators:
            assert await chat_communicator.receive_json_from() == {"type": "chatLostSupport"}

    async with (
        create_chat(async_user, async_user.tenant),
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        support_consumer(
            async_active_supporter_user, async_active_supporter_user.tenant
        ) as support_communicator_supporter,
        chat_consumer(async_other_user, async_other_user.tenant),
    ):
        await chat_communicator_user.send_json_to({"type": "joinChat"})
        await assert_error(chat_communicator_user, Error.PERMISSION_DENIED)

        async with chat_consumer(
            async_active_supporter_user, async_user.tenant, async_user.username
        ) as chat_communicator_supporter:
            await chat_communicator_supporter.send_json_to({"type": "joinChat"})

            await assert_gained_support(
                support_communicator_supporter, chat_communicator_user, chat_communicator_supporter
            )

            await chat_communicator_supporter.send_json_to({"type": "joinChat"})

        await assert_lost_support(support_communicator_supporter, chat_communicator_user)

        async with chat_consumer(
            async_active_supporter_user, async_user.tenant, async_user.username
        ) as chat_communicator_supporter:
            async with join_chat(
                async_user,
                chat_communicator_supporter,
                chat_communicator_user,
                support_communicator_supporter,
            ):
                await chat_communicator_supporter.send_json_to({"type": "joinChat"})

            await chat_communicator_supporter.send_json_to({"type": "leaveChat"})
            await chat_communicator_supporter.receive_nothing(timeout=1)

        async with (
            chat_consumer(
                async_active_supporter_user, async_user.tenant, async_user.username
            ) as chat_communicator_supporter,
            join_chat(
                async_user,
                chat_communicator_supporter,
                chat_communicator_user,
                support_communicator_supporter,
            ),
        ):
            await support_communicator_supporter.send_json_to({"type": "setStatus", "status": "inactive"})
            assert await support_communicator_supporter.receive_json_from() == {
                "type": "status",
                "status": "inactive",
            }
            assert await chat_communicator_user.receive_json_from() == {"type": "supportOffline"}
            assert await chat_communicator_supporter.receive_json_from() == {"type": "supportOffline"}

            await assert_lost_support(
                support_communicator_supporter, chat_communicator_user, chat_communicator_supporter
            )

            await support_communicator_supporter.send_json_to({"type": "setStatus", "status": "active"})
            assert await support_communicator_supporter.receive_json_from() == {
                "type": "status",
                "status": "active",
            }
            assert await chat_communicator_user.receive_json_from() == {"type": "supportOnline"}
            assert await chat_communicator_supporter.receive_json_from() == {"type": "supportOnline"}

            await support_communicator_supporter.receive_nothing(timeout=1)
            await chat_communicator_user.receive_nothing(timeout=1)
            await chat_communicator_supporter.receive_nothing(timeout=1)

            await chat_communicator_supporter.send_json_to({"type": "joinChat"})

            await assert_gained_support(
                support_communicator_supporter, chat_communicator_user, chat_communicator_supporter
            )

        async with chat_consumer(
            async_inactive_supporter_user, async_user.tenant, async_user.username
        ) as chat_communicator_inactive_supporter_user:
            await chat_communicator_inactive_supporter_user.send_json_to({"type": "joinChat"})
            await assert_error(chat_communicator_inactive_supporter_user, Error.SUPPORTER_STATUS_INACTIVE)

            await chat_communicator_inactive_supporter_user.send_json_to({"type": "leaveChat"})
            await assert_error(chat_communicator_inactive_supporter_user, Error.SUPPORTER_STATUS_INACTIVE)


@pytest.mark.asyncio
async def test_get_support_status(
    async_user: User, async_other_user: User, async_inactive_supporter_user: User
) -> None:
    async def send_get_support_status(chat_communicator: WebsocketCommunicator) -> bool:
        await chat_communicator.send_json_to({"type": "getSupportStatus"})

        response = await chat_communicator.receive_json_from()
        response_type = response.pop("type")
        assert response_type in ["supportOnline", "supportOffline"]
        assert len(response) == 0

        return response_type == "supportOnline"

    # Clear cached value to ensure `filter_support_eligible` will be called inside a lambda,
    # otherwise this would result in an SynchronousOnlyOperation exception being raised.
    support_chat.models._cached_support_users_permission._wrapped = empty

    async with (
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        support_consumer(
            async_inactive_supporter_user, async_inactive_supporter_user.tenant
        ) as support_communicator_inactive_supporter_user,
        chat_consumer(async_other_user, async_other_user.tenant),
    ):
        assert await send_get_support_status(chat_communicator_user) is False

        await support_communicator_inactive_supporter_user.send_json_to({"type": "setStatus", "status": "active"})

        assert await chat_communicator_user.receive_json_from() == {"type": "supportOnline"}
        assert await send_get_support_status(chat_communicator_user) is True

        await support_communicator_inactive_supporter_user.send_json_to({"type": "setStatus", "status": "inactive"})

        assert await chat_communicator_user.receive_json_from() == {"type": "supportOffline"}
        assert await send_get_support_status(chat_communicator_user) is False


@pytest.mark.asyncio
async def test_get_unread_messages_count(
    async_user: User, async_other_user: User, async_active_supporter_user: User
) -> None:
    async def send_get_unread_messages_count(chat_communicator: WebsocketCommunicator) -> int:
        await chat_communicator.send_json_to({"type": "getUnreadMessagesCount"})
        unread_messages_count_response = await chat_communicator.receive_json_from()
        unread_messages_count = unread_messages_count_response.pop("unreadMessagesCount")

        assert unread_messages_count_response == {"type": "unreadMessagesCount"}
        assert isinstance(unread_messages_count, int)
        return unread_messages_count

    async with (
        create_chat(async_user, async_user.tenant) as chat,
        fill_chat(chat, async_user, async_active_supporter_user),
        chat_consumer(async_user, async_user.tenant) as chat_communicator_user,
        chat_consumer(
            async_active_supporter_user, async_user.tenant, async_user.username
        ) as chat_communicator_supporter,
        chat_consumer(async_user, async_user.tenant),
        chat_consumer(async_other_user, async_other_user.tenant),
    ):
        assert chat.unread_support_messages == await send_get_unread_messages_count(chat_communicator_user)

        assert chat.unread_owner_messages == await send_get_unread_messages_count(chat_communicator_supporter)
