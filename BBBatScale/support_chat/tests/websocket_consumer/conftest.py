from __future__ import annotations

import random
import string
from contextlib import asynccontextmanager
from typing import AsyncIterator, List, Optional, Tuple, Union

import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator as ChannelsWebsocketCommunicator
from core.models import User
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.models import Site
from django.db import transaction
from support_chat.models import Chat, ChatMessage
from utils.websockets import Error, WebsocketConsumerNotOpenError

from BBBatScale.routing import websocket_urlrouter


def random_string(length: int) -> str:
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for _ in range(length))


def random_message() -> str:
    return random_string(random.randint(16, 128))


class WebsocketCommunicator(ChannelsWebsocketCommunicator):
    def __init__(self, path: str, tenant: Site, user: Union[User, AnonymousUser]) -> None:
        super().__init__(websocket_urlrouter, path)
        self.user = user
        self.scope["tenant"] = tenant
        self.scope["user"] = user

    async def __aenter__(self) -> WebsocketCommunicator:
        connected, _ = await self.connect()
        assert connected is True
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb) -> bool:
        receive_nothing_error = None
        try:
            assert await self.receive_nothing(timeout=1)
        except BaseException as receive_nothing_error:
            receive_nothing_error.__cause__ = exc_val

        try:
            await self.disconnect()
        except BaseException as e:
            if receive_nothing_error:
                e.__cause__ = receive_nothing_error
            else:
                e.__cause__ = exc_val
            raise e
        return False

    async def get_close_code(self, timeout: int = 1) -> int:
        response = await self.receive_output(timeout)
        assert response["type"] == "websocket.close"
        assert list(response.keys()) == ["type", "code"]

        code = response["code"]
        assert isinstance(code, int)
        return code


def chat_consumer(
    user: Union[User, AnonymousUser], tenant: Site, chat_owner: Optional[str] = None
) -> WebsocketCommunicator:
    if chat_owner is None:
        return WebsocketCommunicator("/ws/supportchat/chat", tenant, user)
    else:
        return WebsocketCommunicator(f"/ws/supportchat/chat/{chat_owner}", tenant, user)


def support_consumer(user: Union[User, AnonymousUser], tenant: Site) -> WebsocketCommunicator:
    return WebsocketCommunicator("/ws/supportchat/support", tenant, user)


@asynccontextmanager
async def create_chat(owner: User, tenant: Site) -> AsyncIterator[Chat]:
    chat, created = await database_sync_to_async(Chat.objects.get_or_create)(owner=owner, tenant=tenant)
    assert created

    yield chat

    await database_sync_to_async(chat.delete)()


@asynccontextmanager
async def fill_chat(
    chat: Chat, owner: User, supporter: User, message_count: Optional[Union[int, Tuple[int, int]]] = None
) -> AsyncIterator[List[ChatMessage]]:
    @transaction.atomic
    def create() -> List[ChatMessage]:
        chat.refresh_from_db()
        previously_unread_messages = chat.unread_owner_messages + chat.unread_support_messages

        if message_count is None or isinstance(message_count, int):
            _messages = list(
                ChatMessage.objects.create_and_update(
                    user=supporter if is_supporter else owner,
                    chat=chat,
                    message=random_message(),
                    is_supporter=is_supporter,
                )
                for is_supporter in random.choices([True, False], k=message_count or random.randint(2, 4))
            )
        else:
            owner_message_count, supporter_message_count = message_count
            owner_messages = list(
                ChatMessage.objects.create_and_update(
                    user=owner, chat=chat, message=random_message(), is_supporter=False
                )
                for _ in range(owner_message_count)
            )
            supporter_messages = list(
                ChatMessage.objects.create_and_update(
                    user=supporter, chat=chat, message=random_message(), is_supporter=True
                )
                for _ in range(supporter_message_count)
            )

            _messages = owner_messages + supporter_messages

        assert chat.unread_owner_messages + chat.unread_support_messages == previously_unread_messages + len(_messages)
        return _messages

    @transaction.atomic
    def delete(_messages) -> None:
        for message in _messages:
            message.delete()

    messages = await database_sync_to_async(create)()

    yield messages

    await database_sync_to_async(delete)(messages)


@asynccontextmanager
async def join_chat(
    user: User,
    chat_communicator_supporter: WebsocketCommunicator,
    chat_communicators: Union[None, WebsocketCommunicator, List[WebsocketCommunicator]] = None,
    support_communicators: Union[None, WebsocketCommunicator, List[WebsocketCommunicator]] = None,
) -> AsyncIterator[None]:
    if chat_communicators is None:
        chat_communicators = []
    elif isinstance(chat_communicators, WebsocketCommunicator):
        chat_communicators = [chat_communicators]

    if support_communicators is None:
        support_communicators = []
    elif isinstance(support_communicators, WebsocketCommunicator):
        support_communicators = [support_communicators]

    await chat_communicator_supporter.send_json_to({"type": "joinChat"})

    response: dict = await chat_communicator_supporter.receive_json_from()
    assert response == {"type": "chatGainedSupport"}
    for chat_communicator in chat_communicators:
        response: dict = await chat_communicator.receive_json_from()
        assert response == {"type": "chatGainedSupport"}
    for support_communicator in support_communicators:
        response: dict = await support_communicator.receive_json_from()
        assert response == {"type": "chatGainedSupport", "chatOwner": user.username}

    yield

    await chat_communicator_supporter.send_json_to({"type": "leaveChat"})

    response: dict = await chat_communicator_supporter.receive_json_from()
    assert response == {"type": "chatLostSupport"}
    for chat_communicator in chat_communicators:
        response: dict = await chat_communicator.receive_json_from()
        assert response == {"type": "chatLostSupport"}
    for support_communicator in support_communicators:
        response: dict = await support_communicator.receive_json_from()
        assert response == {"type": "chatLostSupport", "chatOwner": user.username}


async def assert_error(communicator: WebsocketCommunicator, error: Error) -> None:
    response = await communicator.receive_json_from()
    assert "type" in response
    assert response["type"] == "error"
    assert Error(response["code"]) == error


async def assert_connection_refuse(communicator: WebsocketCommunicator, error: Error) -> None:
    with pytest.raises(WebsocketConsumerNotOpenError) as exc_info:
        async with communicator:
            await assert_error(communicator, error)
            assert await communicator.get_close_code() == 1000

    if exc_info.value.__cause__ is not None:
        raise exc_info.value.__cause__


async def assert_illegal_request(communicator: WebsocketCommunicator) -> None:
    await communicator.send_json_to({})
    await assert_error(communicator, Error.MALFORMED_REQUEST)

    await communicator.send_json_to({"type": "I am an illegal request!"})
    await assert_error(communicator, Error.UNKNOWN_REQUEST)
