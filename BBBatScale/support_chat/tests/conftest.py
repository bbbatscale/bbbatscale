from __future__ import annotations

from contextlib import asynccontextmanager
from typing import AsyncIterator, Optional, Tuple

import pytest
from channels.db import database_sync_to_async
from core.models import User
from core.utils import get_permission, load_moderator_group
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.db import transaction
from support_chat.models import Supporter


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_moderator_group(async_example_tenant: Site) -> AsyncIterator[Group]:
    moderator_group = await database_sync_to_async(load_moderator_group)(async_example_tenant)
    yield moderator_group
    await database_sync_to_async(moderator_group.delete)()


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_user(async_example_tenant: Site) -> AsyncIterator[User]:
    user = await database_sync_to_async(User.objects.create)(
        username="john_doe", first_name="John", last_name="Doe", tenant=async_example_tenant
    )
    yield user
    await database_sync_to_async(user.delete)()


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_moderator(async_moderator_group: Group, async_example_tenant: Site) -> AsyncIterator[User]:
    moderator = await database_sync_to_async(User.objects.create)(
        username="jeff_doe", first_name="Jeff", last_name="Doe", tenant=async_example_tenant
    )
    await database_sync_to_async(moderator.groups.add)(async_moderator_group)

    yield moderator
    await database_sync_to_async(moderator.delete)()


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_other_user(async_example_tenant: Site) -> AsyncIterator[User]:
    user = await database_sync_to_async(User.objects.create)(
        username="jane_doe", first_name="Jane", last_name="Doe", tenant=async_example_tenant
    )
    yield user
    await database_sync_to_async(user.delete)()


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_user_other_tenant(async_other_example_tenant: Site) -> AsyncIterator[User]:
    user = await database_sync_to_async(User.objects.create)(
        username="john_cloe", first_name="John", last_name="Cloe", tenant=async_other_example_tenant
    )
    yield user
    await database_sync_to_async(user.delete)()


@asynccontextmanager
async def create_supporter_user(
    active: bool, tenant: Site, *, names: Optional[Tuple[str, str, str]] = None
) -> AsyncIterator[User]:
    @transaction.atomic
    def create() -> Tuple[User, Supporter]:
        if names:
            username, first_name, last_name = names
        elif active:
            username, first_name, last_name = ("jane_roe", "Jane", "Roe")
        else:
            username, first_name, last_name = ("john_roe", "John", "Roe")

        _user = User.objects.create(username=username, first_name=first_name, last_name=last_name, tenant=tenant)
        _user.user_permissions.add(get_permission("support_chat.support_users"))

        _supporter = Supporter.objects.create(user=_user, tenant=tenant, is_status_active=active)
        return _user, _supporter

    @transaction.atomic
    def delete(_user: User, _supporter: Supporter):
        _supporter.delete()
        _user.delete()

    user, supporter = await database_sync_to_async(create)()

    yield user

    await database_sync_to_async(delete)(user, supporter)


# TODO add test using staff and superuser instead of the supporter group
#  to inactive_supporter_user and active_supporter_user


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_inactive_supporter_user(async_example_tenant: Site) -> AsyncIterator[User]:
    async with create_supporter_user(False, async_example_tenant) as supporter:
        yield supporter


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_active_supporter_user(async_example_tenant: Site) -> AsyncIterator[User]:
    async with create_supporter_user(True, async_example_tenant) as supporter:
        yield supporter


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_inactive_supporter_user_other_tenant(async_other_example_tenant: Site) -> AsyncIterator[User]:
    async with create_supporter_user(
        False, async_other_example_tenant, names=("jane_cloe", "Jane", "Cloe")
    ) as supporter:
        yield supporter


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_active_supporter_user_other_tenant(async_other_example_tenant: Site) -> AsyncIterator[User]:
    async with create_supporter_user(True, async_other_example_tenant, names=("john_zoe", "John", "Zoe")) as supporter:
        yield supporter


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_staff_user(async_example_tenant: Site) -> AsyncIterator[User]:
    user = await database_sync_to_async(User.objects.create)(
        username="jeff_roe", first_name="Jeff", last_name="Roe", is_staff=True, tenant=async_example_tenant
    )
    yield user
    await database_sync_to_async(user.delete)()


@pytest.mark.asyncio
@pytest.fixture(scope="function")
@pytest.mark.django_db
async def async_superuser_user(async_example_tenant: Site) -> AsyncIterator[User]:
    user = await database_sync_to_async(User.objects.create)(
        username="jodi_roe", first_name="Jodi", last_name="Doe", is_staff=True, tenant=async_example_tenant
    )
    yield user
    await database_sync_to_async(user.delete)()
