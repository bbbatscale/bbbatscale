from functools import wraps
from typing import Callable

import pytest
from channels.db import database_sync_to_async
from core.models import User
from django.contrib.sites.models import Site
from support_chat.models import SupportChatParameter
from support_chat.utils import can_inactivate_all_supporters


def database_sync_to_async_decorator(func: Callable) -> Callable:
    @wraps(func)
    async def wrapper(*args, **kwargs):
        return await database_sync_to_async(func)(*args, **kwargs)

    return wrapper


def assert_can_inactivate_all_supporters(
    tenant: Site, user: User, expectation_without_allow: bool, expectation_with_allow: bool
) -> None:
    support_chat_parameter = SupportChatParameter.objects.load(tenant)
    support_chat_parameter.allow_supporter_to_inactivate_all = False
    support_chat_parameter.save(update_fields=["allow_supporter_to_inactivate_all"])

    assert can_inactivate_all_supporters(tenant, user) is expectation_without_allow

    support_chat_parameter.allow_supporter_to_inactivate_all = True
    support_chat_parameter.save(update_fields=["allow_supporter_to_inactivate_all"])

    assert can_inactivate_all_supporters(tenant, user) is expectation_with_allow


@pytest.mark.asyncio
@database_sync_to_async_decorator
def test_normal_user_can_inactivate_all_supporters(async_example_tenant: Site, async_user: User) -> None:
    assert_can_inactivate_all_supporters(async_example_tenant, async_user, False, False)


@pytest.mark.asyncio
@database_sync_to_async_decorator
def test_staff_user_can_inactivate_all_supporters(async_example_tenant: Site, async_staff_user: User) -> None:
    assert_can_inactivate_all_supporters(async_example_tenant, async_staff_user, True, True)


@pytest.mark.asyncio
@database_sync_to_async_decorator
def test_superuser_user_can_inactivate_all_supporters(async_example_tenant: Site, async_superuser_user: User) -> None:
    assert_can_inactivate_all_supporters(async_example_tenant, async_superuser_user, True, True)


@pytest.mark.asyncio
@database_sync_to_async_decorator
def test_inactive_supporter_user_can_inactivate_all_supporters(
    async_example_tenant: Site, async_inactive_supporter_user: User
) -> None:
    assert_can_inactivate_all_supporters(async_example_tenant, async_inactive_supporter_user, False, True)


@pytest.mark.asyncio
@database_sync_to_async_decorator
def test_active_supporter_user_can_inactivate_all_supporters(
    async_example_tenant: Site, async_active_supporter_user: User
) -> None:
    assert_can_inactivate_all_supporters(async_example_tenant, async_active_supporter_user, False, True)
