from typing import Callable

import pytest
from core.models import User
from core.utils import get_permission, has_tenant_based_perm
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from support_chat.models import Supporter


@pytest.mark.django_db
def test_filter_support_eligible_equals_has_tenant_based_perm(testserver_tenant: Site, testserver2_tenant: Site):
    def fetch_user() -> User:
        # Always fetch a new instance, since the ModelBackend does cache the permissions.
        return User.objects.get(username="example_user")

    def filter_support_eligible(tenant: Site) -> bool:
        return Supporter.objects.filter_support_eligible(tenant).filter(user=fetch_user()).exists()

    def _assert_filter_support_eligible(value: bool, *, on_all_tenants: bool = False) -> None:
        assert filter_support_eligible(testserver_tenant) == has_tenant_based_perm(
            fetch_user(), "support_chat.support_users", testserver_tenant
        )
        assert filter_support_eligible(testserver_tenant) is value

        assert filter_support_eligible(testserver2_tenant) == has_tenant_based_perm(
            fetch_user(), "support_chat.support_users", testserver2_tenant
        )
        assert filter_support_eligible(testserver2_tenant) is (value if on_all_tenants else False)

    def assert_filter_support_eligible(
        set_up: Callable[[], None], tear_down: Callable[[], None], *, on_all_tenants: bool = False
    ) -> None:
        set_up()
        _assert_filter_support_eligible(True, on_all_tenants=on_all_tenants)

        tear_down()
        _assert_filter_support_eligible(False)

    user = User.objects.create_user("example_user", tenant=testserver_tenant)
    Supporter.objects.create(user=user, tenant=testserver_tenant)
    Supporter.objects.create(user=user, tenant=testserver2_tenant)

    permission = get_permission("support_chat.support_users")
    group = Group.objects.create(name="support_users_group")
    group.permissions.add(permission)

    _assert_filter_support_eligible(False)

    assert_filter_support_eligible(
        lambda: fetch_user().user_permissions.add(permission),
        lambda: fetch_user().user_permissions.remove(permission),
    )

    assert_filter_support_eligible(
        lambda: fetch_user().groups.add(group),
        lambda: fetch_user().groups.remove(group),
    )

    assert_filter_support_eligible(
        lambda: User.objects.filter(username="example_user").update(is_staff=True),
        lambda: User.objects.filter(username="example_user").update(is_staff=False),
    )

    assert_filter_support_eligible(
        lambda: User.objects.filter(username="example_user").update(is_superuser=True),
        lambda: User.objects.filter(username="example_user").update(is_superuser=False),
        on_all_tenants=True,
    )
