from __future__ import annotations

from typing import Any

from core.models import User
from core.utils import get_permission, has_tenant_based_perm
from django.contrib.sites.models import Site
from django.core.exceptions import PermissionDenied
from django.db import models, transaction
from django.db.models import F, Q
from django.utils.functional import SimpleLazyObject
from django.utils.translation import gettext_lazy as _


class SupportChatParameterManager(models.Manager):
    def load(self, tenant: Site) -> SupportChatParameter:
        return self.get_or_create(tenant=tenant)[0]


class SupportChatParameter(models.Model):
    tenant = models.OneToOneField(Site, on_delete=models.CASCADE)
    enabled = models.BooleanField(_("Enable Support Chat"), default=False)
    disable_chat_if_offline = models.BooleanField(_("Disable Support Chat if offline"), default=False)
    allow_supporter_to_inactivate_all = models.BooleanField(
        _("Allow supporter to inactivate all supporters"), default=False
    )
    message_max_length = models.IntegerField(_("Maximum message length"), default=255)

    objects = SupportChatParameterManager()


class Chat(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["owner", "tenant"], name="support_chat_chat_owner_tenant_unique")
        ]

    owner = models.ForeignKey(User, on_delete=models.CASCADE, editable=False)
    tenant = models.ForeignKey(Site, on_delete=models.CASCADE, editable=False)
    support_active = models.PositiveIntegerField(default=0)
    unread_owner_messages = models.PositiveIntegerField(default=0)
    unread_support_messages = models.PositiveIntegerField(default=0)

    @property
    def is_support_active(self) -> bool:
        return self.support_active > 0


class ChatMessageManager(models.Manager):
    @transaction.atomic
    def create_and_update(
        self, chat: Chat, user: User, message: str, *args: Any, is_supporter: bool, **kwargs: Any
    ) -> ChatMessage:
        if is_supporter:
            chat.unread_support_messages = F("unread_support_messages") + 1
            chat.save(update_fields=["unread_support_messages"])
            chat.refresh_from_db(fields=["unread_support_messages"])
        else:
            chat.unread_owner_messages = F("unread_owner_messages") + 1
            chat.save(update_fields=["unread_owner_messages"])
            chat.refresh_from_db(fields=["unread_owner_messages"])
        return self.create(*args, chat=chat, user=user, message=message, **kwargs)


class ChatMessage(models.Model):
    id = models.BigAutoField(primary_key=True, editable=False)
    timestamp = models.DateTimeField(auto_now_add=True, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, editable=False)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, editable=False)
    message = models.TextField(editable=False)

    objects = ChatMessageManager()


class PreparedAnswer(models.Model):
    tenant = models.ForeignKey(Site, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    text = models.TextField()


class SupporterManager(models.Manager):
    def load(self, user: User, tenant: Site) -> Supporter:
        if not has_tenant_based_perm(user, "support_chat.support_users", tenant):
            raise PermissionDenied

        return self.get_or_create(user=user, tenant=tenant)[0]


# This can not be a class member of SupporterQuerySet since it would be evaluated before the Models are loaded,
# caused by the inspection of the Manager.from_queryset method.
_cached_support_users_permission = SimpleLazyObject(lambda: get_permission("support_chat.support_users"))


class SupporterQuerySet(models.QuerySet):
    def filter_support_eligible(self, tenant: Site) -> SupporterQuerySet:
        filter_is_superuser = Q(user__is_superuser=True)
        filter_user_in_tenant = Q(user__tenant=tenant)
        filter_user_is_staff = Q(user__is_staff=True)
        filter_user_has_user_perm = Q(user__user_permissions__in=[_cached_support_users_permission])
        filter_user_has_group_perm = Q(user__groups__permissions__in=[_cached_support_users_permission])

        filter_user_has_perm = filter_user_has_user_perm | filter_user_has_group_perm

        filter_user_has_tenant_based_perm = filter_user_in_tenant & (filter_user_is_staff | filter_user_has_perm)

        filter_is_support_eligible = filter_is_superuser | filter_user_has_tenant_based_perm

        return self.filter(filter_is_support_eligible, tenant=tenant)

    def filter_status_active(self, tenant: Site, status_active: bool) -> SupporterQuerySet:
        return self.filter_support_eligible(tenant).filter(is_status_active=status_active)


class Supporter(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["user", "tenant"], name="support_chat_supporter_user_tenant_unique")
        ]
        permissions = [
            ("support_users", _("Can support users")),
        ]

    user = models.ForeignKey(User, on_delete=models.CASCADE, editable=False)
    tenant = models.ForeignKey(Site, on_delete=models.CASCADE, editable=False)
    is_status_active = models.BooleanField(
        _("Status active"),
        default=False,
        help_text=_("If enabled, the user could support other users in the support chat."),
    )

    objects = SupporterManager.from_queryset(SupporterQuerySet)()

    def __str__(self) -> str:
        return str(self.user)
