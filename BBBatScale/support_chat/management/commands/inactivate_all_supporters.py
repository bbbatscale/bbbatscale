import logging
import sys
from typing import Any, Optional

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib.sites.models import Site
from django.core.management import BaseCommand, CommandParser
from django.db import transaction
from support_chat.utils import inactivate_all_supporters

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Inactivate the supporters of one or all tenants."

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--tenant",
            nargs="?",
            help="The tenant to whose supporters should be inactivated. The default is all tenants.",
        )

    @transaction.atomic
    def handle(self, *args: Any, tenant: Optional[str], **options: Any) -> None:
        inactivated_supporters = list()

        if tenant is not None:
            tenant_query = Site.objects.filter(domain=tenant)
            if tenant_query.count() == 0:
                logger.error("Unable to find tenant: " + str(tenant))
                sys.exit(1)
        else:
            tenant_query = Site.objects.all()

        channel_layer = get_channel_layer()
        for tenant in tenant_query:
            inactivated_supporters.extend(async_to_sync(inactivate_all_supporters)(channel_layer, tenant))

        if inactivated_supporters:
            logger.info("\n  ".join(["Inactivated supporters:", *inactivated_supporters]))
        else:
            logger.info("All supporters were already inactive.")
