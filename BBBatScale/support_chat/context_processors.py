from typing import Dict

from django.http import HttpRequest
from support_chat.models import SupportChatParameter
from support_chat.utils import get_support_chat_parameter


def support_chat_parameter(request: HttpRequest) -> Dict[str, SupportChatParameter]:
    return {"support_chat_parameter": get_support_chat_parameter(request)}
