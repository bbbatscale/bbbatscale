from django.urls import path
from support_chat.websockets import ChatConsumer, SupportConsumer

websocket_urlpatterns = [
    path("chat", ChatConsumer),
    path("chat/<path:username>", ChatConsumer),
    path("support", SupportConsumer),
]
