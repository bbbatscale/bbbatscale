from typing import Callable

from core.utils import get_tenant
from django.http import HttpRequest, HttpResponse
from support_chat.models import SupportChatParameter


class CurrentSupportChatParameterMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        assert hasattr(request, "tenant"), (
            "The support chat parameter middleware requires the tenant middleware "
            "to be installed. Edit your MIDDLEWARE setting to insert "
            "'core.middleware.CurrentTenantMiddleware' before "
            "'support_chat.middleware.CurrentSupportChatParameterMiddleware'."
        )
        request.support_chat_parameter = SupportChatParameter.objects.load(get_tenant(request))
        return self.get_response(request)
