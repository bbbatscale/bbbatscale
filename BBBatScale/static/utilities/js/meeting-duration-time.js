function MeetingTimeManager(endTimeElement,
                            durationElement,
                            unixElement) {

    if (durationElement.value) {
        const meeting_duration = durationElement.value;
        setEndTime(meeting_duration);
    }

    durationElement.oninput = function () {
        const meeting_duration = this.value;
        if (meeting_duration === '') {
            setFieldsToNull();
            return;
        }
        setEndTime(meeting_duration);
        setUnixTimestampField(meeting_duration);
    };

    endTimeElement.oninput = function () {
        const end_time = endTimeElement.value;
        if (end_time === '') {
            setFieldsToNull();
            return;
        }
        setDuration(end_time);
        setUnixTimestampField(convertEndTimeToDuration(end_time))
    };

    function setDuration(end_time) {
        durationElement.value = convertEndTimeToDuration(end_time);
    }

    function setEndTime(duration) {
        endTimeElement.value = getTimeString(convertDurationToEndTime(duration));
    }

    function setUnixTimestampField(minutes) {
        const current_browser_time_unix = ((new Date).getTime() / 1000).toFixed(0);
        unixElement.value = parseInt(current_browser_time_unix) + minutes * 60;
    }

    function setFieldsToNull() {
        [endTimeElement, durationElement, unixElement].forEach(item => {
            item.value = ''
        })
    }

    function convertEndTimeToDuration(end_time) {
        const browser_time_as_start_time = getTimeString(new Date());
        return getDifferenceOfTwoTimeStringsInMinutes(browser_time_as_start_time, end_time);
    }

    function convertDurationToEndTime(duration) {
        const browser_time = new Date();
        return new Date(browser_time.getTime() + (duration * 60000));
    }

    function getTimeString(date) {
        let hours = date.getHours().toString().padStart(2, "0");
        let minutes = date.getMinutes().toString().padStart(2, "0");
        return hours + ":" + minutes;
    }

    function getDifferenceOfTwoTimeStringsInMinutes(startTime, endTime) {
        const startTime_minutes = TimeStringToMinutes(startTime);
        let endTime_minutes = TimeStringToMinutes(endTime);
        if (endTime_minutes < startTime_minutes) {
            endTime_minutes += 24 * 60;
        }
        return endTime_minutes - startTime_minutes;
    }

    function TimeStringToMinutes(timeString) {
        const hours = timeString.match(/(\d{2}):(\d{2})/)[1];
        const minutes = timeString.match(/(\d{2}):(\d{2})/)[2];
        return parseInt(hours) * 60 + parseInt(minutes);
    }
}
