from itertools import islice
from typing import Sequence


def join_strings(sequence: Sequence[str]) -> str:
    """
    Joins the given strings to a human-readable string.

    Examples:
        >>> join_strings([])
        ''
        >>> join_strings(['value 1'])
        'value 1'
        >>> join_strings(['value 1', 'value 2'])
        'value 1 and value 2'
        >>> join_strings(['value 1', 'value 2', 'value 3'])
        'value 1, value 2 and value 3'
        >>> join_strings(['value 1', 'value 2', 'value 3', 'value 4'])
        'value 1, value 2, value 3 and value 4'
    """

    iterator = iter(sequence)

    if len(sequence) <= 1:
        return next(iterator, "")

    return "%(comma_seperated)s and %(last)s" % {
        "comma_seperated": ", ".join(islice(iterator, len(sequence) - 1)),
        "last": next(iterator),
    }
