from abc import ABC, abstractmethod

from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _


class BaseHttpException(Exception, ABC):
    # prevent passing arbitrary arguments
    def __init__(self):
        super().__init__()

    @abstractmethod
    def response(self, request: HttpRequest) -> HttpResponse:
        ...


_error_template = "error.html"


class Http400Exception(BaseHttpException):
    def response(self, request: HttpRequest) -> HttpResponse:
        return render(
            request,
            _error_template,
            {
                "header": _("400 - Bad request"),
                "body": _("Your request could not be processed."),
            },
            status=400,
        )


class Http403Exception(BaseHttpException):
    def response(self, request: HttpRequest) -> HttpResponse:
        return render(
            request,
            _error_template,
            {
                "header": _("403 - Forbidden"),
                "body": _("You are not allowed to access this content. Please make sure you are logged in."),
            },
            status=403,
        )


class Http404Exception(BaseHttpException):
    def response(self, request: HttpRequest) -> HttpResponse:
        return render(
            request,
            _error_template,
            {
                "header": _("404 - Not found"),
                "body": _("The content you are looking for could not be found."),
            },
            status=404,
        )


class Http500Exception(BaseHttpException):
    def response(self, request: HttpRequest) -> HttpResponse:
        return render(
            request,
            _error_template,
            {
                "header": _("500 - Internal server error"),
                "body": _("Something went wrong. Please try again later."),
            },
            status=500,
        )
