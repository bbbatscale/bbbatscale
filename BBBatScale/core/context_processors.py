from urllib.parse import quote

from core.utils import get_general_parameter
from django.conf import settings
from django.http import HttpRequest
from django.urls import Resolver404, resolve


def general_parameter(request: HttpRequest):
    return {"general_parameter": get_general_parameter(request)}


def auth_urls(request: HttpRequest):
    login_url = str(settings.LOGIN_URL) + "?next=" + quote(request.get_full_path_info())
    try:
        matcher = resolve(request.path_info)
        # prevent unnecessary redirects to login page on successful login
        if matcher.url_name == "login":
            login_url = request.get_full_path_info()
    except Resolver404:
        pass

    return {
        "login_url": login_url,
        "logout_url": str(settings.LOGOUT_URL),
    }
