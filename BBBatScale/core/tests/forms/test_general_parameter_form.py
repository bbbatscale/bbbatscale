import pytest
from core.forms import GeneralParametersForm


@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"app_title": "Virtual Rooms", "recording_deletion_period": 0}, True),
        ({"app_title": "Virtual Rooms", "recording_deletion_period": -1}, True),
        ({"app_title": None, "recording_deletion_period": 0}, False),
        ({"app_title": "", "recording_deletion_period": 0}, False),
        ({"app_title": "Virtual Rooms", "recording_deletion_period": None}, False),
        ({"app_title": "Virtual Rooms", "recording_deletion_period": 2147483648}, False),
        ({"app_title": "Virtual Rooms"}, False),
        ({"recording_deletion_period": 0}, False),
        ({}, False),
    ],
)
def test_general_parameters_update_form(
    data,
    is_valid,
    example_superuser,
    example_theme,
    gp_test_tenant,
):
    form_data = {
        "default_theme": example_theme,
        "personal_rooms_teacher_max_number": 2,
        "personal_rooms_non_teacher_max_number": 2,
    }
    form_data.update(data)
    form = GeneralParametersForm(
        data=form_data,
        requesting_user=example_superuser,
        instance=gp_test_tenant,
    )

    assert form.is_valid() is is_valid


def test_settings_media_disabled(testserver_tenant, gp_test_tenant, example_user, settings):
    settings.BBBATSCALE_MEDIA_ENABLED = False
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    form = GeneralParametersForm(requesting_user=example_user, instance=gp_test_tenant)
    assert form.fields["media_enabled"].disabled is True


def test_settings_media_enabled_gp_media_disabled(example_user, settings, gp_test_tenant):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = False
    gp_test_tenant.save()
    form = GeneralParametersForm(requesting_user=example_user, instance=gp_test_tenant)
    assert form.fields["media_enabled"].disabled is False
    assert form.fields["moderators_can_upload_slides"].disabled is True
    assert form.fields["slides_max_upload_size"].disabled is True


def test_settings_media_enabled_gp_media_enabled_user_no_perms_slides(example_user, settings, gp_test_tenant):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    form = GeneralParametersForm(requesting_user=example_user, instance=gp_test_tenant)
    assert form.fields["media_enabled"].disabled is False
    assert form.fields["moderators_can_upload_slides"].disabled is False
    assert form.fields["slides_max_upload_size"].disabled is False
    assert form.fields["slides"].disabled is True
    assert form.fields["initial_slides"].disabled is True


def test_settings_media_enabled_gp_media_enabled_user_has_perms_slides(example_superuser, settings, gp_test_tenant):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = True
    gp_test_tenant.save()
    form = GeneralParametersForm(requesting_user=example_superuser, instance=gp_test_tenant)

    assert form.fields["media_enabled"].disabled is False
    assert form.fields["moderators_can_upload_slides"].disabled is False
    assert form.fields["slides_max_upload_size"].disabled is False
    assert form.fields["slides"].disabled is False
    assert form.fields["initial_slides"].disabled is False
