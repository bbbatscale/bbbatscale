import pytest
from core.forms import ApiTokenForm
from core.models import ApiToken, GeneralParameter, SchedulingStrategy
from django.contrib.sites.models import Site


@pytest.mark.django_db
@pytest.fixture(scope="function")
def other_scheduling_strategy(testserver_tenant: Site) -> SchedulingStrategy:
    scheduling_strategy = SchedulingStrategy.objects.create(name="Other example scheduling strategy")
    scheduling_strategy.tenants.set([testserver_tenant])
    return scheduling_strategy


@pytest.mark.django_db
def test_api_token_form_create(
    gp_test_tenant: GeneralParameter,
    testserver_tenant: Site,
    example_scheduling_strategy: SchedulingStrategy,
) -> None:
    form_data = {
        "name": "Example",
        "tenants": [testserver_tenant],
        "scheduling_strategy": example_scheduling_strategy,
    }

    form = ApiTokenForm(data=form_data, general_parameter=gp_test_tenant)

    assert form.is_valid() is True
    api_token = form.save()

    assert api_token.name == "Example"
    assert set(api_token.tenants.values_list("id", flat=True)) == {testserver_tenant.id}
    assert api_token.scheduling_strategy == example_scheduling_strategy
    assert api_token.slug == "example"
    assert len(api_token.secret) != 0


@pytest.mark.django_db
def test_api_token_form_create_with_initial_slug(
    gp_test_tenant: GeneralParameter,
    testserver_tenant: Site,
    example_scheduling_strategy: SchedulingStrategy,
) -> None:
    form_data = {
        "name": "Example",
        "tenants": [testserver_tenant],
        "scheduling_strategy": example_scheduling_strategy,
    }

    form = ApiTokenForm(data=form_data, general_parameter=gp_test_tenant, initial={"slug": "keep_me"})

    assert form.is_valid() is True
    api_token = form.save()

    assert api_token.name == "Example"
    assert set(api_token.tenants.values_list("id", flat=True)) == {testserver_tenant.id}
    assert api_token.scheduling_strategy == example_scheduling_strategy
    assert api_token.slug == "keep_me"
    assert len(api_token.secret) != 0


@pytest.mark.django_db
def test_api_token_form_update(
    gp_test_tenant: GeneralParameter,
    testserver_tenant: Site,
    testserver2_tenant: Site,
    example_scheduling_strategy: SchedulingStrategy,
    other_scheduling_strategy: SchedulingStrategy,
) -> None:
    api_token = ApiToken.objects.create(
        name="Old name",
        scheduling_strategy=example_scheduling_strategy,
        slug="old_slug",
    )
    api_token.tenants.add(testserver_tenant)
    original_secret = api_token.secret

    form_data = {
        "name": "Updated name",
        "tenants": [testserver2_tenant],
        "scheduling_strategy": other_scheduling_strategy,
    }

    form = ApiTokenForm(data=form_data, general_parameter=gp_test_tenant, instance=api_token)

    assert form.is_valid() is True
    api_token_updated = form.save()

    assert api_token_updated.name == "Updated name"
    assert set(api_token_updated.tenants.values_list("id", flat=True)) == {testserver2_tenant.id}
    assert api_token_updated.scheduling_strategy == other_scheduling_strategy
    assert api_token_updated.slug == "old_slug"
    assert api_token_updated.secret == original_secret


@pytest.mark.django_db
def test_api_token_form_update_with_initial_slug(
    gp_test_tenant: GeneralParameter,
    testserver_tenant: Site,
    testserver2_tenant: Site,
    example_scheduling_strategy: SchedulingStrategy,
    other_scheduling_strategy: SchedulingStrategy,
) -> None:
    api_token = ApiToken.objects.create(
        name="Old name",
        scheduling_strategy=example_scheduling_strategy,
        slug="old_slug",
    )
    api_token.tenants.add(testserver_tenant)
    original_secret = api_token.secret

    form_data = {
        "name": "Updated name",
        "tenants": [testserver2_tenant],
        "scheduling_strategy": other_scheduling_strategy,
    }

    form = ApiTokenForm(
        data=form_data, general_parameter=gp_test_tenant, instance=api_token, initial={"slug": "override"}
    )

    assert form.is_valid() is True
    api_token_updated = form.save()

    assert api_token_updated.name == "Updated name"
    assert set(api_token_updated.tenants.values_list("id", flat=True)) == {testserver2_tenant.id}
    assert api_token_updated.scheduling_strategy == other_scheduling_strategy
    assert api_token_updated.slug == "override"
    assert api_token_updated.secret == original_secret
