from datetime import datetime, timedelta

import pytest
from core.constants import GUEST_POLICY_ALLOW, MEETINGLAYOUT_SMART_LAYOUT, MODERATION_MODE_STARTER
from core.forms import ConfigureMeetingForm, EndTimeField, EndTimeWidget
from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ValidationError
from freezegun import freeze_time


def test_end_time_widget_decompress_none():
    widget = EndTimeWidget()
    assert widget.decompress(None) == [None, None, None]


@freeze_time("2020-04-26T16:29:55+00:00")
def test_end_time_widget_decompress_not_none():
    widget = EndTimeWidget()
    offset_in_minutes = 12
    end_time_stamp = int((datetime.utcnow() + timedelta(minutes=offset_in_minutes)).timestamp())
    assert widget.decompress(offset_in_minutes) == [None, offset_in_minutes, end_time_stamp]


def test_end_time_field_compress_no_fields():
    field = EndTimeField()
    assert field.compress([]) is None


@freeze_time("2020-04-26T16:29:55+00:00")
def test_end_time_field_compress_timestamp_is_in_the_past():
    field = EndTimeField()
    end_time_stamp = int((datetime.utcnow() - timedelta(minutes=1)).timestamp())

    with pytest.raises(ValidationError) as err_info:
        field.compress([None, None, end_time_stamp])
    assert str(err_info.value) == "['Meeting ends in the past']"


@freeze_time("2020-04-26T16:29:55+00:00")
def test_end_time_field_compress_meeting_duration_is_too_short():
    field = EndTimeField()
    end_time_stamp = int((datetime.utcnow() + timedelta(minutes=1)).timestamp())

    with pytest.raises(ValidationError) as err_info:
        field.compress([None, None, end_time_stamp])
    assert str(err_info.value) == "['Meeting duration is too short (min 5m)']"


@freeze_time("2020-04-26T16:29:55+00:00")
def test_end_time_field_compress_meeting_duration_is_too_long():
    field = EndTimeField()
    end_time_stamp = int((datetime.utcnow() + timedelta(minutes=13 * 60)).timestamp())

    with pytest.raises(ValidationError) as err_info:
        field.compress([None, None, end_time_stamp])
    assert str(err_info.value) == "['Meeting duration is too long (max 12h)']"


@freeze_time("2020-04-26T16:29:55+00:00")
def test_end_time_field_compress_meeting_duration_is_allowed():
    field = EndTimeField()
    end_time_stamp = int((datetime.utcnow() + timedelta(minutes=12)).timestamp())
    assert field.compress([None, None, end_time_stamp]) == 12


@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"moderation_mode": MODERATION_MODE_STARTER}, True),
        ({"moderation_mode": None}, False),
        ({}, False),
    ],
)
def test_configuration_meeting_form_creation(testserver_tenant, gp_test_tenant, data, is_valid):
    form_data = {
        "meetingLayout": MEETINGLAYOUT_SMART_LAYOUT,
        "guest_policy": GUEST_POLICY_ALLOW,
    }
    form_data.update(data)
    form = ConfigureMeetingForm(
        data=form_data,
        enforced_slides=[],
        initial_slides=None,
        requesting_user=AnonymousUser(),
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
    )

    assert form.is_valid() == is_valid


@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"moderation_mode": MODERATION_MODE_STARTER}, True),
        ({"moderation_mode": None}, False),
        ({}, False),
    ],
)
def test_configuration_meeting_form_update(
    testserver_tenant, gp_test_tenant, example_meeting_configuration_template, data, is_valid
):
    form_data = {
        "meetingLayout": MEETINGLAYOUT_SMART_LAYOUT,
        "guest_policy": GUEST_POLICY_ALLOW,
    }
    form_data.update(data)
    form = ConfigureMeetingForm(
        data=form_data,
        enforced_slides=[],
        initial_slides=None,
        requesting_user=AnonymousUser(),
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        instance=example_meeting_configuration_template,
    )

    assert form.is_valid() == is_valid


@pytest.mark.parametrize(
    "media_enabled, slide_fields_are_disabled",
    [
        (True, False),
        (False, True),
    ],
)
def test_configuration_meeting_form_with_and_without_slides(
    settings, testserver_tenant, gp_test_tenant, example_superuser, media_enabled, slide_fields_are_disabled
):
    settings.BBBATSCALE_MEDIA_ENABLED = media_enabled
    gp_test_tenant.media_enabled = media_enabled

    form = ConfigureMeetingForm(
        enforced_slides=[],
        initial_slides=None,
        requesting_user=example_superuser,
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
    )

    assert form.fields["slides"].disabled is slide_fields_are_disabled
    assert form.fields["initial_slides"].disabled is slide_fields_are_disabled


def test_configuration_meeting_form_post_clean_check_correct_initial_slides(
    settings, example_meeting_configuration, example_slides, testserver_tenant, gp_test_tenant
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = True

    form = ConfigureMeetingForm(
        data={
            "moderation_mode": MODERATION_MODE_STARTER,
            "meetingLayout": MEETINGLAYOUT_SMART_LAYOUT,
            "guest_policy": GUEST_POLICY_ALLOW,
        },
        enforced_slides=MEETINGLAYOUT_SMART_LAYOUT,
        initial_slides=example_slides,
        requesting_user=AnonymousUser(),
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        instance=example_meeting_configuration,
    )

    form.is_valid()
    assert example_meeting_configuration.initial_slides is example_slides


@pytest.mark.parametrize(
    "enforced_slide, number_of_slides",
    [
        (True, 1),
        (False, 0),
    ],
)
@pytest.mark.django_db
def test_configuration_meeting_form_save_m2m(
    settings,
    example_slides,
    example_meeting_configuration,
    testserver_tenant,
    gp_test_tenant,
    enforced_slide,
    number_of_slides,
):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = True

    form = ConfigureMeetingForm(
        data={
            "moderation_mode": MODERATION_MODE_STARTER,
            "meetingLayout": MEETINGLAYOUT_SMART_LAYOUT,
            "guest_policy": GUEST_POLICY_ALLOW,
        },
        enforced_slides=[example_slides] if enforced_slide else None,
        initial_slides=example_slides,
        requesting_user=AnonymousUser(),
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        instance=example_meeting_configuration,
    )
    form.is_valid()
    form._save_m2m()

    assert example_meeting_configuration.slides.filter(pk=example_slides.pk).count() == number_of_slides
