import pytest
from core.constants import GUEST_POLICY_ALLOW, MEETINGLAYOUT_SMART_LAYOUT, MODERATION_MODE_MODERATORS
from core.forms import MeetingConfigurationsTemplateForm


@pytest.mark.parametrize(
    "data, is_valid",
    [
        (
            {
                "name": "Example config",
                "moderation_mode": MODERATION_MODE_MODERATORS,
                "meetingLayout": MEETINGLAYOUT_SMART_LAYOUT,
                "guest_policy": GUEST_POLICY_ALLOW,
            },
            True,
        ),
        (
            {
                "name": "Example config",
                "meetingLayout": MEETINGLAYOUT_SMART_LAYOUT,
                "guest_policy": GUEST_POLICY_ALLOW,
            },
            False,
        ),
    ],
)
def test_room_configuration_create_form(
    data,
    is_valid,
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
    testserver_tenant,
):
    form = MeetingConfigurationsTemplateForm(
        data=data, general_parameter=gp_test_tenant, initial={"tenants": [testserver_tenant]}
    )
    assert form.is_valid() is is_valid


@pytest.mark.parametrize(
    "data, is_valid",
    [
        (
            {
                "name": "Example config",
                "moderation_mode": MODERATION_MODE_MODERATORS,
                "meetingLayout": MEETINGLAYOUT_SMART_LAYOUT,
                "guest_policy": GUEST_POLICY_ALLOW,
            },
            True,
        ),
        (
            {
                "name": "Example config",
                "meetingLayout": MEETINGLAYOUT_SMART_LAYOUT,
                "guest_policy": GUEST_POLICY_ALLOW,
            },
            False,
        ),
    ],
)
def test_room_configuration_update_form(
    data,
    is_valid,
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
    testserver_tenant,
):
    form = MeetingConfigurationsTemplateForm(
        data=data, general_parameter=gp_test_tenant, instance=example_meeting_configuration_template
    )
    assert form.is_valid() is is_valid
