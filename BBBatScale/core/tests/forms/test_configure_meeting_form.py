import pytest
from core.forms import ConfigureMeetingForm
from core.models import GeneralParameter, Slides
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.models import Site


@pytest.mark.django_db
def test_configure_meeting_room_media_enabled_no_selectable_slides(
    gp_test_tenant_media_upload_enabled: GeneralParameter,
    example_slides: Slides,
    testserver_tenant: Site,
) -> None:
    form_data = {
        "moderation_mode": "ALL_MODERATORS",
        "meetingLayout": "SMART_LAYOUT",
        "guest_policy": "ALWAYS_ACCEPT",
        "tenants": [testserver_tenant],
    }

    form = ConfigureMeetingForm(
        data=form_data,
        enforced_slides=[example_slides],
        initial_slides=None,
        requesting_user=AnonymousUser(),
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant_media_upload_enabled,
    )

    assert form.is_valid() is True
    assert form.enforced_slides == [example_slides]
    assert form.fields["slides"].disabled is True
    assert form.fields["initial_slides"].disabled is True


@pytest.mark.django_db
def test_configure_meeting_room_media_enabled_selectable_slides_but_no_enforced(
    gp_test_tenant_media_upload_enabled: GeneralParameter,
    example_slides_with_owner: Slides,
    testserver_tenant: Site,
) -> None:
    form_data = {
        "moderation_mode": "ALL_MODERATORS",
        "meetingLayout": "SMART_LAYOUT",
        "guest_policy": "ALWAYS_ACCEPT",
        "tenants": [testserver_tenant],
    }

    form = ConfigureMeetingForm(
        data=form_data,
        enforced_slides=[],
        initial_slides=None,
        requesting_user=example_slides_with_owner.owner,
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant_media_upload_enabled,
    )

    assert form.is_valid() is True
    assert form.enforced_slides == []
    assert form.fields["slides"].disabled is False
    assert form.fields["initial_slides"].disabled is False
    assert form.fields["slides"].queryset.count() == 1
    assert form.fields["initial_slides"].queryset.count() == 1


@pytest.mark.django_db
def test_configure_meeting_room_media_disabled(
    gp_test_tenant_media_upload_disabled: GeneralParameter,
    example_slides: Slides,
    example_slides_with_owner: Slides,
    testserver_tenant: Site,
) -> None:
    form_data = {
        "moderation_mode": "ALL_MODERATORS",
        "meetingLayout": "SMART_LAYOUT",
        "guest_policy": "ALWAYS_ACCEPT",
        "tenants": [testserver_tenant],
    }

    form = ConfigureMeetingForm(
        data=form_data,
        enforced_slides=[example_slides],
        initial_slides=None,
        requesting_user=example_slides_with_owner.owner,
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant_media_upload_disabled,
    )

    assert form.is_valid() is True
    assert form.enforced_slides is None
    assert form.fields["slides"].disabled is True
    assert form.fields["initial_slides"].disabled is True


@pytest.mark.django_db
def test_configure_meeting_room_media_enabled(
    gp_test_tenant_media_upload_enabled: GeneralParameter,
    example_slides: Slides,
    example_slides_with_owner: Slides,
    testserver_tenant: Site,
) -> None:
    form_data = {
        "moderation_mode": "ALL_MODERATORS",
        "meetingLayout": "SMART_LAYOUT",
        "guest_policy": "ALWAYS_ACCEPT",
        "tenants": [testserver_tenant],
    }

    form = ConfigureMeetingForm(
        data=form_data,
        enforced_slides=[example_slides],
        initial_slides=None,
        requesting_user=example_slides_with_owner.owner,
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant_media_upload_enabled,
    )

    assert form.is_valid() is True
    assert form.enforced_slides == [example_slides]
    assert form.fields["slides"].disabled is False
    assert form.fields["initial_slides"].disabled is False
    assert form.fields["slides"].queryset.count() == 1
    assert form.fields["initial_slides"].queryset.count() == 1
