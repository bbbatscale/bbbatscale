import pytest
from core.constants import ROOM_STATE_INACTIVE, ROOM_VISIBILITY_PRIVATE, ROOM_VISIBILITY_PUBLIC
from core.forms import RoomForm


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"state": ROOM_STATE_INACTIVE, "visibility": ROOM_VISIBILITY_PRIVATE, "click_counter": 0}, True),
        ({"state": ROOM_STATE_INACTIVE, "visibility": ROOM_VISIBILITY_PRIVATE, "click_counter": None}, False),
        ({"state": ROOM_STATE_INACTIVE, "visibility": None, "click_counter": 0}, False),
        ({"state": None, "visibility": ROOM_VISIBILITY_PRIVATE, "click_counter": 0}, False),
        ({"state": ROOM_STATE_INACTIVE, "visibility": ROOM_VISIBILITY_PRIVATE}, False),
        ({"state": ROOM_STATE_INACTIVE, "click_counter": 0}, False),
        ({"visibility": ROOM_VISIBILITY_PRIVATE, "click_counter": 0}, False),
        ({}, False),
    ],
)
def test_room_creation_form(
    data,
    is_valid,
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
    testserver_tenant,
):
    form_data = {
        "scheduling_strategy": example_scheduling_strategy,
        "name": "Example Strategy",
        "default_meeting_configuration": example_meeting_configuration_template,
        "tenants": [testserver_tenant],
    }
    form_data.update(data)
    form = RoomForm(
        data=form_data,
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        requesting_user=example_superuser,
    )

    assert form.is_valid() is is_valid


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"state": ROOM_STATE_INACTIVE, "visibility": ROOM_VISIBILITY_PRIVATE, "click_counter": 0}, True),
        ({"state": ROOM_STATE_INACTIVE, "visibility": ROOM_VISIBILITY_PRIVATE, "click_counter": None}, False),
        ({"state": ROOM_STATE_INACTIVE, "visibility": None, "click_counter": 0}, False),
        ({"state": None, "visibility": ROOM_VISIBILITY_PRIVATE, "click_counter": 0}, False),
        ({"state": ROOM_STATE_INACTIVE, "visibility": ROOM_VISIBILITY_PRIVATE}, False),
        ({"state": ROOM_STATE_INACTIVE, "click_counter": 0}, False),
        ({"visibility": ROOM_VISIBILITY_PRIVATE, "click_counter": 0}, False),
        ({}, False),
    ],
)
def test_room_update_form(
    data,
    is_valid,
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
    example_room,
    testserver_tenant,
):
    form_data = {
        "scheduling_strategy": example_scheduling_strategy,
        "name": "Example Strategy",
        "default_meeting_configuration": example_meeting_configuration_template,
        "tenants": [testserver_tenant],
    }
    form_data.update(data)
    form = RoomForm(
        data=form_data,
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        requesting_user=example_superuser,
        instance=example_room,
    )

    assert form.is_valid() is is_valid


@pytest.mark.django_db
def test_room_update_form_shared_resources(
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
    example_room,
    testserver_tenant,
    testserver2_tenant,
):
    example_scheduling_strategy.tenants.add(testserver2_tenant)
    example_meeting_configuration_template.tenants.add(testserver2_tenant)
    form = RoomForm(
        data={
            "scheduling_strategy": example_scheduling_strategy,
            "name": "Example Strategy",
            "default_meeting_configuration": example_meeting_configuration_template,
            "state": ROOM_STATE_INACTIVE,
            "visibility": ROOM_VISIBILITY_PUBLIC,
            "click_counter": 0,
            "tenants": [testserver_tenant],
        },
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        requesting_user=example_superuser,
        instance=example_room,
    )

    assert form.is_valid() is True
    assert form.fields["scheduling_strategy"].queryset.count() == 1
    assert form.fields["default_meeting_configuration"].queryset.count() == 1
