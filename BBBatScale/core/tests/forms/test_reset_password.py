from typing import List

import pytest
from core.forms import PasswordResetForm
from core.models import User
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from pytest_mock import MockerFixture


@pytest.mark.djangodb
def test_password_reset_form_ok(
    mocker: MockerFixture, example_user: User, testserver_tenant: Site, outbox: List[EmailMessage]
) -> None:
    reset_url = "/reset/MQ/static_token/"
    sender_email_address = "bbbatscale@example.org"
    link_builder = mocker.MagicMock(return_value=reset_url)

    form = PasswordResetForm({"username": example_user.username}, tenant=testserver_tenant)
    assert form.is_valid()

    form.save(link_builder, sender_email_address)

    assert len(outbox) == 1
    message = outbox[0]
    assert message.to == [example_user.email]
    assert message.from_email == sender_email_address
    assert message.subject == f"Password reset on {testserver_tenant.domain}"

    email_body = message.body

    assert (
        email_body
        == f"""Dear {str(example_user)},

You're receiving this email because you requested a password reset for your user account at {testserver_tenant.domain}.

Please go to the following page and choose a new password:
{reset_url}
This link is valid for 15 minutes after creation."""
    )
