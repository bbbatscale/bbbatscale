import re
import uuid
from typing import Sequence, Tuple
from unittest.mock import Mock
from urllib.parse import urlencode

import pytest
import xmltodict
from core.constants import MEETING_STATE_CREATING, MEETING_STATE_FINISHED, MEETING_STATE_RUNNING, SERVER_STATE_UP
from core.models import ApiToken, ExternalMeeting, GeneralParameter, Meeting, SchedulingStrategy, Server
from core.services import APIMethod, calculate_bbb_api_checksum
from core.tests.helper import get_meeting_info_full_response_xml, mock_response
from core.utils import BigBlueButton
from core.views.api import bbb_xml_meeting_running_false, bbb_xml_response, collect_get_meetings_items
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse


def get_api_request_url(
    api_method: APIMethod, query: Sequence[Tuple[str, str]], api_token: ApiToken, real_checksum: bool
) -> str:
    api_method_snake_case = re.sub(r"(?<!^)(?=[A-Z])", "_", api_method).lower()
    url = reverse(f"bbb_api_{api_method_snake_case}", kwargs={"slug": api_token.slug})
    checksum = (
        calculate_bbb_api_checksum(api_method, query, api_token.secret) if real_checksum else "THIS_IS_A_FAKE_CHECKSUM"
    )
    url_params = urlencode((*query, ("checksum", checksum)))
    return f"{url}?{url_params}"


@pytest.fixture(scope="function")
def gp_test_tenant(testserver_tenant):
    return GeneralParameter.objects.create(
        tenant=testserver_tenant,
        recording_key="TESTKEY",
        recording_cert="TESTCERT",
        recording_management_url="example.org:5845",
        default_theme_id=1,
    )


@pytest.fixture(scope="function")
def gp_test_tenant_no_rpc_params(testserver_tenant):
    return GeneralParameter.objects.create(
        tenant=testserver_tenant,
        default_theme_id=1,
    )


@pytest.fixture(scope="function")
def bbb_server(example_scheduling_strategy: SchedulingStrategy) -> Server:
    return Server.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        dns="bbb-example.example.org",
        state=SERVER_STATE_UP,
        shared_secret="EXTRA_SECRET_SECRET",
    )


@pytest.fixture(scope="function")
def bbb_server2(example_scheduling_strategy: SchedulingStrategy) -> Server:
    return Server.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        dns="bbb-example-2.example.org",
        state=SERVER_STATE_UP,
        shared_secret="EXTRA_SECRET_SECRET_2",
    )


@pytest.mark.django_db
def test_bbb_initialization_request(testserver_tenant, client, example_api_token):
    response = client.get(f"/core/{example_api_token.slug}/bigbluebutton/api/")
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["version"] == "2.0"

    response = client.get("/core/2/bigbluebutton/api/")
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "unknownResource"
    assert content["response"]["message"] == "Your requested resource is not available."


@pytest.mark.django_db
def test_bbb_api_create(testserver_tenant, client, example_api_token, mocker):
    response = client.get(get_api_request_url("create", (), example_api_token, False))

    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"
    ex = ExternalMeeting.objects.create(
        external_meeting_id="random-4425951", name="{}-{}".format(example_api_token.name, "random-4425951")
    )
    _meeting = Meeting.objects.create(
        room_name="{}-{}".format(example_api_token.name, "random-4425951"),
        attendee_pw="ap",
        moderator_pw="mp",
        state=MEETING_STATE_CREATING,
        creator_name=example_api_token.name,
        external_meeting=ex,
    )
    response = client.get(
        get_api_request_url(
            "create",
            (
                ("allowStartStopRecording", "true"),
                ("attendeePW", "ap"),
                ("autoStartRecording", "false"),
                ("meetingID", "random-4425951"),
                ("moderatorPW", "mp"),
                ("name", "random-4425951"),
                ("record", "false"),
                ("voiceBridge", "72790"),
                ("welcome", "<br>Welcome+to+<b>%%CONFNAME%%</b>!"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["messageKey"] == "duplicateWarning"
    assert (
        content["response"]["message"] == "This conference was already in existence and may currently be in progress."
    )
    _meeting.state = MEETING_STATE_FINISHED
    _meeting.save()

    def mock_meeting_create(request, params, scheduling_strategy, meeting, body):
        class Response:
            text = """
        <response><returncode>SUCCESS</returncode><meetingID>random-4425951</meetingID>
        <internalMeetingID>2ae2adcbbb4472fa567f9284bc7411112213eec5-1627476400234</internalMeetingID>
        <parentMeetingID>bbb-none</parentMeetingID><attendeePW>ap</attendeePW><moderatorPW>mp</moderatorPW>
        <createTime>1627476400234</createTime><voiceBridge>751208885</voiceBridge><dialNumber>18632080022</dialNumber>
        <createDate>Wed Jul 28 12:46:40 UTC 2021</createDate>
        <hasUserJoined>false</hasUserJoined><duration>540</duration>
        <hasBeenForciblyEnded>false</hasBeenForciblyEnded></response>
        """

        return Response()

    mocker.patch("core.views.api.meeting_create", mock_meeting_create)
    response = client.get(
        get_api_request_url(
            "create",
            (
                ("allowStartStopRecording", "true"),
                ("attendeePW", "ap"),
                ("autoStartRecording", "false"),
                ("meetingID", "random-4425951"),
                ("moderatorPW", "mp"),
                ("name", "random-4425951"),
                ("record", "false"),
                ("voiceBridge", "72790"),
                ("welcome", "<br>Welcome+to+<b>%%CONFNAME%%</b>!"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["meetingID"] == ex.external_meeting_id


@pytest.mark.django_db
def test_bbb_api_join(testserver_tenant, client, example_api_token, bbb_server):
    response = client.get(
        get_api_request_url(
            "join",
            (
                ("fullName", "User+8025106"),
                ("meetingID", "random-8094211"),
                ("password", "mp"),
                ("redirect", "true"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert (
        content["response"]["message"]
        == "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?"
    )
    ex = ExternalMeeting.objects.create(external_meeting_id="random-8094211", name="random-8094211")
    response = client.get(
        get_api_request_url(
            "join",
            (
                ("fullName", "User+8025106"),
                ("meetingID", "random-8094211"),
                ("password", "mp"),
                ("redirect", "true"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert (
        content["response"]["message"]
        == "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?"
    )
    Meeting.objects.create(
        room_name="{}-{}".format(example_api_token.name, "random-8094211"),
        id="dbb41a4f-6cc7-44dc-86af-430164b92b95",
        server=bbb_server,
        attendee_pw="ap",
        moderator_pw="mp",
        state=MEETING_STATE_RUNNING,
        creator_name=example_api_token.name,
        external_meeting=ex,
    )

    response = client.get(
        get_api_request_url(
            "join",
            (
                ("fullName", "User+8025106"),
                ("meetingID", "random-8094211"),
                ("password", "mp"),
                ("redirect", "true"),
            ),
            example_api_token,
            True,
        )
    )
    assert (
        response.url == "https://bbb-example.example.org/bigbluebutton/api/join?fullName=User%2B8025106&"
        "meetingID=dbb41a4f-6cc7-44dc-86af-430164b92b95&password=mp&redirect=true&checksum="
        "330cc3ae6dfab243bcb8833063809a08540e851f"
    )


@pytest.mark.django_db
def test_bbb_api_end(testserver_tenant, client, example_api_token, bbb_server, mocker):
    response = client.get(
        get_api_request_url(
            "end",
            (
                ("meetingID", "random-8094211"),
                ("password", "mp"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert (
        content["response"]["message"]
        == "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?"
    )
    ex = ExternalMeeting.objects.create(external_meeting_id="random-8094211", name="random-8094211")
    Meeting.objects.create(
        room_name="{}-{}".format(example_api_token.name, "random-8094211"),
        server=bbb_server,
        attendee_pw="ap",
        moderator_pw="mp",
        state=MEETING_STATE_RUNNING,
        creator_name=example_api_token.name,
        external_meeting=ex,
    )

    def mock_end(self, meeting_id, pw):
        class Response:
            text = """
            <response><returncode>SUCCESS</returncode>
            <messageKey>sentEndMeetingRequest</messageKey>
            <message>A request to end the meeting was sent. Please wait a few seconds,
            and then use the getMeetingInfo or isMeetingRunning API calls to verify that it was ended.</message>
            </response>
            """

        return Response()

    mocker.patch("core.utils.BigBlueButton.end", mock_end)
    response = client.get(
        get_api_request_url(
            "end",
            (
                ("meetingID", "random-8094211"),
                ("password", "mp"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["messageKey"] == "sentEndMeetingRequest"
    assert (
        content["response"]["message"]
        == """A request to end the meeting was sent. Please wait a few seconds,
            and then use the getMeetingInfo or isMeetingRunning API calls to verify that it was ended."""
    )

    response = client.get(
        get_api_request_url(
            "end",
            (
                ("meetingID", "random-8094211"),
                ("password", "mp"),
            ),
            example_api_token,
            False,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_is_meeting_running(testserver_tenant, client, example_api_token, bbb_server, mocker):
    response = client.get(
        get_api_request_url("isMeetingRunning", (("meetingID", "random-8094211"),), example_api_token, True)
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["running"] == "false"
    ex = ExternalMeeting.objects.create(external_meeting_id="random-8094211", name="random-8094211")
    Meeting.objects.create(
        room_name="{}-{}".format(example_api_token.name, "random-8094211"),
        server=bbb_server,
        attendee_pw="ap",
        moderator_pw="mp",
        state=MEETING_STATE_RUNNING,
        creator_name=example_api_token.name,
        external_meeting=ex,
    )

    def mock_is_meeting_running(self, meeting_id):
        class Response:
            text = """
            <response>
            <returncode>SUCCESS</returncode>
            <running>true</running>
            </response>
            """

        return Response()

    mocker.patch("core.utils.BigBlueButton.is_meeting_running", mock_is_meeting_running)
    response = client.get(
        get_api_request_url("isMeetingRunning", (("meetingID", "random-8094211"),), example_api_token, True)
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["running"] == "true"
    response = client.get(
        get_api_request_url("isMeetingRunning", (("meetingID", "random-8094211"),), example_api_token, False)
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_get_meeting_info(testserver_tenant, client, example_api_token, bbb_server, mocker):
    response = client.get(
        get_api_request_url(
            "getMeetingInfo",
            (
                ("meetingID", "random-8094211"),
                ("password", "mp"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert content["response"]["message"] == "We could not find a meeting with that meeting ID"
    ex = ExternalMeeting.objects.create(external_meeting_id="random-8094211", name="random-8094211")
    Meeting.objects.create(
        room_name="{}-{}".format(example_api_token.name, "random-8094211"),
        server=bbb_server,
        attendee_pw="ap",
        moderator_pw="mp",
        state=MEETING_STATE_RUNNING,
        creator_name=example_api_token.name,
        external_meeting=ex,
    )

    def mock_get_meeting_info(self, meeting_id):
        class Response:
            text = """
            <response>
                <returncode>SUCCESS</returncode>
                <meetingName>random-8094211</meetingName>
                <meetingID>random-8094211</meetingID>
                <internalMeetingID>2784ee866a934d9f27781875918772621e550776-1628182217015</internalMeetingID>
                <createTime>1628182217015</createTime>
                <createDate>Thu Aug 05 16:50:17 UTC 2021</createDate>
                <voiceBridge>674733854</voiceBridge>
                <dialNumber>18632080022</dialNumber>
                <attendeePW>ap</attendeePW>
                <moderatorPW>mp</moderatorPW>
                <running>false</running>
                <duration>540</duration>
                <hasUserJoined>false</hasUserJoined>
                <recording>false</recording>
                <hasBeenForciblyEnded>false</hasBeenForciblyEnded>
                <startTime>1628182217035</startTime>
                <endTime>0</endTime>
                <participantCount>0</participantCount>
                <listenerCount>0</listenerCount>
                <voiceParticipantCount>0</voiceParticipantCount>
                <videoCount>0</videoCount>
                <maxUsers>0</maxUsers>
                <moderatorCount>0</moderatorCount>
                <attendees/>
                <metadata>
                <bn-userid>test-install</bn-userid>
                <bn-meetingid>random-8094211</bn-meetingid>
                <bn-priority>20</bn-priority>
                </metadata>
                <isBreakout>false</isBreakout>
            </response>
            """

        return Response()

    mocker.patch("core.utils.BigBlueButton.get_meeting_infos", mock_get_meeting_info)
    response = client.get(
        get_api_request_url(
            "getMeetingInfo",
            (
                ("meetingID", "random-8094211"),
                ("password", "mp"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["meetingName"] == "random-8094211"
    assert content["response"]["meetingID"] == "random-8094211"
    assert content["response"]["internalMeetingID"] == "2784ee866a934d9f27781875918772621e550776-1628182217015"
    assert content["response"]["createTime"] == "1628182217015"
    assert content["response"]["isBreakout"] == "false"
    assert content["response"]["attendeePW"] == "ap"
    assert content["response"]["moderatorPW"] == "mp"

    response = client.get(
        get_api_request_url(
            "getMeetingInfo",
            (
                ("meetingID", "random-8094211"),
                ("password", "mp"),
            ),
            example_api_token,
            False,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_get_recordings(testserver_tenant, client, example_api_token, mocker):
    response = client.get(
        get_api_request_url(
            "getRecordings",
            (
                ("meetingID", "random-8094211"),
                ("recordID", "random-8094211"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["messageKey"] == "noRecordings"
    assert content["response"]["message"] == "There are no recordings for the meeting(s)."

    def mock_parse_recordings_xml(external_meeting):
        xml_response = """
        <response>
        <returncode>SUCCESS</returncode>
        <recordings>
        <recording>
        <recordID>ee95ce67a5ebf69058afdf1b3364e868b6fe59b3-1627994200254</recordID>
        <meetingID>20d80cb64e834e209b5b0909b8c6558a</meetingID>
        <internalMeetingID>ee95ce67a5ebf69058afdf1b3364e868b6fe59b3-1627994200254</internalMeetingID>
        <name>1337</name>
        <isBreakout>false</isBreakout>
        <published>true</published>
        <state>published</state>
        <startTime>1627994200254</startTime>
        <endTime>1627994244463</endTime>
        <participants>1</participants>
        <rawSize>2231777</rawSize>
        <metadata>
        <onlypromptguestsforaccesscode>False</onlypromptguestsforaccesscode>
        <muteonstart>False</muteonstart>
        <accesscode>None</accesscode>
        <disablemic>False</disablemic>
        <roomsmeetingid>86690</roomsmeetingid>
        <logouturl>None</logouturl>
        <streamingurl>None</streamingurl>
        <disableprivatechat>False</disableprivatechat>
        <meetingName>1337</meetingName>
        <creator>fbi1059@rooms.h-da.de</creator>
        <disablecam>False</disablecam>
        <meetingId>20d80cb64e834e209b5b0909b8c6558a</meetingId>
        <moderationmode>ROOM_STARTER</moderationmode>
        <disablenote>False</disablenote>
        <maxparticipants>None</maxparticipants>
        <allowrecording>True</allowrecording>
        <isBreakout>false</isBreakout>
        <guestpolicy>ALWAYS_ACCEPT</guestpolicy>
        <disablepublicchat>False</disablepublicchat>
        <allowguestentry>True</allowguestentry>
        </metadata>
        <size>1554462</size>
        <data/>
        </recording>
        </recordings>
        </response>
        """
        return xmltodict.parse(xml_response)

    mocker.patch("core.views.api.create_getrecordings_response_xml", mock_parse_recordings_xml)
    response = client.get(get_api_request_url("getRecordings", (), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "missingParameters"
    assert content["response"]["message"] == "param meetingID or recordID must be included."

    response = client.get(
        get_api_request_url(
            "getRecordings",
            (
                ("meetingID", "random-8094211"),
                ("recordID", "random-8094211"),
            ),
            example_api_token,
            False,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


# BBB API publishRecordings; Needed GET parameters: recordID, publish & checksum;


@pytest.mark.django_db
def test_bbb_api_publish_recordings_checksum_error(client, example_api_token, gp_test_tenant):
    response = client.get(get_api_request_url("publishRecordings", (("publish", "false"),), example_api_token, False))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_publish_recordings_record_id_missing(client, example_api_token, gp_test_tenant):
    response = client.get(get_api_request_url("publishRecordings", (("publish", "false"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "missingParamRecordID"
    assert content["response"]["message"] == "You must specify a recordID."


@pytest.mark.django_db
def test_bbb_api_publish_recordings_publish_missing(client, example_api_token, gp_test_tenant):
    response = client.get(get_api_request_url("publishRecordings", (("recordID", "TESTID"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "missingParamPublish"
    assert content["response"]["message"] == "You must specify a publish value true or false."


@pytest.mark.django_db
def test_bbb_api_publish_recordings_grpc_parameters_missing(client, example_api_token, gp_test_tenant_no_rpc_params):
    response = client.get(
        get_api_request_url(
            "publishRecordings",
            (
                ("publish", "false"),
                ("recordID", "TESTID"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "missingParameters"
    assert content["response"]["message"] == "recording grpc authentication parameter not set"


@pytest.mark.django_db
def test_bbb_api_publish_recordings_object_does_not_exists(client, example_api_token, mocker, gp_test_tenant):
    def mock_update_publish_recordings(params, rpc, meta):
        raise ObjectDoesNotExist()

    mocker.patch("core.views.api.update_publish_recordings", mock_update_publish_recordings)
    response = client.get(
        get_api_request_url(
            "publishRecordings",
            (
                ("publish", "false"),
                ("recordID", "TESTID"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert content["response"]["message"] == "We could not find recordings"


@pytest.mark.django_db
def test_bbb_api_publish_recordings_generic_exception(client, example_api_token, mocker, gp_test_tenant):
    def mock_update_publish_recordings(params, rpc, meta):
        raise Exception()

    mocker.patch("core.views.api.update_publish_recordings", mock_update_publish_recordings)
    response = client.get(
        get_api_request_url(
            "publishRecordings",
            (
                ("publish", "false"),
                ("recordID", "TESTID"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "internalError"


@pytest.mark.django_db
def test_bbb_api_publish_recordings_happy_path(client, example_api_token, mocker, gp_test_tenant):
    def mock_update_publish_recordings(params, rpc, meta):
        pass

    mocker.patch("core.views.api.update_publish_recordings", mock_update_publish_recordings)
    response = client.get(
        get_api_request_url(
            "publishRecordings",
            (
                ("publish", "false"),
                ("recordID", "TESTID"),
            ),
            example_api_token,
            True,
        )
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["published"] == "false"


# BBB API updateRecordings; Needed GET parameters: recordID & checksum;
# Accepted update parameters: everything with a prefix `meta_`


@pytest.mark.django_db
def test_bbb_api_update_recordings_checksum_error(client, example_api_token, gp_test_tenant):
    response = client.get(get_api_request_url("updateRecordings", (("recordID", "TESTID"),), example_api_token, False))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_update_recordings_record_id_missing(client, example_api_token, gp_test_tenant):
    response = client.get(get_api_request_url("updateRecordings", (), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "missingParamRecordID"
    assert content["response"]["message"] == "You must specify a recordID."


@pytest.mark.django_db
def test_bbb_api_update_recordings_grpc_parameters_missing(client, example_api_token, gp_test_tenant_no_rpc_params):
    response = client.get(get_api_request_url("updateRecordings", (("recordID", "TESTID"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "missingParameters"
    assert content["response"]["message"] == "recording grpc authentication parameter not set"


@pytest.mark.django_db
def test_bbb_api_update_recordings_object_does_not_exists(client, example_api_token, mocker, gp_test_tenant):
    def mock_update_publish_recordings(params, rpc, meta):
        raise ObjectDoesNotExist()

    mocker.patch("core.views.api.update_publish_recordings", mock_update_publish_recordings)
    response = client.get(get_api_request_url("updateRecordings", (("recordID", "TESTID"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert content["response"]["message"] == "We could not find recordings"


@pytest.mark.django_db
def test_bbb_api_update_recordings_generic_exception(client, example_api_token, mocker, gp_test_tenant):
    def mock_update_publish_recordings(params, rpc, meta):
        raise Exception()

    mocker.patch("core.views.api.update_publish_recordings", mock_update_publish_recordings)
    response = client.get(get_api_request_url("updateRecordings", (("recordID", "TESTID"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "internalError"


@pytest.mark.django_db
def test_bbb_api_update_recordings_happy_path(client, example_api_token, mocker, gp_test_tenant):
    def mock_update_publish_recordings(params, rpc, meta):
        pass

    mocker.patch("core.views.api.update_publish_recordings", mock_update_publish_recordings)
    response = client.get(get_api_request_url("updateRecordings", (("recordID", "TESTID"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["updated"] == "true"


# BBB API deleteRecordings; Needed GET parameters: recordID & checksum


@pytest.mark.django_db
def test_bbb_api_delete_recordings_checksum_error(client, example_api_token, gp_test_tenant):
    response = client.get(get_api_request_url("deleteRecordings", (("recordID", "TESTID"),), example_api_token, False))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"


@pytest.mark.django_db
def test_bbb_api_delete_recordings_record_id_missing(client, example_api_token, gp_test_tenant):
    response = client.get(get_api_request_url("deleteRecordings", (), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "missingParamRecordID"
    assert content["response"]["message"] == "You must specify a recordID."


@pytest.mark.django_db
def test_bbb_api_delete_recordings_grpc_parameters_missing(client, example_api_token, gp_test_tenant_no_rpc_params):
    response = client.get(get_api_request_url("deleteRecordings", (("recordID", "TESTID"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "missingParameters"
    assert content["response"]["message"] == "recording grpc authentication parameter not set"


@pytest.mark.django_db
def test_bbb_api_delete_recordings_object_does_not_exists(client, example_api_token, mocker, gp_test_tenant):
    def mock_delete_recordings(params, rpc):
        raise ObjectDoesNotExist()

    mocker.patch("core.views.api.delete_recordings", mock_delete_recordings)
    response = client.get(get_api_request_url("deleteRecordings", (("recordID", "TESTID"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "notFound"
    assert content["response"]["message"] == "We could not find recordings"


@pytest.mark.django_db
def test_bbb_api_delete_recordings_generic_exception(client, example_api_token, mocker, gp_test_tenant):
    def mock_delete_recordings(params, rpc):
        raise Exception()

    mocker.patch("core.views.api.delete_recordings", mock_delete_recordings)
    response = client.get(get_api_request_url("deleteRecordings", (("recordID", "TESTID"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "internalError"


@pytest.mark.django_db
def test_bbb_api_delete_recordings_happy_path(client, example_api_token, mocker, gp_test_tenant):
    def mock_delete_recordings(params, rpc):
        pass

    mocker.patch("core.views.api.delete_recordings", mock_delete_recordings)
    response = client.get(get_api_request_url("deleteRecordings", (("recordID", "TESTID"),), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["deleted"] == "true"


def test_bbb_xml_response():
    response = bbb_xml_response(
        "SUCCESS", "noRecordings", "There are no recordings for the meeting(s).", recordings=None
    )
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["messageKey"] == "noRecordings"
    assert content["response"]["message"] == "There are no recordings for the meeting(s)."
    assert content["response"]["recordings"] is None


def test_bbb_xml_meeting_running_false():
    running = bbb_xml_meeting_running_false(True)
    assert running.status_code == 200
    content = xmltodict.parse(running.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["running"] == "true"

    not_running = bbb_xml_meeting_running_false()
    assert not_running.status_code == 200
    content = xmltodict.parse(not_running.content)
    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["running"] == "false"


@pytest.mark.django_db
def test_bbb_api_get_meetings_ok(client, example_api_token, testserver_tenant, example_meeting, mocker):
    example_meeting.tenants.set([testserver_tenant])
    example_meeting.save()

    configuration_2 = example_meeting.configuration
    configuration_2.pk = None
    configuration_2._state.adding = True
    configuration_2.save()

    example_meeting_2 = example_meeting
    example_meeting_2.pk = uuid.uuid4()
    example_meeting_2._state.adding = True
    example_meeting_2.configuration = configuration_2
    example_meeting_2.tenants.set([testserver_tenant])
    example_meeting_2.save()

    bbb = Mock(spec=BigBlueButton)
    bbb.get_meeting_infos.side_effect = [
        mock_response(get_meeting_info_full_response_xml(example_meeting.id)),
        mock_response(get_meeting_info_full_response_xml(example_meeting_2.id)),
    ]

    def create_mock_bbb(dns, secret):
        return bbb

    mocker.patch.object(collect_get_meetings_items, "__defaults__", (create_mock_bbb,))

    response = client.get(get_api_request_url("getMeetings", (), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)

    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"].get("messageKey") is None
    assert content["response"].get("message") is None

    meeting_1_dict = xmltodict.parse(get_meeting_info_full_response_xml(example_meeting.id))
    meeting_1_dict = meeting_1_dict["response"]
    del meeting_1_dict["returncode"]

    meeting_2_dict = xmltodict.parse(get_meeting_info_full_response_xml(example_meeting_2.id))
    meeting_2_dict = meeting_2_dict["response"]
    del meeting_2_dict["returncode"]

    assert content["response"]["meetings"]["meeting"] == [
        meeting_1_dict,
        meeting_2_dict,
    ]


@pytest.mark.django_db
def test_bbb_api_get_meetings_no_meetings(client, example_api_token, testserver_tenant, mocker):
    response = client.get(get_api_request_url("getMeetings", (), example_api_token, True))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)

    assert content["response"]["returncode"] == "SUCCESS"
    assert content["response"]["messageKey"] == "noMeetings"
    assert content["response"]["message"] == "no meetings were found on this server"


@pytest.mark.django_db
def test_bbb_api_get_meetings_invalid_checksum(testserver_tenant, client, example_api_token):
    response = client.get(get_api_request_url("getMeetings", (), example_api_token, False))
    assert response.status_code == 200
    assert response.headers["Content-Type"] == "text/xml"
    content = xmltodict.parse(response.content)

    assert content["response"]["returncode"] == "FAILED"
    assert content["response"]["messageKey"] == "checksumError"
    assert content["response"]["message"] == "You did not pass the checksum security check"
