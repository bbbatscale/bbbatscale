import pytest
from core.constants import MEETING_STATE
from core.models import Meeting


@pytest.mark.django_db
@pytest.fixture(scope="function")
def example_meeting() -> Meeting:
    return Meeting.objects.create(room_name="ExampleMeeting", creator_name="Example User")


@pytest.mark.django_db
def test_meeting_zip_download_url(example_meeting):
    example_meeting.replay_url = "https://example.org/r/RANDOMUUID_LIKE_AGENT_SEND"
    assert example_meeting.get_zip_url() == "https://example.org/r/RANDOMUUID_LIKE_AGENT_SEND/zip"


@pytest.mark.django_db
def test_meeting_zip_download_url_ending_slash(example_meeting):
    example_meeting.replay_url = "https://example.org/r/RANDOMUUID_LIKE_AGENT_SEND/"
    assert example_meeting.get_zip_url() == "https://example.org/r/RANDOMUUID_LIKE_AGENT_SEND/zip"


@pytest.mark.django_db
def test_meeting_zip_download_url_missing_schema(example_meeting):
    example_meeting.replay_url = "example.org/r/RANDOMUUID_LIKE_AGENT_SEND"
    assert example_meeting.get_zip_url() == "example.org/r/RANDOMUUID_LIKE_AGENT_SEND/zip"


@pytest.mark.django_db
def test_meeting_zip_download_url_ending_slash_missing_schema(example_meeting):
    example_meeting.replay_url = "example.org/r/RANDOMUUID_LIKE_AGENT_SEND/"
    assert example_meeting.get_zip_url() == "example.org/r/RANDOMUUID_LIKE_AGENT_SEND/zip"


@pytest.mark.django_db
@pytest.mark.parametrize(
    "amount_states",
    [
        len(MEETING_STATE),
        1,
        0,
    ],
)
def test_meetings_get_by_state(amount_states):
    states = [s for s, _ in MEETING_STATE]
    for s in states:
        Meeting.objects.create(state=s)

    states_for_filter = states[:amount_states]
    meetings_by_states = Meeting.objects.filter_by_states(states_for_filter)

    assert meetings_by_states.count() == amount_states
    assert sorted([m.state for m in meetings_by_states]) == sorted(states_for_filter)


@pytest.mark.django_db
def test_update_available_format_urls_empty_array(example_meeting):
    example_meeting.replay_url = "example.org/r/RANDOMUUID_LIKE_AGENT_SEND"
    formats = []
    example_meeting.update_available_format_urls(formats)
    example_meeting = Meeting.objects.get(pk=example_meeting.pk)
    assert example_meeting.replay_video_url == ""
    assert example_meeting.replay_notes_url == ""
    assert example_meeting.replay_podcast_url == ""
    assert example_meeting.replay_screenshare_url == ""


@pytest.mark.django_db
def test_update_available_format_urls_full_array(example_meeting):
    example_meeting.replay_url = "example.org/r/RANDOMUUID_LIKE_AGENT_SEND"
    formats = ["video", "notes", "podcast", "screenshare"]
    example_meeting.update_available_format_urls(formats)
    example_meeting = Meeting.objects.get(pk=example_meeting.pk)

    assert example_meeting.replay_video_url == "example.org/r/RANDOMUUID_LIKE_AGENT_SEND/formats/video"
    assert example_meeting.replay_notes_url == "example.org/r/RANDOMUUID_LIKE_AGENT_SEND/formats/notes/notes.pdf"
    assert example_meeting.replay_podcast_url == "example.org/r/RANDOMUUID_LIKE_AGENT_SEND/formats/podcast/audio.ogg"
    assert example_meeting.replay_screenshare_url == "example.org/r/RANDOMUUID_LIKE_AGENT_SEND/formats/screenshare"
