import pytest
from core.models import GeneralParameter


@pytest.mark.django_db
def test_load_gp_does_not_exist(testserver_tenant):
    GeneralParameter.objects.load(testserver_tenant)
    assert len(GeneralParameter.objects.filter(tenant=testserver_tenant)) == 1


@pytest.mark.django_db
def test_load_gp_does_exist(testserver_tenant, gp_test_tenant):
    gp = GeneralParameter.objects.load(testserver_tenant)
    assert gp == gp_test_tenant


@pytest.mark.django_db
def test_string(gp_test_tenant):
    assert "GeneralParameter(tenant='testserver')" == gp_test_tenant.__str__()
