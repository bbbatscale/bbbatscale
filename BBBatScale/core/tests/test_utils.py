import json
from unittest.mock import Mock, call

import pytest
from core.models import User
from core.utils import format_human_readable_size, paginate, parse_human_readable_size, snakecase_to_camelcase
from django.contrib.sites.models import Site


def test_format_human_readable_size() -> None:
    assert format_human_readable_size(1) == "1B"
    assert format_human_readable_size(1_100) == "1.1kB"
    assert format_human_readable_size(1_000) == "1.0kB"
    assert format_human_readable_size(1_000, decimal_pos=0) == "1kB"
    assert format_human_readable_size(1_000, decimal_pos=3) == "1.000kB"
    assert format_human_readable_size(123_456_789) == "123.4MB"
    assert format_human_readable_size(123_456_789, decimal_pos=None) == "123.456789MB"
    assert format_human_readable_size(123_456_789, decimal_pos=3) == "123.456MB"


def test_format_human_readable_size_decimal() -> None:
    expectation = 1
    for prefix in ["", "k", "M", "G", "T", "P", "E", "Z", "Y"]:
        assert format_human_readable_size(expectation, decimal_pos=0) == "1" + prefix + "B"
        expectation *= 1000


def test_format_human_readable_size_binary() -> None:
    expectation = 1024
    for prefix in ["k", "M", "G", "T", "P", "E", "Z", "Y"]:
        assert format_human_readable_size(expectation, binary=True, decimal_pos=0) == "1" + prefix + "iB"
        expectation *= 1024


def test_parse_human_readable_size() -> None:
    with pytest.raises(ValueError):
        parse_human_readable_size("")
    with pytest.raises(ValueError):
        parse_human_readable_size("B")
    with pytest.raises(ValueError):
        parse_human_readable_size("1iB")
    with pytest.raises(ValueError):
        parse_human_readable_size("wasd")

    assert parse_human_readable_size("1B") == 1
    assert parse_human_readable_size("1.1B") == 1
    assert parse_human_readable_size("1100B") == 1_100
    assert parse_human_readable_size("123456789.123kB") == 123_456_789_123
    assert parse_human_readable_size("123,456,789.123kB") == 123_456_789_123
    assert parse_human_readable_size("1.1987kB") == 1_198


def test_parse_human_readable_size_decimal() -> None:
    expectation = 1
    for prefix in ["", "k", "M", "G", "T", "P", "E", "Z", "Y"]:
        assert parse_human_readable_size("1" + prefix + "B") == expectation
        expectation *= 1000


def test_parse_human_readable_size_binary() -> None:
    expectation = 1024
    for prefix in ["k", "M", "G", "T", "P", "E", "Z", "Y"]:
        assert parse_human_readable_size("1" + prefix + "iB") == expectation
        expectation *= 1024


@pytest.mark.django_db
def test_paginate(testserver_tenant: Site) -> None:
    def mapper(entry: User) -> dict:
        return {"username": entry.username}

    mapping_mock = Mock()
    mapping_mock.side_effect = mapper

    for i in range(75):
        User.objects.create_user("user-%02d" % i, display_name="user-%02d" % i, tenant=testserver_tenant)

    query = User.objects.order_by("username")
    assert query.count() == 75

    sites = list(query)

    response = json.loads(paginate(query, 1, mapping_mock).content)
    assert mapping_mock.call_count == 50
    mapping_mock.assert_has_calls([call(site) for site in sites[:50]])
    assert isinstance(response, dict)
    assert response.get("page", None) == 1
    assert response.get("pageCount", None) == 2
    assert response.get("entries", None) == [mapper(site) for site in sites[:50]]

    mapping_mock.reset_mock()

    response = json.loads(paginate(query, -1, mapping_mock).content)
    assert mapping_mock.call_count == 50
    mapping_mock.assert_has_calls([call(site) for site in sites[:50]])
    assert isinstance(response, dict)
    assert response.get("page", None) == 1
    assert response.get("pageCount", None) == 2
    assert response.get("entries", None) == [mapper(site) for site in sites[:50]]

    mapping_mock.reset_mock()

    response = json.loads(paginate(query, 2, mapping_mock).content)
    assert mapping_mock.call_count == 25
    mapping_mock.assert_has_calls([call(site) for site in sites[50:75]])
    assert isinstance(response, dict)
    assert response.get("page", None) == 2
    assert response.get("pageCount", None) == 2
    assert response.get("entries", None) == [mapper(site) for site in sites[50:75]]

    mapping_mock.reset_mock()

    response = json.loads(paginate(query, 10, mapping_mock).content)
    assert mapping_mock.call_count == 25
    mapping_mock.assert_has_calls([call(site) for site in sites[50:75]])
    assert isinstance(response, dict)
    assert response.get("page", None) == 2
    assert response.get("pageCount", None) == 2
    assert response.get("entries", None) == [mapper(site) for site in sites[50:75]]


def test_snakecase_to_camelcase() -> None:
    assert snakecase_to_camelcase("abc_def_hij") == "abcDefHij"


def test_snakecase_to_camelcase__empty_string() -> None:
    assert snakecase_to_camelcase("") == ""


def test_snakecase_to_camelcase__underscore() -> None:
    assert snakecase_to_camelcase("_") == "_"


def test_snakecase_to_camelcase__starts_with_underscore() -> None:
    assert snakecase_to_camelcase("_abc_def") == "abcDef"


def test_snakecase_to_camelcase__ends_with_underscore() -> None:
    assert snakecase_to_camelcase("abc_def_") == "abcDef"


def test_snakecase_to_camelcase__surrounded_by_underscore() -> None:
    assert snakecase_to_camelcase("_abc_def_") == "abcDef"


def test_snakecase_to_camelcase__starts_with_camelcase_inside() -> None:
    assert snakecase_to_camelcase("abcDef_hij") == "abcDefHij"


def test_snakecase_to_camelcase__with_camelcase_inside() -> None:
    assert snakecase_to_camelcase("abc_defHij") == "abcDefHij"


def test_snakecase_to_camelcase__starts_with_uppercase() -> None:
    assert snakecase_to_camelcase("Abc_def") == "abcDef"


def test_snakecase_to_camelcase__starts_with_uppercase_inside() -> None:
    assert snakecase_to_camelcase("abc_Def") == "abcDef"
