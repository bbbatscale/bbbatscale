import pytest
import responses
from freezegun import freeze_time


@freeze_time("2020-04-26T16:29:55+00:00")
@responses.activate
@pytest.mark.django_db
@pytest.mark.parametrize(
    "data_fixture,expected_response",
    [
        ({"token": "asdasdasd", "username": "blabla"}, 401),
        ({"token": "asdf", "username": "example-user"}, 200),
        ({"token": "asdf", "username": "mondry"}, 404),
        ({"token": "asdf"}, 400),
        ({"username": "mondry"}, 400),
        ({}, 400),
    ],
)
def test_delete_user_callback_happypath(
    request, client, testserver_tenant, gp_test_tenant, example_user, data_fixture, expected_response
):
    response = client.post("/core/api/user/delete", data_fixture, content_type="application/json")
    assert response.status_code == expected_response
