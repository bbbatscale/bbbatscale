from typing import Protocol, Type

import pytest
from conftest import RequestFactory
from core.feature_flag import Feature, feature_flag_context_preprocessor
from core.models import GeneralParameter
from django.conf import Settings
from django.contrib.sites.models import Site
from support_chat.models import SupportChatParameter


class FeatureFlagOnlyConsultingGeneralParameter(Protocol):
    def __init__(self, _: GeneralParameter) -> None:
        ...

    def is_enabled(self) -> bool:
        ...


@pytest.mark.parametrize(
    "general_parameter_key, feature",
    [
        ("home_room_enabled", Feature.HomeRooms),
        ("jitsi_enable", Feature.Jitsi),
        ("enable_occupancy", Feature.Occupancy),
        ("personal_rooms_enabled", Feature.PersonalRooms),
        ("enable_recordings", Feature.Recordings),
        ("enable_adminunmute", Feature.AdminUnmute),
        ("enable_meeting_dial_in", Feature.MeetingDialIn),
        ("enable_meeting_rtmp_streaming", Feature.MeetingRTMPStreaming),
    ],
)
@pytest.mark.django_db
def test_features_consulting_only_general_parameter(
    testserver_tenant: Site, general_parameter_key: str, feature: Type[FeatureFlagOnlyConsultingGeneralParameter]
) -> None:
    general_parameter = GeneralParameter.objects.load(testserver_tenant)
    setattr(general_parameter, general_parameter_key, False)
    assert not feature(general_parameter).is_enabled()

    setattr(general_parameter, general_parameter_key, True)
    assert feature(general_parameter).is_enabled()


@pytest.mark.django_db
def test_media(settings: Settings, testserver_tenant: Site) -> None:
    settings.BBBATSCALE_MEDIA_ENABLED = False

    general_parameter = GeneralParameter.objects.load(testserver_tenant)
    general_parameter.media_enabled = False
    assert not Feature.Media(general_parameter).is_enabled()

    general_parameter.media_enabled = True
    assert not Feature.Media(general_parameter).is_enabled()

    settings.BBBATSCALE_MEDIA_ENABLED = True
    assert Feature.Media(general_parameter).is_enabled()

    general_parameter.media_enabled = False
    assert not Feature.Media(general_parameter).is_enabled()


@pytest.mark.django_db
def test_support(testserver_tenant: Site) -> None:
    support_chat_parameter = SupportChatParameter.objects.load(testserver_tenant)
    support_chat_parameter.enabled = False
    assert not Feature.Support(support_chat_parameter).is_enabled()

    support_chat_parameter.enabled = True
    assert Feature.Support(support_chat_parameter).is_enabled()


def test_webhooks(settings: Settings, testserver_tenant: Site) -> None:
    settings.WEBHOOKS_ENABLED = False
    assert not Feature.Webhooks().is_enabled()

    settings.WEBHOOKS_ENABLED = True
    assert Feature.Webhooks().is_enabled()


@pytest.mark.django_db
def test_context_preprocessor(settings: Settings, testserver_tenant: Site) -> None:
    general_parameter = GeneralParameter.objects.load(testserver_tenant)
    general_parameter.home_room_enabled = False
    general_parameter.jitsi_enable = False
    general_parameter.media_enabled = False
    general_parameter.enable_occupancy = False
    general_parameter.personal_rooms_enabled = False
    general_parameter.enable_recordings = False

    support_chat_parameter = SupportChatParameter.objects.load(testserver_tenant)
    support_chat_parameter.enabled = False

    settings.BBBATSCALE_MEDIA_ENABLED = False
    settings.WEBHOOKS_ENABLED = False

    request = RequestFactory(
        testserver_tenant, general_parameter=general_parameter, support_chat_parameter=support_chat_parameter
    ).get("/")

    assert feature_flag_context_preprocessor(request) == {
        "Feature": {
            "HomeRooms": {"is_enabled": False},
            "Jitsi": {"is_enabled": False},
            "Media": {"is_enabled": False},
            "Occupancy": {"is_enabled": False},
            "PersonalRooms": {"is_enabled": False},
            "Recordings": {"is_enabled": False},
            "Support": {"is_enabled": False},
            "Webhooks": {"is_enabled": False},
            "AdminUnmute": {"is_enabled": False},
            "MeetingDialIn": {"is_enabled": False},
            "MeetingRTMPStreaming": {"is_enabled": False},
        }
    }

    general_parameter.media_enabled = True  # `BBBATSCALE_MEDIA_ENABLED` is still `False`
    general_parameter.personal_rooms_enabled = True

    settings.WEBHOOKS_ENABLED = True

    assert feature_flag_context_preprocessor(request) == {
        "Feature": {
            "HomeRooms": {"is_enabled": False},
            "Jitsi": {"is_enabled": False},
            "Media": {"is_enabled": False},
            "Occupancy": {"is_enabled": False},
            "PersonalRooms": {"is_enabled": True},
            "Recordings": {"is_enabled": False},
            "Support": {"is_enabled": False},
            "Webhooks": {"is_enabled": True},
            "AdminUnmute": {"is_enabled": False},
            "MeetingDialIn": {"is_enabled": False},
            "MeetingRTMPStreaming": {"is_enabled": False},
        }
    }
