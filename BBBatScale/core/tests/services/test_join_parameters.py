import freezegun
import pytest
from core.services import create_join_parameters


@pytest.mark.parametrize(
    "attempt_direct_join,join_secret,room_direct_join_secret,expected_result",
    [
        (True, "asdf", None, {"direct_join": False, "valid_secret": False}),
        (True, "asdf", "", {"direct_join": False, "valid_secret": False}),
        (True, "asdf", "asdf", {"direct_join": True, "valid_secret": True}),
        (True, "qwer", "asdf", {"direct_join": True, "valid_secret": False}),
        (False, "asdf", "asdf", {"direct_join": False, "valid_secret": True}),
    ],
)
def test_create_join_parameters(
    example_room, attempt_direct_join, join_secret, room_direct_join_secret, expected_result, current_timestamp
):
    example_room.direct_join_secret = room_direct_join_secret

    with freezegun.freeze_time(current_timestamp):
        actual_join_parameters = create_join_parameters(example_room, attempt_direct_join, join_secret)

    expected_timestamp = round(current_timestamp.timestamp()) * 1000
    expected_join_parameters = {"room_id": example_room.pk, "timestamp": expected_timestamp, **expected_result}

    assert actual_join_parameters == expected_join_parameters
