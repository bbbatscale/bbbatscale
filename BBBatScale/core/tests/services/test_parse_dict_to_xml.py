import pytest
from bs4 import BeautifulSoup
from core.services import parse_dict_to_xml


@pytest.fixture(scope="function")
def sample_json_for_parse_to_xml(db):
    return {"response": {"returncode": "TEST", "recordings": {"recording": "RECORDING_1"}}}


def test_parse_dict_to_xml(sample_json_for_parse_to_xml):
    parsed = parse_dict_to_xml(sample_json_for_parse_to_xml)

    # convert str to xml object
    soup = BeautifulSoup(parsed, "xml")

    assert soup.returncode.get_text() == "TEST"
    assert soup.recordings.get_text() == "RECORDING_1"
