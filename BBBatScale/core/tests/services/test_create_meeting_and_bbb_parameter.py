from typing import Any, Callable, Dict, Optional
from uuid import UUID

import pytest
from conftest import RequestFactory
from core.constants import MEETING_STATE_RUNNING
from core.models import GeneralParameter, Meeting, MeetingConfiguration, Room, Server, User
from core.services import create_meeting_and_bbb_parameter, moderator_message
from core.utils import join_or_create_url
from django.contrib.sites.models import Site
from faker import Faker


@pytest.fixture(scope="function")
@pytest.mark.djang_db
def meeting_with_dial_in_welcome(faker: Faker, example_room: Room, example_server: Server) -> Meeting:
    meeting_configuration = MeetingConfiguration.objects.create(
        welcome_message="Dial in via %%DIALNUM%% and %%CONFNUM%%."
    )

    return Meeting.objects.create(
        room=example_room,
        room_name=example_room.name,
        server=example_server,
        id=UUID(faker.uuid4()),
        state=MEETING_STATE_RUNNING,
        attendee_pw="attendee_password",
        moderator_pw="moderator_password",
        configuration=meeting_configuration,
    )


@pytest.fixture(scope="function")
@pytest.mark.djang_db
def external_meeting_create_parameter_with_dial_in_messages() -> Dict[str, Any]:
    return {
        "welcome": "Dial in via %%DIALNUM%% and %%CONFNUM%%.",
        "moderatorOnlyMessage": "Dial in via %%DIALNUM%% and %%CONFNUM%%.",
    }


def test_create_meeting_and_bbb_parameter(
    testserver_tenant: Site, gp_test_tenant: GeneralParameter, example_meeting: Meeting, example_user: User
) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration
    meeting_config.logoutUrl = "https://testserver"
    example_meeting.creator = example_user
    example_meeting.creator_name = example_user.username + "-not-only-the-username"

    gp_test_tenant.enable_meeting_rtmp_streaming = True
    gp_test_tenant.save()

    request = RequestFactory(testserver_tenant, general_parameter=gp_test_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)

    assert bbb_parameter["name"] == example_meeting.room_name
    assert bbb_parameter["meetingID"] == example_meeting.id
    assert bbb_parameter["attendeePW"] == example_meeting.attendee_pw
    assert bbb_parameter["moderatorPW"] == example_meeting.moderator_pw
    assert bbb_parameter["logoutURL"] == meeting_config.logoutUrl
    assert bbb_parameter["muteOnStart"] == "true"
    assert bbb_parameter["lockSettingsDisableCam"] == "false"
    assert bbb_parameter["lockSettingsDisableMic"] == "false"
    assert bbb_parameter["lockSettingsDisableNote"] == "false"
    assert bbb_parameter["lockSettingsDisablePublicChat"] == "false"
    assert bbb_parameter["lockSettingsDisablePrivateChat"] == "false"
    assert bbb_parameter["allowModsToUnmuteUsers"] == "false"
    assert bbb_parameter["allowModsToEjectCameras"] == "false"
    assert bbb_parameter["learningDashboardEnabled"] == "false"
    assert bbb_parameter["meetingLayout"] == meeting_config.meetingLayout
    assert bbb_parameter["guestPolicy"] == meeting_config.guest_policy
    assert bbb_parameter["record"] == "false"
    assert bbb_parameter["allowStartStopRecording"] == "false"
    assert bbb_parameter["meta_creator"] == example_meeting.creator_display_name
    assert bbb_parameter["meta_roomsmeetingid"] == example_meeting.pk
    assert bbb_parameter["meta_bbb-origin"] == "BBB@Scale"
    assert bbb_parameter["meta_streamingUrl"] == meeting_config.streamingUrl

    example_meeting.creator = None
    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)
    assert bbb_parameter["meta_creator"] == example_meeting.creator_name


def test_create_meeting_and_bbb_parameter_moderator_message(testserver_tenant: Site, example_meeting: Meeting) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration

    request = RequestFactory(testserver_tenant).get("/")

    meeting_config.meeting_duration_time = 200
    meeting_config.save()

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)
    assert bbb_parameter["moderatorOnlyMessage"] == moderator_message(
        request.build_absolute_uri(join_or_create_url(example_meeting.room)),
        example_meeting.room_name,
        meeting_config.access_code,
        meeting_config.only_prompt_guests_for_access_code,
        meeting_config.guest_policy,
        meeting_config.maxParticipants,
        False,
    )


def test_create_meeting_and_bbb_parameter_duration_set(testserver_tenant: Site, example_meeting: Meeting) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration

    request = RequestFactory(testserver_tenant).get("/")

    meeting_config.meeting_duration_time = 200
    meeting_config.save()

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)

    assert bbb_parameter["duration"] == 200


def test_create_meeting_and_bbb_parameter_no_duration_set(testserver_tenant: Site, example_meeting: Meeting) -> None:
    request = RequestFactory(testserver_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)

    assert "duration" not in bbb_parameter


def test_create_meeting_and_bbb_parameter_welcome_set(testserver_tenant: Site, example_meeting: Meeting) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration
    meeting_config.welcome_message = "Hello"
    request = RequestFactory(testserver_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)

    assert bbb_parameter["welcome"] == "Hello"


def test_create_meeting_and_bbb_parameter_no_welcome_set(testserver_tenant: Site, example_meeting: Meeting) -> None:
    request = RequestFactory(testserver_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)

    assert "welcome" not in bbb_parameter


def test_create_meeting_and_bbb_parameter_dial_number_set(
    testserver_tenant: Site, gp_test_tenant: GeneralParameter, example_meeting: Meeting
) -> None:
    gp_test_tenant.enable_meeting_dial_in = True
    gp_test_tenant.save()

    request = RequestFactory(testserver_tenant, general_parameter=gp_test_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, "0123456789")

    assert bbb_parameter["dialNumber"] == "0123456789"


def test_create_meeting_and_bbb_parameter_no_dial_number_set(
    testserver_tenant: Site, gp_test_tenant: GeneralParameter, example_meeting: Meeting
) -> None:
    gp_test_tenant.enable_meeting_dial_in = True
    gp_test_tenant.save()

    request = RequestFactory(testserver_tenant, general_parameter=gp_test_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)

    assert "dialNumber" not in bbb_parameter


def test_create_meeting_and_bbb_parameter_maxParticipants_set(
    testserver_tenant: Site, example_meeting: Meeting
) -> None:
    meeting_config: MeetingConfiguration = example_meeting.configuration
    meeting_config.maxParticipants = "10"
    request = RequestFactory(testserver_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)

    assert bbb_parameter["maxParticipants"] == "10"


def test_create_meeting_and_bbb_parameter_no_maxParticipants_set(
    testserver_tenant: Site, example_meeting: Meeting
) -> None:
    request = RequestFactory(testserver_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)

    assert "maxParticipants" not in bbb_parameter


@pytest.mark.parametrize(
    (
        "general_parameter_key",
        "meeting_config_key",
        "meeting_config_value_before",
        "meeting_config_value_after",
        "test_expression",
    ),
    [
        (
            "enable_recordings",
            "allow_recording",
            False,
            True,
            lambda bbb_parameter: bbb_parameter["record"] == "false"
            and bbb_parameter["allowStartStopRecording"] == "false",
        ),
        (
            "enable_adminunmute",
            "allow_unmuteusers",
            False,
            True,
            lambda bbb_parameter: bbb_parameter["allowModsToUnmuteUsers"] == "false",
        ),
        (
            "enable_meeting_rtmp_streaming",
            "streamingUrl",
            "",
            "rtmp://example.org/",
            lambda bbb_parameter: bbb_parameter["meta_streamingUrl"] == "",
        ),
    ],
)
def test_create_meeting_and_bbb_parameter_in_conjunction_with_feature_flags(
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    example_meeting: Meeting,
    general_parameter_key: str,
    meeting_config_key: str,
    meeting_config_value_before: Any,
    meeting_config_value_after: Any,
    test_expression: Callable[[dict], bool],
) -> None:
    setattr(gp_test_tenant, general_parameter_key, False)
    gp_test_tenant.save()

    request = RequestFactory(testserver_tenant, general_parameter=gp_test_tenant).get("/")

    setattr(example_meeting.configuration, meeting_config_key, meeting_config_value_before)
    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)
    assert test_expression(bbb_parameter)

    setattr(example_meeting.configuration, meeting_config_key, meeting_config_value_after)
    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)
    assert test_expression(bbb_parameter)


def test_create_meeting_and_bbb_parameter_in_conjunction_with_feature_flags__dial_number(
    testserver_tenant: Site, gp_test_tenant: GeneralParameter, example_meeting: Meeting
) -> None:
    gp_test_tenant.enable_meeting_dial_in = False
    gp_test_tenant.save()

    request = RequestFactory(testserver_tenant, general_parameter=gp_test_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, None)
    assert "dialNumber" not in bbb_parameter

    bbb_parameter = create_meeting_and_bbb_parameter(request, example_meeting, None, "0123456789")
    assert "dialNumber" not in bbb_parameter


@pytest.mark.parametrize(
    ("enable_meeting_dial_in", "meeting_fixture_name", "create_parameter_fixture_name", "dial_number"),
    [
        (False, "meeting_with_dial_in_welcome", None, "0123456789"),
        (True, "meeting_with_dial_in_welcome", None, None),
        (False, "external_meeting", "external_meeting_create_parameter_with_dial_in_messages", "0123456789"),
        (True, "external_meeting", "external_meeting_create_parameter_with_dial_in_messages", None),
    ],
)
def test_create_meeting_and_bbb_parameter__escaping_welcome(
    request: pytest.FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    enable_meeting_dial_in: bool,
    meeting_fixture_name: str,
    create_parameter_fixture_name: Optional[str],
    dial_number: Optional[str],
) -> None:
    gp_test_tenant.enable_meeting_dial_in = enable_meeting_dial_in
    gp_test_tenant.save()

    http_request = RequestFactory(testserver_tenant, general_parameter=gp_test_tenant).get("/")

    bbb_parameter = create_meeting_and_bbb_parameter(
        http_request,
        request.getfixturevalue(meeting_fixture_name),
        request.getfixturevalue(create_parameter_fixture_name) if create_parameter_fixture_name else None,
        dial_number,
    )

    assert "%%DIALNUM%%" not in bbb_parameter["welcome"]
    assert "%%CONFNUM%%" not in bbb_parameter["welcome"]

    assert "%%DIALNUM%%" not in bbb_parameter["moderatorOnlyMessage"]
    assert "%%CONFNUM%%" not in bbb_parameter["moderatorOnlyMessage"]
