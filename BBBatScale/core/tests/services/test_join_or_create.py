import dataclasses
import itertools
from contextlib import contextmanager
from datetime import datetime
from inspect import currentframe
from typing import Any, Iterator, Optional, Tuple, Type

import pytest
from core import bigbluebutton
from core.constants import (
    MEETING_STATE_CREATING,
    MEETING_STATE_FINISHED,
    MEETING_STATE_RUNNING,
    MODERATION_MODE_ALL,
    MODERATION_MODE_MODERATORS,
    MODERATION_MODE_STARTER,
    ROOM_STATE_ACTIVE,
    ROOM_STATE_CREATING,
    ROOM_STATE_INACTIVE,
)
from core.forms import ConfigureMeetingForm
from core.models import (
    GeneralParameter,
    HomeRoom,
    Meeting,
    MeetingConfiguration,
    MeetingConfigurationTemplate,
    PersonalRoom,
    Room,
    SchedulingStrategy,
    Server,
    User,
)
from core.services import join_or_create
from core.services.join_or_create import (
    ConfigureMeetingAction,
    CreateJoinToken,
    DisplayMeetingEndedAction,
    PromptAccessCodeAction,
    PromptJoinNameAction,
    PromptLogInAction,
    RedirectAction,
    WaitUntilConfiguredAction,
    WaitUntilRunningAction,
    _serialize_bbb_join_parameters,
    can_user_create_meeting,
    can_user_skip_join_preconditions,
    create_meeting,
    get_next_action,
    is_user_moderator,
)
from core.utils import get_permission
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.models import Site
from django.utils.functional import LazyObject
from freezegun import freeze_time
from pytest_mock import MockerFixture


class DefaultElement(LazyObject):
    def __init__(self, value: Any) -> None:
        super().__init__()
        self._wrapped = value

    # Return a meaningful representation of the lazy object for debugging
    # without evaluating the wrapped object.
    def __repr__(self) -> str:
        return "<%s: %r>" % (type(self).__name__, self._wrapped)

    def _setup(self) -> None:
        pass


@dataclasses.dataclass(frozen=True, eq=False)
class MockedCreateJoinToken(CreateJoinToken):
    """
    A subclass of CreateJoinToken to test whether a value has been explicitly set or the default has been used.
    """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        field_names = [field.name for field in dataclasses.fields(self)]
        assigned_fields = {*field_names[: len(args)], *kwargs}

        for field_name in field_names:
            if field_name not in assigned_fields:
                object.__setattr__(self, field_name, DefaultElement(getattr(self, field_name)))

    def __getattribute__(self, item: str) -> Any:
        # Unwrap default if the caller module is `join_or_create` to be able to perform
        # identity checks (like `token.meeting is None`), otherwise they would always
        # result in `False`.
        value = object.__getattribute__(self, item)

        if isinstance(value, DefaultElement):
            caller_frame = currentframe().f_back
            caller_module_name = caller_frame.f_globals["__name__"]

            if caller_module_name == join_or_create.__name__:
                return getattr(value, "_wrapped")

        return value

    def __repr__(self) -> str:
        # Add line breaks to increase readability when displaying failed assertion diffs.
        fields = ",\n    ".join(
            f"{field.name}={object.__getattribute__(self, field.name)!r}" for field in dataclasses.fields(self)
        )
        return f"{type(self).__name__}(\n    {fields}\n)"

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, type(self)):
            return False

        for field in dataclasses.fields(self):
            if field.compare:
                value = object.__getattribute__(self, field.name)
                other_value = object.__getattribute__(other, field.name)
                if isinstance(value, DefaultElement) != isinstance(other_value, DefaultElement):
                    return False

                if value != other_value:
                    return False

        return True


@contextmanager
def assert_called(mocker: MockerFixture, **functions: bool) -> Iterator[None]:
    spies = dict()

    try:
        for function_name in functions:
            spies[function_name] = mocker.spy(join_or_create, function_name)

        yield
    finally:
        for function_name, should_be_called in functions.items():
            if should_be_called:
                spies[function_name].assert_called_once()
            else:
                spies[function_name].assert_not_called()


@pytest.mark.django_db
@pytest.fixture
def running_meeting__without_configuration(testserver_tenant: Site, example_server: Server) -> Meeting:
    meeting = Meeting.objects.create(
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=None,
    )
    meeting.tenants.add(testserver_tenant)

    return meeting


@pytest.mark.django_db
@pytest.fixture
def running_meeting__moderation_mode_starter(testserver_tenant: Site, example_server: Server) -> Meeting:
    meeting_configuration = MeetingConfiguration.objects.create(moderation_mode=MODERATION_MODE_STARTER)

    meeting = Meeting.objects.create(
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=meeting_configuration,
    )
    meeting.tenants.add(testserver_tenant)

    return meeting


@pytest.mark.django_db
@pytest.fixture
def running_meeting__moderation_mode_moderators(testserver_tenant: Site, example_server: Server) -> Meeting:
    meeting_configuration = MeetingConfiguration.objects.create(moderation_mode=MODERATION_MODE_MODERATORS)

    meeting = Meeting.objects.create(
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=meeting_configuration,
    )
    meeting.tenants.add(testserver_tenant)

    return meeting


@pytest.mark.django_db
@pytest.fixture
def running_meeting__moderation_mode_all(testserver_tenant: Site, example_server: Server) -> Meeting:
    meeting_configuration = MeetingConfiguration.objects.create(moderation_mode=MODERATION_MODE_ALL)

    meeting = Meeting.objects.create(
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=meeting_configuration,
    )
    meeting.tenants.add(testserver_tenant)

    return meeting


@pytest.mark.django_db
@pytest.fixture
def inactive_room__without_secret(
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> Room:
    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="inactive_room__without_secret",
        default_meeting_configuration=example_meeting_configuration_template,
        state=ROOM_STATE_INACTIVE,
    )
    room.tenants.add(testserver_tenant)

    return room


@pytest.mark.django_db
@pytest.fixture
def inactive_room__with_secret(
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> Room:
    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="inactive_room__with_secret",
        default_meeting_configuration=example_meeting_configuration_template,
        state=ROOM_STATE_INACTIVE,
        direct_join_secret="SUPER_SECRET",
    )
    room.tenants.add(testserver_tenant)

    return room


@pytest.mark.django_db
@pytest.fixture
def meeting_configuration_template__create_meeting_permission_required(
    testserver_tenant: Site,
) -> MeetingConfigurationTemplate:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(
        name="meeting_configuration_template__create_meeting_permission_required",
        authenticated_user_can_start=False,
        everyone_can_start=False,
    )
    meeting_configuration_template.tenants.add(testserver_tenant)

    return meeting_configuration_template


@pytest.mark.django_db
@pytest.fixture
def inactive_room__create_meeting_permission_required(
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    meeting_configuration_template__create_meeting_permission_required: MeetingConfigurationTemplate,
) -> Room:
    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="inactive_room__create_meeting_permission_required",
        default_meeting_configuration=meeting_configuration_template__create_meeting_permission_required,
        state=ROOM_STATE_INACTIVE,
    )
    room.tenants.add(testserver_tenant)

    return room


@pytest.mark.django_db
@pytest.fixture
def homeroom__example_user(
    example_user: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    meeting_configuration_template__create_meeting_permission_required: MeetingConfigurationTemplate,
) -> HomeRoom:
    room = HomeRoom.objects.create(
        name="homeroom__example_user",
        owner=example_user,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=meeting_configuration_template__create_meeting_permission_required,
    )
    room.tenants.set([testserver_tenant])

    return room


@pytest.mark.django_db
@pytest.fixture
def personal_room__with_co_owner(
    example_moderator_user: User,
    example_user: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    meeting_configuration_template__create_meeting_permission_required: MeetingConfigurationTemplate,
) -> PersonalRoom:
    room = PersonalRoom.objects.create(
        name="personal_room__with_co_owner",
        owner=example_moderator_user,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=meeting_configuration_template__create_meeting_permission_required,
    )
    room.tenants.set([testserver_tenant])
    room.co_owners.set([example_user])

    return room


@pytest.mark.django_db
@pytest.fixture
def user__testserver2_tenant(testserver2_tenant: Site) -> User:
    return User.objects.create_user(
        "user__testserver2_tenant",
        tenant=testserver2_tenant,
        is_staff=False,
        is_superuser=False,
    )


@pytest.mark.django_db
@pytest.fixture
def meeting_configuration_template__everyone_can_start(testserver_tenant: Site) -> MeetingConfigurationTemplate:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(
        name="meeting_configuration_template__everyone_can_start", everyone_can_start=True
    )
    meeting_configuration_template.tenants.add(testserver_tenant)

    return meeting_configuration_template


@pytest.mark.django_db
@pytest.fixture
def inactive_room__everyone_can_start(
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    meeting_configuration_template__everyone_can_start: MeetingConfigurationTemplate,
) -> Room:
    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="inactive_room__everyone_can_start",
        default_meeting_configuration=meeting_configuration_template__everyone_can_start,
        state=ROOM_STATE_INACTIVE,
    )
    room.tenants.add(testserver_tenant)

    return room


@pytest.mark.django_db
@pytest.fixture
def creating_room_with_meeting_and_no_creator(
    example_scheduling_strategy: SchedulingStrategy,
    example_server: Server,
    testserver_tenant: Site,
    meeting_configuration_template__everyone_can_start: MeetingConfigurationTemplate,
) -> Tuple[Room, Meeting]:
    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="creating_room_with_meeting_and_no_creator",
        default_meeting_configuration=meeting_configuration_template__everyone_can_start,
        state=ROOM_STATE_CREATING,
    )
    room.tenants.add(testserver_tenant)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        creator=None,
        creator_name="",
        server=example_server,
        state=MEETING_STATE_CREATING,
        configuration=meeting_configuration_template__everyone_can_start.instantiate(),
    )

    return room, meeting


@pytest.mark.django_db
@pytest.fixture
def creating_room_with_meeting_and_creator(
    example_scheduling_strategy: SchedulingStrategy,
    example_server: Server,
    testserver_tenant: Site,
    meeting_configuration_template__everyone_can_start: MeetingConfigurationTemplate,
) -> Tuple[Room, Meeting, User]:
    user = User.objects.create_user(
        "creator",
        "creator@example.org",
        tenant=testserver_tenant,
        is_staff=False,
        is_superuser=False,
        first_name="Crea",
        last_name="Tor",
        display_name="Crea Tor",
    )

    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="creating_room_with_meeting_and_creator",
        default_meeting_configuration=meeting_configuration_template__everyone_can_start,
        state=ROOM_STATE_CREATING,
    )
    room.tenants.add(testserver_tenant)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        creator=user,
        creator_name=str(user),
        server=example_server,
        state=MEETING_STATE_CREATING,
        configuration=meeting_configuration_template__everyone_can_start.instantiate(),
    )

    return room, meeting, user


@pytest.mark.django_db
@pytest.fixture
def inactive_room_with_finished_meeting(
    example_scheduling_strategy: SchedulingStrategy,
    example_server: Server,
    testserver_tenant: Site,
    meeting_configuration_template__everyone_can_start: MeetingConfigurationTemplate,
) -> Tuple[Room, Meeting]:
    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="inactive_room_with_finished_meeting",
        default_meeting_configuration=meeting_configuration_template__everyone_can_start,
        state=ROOM_STATE_INACTIVE,
    )
    room.tenants.add(testserver_tenant)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        creator=None,
        creator_name="",
        server=example_server,
        state=MEETING_STATE_FINISHED,
        configuration=meeting_configuration_template__everyone_can_start.instantiate(),
    )

    return room, meeting


@pytest.mark.django_db
@pytest.fixture
def inactive_room__authenticated_users_can_start(
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
) -> Room:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(
        name="meeting_configuration_template__authenticated_users_can_start",
        authenticated_user_can_start=True,
        everyone_can_start=False,
    )
    meeting_configuration_template.tenants.add(testserver_tenant)

    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="inactive_room__authenticated_users_can_start",
        default_meeting_configuration=meeting_configuration_template,
        state=ROOM_STATE_INACTIVE,
    )
    room.tenants.add(testserver_tenant)

    return room


@pytest.mark.django_db
@pytest.fixture
def active_room_with_meeting_and_no_creator__dont_allow_guests(
    example_scheduling_strategy: SchedulingStrategy,
    example_server: Server,
    testserver_tenant: Site,
) -> Tuple[Room, Meeting]:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(
        name="meeting_configuration_template__dont_allow_guests", allow_guest_entry=False
    )
    meeting_configuration_template.tenants.add(testserver_tenant)

    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="active_room_with_meeting_and_no_creator__dont_allow_guests",
        default_meeting_configuration=meeting_configuration_template,
        state=ROOM_STATE_ACTIVE,
    )
    room.tenants.add(testserver_tenant)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        creator=None,
        creator_name="",
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=meeting_configuration_template.instantiate(),
    )

    return room, meeting


@pytest.mark.django_db
@pytest.fixture
def active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code(
    example_scheduling_strategy: SchedulingStrategy,
    example_server: Server,
    testserver_tenant: Site,
) -> Tuple[Room, Meeting]:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(
        name="meeting_configuration_template__dont_allow_guests__needs_access_code",
        allow_guest_entry=False,
        access_code="12345",
        only_prompt_guests_for_access_code=False,
    )
    meeting_configuration_template.tenants.add(testserver_tenant)

    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code",
        default_meeting_configuration=meeting_configuration_template,
        state=ROOM_STATE_ACTIVE,
    )
    room.tenants.add(testserver_tenant)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        creator=None,
        creator_name="",
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=meeting_configuration_template.instantiate(),
    )

    return room, meeting


@pytest.mark.django_db
@pytest.fixture
def active_room_with_meeting_and_no_creator__allow_guests__needs_access_code(
    example_scheduling_strategy: SchedulingStrategy,
    example_server: Server,
    testserver_tenant: Site,
) -> Tuple[Room, Meeting]:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(
        name="meeting_configuration_template__allow_guests__needs_access_code",
        allow_guest_entry=True,
        access_code="12345",
        only_prompt_guests_for_access_code=False,
    )
    meeting_configuration_template.tenants.add(testserver_tenant)

    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="active_room_with_meeting_and_no_creator__allow_guests__needs_access_code",
        default_meeting_configuration=meeting_configuration_template,
        state=ROOM_STATE_ACTIVE,
    )
    room.tenants.add(testserver_tenant)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        creator=None,
        creator_name="",
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=meeting_configuration_template.instantiate(),
    )

    return room, meeting


@pytest.mark.django_db
@pytest.fixture
def active_room_with_meeting_and_no_creator__allow_guests__needs_guest_only_access_code(
    example_scheduling_strategy: SchedulingStrategy,
    example_server: Server,
    testserver_tenant: Site,
) -> Tuple[Room, Meeting]:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(
        name="meeting_configuration_template__allow_guests__needs_guest_only_access_code",
        allow_guest_entry=True,
        access_code="12345",
        only_prompt_guests_for_access_code=True,
    )
    meeting_configuration_template.tenants.add(testserver_tenant)

    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="active_room_with_meeting_and_no_creator__allow_guests__needs_guest_only_access_code",
        default_meeting_configuration=meeting_configuration_template,
        state=ROOM_STATE_ACTIVE,
    )
    room.tenants.add(testserver_tenant)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        creator=None,
        creator_name="",
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=meeting_configuration_template.instantiate(),
    )

    return room, meeting


@pytest.mark.django_db
@pytest.fixture
def active_room_with_meeting_and_no_creator__allow_guests__no_access_code(
    example_scheduling_strategy: SchedulingStrategy,
    example_server: Server,
    testserver_tenant: Site,
) -> Tuple[Room, Meeting]:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(
        name="meeting_configuration_template__allow_guests__no_access_code",
        allow_guest_entry=True,
        access_code="",
    )
    meeting_configuration_template.tenants.add(testserver_tenant)

    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="active_room_with_meeting_and_no_creator__allow_guests__no_access_code",
        default_meeting_configuration=meeting_configuration_template,
        state=ROOM_STATE_ACTIVE,
    )
    room.tenants.add(testserver_tenant)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        creator=None,
        creator_name="",
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=meeting_configuration_template.instantiate(),
    )

    return room, meeting


@pytest.mark.django_db
@pytest.fixture
def active_room_with_meeting_and_creator__dont_allow_guests__needs_access_code(
    example_scheduling_strategy: SchedulingStrategy,
    example_server: Server,
    testserver_tenant: Site,
) -> Tuple[Room, Meeting, User]:
    user = User.objects.create_user(
        "creator",
        "creator@example.org",
        tenant=testserver_tenant,
        is_staff=False,
        is_superuser=False,
        first_name="Crea",
        last_name="Tor",
        display_name="Crea Tor",
    )

    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(
        name="meeting_configuration_template__dont_allow_guests__needs_access_code",
        allow_guest_entry=False,
        access_code="12345",
        only_prompt_guests_for_access_code=False,
    )
    meeting_configuration_template.tenants.add(testserver_tenant)

    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="active_room_with_meeting_and_creator__dont_allow_guests__needs_access_code",
        default_meeting_configuration=meeting_configuration_template,
        state=ROOM_STATE_ACTIVE,
    )
    room.tenants.add(testserver_tenant)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        creator=user,
        creator_name=str(user),
        server=example_server,
        state=MEETING_STATE_RUNNING,
        configuration=meeting_configuration_template.instantiate(),
    )

    return room, meeting, user


@pytest.mark.django_db
@pytest.fixture
def user_with_join_meeting_permission(testserver_tenant: Site) -> User:
    user = User.objects.create_user(
        "user_with_join_meeting_permission",
        "user_with_join_meeting_permission@example.org",
        tenant=testserver_tenant,
        is_staff=False,
        is_superuser=False,
        first_name="User",
        last_name="With-join-meeting-permission",
        display_name="User With-join-meeting-permission",
    )

    get_permission("core.join_meeting").user_set.add(user)

    return user


@pytest.mark.django_db
@pytest.fixture
def user_with_join_meeting_and_moderate_permissions(testserver_tenant: Site) -> User:
    user = User.objects.create_user(
        "user_with_join_meeting_and_moderate_permissions",
        "user_with_join_meeting_and_moderate_permissions@example.org",
        tenant=testserver_tenant,
        is_staff=False,
        is_superuser=False,
        first_name="User",
        last_name="With-join-meeting-and-moderate-permissions",
        display_name="User With-join-meeting-and-moderate-permissions",
    )

    get_permission("core.join_meeting").user_set.add(user)
    get_permission("core.moderate").user_set.add(user)

    return user


@pytest.fixture
def default_bbb_join_parameters() -> bigbluebutton.JoinParameters:
    return bigbluebutton.JoinParameters()


@pytest.mark.django_db
def test_create_join_token_is_immutable(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters
) -> None:
    original_token = CreateJoinToken(example_room, default_bbb_join_parameters)
    token = dataclasses.replace(original_token)

    for field in dataclasses.fields(CreateJoinToken):
        with pytest.raises(dataclasses.FrozenInstanceError):
            setattr(token, field.name, 123)

    assert token == original_token


@pytest.mark.django_db
def test_create_join_token__from_init_request__room_name(
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    inactive_room__without_secret: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=inactive_room__without_secret.name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret=None,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=None,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=inactive_room__without_secret,
        bbb_join_parameters=default_bbb_join_parameters,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_init_request__skip_meeting_configuration(
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    inactive_room__with_secret: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=inactive_room__with_secret.name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=value,
        secret=inactive_room__with_secret.direct_join_secret,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=None,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=inactive_room__with_secret,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("room", "pass_invalid_secret", "value"),
    itertools.product(
        ["inactive_room__without_secret", "inactive_room__with_secret"],
        [False, True],
        [False, True],
    ),
)
def test_create_join_token__from_init_request__skip_meeting_configuration__invalid_secret(
    request: pytest.FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    room: str,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    pass_invalid_secret: bool,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=request.getfixturevalue(room).name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=value,
        secret="INVALID_SECRET" if pass_invalid_secret else None,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=None,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=request.getfixturevalue(room),
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=value,
    )


@pytest.mark.django_db
def test_create_join_token__from_init_request__secret(
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    inactive_room__with_secret: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=inactive_room__with_secret.name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret=inactive_room__with_secret.direct_join_secret,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=None,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=inactive_room__with_secret,
        bbb_join_parameters=default_bbb_join_parameters,
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("room", "pass_invalid_secret"),
    itertools.product(
        ["inactive_room__without_secret", "inactive_room__with_secret"],
        [False, True],
    ),
)
def test_create_join_token__from_init_request__secret__invalid_secret(
    request: pytest.FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    room: str,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    pass_invalid_secret: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=request.getfixturevalue(room).name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret="INVALID_SECRET" if pass_invalid_secret else None,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=None,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=request.getfixturevalue(room),
        bbb_join_parameters=default_bbb_join_parameters,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_init_request__enforce_create_meeting_permission(
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    inactive_room__with_secret: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=inactive_room__with_secret.name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret=inactive_room__with_secret.direct_join_secret,
        enforce_create_meeting_permission=value,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=None,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=inactive_room__with_secret,
        bbb_join_parameters=default_bbb_join_parameters,
        enforce_create_meeting_permission=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("room", "pass_invalid_secret", "value"),
    itertools.product(
        ["inactive_room__without_secret", "inactive_room__with_secret"],
        [False, True],
        [False, True],
    ),
)
def test_create_join_token__from_init_request__enforce_create_meeting_permission__invalid_secret(
    request: pytest.FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    room: str,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    pass_invalid_secret: bool,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=request.getfixturevalue(room).name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret="INVALID_SECRET" if pass_invalid_secret else None,
        enforce_create_meeting_permission=value,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=None,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=request.getfixturevalue(room),
        bbb_join_parameters=default_bbb_join_parameters,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_init_request__enforce_join_meeting_permission(
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    inactive_room__with_secret: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=inactive_room__with_secret.name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret=inactive_room__with_secret.direct_join_secret,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=value,
        enforce_moderate_permission=None,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=inactive_room__with_secret,
        bbb_join_parameters=default_bbb_join_parameters,
        enforce_join_meeting_permission=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("room", "pass_invalid_secret", "value"),
    itertools.product(
        ["inactive_room__without_secret", "inactive_room__with_secret"],
        [False, True],
        [False, True],
    ),
)
def test_create_join_token__from_init_request__enforce_join_meeting_permission__invalid_secret(
    request: pytest.FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    room: str,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    pass_invalid_secret: bool,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=request.getfixturevalue(room).name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret="INVALID_SECRET" if pass_invalid_secret else None,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=value,
        enforce_moderate_permission=None,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=request.getfixturevalue(room),
        bbb_join_parameters=default_bbb_join_parameters,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_init_request__enforce_moderate_permission(
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    inactive_room__with_secret: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=inactive_room__with_secret.name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret=inactive_room__with_secret.direct_join_secret,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=value,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=inactive_room__with_secret,
        bbb_join_parameters=default_bbb_join_parameters,
        enforce_moderate_permission=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("room", "pass_invalid_secret", "value"),
    itertools.product(
        ["inactive_room__without_secret", "inactive_room__with_secret"],
        [False, True],
        [False, True],
    ),
)
def test_create_join_token__from_init_request__enforce_moderate_permission__invalid_secret(
    request: pytest.FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    room: str,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    pass_invalid_secret: bool,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=request.getfixturevalue(room).name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret="INVALID_SECRET" if pass_invalid_secret else None,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=value,
        join_as_room=None,
    ) == MockedCreateJoinToken(
        room=request.getfixturevalue(room),
        bbb_join_parameters=default_bbb_join_parameters,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_init_request__join_as_room(
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    inactive_room__with_secret: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=inactive_room__with_secret.name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret=inactive_room__with_secret.direct_join_secret,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=None,
        join_as_room=value,
    ) == MockedCreateJoinToken(
        room=inactive_room__with_secret,
        bbb_join_parameters=default_bbb_join_parameters,
        join_as_room=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("room", "pass_invalid_secret", "value"),
    itertools.product(
        ["inactive_room__without_secret", "inactive_room__with_secret"],
        [False, True],
        [False, True],
    ),
)
def test_create_join_token__from_init_request__join_as_room__invalid_secret(
    request: pytest.FixtureRequest,
    testserver_tenant: Site,
    gp_test_tenant: GeneralParameter,
    room: str,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    pass_invalid_secret: bool,
    value: bool,
) -> None:
    assert MockedCreateJoinToken.from_init_request(
        tenant=testserver_tenant,
        general_parameter=gp_test_tenant,
        room_name=request.getfixturevalue(room).name,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=None,
        secret="INVALID_SECRET" if pass_invalid_secret else None,
        enforce_create_meeting_permission=None,
        enforce_join_meeting_permission=None,
        enforce_moderate_permission=None,
        join_as_room=value,
    ) == MockedCreateJoinToken(
        room=request.getfixturevalue(room),
        bbb_join_parameters=default_bbb_join_parameters,
    )


def test_serialize_bbb_join_parameters(default_bbb_join_parameters: bigbluebutton.JoinParameters) -> None:
    assert _serialize_bbb_join_parameters(default_bbb_join_parameters) == {
        "autoJoinAudio": True,
        "listenOnlyMode": True,
        "skipCheckAudio": False,
        "skipCheckAudioOnFirstJoin": False,
        "autoShareWebcam": False,
        "recordVideo": True,
        "skipVideoPreview": False,
        "skipVideoPreviewOnFirstJoin": False,
        "mirrorOwnWebcam": False,
        "forceRestorePresentationOnNewEvents": False,
        "autoSwapLayout": False,
        "showParticipantsOnLogin": True,
        "showPublicChatOnLogin": True,
    }


@pytest.mark.django_db
def test_create_join_token__from_json_token__room_id(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters
) -> None:
    assert MockedCreateJoinToken.from_json_token(
        {"roomId": example_room.id, "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters)}
    ) == MockedCreateJoinToken(
        room=example_room,
        bbb_join_parameters=default_bbb_join_parameters,
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("value", "expectation_exception"),
    [
        (False, TypeError),
        (True, TypeError),
        ("", TypeError),
        ("abc", TypeError),
        (-123, Room.DoesNotExist),
        (list(), TypeError),
        ([1, 2, 3], TypeError),
        (dict(), TypeError),
        ({"a": 1, "b": 2, "c": 3}, TypeError),
    ],
)
def test_create_join_token__from_json_token__invalid_room_id(
    value: Any, expectation_exception: Type[Exception], default_bbb_join_parameters: bigbluebutton.JoinParameters
) -> None:
    with pytest.raises(expectation_exception):
        CreateJoinToken.from_json_token(
            {"roomId": value, "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters)}
        )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_json_token__skip_meeting_configuration(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert MockedCreateJoinToken.from_json_token(
        {
            "roomId": example_room.id,
            "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
            "skipMeetingConfiguration": value,
        }
    ) == MockedCreateJoinToken(
        room=example_room,
        bbb_join_parameters=default_bbb_join_parameters,
        skip_meeting_configuration=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", ["", "abc", -123, 0, 123, list(), [1, 2, 3], dict(), {"a": 1, "b": 2, "c": 3}])
def test_create_join_token__from_json_token__invalid_skip_meeting_configuration(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: Any
) -> None:
    with pytest.raises(TypeError):
        CreateJoinToken.from_json_token(
            {
                "roomId": example_room.id,
                "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
                "skipMeetingConfiguration": value,
            }
        )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_json_token__enforce_create_meeting_permission(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert MockedCreateJoinToken.from_json_token(
        {
            "roomId": example_room.id,
            "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
            "enforceCreateMeetingPermission": value,
        }
    ) == MockedCreateJoinToken(
        room=example_room,
        bbb_join_parameters=default_bbb_join_parameters,
        enforce_create_meeting_permission=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", ["", "abc", -123, 0, 123, list(), [1, 2, 3], dict(), {"a": 1, "b": 2, "c": 3}])
def test_create_join_token__from_json_token__invalid_enforce_create_meeting_permission(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: Any
) -> None:
    with pytest.raises(TypeError):
        CreateJoinToken.from_json_token(
            {
                "roomId": example_room.id,
                "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
                "enforceCreateMeetingPermission": value,
            }
        )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_json_token__enforce_join_meeting_permission(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert MockedCreateJoinToken.from_json_token(
        {
            "roomId": example_room.id,
            "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
            "enforceJoinMeetingPermission": value,
        }
    ) == MockedCreateJoinToken(
        room=example_room,
        bbb_join_parameters=default_bbb_join_parameters,
        enforce_join_meeting_permission=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", ["", "abc", -123, 0, 123, list(), [1, 2, 3], dict(), {"a": 1, "b": 2, "c": 3}])
def test_create_join_token__from_json_token__invalid_enforce_join_meeting_permission(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: Any
) -> None:
    with pytest.raises(TypeError):
        CreateJoinToken.from_json_token(
            {
                "roomId": example_room.id,
                "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
                "enforceJoinMeetingPermission": value,
            }
        )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_json_token__enforce_moderate_permission(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert MockedCreateJoinToken.from_json_token(
        {
            "roomId": example_room.id,
            "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
            "enforceModeratePermission": value,
        }
    ) == MockedCreateJoinToken(
        room=example_room,
        bbb_join_parameters=default_bbb_join_parameters,
        enforce_moderate_permission=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", ["", "abc", -123, 0, 123, list(), [1, 2, 3], dict(), {"a": 1, "b": 2, "c": 3}])
def test_create_join_token__from_json_token__invalid_enforce_moderate_permission(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: Any
) -> None:
    with pytest.raises(TypeError):
        CreateJoinToken.from_json_token(
            {
                "roomId": example_room.id,
                "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
                "enforceModeratePermission": value,
            }
        )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_json_token__join_as_room(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert MockedCreateJoinToken.from_json_token(
        {
            "roomId": example_room.id,
            "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
            "joinAsRoom": value,
        }
    ) == MockedCreateJoinToken(
        room=example_room,
        bbb_join_parameters=default_bbb_join_parameters,
        join_as_room=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", ["", "abc", -123, 0, 123, list(), [1, 2, 3], dict(), {"a": 1, "b": 2, "c": 3}])
def test_create_join_token__from_json_token__invalid_join_as_room(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: Any
) -> None:
    with pytest.raises(TypeError):
        CreateJoinToken.from_json_token(
            {
                "roomId": example_room.id,
                "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
                "joinAsRoom": value,
            }
        )


@pytest.mark.django_db
def test_create_join_token__from_json_token__meeting_id(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, example_meeting: Meeting
) -> None:
    assert MockedCreateJoinToken.from_json_token(
        {
            "roomId": example_room.id,
            "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
            "meetingId": str(example_meeting.id),
        }
    ) == MockedCreateJoinToken(
        room=example_room,
        bbb_join_parameters=default_bbb_join_parameters,
        meeting=example_meeting,
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("value", "expectation_exception"),
    [
        (False, TypeError),
        (True, TypeError),
        ("00000000-0000-0000-0000-000000000000", Meeting.DoesNotExist),
        ("", ValueError),
        ("abc", ValueError),
        (-123, TypeError),
        (0, TypeError),
        (123, TypeError),
        (list(), TypeError),
        ([1, 2, 3], TypeError),
        (dict(), TypeError),
        ({"a": 1, "b": 2, "c": 3}, TypeError),
    ],
)
def test_create_join_token__from_json_token__invalid_meeting_id(
    example_room: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    value: Any,
    expectation_exception: Type[Exception],
) -> None:
    with pytest.raises(expectation_exception):
        CreateJoinToken.from_json_token(
            {
                "roomId": example_room.id,
                "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
                "meetingId": value,
            }
        )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_json_token__is_creator(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert MockedCreateJoinToken.from_json_token(
        {
            "roomId": example_room.id,
            "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
            "isCreator": value,
        }
    ) == MockedCreateJoinToken(
        room=example_room,
        bbb_join_parameters=default_bbb_join_parameters,
        is_creator=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", ["", "abc", -123, 0, 123, list(), [1, 2, 3], dict(), {"a": 1, "b": 2, "c": 3}])
def test_create_join_token__from_json_token__invalid_is_creator(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: Any
) -> None:
    with pytest.raises(TypeError):
        CreateJoinToken.from_json_token(
            {
                "roomId": example_room.id,
                "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
                "isCreator": value,
            }
        )


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__from_json_token__access_granted(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert MockedCreateJoinToken.from_json_token(
        {
            "roomId": example_room.id,
            "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
            "accessGranted": value,
        }
    ) == MockedCreateJoinToken(
        room=example_room,
        bbb_join_parameters=default_bbb_join_parameters,
        access_granted=value,
    )


@pytest.mark.django_db
@pytest.mark.parametrize("value", ["", "abc", -123, 0, 123, list(), [1, 2, 3], dict(), {"a": 1, "b": 2, "c": 3}])
def test_create_join_token__from_json_token__invalid_access_granted(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: Any
) -> None:
    with pytest.raises(TypeError):
        CreateJoinToken.from_json_token(
            {
                "roomId": example_room.id,
                "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
                "accessGranted": value,
            }
        )


@pytest.mark.django_db
def test_create_join_token__serialize__room(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters
) -> None:
    assert CreateJoinToken(example_room, default_bbb_join_parameters).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": False,
        "enforceCreateMeetingPermission": False,
        "enforceJoinMeetingPermission": False,
        "enforceModeratePermission": False,
        "joinAsRoom": False,
        "optOutCreateMeeting": False,
        "meetingId": None,
        "isCreator": False,
        "accessGranted": False,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__serialize__skip_meeting_configuration(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert CreateJoinToken(example_room, default_bbb_join_parameters, skip_meeting_configuration=value).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": value,
        "enforceCreateMeetingPermission": False,
        "enforceJoinMeetingPermission": False,
        "enforceModeratePermission": False,
        "joinAsRoom": False,
        "optOutCreateMeeting": False,
        "meetingId": None,
        "isCreator": False,
        "accessGranted": False,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__serialize__enforce_create_meeting_permission(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert CreateJoinToken(
        example_room, default_bbb_join_parameters, enforce_create_meeting_permission=value
    ).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": False,
        "enforceCreateMeetingPermission": value,
        "enforceJoinMeetingPermission": False,
        "enforceModeratePermission": False,
        "joinAsRoom": False,
        "optOutCreateMeeting": False,
        "meetingId": None,
        "isCreator": False,
        "accessGranted": False,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__serialize__enforce_join_meeting_permission(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert CreateJoinToken(
        example_room, default_bbb_join_parameters, enforce_join_meeting_permission=value
    ).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": False,
        "enforceCreateMeetingPermission": False,
        "enforceJoinMeetingPermission": value,
        "enforceModeratePermission": False,
        "joinAsRoom": False,
        "optOutCreateMeeting": False,
        "meetingId": None,
        "isCreator": False,
        "accessGranted": False,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__serialize__enforce_moderate_permission(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert CreateJoinToken(
        example_room, default_bbb_join_parameters, enforce_moderate_permission=value
    ).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": False,
        "enforceCreateMeetingPermission": False,
        "enforceJoinMeetingPermission": False,
        "enforceModeratePermission": value,
        "joinAsRoom": False,
        "optOutCreateMeeting": False,
        "meetingId": None,
        "isCreator": False,
        "accessGranted": False,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__serialize__join_as_room(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert CreateJoinToken(example_room, default_bbb_join_parameters, join_as_room=value).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": False,
        "enforceCreateMeetingPermission": False,
        "enforceJoinMeetingPermission": False,
        "enforceModeratePermission": False,
        "joinAsRoom": value,
        "optOutCreateMeeting": False,
        "meetingId": None,
        "isCreator": False,
        "accessGranted": False,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__serialize__opt_out_create_meeting(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert CreateJoinToken(example_room, default_bbb_join_parameters, opt_out_create_meeting=value).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": False,
        "enforceCreateMeetingPermission": False,
        "enforceJoinMeetingPermission": False,
        "enforceModeratePermission": False,
        "joinAsRoom": False,
        "optOutCreateMeeting": value,
        "meetingId": None,
        "isCreator": False,
        "accessGranted": False,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("value", [None, "example_meeting"])
def test_create_join_token__serialize__meeting(
    request: pytest.FixtureRequest,
    example_room: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    value: Optional[str],
) -> None:
    meeting = request.getfixturevalue(value) if value is not None else None
    meeting_id = str(meeting.id) if value is not None else None

    assert CreateJoinToken(example_room, default_bbb_join_parameters, meeting=meeting).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": False,
        "enforceCreateMeetingPermission": False,
        "enforceJoinMeetingPermission": False,
        "enforceModeratePermission": False,
        "joinAsRoom": False,
        "optOutCreateMeeting": False,
        "meetingId": meeting_id,
        "isCreator": False,
        "accessGranted": False,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__serialize__is_creator(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert CreateJoinToken(example_room, default_bbb_join_parameters, is_creator=value).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": False,
        "enforceCreateMeetingPermission": False,
        "enforceJoinMeetingPermission": False,
        "enforceModeratePermission": False,
        "joinAsRoom": False,
        "optOutCreateMeeting": False,
        "meetingId": None,
        "isCreator": value,
        "accessGranted": False,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("value", [False, True])
def test_create_join_token__serialize__access_granted(
    example_room: Room, default_bbb_join_parameters: bigbluebutton.JoinParameters, value: bool
) -> None:
    assert CreateJoinToken(example_room, default_bbb_join_parameters, access_granted=value).serialize() == {
        "roomId": example_room.id,
        "bbbJoinParameters": _serialize_bbb_join_parameters(default_bbb_join_parameters),
        "skipMeetingConfiguration": False,
        "enforceCreateMeetingPermission": False,
        "enforceJoinMeetingPermission": False,
        "enforceModeratePermission": False,
        "joinAsRoom": False,
        "optOutCreateMeeting": False,
        "meetingId": None,
        "isCreator": False,
        "accessGranted": value,
    }


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("enforce_create_meeting_permission", "room", "user", "tenant", "has_tenant_based_perm", "expectation"),
    (
        itertools.chain(
            # enforce create meeting permission -> True
            itertools.product(
                [True],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user", "example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
            ),
            # everyone can start -> True
            itertools.product(
                [False],
                ["inactive_room__everyone_can_start"],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user", "example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
            ),
            # not everyone can start and anonymous user -> False
            itertools.product(
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                ],
                ["anonymous_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [False],
            ),
            # superuser -> True
            itertools.product(
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                ],
                ["example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
            ),
            # homeroom and owner -> True
            itertools.product(
                [False],
                ["homeroom__example_user"],
                ["example_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
            ),
            # homeroom and not owner -> False
            itertools.product(
                [False],
                ["homeroom__example_user"],
                ["example_moderator_user", "example_staff_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [False],
            ),
            # personal room and (co-)owner -> True
            itertools.product(
                [False],
                ["personal_room__with_co_owner"],
                ["example_moderator_user", "example_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
            ),
            # personal room and not (co-)owner -> False
            itertools.product(
                [False],
                ["personal_room__with_co_owner"],
                ["example_staff_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [False],
            ),
            # authenticated users can start -> True
            itertools.product(
                [False],
                [
                    "inactive_room__authenticated_users_can_start",
                ],
                ["example_user", "example_moderator_user", "example_staff_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
            ),
            # create meeting permission required and has tenant based perm -> True
            itertools.product(
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                ],
                ["example_user", "example_moderator_user", "example_staff_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [True],
                [True],
            ),
            # create meeting permission required and has not tenant based perm -> True
            itertools.product(
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                ],
                ["example_user", "example_moderator_user", "example_staff_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [False],
                [False],
            ),
            # room's tenant not equal to user's tenant -> False
            itertools.product(
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                ],
                ["user__testserver2_tenant"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [False],
            ),
        )
    ),
)
def test_can_user_create_meeting(
    request: pytest.FixtureRequest,
    mocker: MockerFixture,
    enforce_create_meeting_permission: bool,
    room: str,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    user: str,
    tenant: str,
    has_tenant_based_perm: Optional[bool],
    expectation: bool,
) -> None:
    has_tenant_based_perm_mock = mocker.patch.object(
        join_or_create, "has_tenant_based_perm", return_value=has_tenant_based_perm
    )
    token = CreateJoinToken(
        request.getfixturevalue(room),
        default_bbb_join_parameters,
        enforce_create_meeting_permission=enforce_create_meeting_permission,
    )
    _user = request.getfixturevalue(user)
    _tenant = request.getfixturevalue(tenant)

    assert can_user_create_meeting(token, _tenant, _user) is expectation

    if has_tenant_based_perm is None:
        has_tenant_based_perm_mock.assert_not_called()
    else:
        has_tenant_based_perm_mock.assert_called_once_with(_user, "core.create_meeting", _tenant)


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("enforce_join_meeting_permission", "is_creator", "room", "user", "tenant", "has_tenant_based_perm", "expectation"),
    (
        itertools.chain(
            # enforce join meeting permission -> True
            itertools.product(
                [True],
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user", "example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
            ),
            # is creator -> True
            itertools.product(
                [False],
                [True],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user", "example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
            ),
            # has tenant based perm -> True
            itertools.product(
                [False],
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user", "example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [True],
                [True],
            ),
            # has no tenant based perm -> False
            itertools.product(
                [False],
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user", "example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [False],
                [False],
            ),
        )
    ),
)
def test_can_user_skip_join_preconditions(
    request: pytest.FixtureRequest,
    mocker: MockerFixture,
    enforce_join_meeting_permission: bool,
    is_creator: bool,
    room: str,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    user: str,
    tenant: str,
    has_tenant_based_perm: Optional[bool],
    expectation: bool,
) -> None:
    has_tenant_based_perm_mock = mocker.patch.object(
        join_or_create, "has_tenant_based_perm", return_value=has_tenant_based_perm
    )
    token = CreateJoinToken(
        request.getfixturevalue(room),
        default_bbb_join_parameters,
        enforce_join_meeting_permission=enforce_join_meeting_permission,
        is_creator=is_creator,
    )
    _user = request.getfixturevalue(user)
    _tenant = request.getfixturevalue(tenant)

    assert can_user_skip_join_preconditions(token, _tenant, _user) is expectation

    if has_tenant_based_perm is None:
        has_tenant_based_perm_mock.assert_not_called()
    else:
        has_tenant_based_perm_mock.assert_called_once_with(_user, "core.join_meeting", _tenant)


@pytest.mark.django_db
@pytest.mark.parametrize(
    (
        "enforce_moderate_permission",
        "is_creator",
        "room",
        "meeting",
        "user",
        "tenant",
        "has_tenant_based_perm",
        "expectation",
        "expect_error",
    ),
    (
        itertools.chain(
            # enforce moderate permission -> True
            itertools.product(
                [True],
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                [
                    "running_meeting__moderation_mode_starter",
                    "running_meeting__moderation_mode_moderators",
                    "running_meeting__moderation_mode_all",
                ],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user", "example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
                [False],
            ),
            # is creator -> True
            itertools.product(
                [False],
                [True],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                [
                    "running_meeting__moderation_mode_starter",
                    "running_meeting__moderation_mode_moderators",
                    "running_meeting__moderation_mode_all",
                ],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user", "example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
                [False],
            ),
            # superuser -> True
            itertools.product(
                [False],
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                [
                    "running_meeting__moderation_mode_starter",
                    "running_meeting__moderation_mode_moderators",
                    "running_meeting__moderation_mode_all",
                ],
                ["example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
                [False],
            ),
            # moderation mode all -> True
            itertools.product(
                [False],
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                ["running_meeting__moderation_mode_all"],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [True],
                [False],
            ),
            # moderation mode moderators and has tenant based perm -> True
            itertools.product(
                [False],
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                ["running_meeting__moderation_mode_moderators"],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [True],
                [True],
                [False],
            ),
            # moderation mode moderators and has not tenant based perm -> False
            itertools.product(
                [False],
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                ["running_meeting__moderation_mode_moderators"],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [False],
                [False],
                [False],
            ),
            # moderation mode starter and not creator -> False
            itertools.product(
                [False],
                [False],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                ["running_meeting__moderation_mode_starter"],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [False],
                [False],
            ),
            # no meeting or no meeting configuration -> (silent) error
            itertools.product(
                [False, True],
                [False, True],
                [
                    "inactive_room__create_meeting_permission_required",
                    "inactive_room__authenticated_users_can_start",
                    "inactive_room__everyone_can_start",
                ],
                [None, "running_meeting__without_configuration"],
                ["anonymous_user", "example_user", "example_moderator_user", "example_staff_user", "example_superuser"],
                ["testserver_tenant", "testserver2_tenant"],
                [None],
                [False],
                [True],
            ),
        )
    ),
)
def test_is_user_moderator(
    request: pytest.FixtureRequest,
    mocker: MockerFixture,
    enforce_moderate_permission: bool,
    is_creator: bool,
    room: str,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    meeting: Optional[str],
    user: str,
    tenant: str,
    has_tenant_based_perm: Optional[bool],
    expectation: bool,
    expect_error: bool,
) -> None:
    has_tenant_based_perm_mock = mocker.patch.object(
        join_or_create, "has_tenant_based_perm", return_value=has_tenant_based_perm
    )
    logger_error_mock = mocker.patch.object(join_or_create.logger, "error")
    _room = request.getfixturevalue(room)
    _meeting = request.getfixturevalue(meeting) if meeting is not None else None
    token = CreateJoinToken(
        _room,
        default_bbb_join_parameters,
        enforce_moderate_permission=enforce_moderate_permission,
        meeting=_meeting,
        is_creator=is_creator,
    )
    _user = request.getfixturevalue(user)
    _tenant = request.getfixturevalue(tenant)

    assert is_user_moderator(token, _tenant, _user) is expectation

    if has_tenant_based_perm is None:
        has_tenant_based_perm_mock.assert_not_called()
    else:
        has_tenant_based_perm_mock.assert_called_once_with(_user, "core.moderate", _tenant)

    if expect_error:
        logger_error_mock.assert_called_once()
        args = logger_error_mock.call_args[0]
        assert f"room.id={_room.id}" in args[0]

        if _meeting is None:
            assert "meeting.id" not in args[0]
        else:
            assert f"meeting.id={_meeting.id}" in args[0]
    else:
        logger_error_mock.assert_not_called()


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("creator", "meeting_configuration"),
    itertools.product(["anonymous_user", "example_user"], [None, "example_meeting_configuration"]),
)
def test_create_meeting(
    request: pytest.FixtureRequest,
    mocker: MockerFixture,
    inactive_room__everyone_can_start: Room,
    testserver_tenant: Site,
    creator: str,
    meeting_configuration: Optional[str],
) -> None:
    _creator = request.getfixturevalue(creator)
    _meeting_configuration = (
        request.getfixturevalue(meeting_configuration) if meeting_configuration is not None else None
    )
    meeting_create_spy = mocker.spy(Meeting.objects, "create")
    default_meeting_configuration_instantiate_spy = mocker.spy(
        inactive_room__everyone_can_start.default_meeting_configuration, "instantiate"
    )
    frozen_time = datetime.fromisoformat("2020-06-17T01:03:00+02:00")

    with freeze_time(frozen_time):
        assert (
            create_meeting(
                inactive_room__everyone_can_start, testserver_tenant, _creator, configuration=_meeting_configuration
            )
            is meeting_create_spy.spy_return
        )

    expected_creator = _creator if _creator.is_authenticated else None
    expected_creator_name = str(_creator) if _creator.is_authenticated else ""
    expected_meeting_configuration = _meeting_configuration

    if meeting_configuration is None:
        default_meeting_configuration_instantiate_spy.assert_called_once_with()
        expected_meeting_configuration = default_meeting_configuration_instantiate_spy.spy_return

    meeting_create_spy.assert_called_once_with(
        room=inactive_room__everyone_can_start,
        room_name=inactive_room__everyone_can_start.name,
        configuration=expected_meeting_configuration,
        creator=expected_creator,
        creator_name=expected_creator_name,
        state=MEETING_STATE_CREATING,
    )

    inactive_room__everyone_can_start.refresh_from_db()
    assert inactive_room__everyone_can_start.state == ROOM_STATE_CREATING
    assert inactive_room__everyone_can_start.last_running == frozen_time


@pytest.mark.django_db
def test_get_next_action__create_only_with_room_as_authenticated_user(
    mocker: MockerFixture,
    testserver_tenant: Site,
    inactive_room__everyone_can_start: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    example_user: User,
) -> None:
    token = MockedCreateJoinToken(inactive_room__everyone_can_start, default_bbb_join_parameters)

    with assert_called(
        mocker,
        can_user_create_meeting=True,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, example_user)

    assert isinstance(action, ConfigureMeetingAction)
    assert action._token == MockedCreateJoinToken(inactive_room__everyone_can_start, default_bbb_join_parameters)
    assert isinstance(action._form, ConfigureMeetingForm)


@pytest.mark.django_db
def test_get_next_action__create_only_with_room_as_anonymous_user__enforce_create_meeting_permission(
    mocker: MockerFixture,
    testserver_tenant: Site,
    inactive_room__authenticated_users_can_start: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    token = MockedCreateJoinToken(
        inactive_room__authenticated_users_can_start,
        default_bbb_join_parameters,
        enforce_create_meeting_permission=True,
    )

    with assert_called(
        mocker,
        can_user_create_meeting=True,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, ConfigureMeetingAction)
    assert action._token == MockedCreateJoinToken(
        inactive_room__authenticated_users_can_start,
        default_bbb_join_parameters,
        enforce_create_meeting_permission=True,
    )
    assert isinstance(action._form, ConfigureMeetingForm)


@pytest.mark.django_db
def test_get_next_action__join_only_with_room_in_create_state(
    mocker: MockerFixture,
    testserver_tenant: Site,
    creating_room_with_meeting_and_no_creator: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    example_user: User,
) -> None:
    room, meeting = creating_room_with_meeting_and_no_creator
    token = MockedCreateJoinToken(room, default_bbb_join_parameters)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, example_user)

    assert isinstance(action, WaitUntilRunningAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)
    assert action.should_create_meeting is False


@pytest.mark.django_db
def test_get_next_action__join_with_room_and_meeting_in_create_state(
    mocker: MockerFixture,
    testserver_tenant: Site,
    creating_room_with_meeting_and_no_creator: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    example_user: User,
) -> None:
    room, meeting = creating_room_with_meeting_and_no_creator
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, example_user)

    assert isinstance(action, WaitUntilRunningAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)
    assert action.should_create_meeting is False


@pytest.mark.django_db
def test_get_next_action__join_finished_meeting(
    mocker: MockerFixture,
    testserver_tenant: Site,
    inactive_room_with_finished_meeting: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    example_user: User,
) -> None:
    room, meeting = inactive_room_with_finished_meeting
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, example_user)

    assert isinstance(action, DisplayMeetingEndedAction)


@pytest.mark.django_db
def test_get_next_action__create_with_skip_configuration(
    mocker: MockerFixture,
    testserver_tenant: Site,
    inactive_room__everyone_can_start: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    example_user: User,
) -> None:
    token = MockedCreateJoinToken(
        inactive_room__everyone_can_start, default_bbb_join_parameters, skip_meeting_configuration=True
    )
    create_meeting_spy = mocker.spy(join_or_create, "create_meeting")

    with assert_called(
        mocker,
        can_user_create_meeting=True,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, example_user)

    create_meeting_spy.assert_called_once_with(inactive_room__everyone_can_start, testserver_tenant, example_user)
    assert isinstance(action, WaitUntilRunningAction)
    assert action._token == MockedCreateJoinToken(
        inactive_room__everyone_can_start,
        default_bbb_join_parameters,
        skip_meeting_configuration=True,
        meeting=create_meeting_spy.spy_return,
        is_creator=True,
    )
    assert action.should_create_meeting is True


@pytest.mark.django_db
def test_get_next_action__join_only_with_room_in_create_state_as_creator(
    mocker: MockerFixture,
    testserver_tenant: Site,
    creating_room_with_meeting_and_creator: Tuple[Room, Meeting, User],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting, user = creating_room_with_meeting_and_creator
    token = MockedCreateJoinToken(room, default_bbb_join_parameters)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, user)

    assert isinstance(action, WaitUntilRunningAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=True)
    assert action.should_create_meeting is False


@pytest.mark.django_db
def test_get_next_action__join_with_room_and_meeting_in_create_state_as_creator(
    mocker: MockerFixture,
    testserver_tenant: Site,
    creating_room_with_meeting_and_creator: Tuple[Room, Meeting, User],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting, user = creating_room_with_meeting_and_creator
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=True)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, user)

    assert isinstance(action, WaitUntilRunningAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=True)
    assert action.should_create_meeting is False


@pytest.mark.django_db
def test_get_next_action__join_only_with_room_in_inactive_state(
    mocker: MockerFixture,
    testserver_tenant: Site,
    inactive_room__authenticated_users_can_start: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    token = MockedCreateJoinToken(inactive_room__authenticated_users_can_start, default_bbb_join_parameters)

    with assert_called(
        mocker,
        can_user_create_meeting=True,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, WaitUntilConfiguredAction)
    assert action._token == MockedCreateJoinToken(
        inactive_room__authenticated_users_can_start, default_bbb_join_parameters
    )


@pytest.mark.django_db
def test_get_next_action__join_only_with_room_in_inactive_state_everyone_can_start__opt_out_create_meeting(
    mocker: MockerFixture,
    testserver_tenant: Site,
    inactive_room__everyone_can_start: Room,
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    example_user: User,
) -> None:
    token = MockedCreateJoinToken(
        inactive_room__everyone_can_start, default_bbb_join_parameters, opt_out_create_meeting=True
    )

    with assert_called(
        mocker,
        can_user_create_meeting=True,
        can_user_skip_join_preconditions=False,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, example_user)

    assert isinstance(action, WaitUntilConfiguredAction)
    assert action._token == MockedCreateJoinToken(
        inactive_room__everyone_can_start, default_bbb_join_parameters, opt_out_create_meeting=True
    )


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_without_allow_guests_as_anonymous_user(
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__dont_allow_guests: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__dont_allow_guests
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=True,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, PromptLogInAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_without_allow_guests_and_with_access_code_as_authenticated_user(
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    example_user: User,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=True,
        _choice_has_access_code=True,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, example_user)

    assert isinstance(action, PromptAccessCodeAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_with_allow_guests_and_with_access_code_as_anonymous_user(
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__allow_guests__needs_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__allow_guests__needs_access_code
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=True,
        _choice_has_access_code=True,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, PromptAccessCodeAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_with_allow_guests_and_with_guest_only_access_code_as_anonymous_user(
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__allow_guests__needs_guest_only_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__allow_guests__needs_guest_only_access_code
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=True,
        _choice_has_access_code=True,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, PromptAccessCodeAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_with_allow_guests_and_without_access_code_as_anonymous_user(
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__allow_guests__no_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__allow_guests__no_access_code
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=True,
        _choice_has_access_code=True,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, PromptJoinNameAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_with_allow_guests_and_with_access_code_as_anonymous_user__access_granted(  # noqa: E501
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__allow_guests__needs_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__allow_guests__needs_access_code
    token = MockedCreateJoinToken(
        room, default_bbb_join_parameters, meeting=meeting, is_creator=False, access_granted=True
    )

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=True,
        _choice_has_access_code=True,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, PromptJoinNameAction)
    assert action._token == MockedCreateJoinToken(
        room, default_bbb_join_parameters, meeting=meeting, is_creator=False, access_granted=True
    )


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_without_allow_guests_and_with_access_code_as_anonymous_creator(
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=True)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, PromptJoinNameAction)
    assert action._token == MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=True)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_with_allow_guests_and_with_access_code_as_anonymous_user__enforce_join_meeting_permission(  # noqa: E501
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__allow_guests__needs_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__allow_guests__needs_access_code
    token = MockedCreateJoinToken(
        room, default_bbb_join_parameters, meeting=meeting, is_creator=False, enforce_join_meeting_permission=True
    )

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=False,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, PromptJoinNameAction)
    assert action._token == MockedCreateJoinToken(
        room, default_bbb_join_parameters, meeting=meeting, is_creator=False, enforce_join_meeting_permission=True
    )


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_with_allow_guests_and_with_guest_only_access_code_as_authenticated_user(  # noqa: E501
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__allow_guests__needs_guest_only_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    example_user: User,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__allow_guests__needs_guest_only_access_code
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=True,
        _choice_has_access_code=True,
        is_user_moderator=True,
    ):
        action = get_next_action(token, testserver_tenant, example_user)

    assert isinstance(action, RedirectAction)
    assert action._meeting == meeting
    assert action._bbb_join_parameters == default_bbb_join_parameters
    assert action._is_moderator is False
    assert action._join_name == str(example_user)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_without_allow_guests_and_with_access_code_as_user_with_join_meeting_permission(  # noqa: E501
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    user_with_join_meeting_permission: User,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=True,
    ):
        action = get_next_action(token, testserver_tenant, user_with_join_meeting_permission)

    assert isinstance(action, RedirectAction)
    assert action._meeting == meeting
    assert action._bbb_join_parameters == default_bbb_join_parameters
    assert action._is_moderator is False
    assert action._join_name == str(user_with_join_meeting_permission)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_without_allow_guests_and_with_access_code_as_user_with_join_meeting_and_moderate_permission(  # noqa: E501
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    user_with_join_meeting_and_moderate_permissions: User,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=False)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=True,
    ):
        action = get_next_action(token, testserver_tenant, user_with_join_meeting_and_moderate_permissions)

    assert isinstance(action, RedirectAction)
    assert action._meeting == meeting
    assert action._bbb_join_parameters == default_bbb_join_parameters
    assert action._is_moderator is True
    assert action._join_name == str(user_with_join_meeting_and_moderate_permissions)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_without_allow_guests_and_with_access_code_as_creator(
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_creator__dont_allow_guests__needs_access_code: Tuple[Room, Meeting, User],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting, user = active_room_with_meeting_and_creator__dont_allow_guests__needs_access_code
    token = MockedCreateJoinToken(room, default_bbb_join_parameters, meeting=meeting, is_creator=True)

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=True,
    ):
        action = get_next_action(token, testserver_tenant, user)

    assert isinstance(action, RedirectAction)
    assert action._meeting == meeting
    assert action._bbb_join_parameters == default_bbb_join_parameters
    assert action._is_moderator is True
    assert action._join_name == str(user)


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_without_allow_guests_and_with_access_code_as_user_with_join_meeting_permission__join_as_room(  # noqa: E501
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    user_with_join_meeting_permission: User,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code
    token = MockedCreateJoinToken(
        room, default_bbb_join_parameters, meeting=meeting, is_creator=False, join_as_room=True
    )

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=True,
    ):
        action = get_next_action(token, testserver_tenant, user_with_join_meeting_permission)

    assert isinstance(action, RedirectAction)
    assert action._meeting == meeting
    assert action._bbb_join_parameters == default_bbb_join_parameters
    assert action._is_moderator is False
    assert action._join_name == token.room.name


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_without_allow_guests_and_with_access_code_as_user_with_join_meeting_and_moderate_permissions__join_as_room(  # noqa: E501
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
    user_with_join_meeting_and_moderate_permissions: User,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__dont_allow_guests__needs_access_code
    token = MockedCreateJoinToken(
        room, default_bbb_join_parameters, meeting=meeting, is_creator=False, join_as_room=True
    )

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=False,
        _choice_has_access_code=False,
        is_user_moderator=True,
    ):
        action = get_next_action(token, testserver_tenant, user_with_join_meeting_and_moderate_permissions)

    assert isinstance(action, RedirectAction)
    assert action._meeting == meeting
    assert action._bbb_join_parameters == default_bbb_join_parameters
    assert action._is_moderator is True
    assert action._join_name == token.room.name


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_with_allow_guests_and_without_access_code_as_anonymous_user__join_as_room(  # noqa: E501
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__allow_guests__no_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__allow_guests__no_access_code
    token = MockedCreateJoinToken(
        room, default_bbb_join_parameters, meeting=meeting, is_creator=False, join_as_room=True
    )

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=True,
        _choice_has_access_code=True,
        is_user_moderator=True,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, RedirectAction)
    assert action._meeting == meeting
    assert action._bbb_join_parameters == default_bbb_join_parameters
    assert action._is_moderator is False
    assert action._join_name == token.room.name


@pytest.mark.django_db
def test_get_next_action__join_room_in_active_state_with_allow_guests_and_without_access_code_as_anonymous_user__enforce_moderate_permission__join_as_room(  # noqa: E501
    mocker: MockerFixture,
    testserver_tenant: Site,
    active_room_with_meeting_and_no_creator__allow_guests__no_access_code: Tuple[Room, Meeting],
    default_bbb_join_parameters: bigbluebutton.JoinParameters,
) -> None:
    room, meeting = active_room_with_meeting_and_no_creator__allow_guests__no_access_code
    token = MockedCreateJoinToken(
        room,
        default_bbb_join_parameters,
        meeting=meeting,
        is_creator=False,
        enforce_moderate_permission=True,
        join_as_room=True,
    )

    with assert_called(
        mocker,
        can_user_create_meeting=False,
        can_user_skip_join_preconditions=True,
        _choice_allow_guests=True,
        _choice_has_access_code=True,
        is_user_moderator=True,
    ):
        action = get_next_action(token, testserver_tenant, AnonymousUser())

    assert isinstance(action, RedirectAction)
    assert action._meeting == meeting
    assert action._bbb_join_parameters == default_bbb_join_parameters
    assert action._is_moderator is True
    assert action._join_name == token.room.name
