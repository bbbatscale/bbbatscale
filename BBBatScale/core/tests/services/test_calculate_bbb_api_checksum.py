from core.services import calculate_bbb_api_checksum


def test_calculate_bbb_api_checksum() -> None:
    assert (
        calculate_bbb_api_checksum(
            "create", (("param1", "true"), ("param2", "false"), ("param3", "abc123")), "Super-Secret"
        )
        == "c7ed297a3ff9c219fa29b3f7614f340e7c3eb55a"
    )
