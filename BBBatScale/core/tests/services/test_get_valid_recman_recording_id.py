import pytest
from core.services import get_valid_recman_recording_id


@pytest.mark.parametrize(
    "record_id, valid_recman_id",
    [
        (
            "b9495833fb6cb85a71860ec03a1554526ee25ad30e729776",
            "recordings/b9495833fb6cb85a71860ec03a1554526ee25ad30e729776",
        ),
        ("", "recordings/"),
        ("recordings--", "recordings/recordings--"),
    ],
)
def test_get_valid_recman_recording_id(record_id, valid_recman_id):
    assert get_valid_recman_recording_id(record_id) == valid_recman_id
