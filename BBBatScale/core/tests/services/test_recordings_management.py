from unittest.mock import patch

import grpc
import pytest
from core.models import Meeting
from core.services import (
    delete_recordings,
    get_additional_recording_types,
    get_meetings_for_record_ids,
    get_recording_details,
    update_publish_recordings,
)
from django.core.exceptions import ObjectDoesNotExist
from xmltodict import parse


class MockRPC:
    meta_dict = None

    def delete_recording(self, recording_id):
        pass

    def update_recording(self, recording_id, metadata):
        self.meta_dict = parse(metadata.decode())
        return True

    def get_recording(self, recording_id: str):
        class Recording:
            recording_id = "EXAMPLERECORDID"
            available_formats = ["podcast", "shreenshare"]

        return Recording()


@pytest.fixture(scope="function")
def rpc(db) -> MockRPC:
    return MockRPC()


@pytest.fixture(scope="function")
def rpc_grpc_error(db):
    class MockRPCError(MockRPC):
        def delete_recording(self, recording_id):
            raise grpc.RpcError()

        def update_recording(self, recording_id, metadata):
            raise grpc.RpcError()

        def get_recording(self, recording_id: str):
            raise grpc.RpcError()

    return MockRPCError()


@pytest.fixture(scope="function")
def rpc_any_error(db):
    class MockGenericError(MockRPC):
        def delete_recording(self, recording_id):
            raise Exception

        def update_recording(self, recording_id, metadata):
            raise Exception

        def get_recording(self, recording_id: str):
            raise Exception

    return MockGenericError()


@pytest.mark.django_db
def test_get_recording_details(rpc):
    record = get_recording_details(rpc, "recordings--EXAMPLERECORDID")
    assert record.recording_id == "EXAMPLERECORDID"


@pytest.mark.django_db
def test_get_recording_details_grpc_exception(rpc_grpc_error):
    with pytest.raises(grpc.RpcError) as exc_info:
        get_recording_details(rpc_grpc_error, "recordings--EXAMPLERECORDID")
        assert exc_info.type == grpc.RpcError


@pytest.mark.django_db
def test_get_recording_details_generic_exception(rpc_any_error):
    with pytest.raises(Exception) as exc_info:
        get_recording_details(rpc_any_error, "recordings--EXAMPLERECORDID")
        assert exc_info.type == Exception


@pytest.mark.django_db
def test_get_additional_recording_types(rpc):
    available_formats = get_additional_recording_types(rpc, "recordings--EXAMPLERECORDID")
    assert available_formats == ["podcast", "shreenshare"]


@pytest.mark.django_db
def test_get_additional_recording_types_grpc_exception(rpc_grpc_error):
    with pytest.raises(grpc.RpcError) as exc_info:
        get_additional_recording_types(rpc_grpc_error, "recordings--EXAMPLERECORDID")
        assert exc_info.type == grpc.RpcError


@pytest.mark.django_db
def test_get_additional_recording_types_generic_exception(rpc_any_error):
    with pytest.raises(Exception) as exc_info:
        get_additional_recording_types(rpc_any_error, "recordings--EXAMPLERECORDID")
        assert exc_info.type == Exception


@pytest.mark.django_db
def test_delete_recordings_no_meetings(rpc):
    with pytest.raises(ObjectDoesNotExist) as exc_info:
        delete_recordings("EXAMPLE_ID", rpc)
        assert exc_info.type == ObjectDoesNotExist


@pytest.mark.django_db
def test_delete_recordings_grpc_exception(rpc_grpc_error):
    Meeting.objects.create(replay_id="TESTID")
    with pytest.raises(grpc.RpcError) as exc_info:
        delete_recordings("TESTID", rpc_grpc_error)
        assert exc_info.type == grpc.RpcError


@pytest.mark.django_db
def test_delete_recordings_generic_exception(rpc_any_error):
    Meeting.objects.create(replay_id="TESTID")
    with pytest.raises(Exception) as exc_info:
        delete_recordings("TESTID", rpc_any_error)
        assert exc_info.type == Exception


@pytest.mark.django_db
def test_delete_recordings_happy_path(rpc):
    Meeting.objects.create(replay_id="TESTID")
    Meeting.objects.create(replay_id="TESTID2")
    Meeting.objects.create(replay_id="TESTID3")
    Meeting.objects.create(replay_id="TESTID4")
    Meeting.objects.create(replay_id="TESTID5")

    assert Meeting.objects.exclude(replay_id="").count() == 5
    delete_recordings("TESTID,TESTID2", rpc)
    assert Meeting.objects.exclude(replay_id="").count() == 3


@pytest.mark.django_db
def test_update_publish_recordings_no_meetings(rpc):
    with pytest.raises(ObjectDoesNotExist) as exc_info:
        update_publish_recordings({"recordID": "EXAMPLE_ID"}, rpc, True)
        assert exc_info.type == ObjectDoesNotExist

    with pytest.raises(ObjectDoesNotExist) as exc_info:
        update_publish_recordings({"recordID": "EXAMPLE_ID"}, rpc, False)
        assert exc_info.type == ObjectDoesNotExist


@pytest.mark.django_db
def test_update_publish_recordings_grpc_exception(rpc_grpc_error):
    with patch.object(Meeting, "get_recordings_xml") as mocked_get_recordings_xml:

        class Response:
            text = """
                <recording>
                  <id>dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061</id>
                  <state>published</state>
                  <published>true</published>
                  <start_time>1634057825061</start_time>
                  <end_time>1634059335155</end_time>
                  <participants>3</participants>
                  <meeting id="dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
                  externalId="00563b64075043708825c10d67859721" name="Example Meeting" breakout="false"/>
                  <meta>
                    <creator>ExampleUser</creator>
                    <meetingId>00563b64075043708825c10d67859721</meetingId>
                    <roomsmeetingid>92654</roomsmeetingid>
                    <streamingurl>None</streamingurl>
                  </meta>
                </recording>
                """

        mocked_get_recordings_xml.return_value = Response()
        Meeting.objects.create(replay_id="TESTID")
        with pytest.raises(grpc.RpcError) as exc_info:
            update_publish_recordings({"recordID": "TESTID"}, rpc_grpc_error, True)
            assert exc_info.type == grpc.RpcError

        with pytest.raises(grpc.RpcError) as exc_info:
            update_publish_recordings({"recordID": "TESTID", "publish": "true"}, rpc_grpc_error, False)
            assert exc_info.type == grpc.RpcError


@pytest.mark.django_db
def test_update_publish_recordings_generic_exception():
    Meeting.objects.create(replay_id="TESTID")
    with pytest.raises(Exception) as exc_info:
        update_publish_recordings({"recordID": "TESTID"}, rpc_any_error, True)
        assert exc_info.type == Exception

    with pytest.raises(Exception) as exc_info:
        update_publish_recordings({"recordID": "TESTID", "publish": "false"}, rpc_any_error, False)
        assert exc_info.type == Exception


@pytest.mark.django_db
def test_update_publish_recordings_happy_path(rpc, mocker):
    def mock_get_meetings_for_record_ids(record_id):
        class MockMeeting:
            def __init__(self):
                self.replay_id = "TESTID"

            def get_recordings_xml(self):
                class Response:
                    text = """
                    <recording>
                      <id>dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061</id>
                      <state>published</state>
                      <published>false</published>
                      <start_time>1634057825061</start_time>
                      <end_time>1634059335155</end_time>
                      <participants>3</participants>
                      <meeting id="dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
                      externalId="00563b64075043708825c10d67859721" name="Example Meeting" breakout="false"/>
                      <meta>
                        <creator>ExampleUser</creator>
                        <meetingId>00563b64075043708825c10d67859721</meetingId>
                        <roomsmeetingid>92654</roomsmeetingid>
                        <streamingurl>None</streamingurl>
                      </meta>
                    </recording>
                    """

                return Response()

        return [MockMeeting()]

    mocker.patch("core.services.get_meetings_for_record_ids", mock_get_meetings_for_record_ids)

    Meeting.objects.create(replay_id="TESTID")
    params = {
        "recordID": "TESTID",
        "meta_bbb-meeting-name": "Test Meeting Name",
        "meta_bbb-meeting-creator": "Example User",
        "publish": "true",
    }
    update_publish_recordings(params, rpc, True)
    # Values that need to be untouched by updating process
    assert rpc.meta_dict["recording"]["id"] == "dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
    assert rpc.meta_dict["recording"]["state"] == "published"
    assert rpc.meta_dict["recording"]["start_time"] == "1634057825061"
    assert rpc.meta_dict["recording"]["end_time"] == "1634059335155"
    assert rpc.meta_dict["recording"]["participants"] == "3"
    assert rpc.meta_dict["recording"]["meeting"]["@id"] == "dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
    assert rpc.meta_dict["recording"]["meeting"]["@externalId"] == "00563b64075043708825c10d67859721"
    assert rpc.meta_dict["recording"]["meeting"]["@name"] == "Example Meeting"
    assert rpc.meta_dict["recording"]["meeting"]["@breakout"] == "false"
    assert rpc.meta_dict["recording"]["meta"]["creator"] == "ExampleUser"
    assert rpc.meta_dict["recording"]["meta"]["meetingId"] == "00563b64075043708825c10d67859721"
    assert rpc.meta_dict["recording"]["meta"]["roomsmeetingid"] == "92654"
    assert rpc.meta_dict["recording"]["meta"]["streamingurl"] == "None"
    # Updatable Values
    assert rpc.meta_dict["recording"]["published"] == "false"
    assert rpc.meta_dict["recording"]["meta"]["bbb-meeting-name"] == "Test Meeting Name"
    assert rpc.meta_dict["recording"]["meta"]["bbb-meeting-creator"] == "Example User"

    # Reset dict for test
    rpc.meta_dict = None
    params = {"recordID": "TESTID", "publish": "true", "meta_bbb-meeting-name": "Test Meeting Name"}
    update_publish_recordings(params, rpc, False)
    # Values that need to be untouched by updating process
    assert rpc.meta_dict["recording"]["id"] == "dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
    assert rpc.meta_dict["recording"]["start_time"] == "1634057825061"
    assert rpc.meta_dict["recording"]["end_time"] == "1634059335155"
    assert rpc.meta_dict["recording"]["participants"] == "3"
    assert rpc.meta_dict["recording"]["published"] == "true"
    assert rpc.meta_dict["recording"]["meeting"]["@id"] == "dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
    assert rpc.meta_dict["recording"]["meeting"]["@externalId"] == "00563b64075043708825c10d67859721"
    assert rpc.meta_dict["recording"]["meeting"]["@name"] == "Example Meeting"
    assert rpc.meta_dict["recording"]["meeting"]["@breakout"] == "false"
    assert rpc.meta_dict["recording"]["meta"]["creator"] == "ExampleUser"
    assert rpc.meta_dict["recording"]["meta"]["meetingId"] == "00563b64075043708825c10d67859721"
    assert rpc.meta_dict["recording"]["meta"]["roomsmeetingid"] == "92654"
    assert rpc.meta_dict["recording"]["meta"]["streamingurl"] == "None"
    # Updatable Values
    assert rpc.meta_dict["recording"]["state"] == "published"
    assert "bbb-meeting-name" not in rpc.meta_dict["recording"]["meta"]


@pytest.mark.django_db
def test_update_publish_recordings_happy_path_name_update(rpc, mocker):
    def mock_get_meetings_for_record_ids(record_id):
        class MockMeeting:
            def __init__(self):
                self.replay_id = "TESTID"

            def get_recordings_xml(self):
                class Response:
                    text = """
                    <recording>
                      <id>dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061</id>
                      <state>published</state>
                      <published>false</published>
                      <start_time>1634057825061</start_time>
                      <end_time>1634059335155</end_time>
                      <participants>3</participants>
                      <meeting id="dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
                      externalId="00563b64075043708825c10d67859721" name="Example Meeting" breakout="false"/>
                      <meta>
                        <creator>ExampleUser</creator>
                        <meetingId>00563b64075043708825c10d67859721</meetingId>
                        <roomsmeetingid>92654</roomsmeetingid>
                        <streamingurl>None</streamingurl>
                      </meta>
                    </recording>
                    """

                return Response()

        return [MockMeeting()]

    mocker.patch("core.services.get_meetings_for_record_ids", mock_get_meetings_for_record_ids)

    Meeting.objects.create(replay_id="TESTID")
    params = {
        "recordID": "TESTID",
        "meta_meetingName": "Test Meeting Name",
    }
    update_publish_recordings(params, rpc, True)
    # Values that need to be untouched by updating process
    assert rpc.meta_dict["recording"]["id"] == "dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
    assert rpc.meta_dict["recording"]["state"] == "published"
    assert rpc.meta_dict["recording"]["start_time"] == "1634057825061"
    assert rpc.meta_dict["recording"]["end_time"] == "1634059335155"
    assert rpc.meta_dict["recording"]["participants"] == "3"
    assert rpc.meta_dict["recording"]["meeting"]["@id"] == "dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
    assert rpc.meta_dict["recording"]["meeting"]["@externalId"] == "00563b64075043708825c10d67859721"
    assert rpc.meta_dict["recording"]["meeting"]["@breakout"] == "false"
    assert rpc.meta_dict["recording"]["meta"]["creator"] == "ExampleUser"
    assert rpc.meta_dict["recording"]["meta"]["meetingId"] == "00563b64075043708825c10d67859721"
    assert rpc.meta_dict["recording"]["meta"]["roomsmeetingid"] == "92654"
    assert rpc.meta_dict["recording"]["meta"]["streamingurl"] == "None"
    # Updatable Values
    assert rpc.meta_dict["recording"]["published"] == "false"
    assert rpc.meta_dict["recording"]["meeting"]["@name"] == "Test Meeting Name"
    assert rpc.meta_dict["recording"]["meta"]["meetingName"] == "Test Meeting Name"


@pytest.mark.django_db
def test_get_meetings_for_record_ids():
    Meeting.objects.create(replay_id="recordings--TESTID1")
    Meeting.objects.create(replay_id="recordings--TESTID2")
    Meeting.objects.create(replay_id="TESTID3")
    Meeting.objects.create(replay_id="TESTID4")

    assert get_meetings_for_record_ids("TESTID1,TESTID2,TESTID3").count() == 3
    assert get_meetings_for_record_ids("TESTID1").count() == 1
    assert get_meetings_for_record_ids("TESTID1337").count() == 0
