from typing import Optional

import pytest
from core.services import moderator_message
from django.contrib.sites.models import Site

_ABSOLUTE_JOIN_URL = "https://example.org/r?room=test_user"


@pytest.mark.parametrize(
    (
        "room_name",
        "access_code",
        "only_prompt_guests_for_access_code",
        "guest_policy",
        "max_participants",
        "enable_dial_in",
        "expected_message",
    ),
    [
        (
            "test_user",
            None,
            True,
            "ASK_MODERATOR",
            10,
            True,
            f"Meeting link: {_ABSOLUTE_JOIN_URL}<br>"
            "The room has a guest lobby enabled.<br>"
            "The room is limited to 10 participants.<br>"
            "Dial number: %%DIALNUM%%<br>"
            "Conference number for dial-in: %%CONFNUM%%",
        ),
        (
            "test_user",
            "",
            False,
            "ALWAYS_DENY",
            10,
            False,
            f"Meeting link: {_ABSOLUTE_JOIN_URL}<br>The room is limited to 10 participants.",
        ),
        (
            "test_user",
            "<strong>I will be escaped</strong>",
            False,
            "ASK_MODERATOR",
            None,
            True,
            f"Meeting link: {_ABSOLUTE_JOIN_URL}<br>"
            "This room is protected by an access code: &lt;strong&gt;I will be escaped&lt;/strong&gt;<br>"
            "The room has a guest lobby enabled.<br>"
            "Dial number: %%DIALNUM%%<br>"
            "Conference number for dial-in: %%CONFNUM%%",
        ),
    ],
)
def test_moderator_message(
    testserver_tenant: Site,
    room_name: str,
    access_code: Optional[str],
    only_prompt_guests_for_access_code: bool,
    guest_policy: str,
    max_participants: Optional[int],
    enable_dial_in: bool,
    expected_message: str,
) -> None:
    message = moderator_message(
        _ABSOLUTE_JOIN_URL,
        room_name,
        access_code,
        only_prompt_guests_for_access_code,
        guest_policy,
        max_participants,
        enable_dial_in,
    )

    assert message == expected_message
