import pytest
from core.models import SchedulingStrategy, Server
from core.services import create_web_hook_bbb
from core.utils import BigBlueButton
from django.http import HttpRequest


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def bbb_server(db, example) -> Server:
    return Server.objects.create(
        scheduling_strategy=example,
        dns="example.org",
        shared_secret="123456789",
        participant_count_max=10,
        videostream_count_max=2,
    )


def test_create_web_hook_bbb(bbb_server, mocker):
    mocker.patch("core.utils.BigBlueButton.create_web_hook", return_value="https://example.org/bigbluebutton/api/")
    bbb = BigBlueButton(bbb_server.dns, bbb_server.shared_secret)
    request = HttpRequest()
    request.META["HTTP_HOST"] = "testserver"
    web_hook_bbb = create_web_hook_bbb(request, bbb)

    assert web_hook_bbb == "https://example.org/bigbluebutton/api/"
