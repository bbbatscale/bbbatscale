import json

import pytest
from core.constants import ROOM_STATE_INACTIVE, ROOM_VISIBILITY_PRIVATE
from core.models import MeetingConfigurationTemplate, Room, SchedulingStrategy, User
from core.views.rooms import map_room
from django.contrib.sites.models import Site
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_map_room(example_room: Room) -> None:
    assert map_room(example_room) == {
        "updateUrl": f"/core/room/update/{example_room.id}",
        "deleteUrl": f"/core/room/delete/{example_room.id}",
        "forceEndMeetingUrl": f"/core/room/meeting/end/{example_room.id}",
        "roomResetStuckUrl": f"/core/room/reset/stucked/{example_room.id}",
        "createOrJoinUrl": f"/r?room={example_room.name}",
        "name": "room",
        "state": ROOM_STATE_INACTIVE,
        "tenants": ["testserver"],
        "schedulingStrategy": "example",
        "configuration": "Example Config",
        "visibility": ROOM_VISIBILITY_PRIVATE,
        "lastRunning": None,
        "clicks": 0,
        "comment": None,
        "secret": None,
    }


@pytest.mark.django_db
def test_get_rooms(
    client: Client,
    example_superuser: User,
    example_scheduling_strategy: SchedulingStrategy,
    testserver_tenant: Site,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> None:
    for i in range(1, 50):
        r = Room.objects.create(
            scheduling_strategy=example_scheduling_strategy,
            name=f"example-room-{i}",
            default_meeting_configuration=example_meeting_configuration_template,
        )
        r.tenants.add(testserver_tenant)
    client.force_login(example_superuser)
    response = client.get(reverse("api_fetch_rooms"))
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 49

    response = client.get(f"{reverse('api_fetch_rooms')}?filter-search=5")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5

    test_config = MeetingConfigurationTemplate.objects.create(name="Test Config")
    test_config.tenants.set([testserver_tenant])
    Room.objects.filter(name__icontains="6").update(default_meeting_configuration=test_config)

    response = client.get(f"{reverse('api_fetch_rooms')}?filter-config={test_config.pk}")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5

    scheduling_strategy_tenant2 = SchedulingStrategy.objects.create(name="NEW EXAMPLE")
    scheduling_strategy_tenant2.tenants.set([testserver_tenant])

    Room.objects.filter(name__icontains="6").update(scheduling_strategy=scheduling_strategy_tenant2)

    response = client.get(f"{reverse('api_fetch_rooms')}?filter-strategy={scheduling_strategy_tenant2.pk}")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5
