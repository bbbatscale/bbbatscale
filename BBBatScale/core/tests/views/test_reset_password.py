from typing import List

import pytest
from core.models import User
from core.views import reset_password
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
@pytest.fixture
def example_user_with_unusable_password(testserver_tenant: Site) -> User:
    return User.objects.create_user(
        "example-user-unusable-password",
        "example-user-unusable-password@example.org",
        tenant=testserver_tenant,
        is_staff=False,
        is_superuser=False,
        first_name="Example",
        last_name="User Unusable Password",
        display_name="example_user_unusable_password",
    )


@pytest.fixture
def non_existing_user() -> User:
    return User(username="non-existing")


@pytest.mark.django_db
@pytest.fixture
def example_user_different_tenant(testserver2_tenant: Site) -> User:
    return User.objects.create_user(
        "example-user-different-tenant",
        "example-user-different-tenant@example.org",
        "EXAMPLEPASSWORD",
        tenant=testserver2_tenant,
        is_staff=False,
        is_superuser=False,
        first_name="Example",
        last_name="User Different Tenant",
        display_name="example_user_different_tenant",
    )


@pytest.mark.parametrize(
    "user_fixture,has_received_email",
    [
        ("example_user", True),
        ("example_user_with_unusable_password", False),
        ("non_existing_user", False),
        ("example_user_different_tenant", False),
    ],
)
@pytest.mark.django_db
def test_password_reset_ok(
    request: pytest.FixtureRequest,
    client: Client,
    testserver_tenant: Site,
    outbox: List[EmailMessage],
    user_fixture: str,
    has_received_email: bool,
) -> None:
    user = request.getfixturevalue(user_fixture)
    response = client.post(
        reverse(reset_password.password_reset),
        data={"username": user.username},
    )

    assert response.status_code == 302
    assert response.url == reverse("home")

    assert has_received_email == (len(outbox) == 1)
