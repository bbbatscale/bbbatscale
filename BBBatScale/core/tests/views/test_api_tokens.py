import json

import pytest
from core.models import ApiToken, User
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_get_tokens(client: Client, example_superuser: User) -> None:
    for i in range(1, 50):
        slug = f"example-token-{i}"
        ApiToken.objects.create(name=slug, slug=slug)
    client.force_login(example_superuser)
    response = client.get(reverse("api_fetch_tokens"))
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 49
