import json

import pytest
from core.models import SchedulingStrategy, User
from core.views.scheduling_strategies import map_scheduling_strategy
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_map_tenant(example_scheduling_strategy: SchedulingStrategy) -> None:
    assert map_scheduling_strategy(example_scheduling_strategy) == {
        "updateUrl": f"/core/schedulingstrategy/update/{example_scheduling_strategy.id}",
        "deleteUrl": f"/core/schedulingstrategy/delete/{example_scheduling_strategy.id}",
        "agentConfigUrl": f"/core/schedulingstrategy/agentconfiguration/update/{example_scheduling_strategy.id}",
        "name": "example",
        "strategy": "Least utilization",
        "tenants": ["testserver"],
        "description": None,
        "emailNotifications": None,
    }


@pytest.mark.django_db
def test_get_scheduling_strategies(client: Client, example_superuser: User) -> None:
    for i in range(1, 50):
        SchedulingStrategy.objects.create(
            name=f"example-{i}",
        )
    client.force_login(example_superuser)
    response = client.get(reverse("api_fetch_scheduling_strategies"))
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 49

    response = client.get(f"{reverse('api_fetch_scheduling_strategies')}?filter-search=5")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5
