import json

import pytest
from core.models import GeneralParameter, User
from core.views.tenants import map_tenant
from django.contrib.sites.models import Site
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_map_tenant(testserver_tenant: Site, gp_test_tenant: GeneralParameter) -> None:
    assert map_tenant(testserver_tenant) == {
        "updateUrl": f"/core/tenant/update/{testserver_tenant.id}",
        "deleteUrl": f"/core/tenant/delete/{testserver_tenant.id}",
        "createAuthParameterUrl": f"/tenant/authparameter/create/{testserver_tenant.id}",
        "updateAuthParameterUrl": f"/tenant/authparameter/update/{testserver_tenant.id}",
        "deleteAuthParameterUrl": f"/tenant/authparameter/delete/{testserver_tenant.id}",
        "updateGeneralParameterUrl": f"/core/settings/{gp_test_tenant.id}",
        "name": "testserver",
        "domain": "testserver",
        "hasAuthParameters": False,
    }


@pytest.mark.django_db
def test_get_tenants(client: Client, example_superuser: User) -> None:
    for i in range(1, 50):
        Site.objects.create(
            name=f"example-{i}.org",
            domain=f"example-{i}.org",
        )
    client.force_login(example_superuser)
    response = client.get(reverse("api_fetch_tenants"))
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 51
