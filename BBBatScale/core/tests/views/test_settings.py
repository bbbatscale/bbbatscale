import pytest
from core.forms import GeneralParametersForm
from core.models import Theme, User
from django.urls import reverse


@pytest.mark.django_db
def test_get_core_settings_with_super_user(client, example_superuser, testserver_tenant):
    client.force_login(example_superuser)
    response = client.get(reverse("update_general_parameter"), SERVER_NAME=testserver_tenant.name)
    assert type(response.context["form"]) is GeneralParametersForm
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_core_settings_with_staff_user(client, example_staff_user, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.get(reverse("update_general_parameter"), SERVER_NAME=testserver_tenant.name)
    assert type(response.context["form"]) is GeneralParametersForm
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_core_settings_without_login(client, testserver_tenant):
    response = client.get(reverse("update_general_parameter"), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 302
    assert response.url == "/login?next=/core/settings"


@pytest.mark.django_db
def test_get_core_settings_with_normal_user(client, testserver_tenant):
    user = User.objects.create_user(
        username="Testuser",
        tenant=testserver_tenant,
    )
    client.force_login(user)
    response = client.get(reverse("update_general_parameter"), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 403


@pytest.mark.django_db
@pytest.mark.parametrize(
    (
        "form_data",
        "gp_personal_rooms_teacher_max",
        "gp_personal_rooms_non_teacher_max",
        "gp_app_title",
        "gp_recording_deletion_period",
    ),
    [
        (
            {
                "personal_rooms_scheduling_strategy": [""],
                "personal_rooms_teacher_max_numberrrrrr": ["10"],  # is invalid
                "personal_rooms_non_teacher_max_number": ["10"],
                "save": ["Sichern"],
            },
            0,
            0,
            "Virtual Rooms",
            72,
        ),
        (
            {
                "app_title": ["Virtual Rooms"],
                "default_theme": ["1"],
                "recording_deletion_period": ["3"],
                "personal_rooms_teacher_max_number": ["10"],
                "personal_rooms_non_teacher_max_number": ["10"],
            },
            10,
            10,
            "Virtual Rooms",
            3,
        ),
    ],
)
def test_post_with_submitting_form(
    client,
    example_superuser,
    testserver_tenant,
    gp_test_tenant,
    gp_personal_rooms_teacher_max,
    gp_personal_rooms_non_teacher_max,
    gp_app_title,
    gp_recording_deletion_period,
    form_data,
):
    client.force_login(example_superuser)
    client.post(
        reverse("update_general_parameter"),
        SERVER_NAME=testserver_tenant.name,
        data=form_data,
    )

    gp_default_theme = Theme.objects.order_by("pk").first()
    gp_test_tenant.refresh_from_db()
    assert gp_test_tenant.personal_rooms_teacher_max_number == gp_personal_rooms_teacher_max
    assert gp_test_tenant.personal_rooms_non_teacher_max_number == gp_personal_rooms_non_teacher_max
    assert gp_test_tenant.app_title == gp_app_title
    assert gp_test_tenant.default_theme == gp_default_theme
    assert gp_test_tenant.recording_deletion_period == gp_recording_deletion_period
