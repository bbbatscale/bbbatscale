import pytest
from core.constants import MEETING_STATE_RUNNING
from core.models import GeneralParameter, Meeting
from core.views import statistics
from django.urls import reverse


@pytest.fixture(scope="function")
def statistics_disabled_test_tenant(
    testserver_tenant, example_scheduling_strategy, example_meeting_configuration_template
) -> GeneralParameter:
    return GeneralParameter.objects.create(
        tenant=testserver_tenant,
        default_theme_id=1,
        home_room_enabled=True,
        home_room_scheduling_strategy=example_scheduling_strategy,
        home_room_room_configuration=example_meeting_configuration_template,
        hide_statistics_enable=True,
    )


@pytest.fixture(scope="function")
def statistics_disabled_test_tenant2(
    testserver2_tenant, example_scheduling_strategy, example_meeting_configuration_template
) -> GeneralParameter:
    return GeneralParameter.objects.create(
        tenant=testserver2_tenant,
        default_theme_id=1,
        home_room_enabled=True,
        home_room_scheduling_strategy=example_scheduling_strategy,
        home_room_room_configuration=example_meeting_configuration_template,
        hide_statistics_enable=True,
    )


@pytest.fixture(scope="function")
def test_meeting_current_users(example_user, example_room, testserver_tenant) -> Meeting:
    return Meeting.objects.create(
        room=example_room,
        room_name="test_Meeting",
        creator=example_user,
        participant_count=4,
        state=MEETING_STATE_RUNNING,
    )


@pytest.mark.django_db
def test_statistics_site_no_permission_redirect(client, testserver_tenant, statistics_disabled_test_tenant):
    response = client.get(reverse(statistics.statistics), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 302


@pytest.mark.django_db
def test_statistics_has_tenant_permission_reachable(
    client, example_user, testserver2_tenant, statistics_disabled_test_tenant2
):
    client.force_login(example_user)
    response = client.get(reverse(statistics.statistics), SERVER_NAME=testserver2_tenant.name)
    assert response.status_code == 403
