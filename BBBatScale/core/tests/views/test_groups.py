import json

import pytest
from core.models import User
from core.views import groups
from core.views.groups import map_group
from django.contrib.auth.models import Group
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_get_group_overview_normal_user(client, example_user, testserver_tenant):
    client.force_login(example_user)
    response = client.get(reverse(groups.groups_overview), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 403


@pytest.mark.django_db
def test_get_group_overview_super_user(client, example_superuser, testserver_tenant):
    client.force_login(example_superuser)
    response = client.get(reverse(groups.groups_overview), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_group_overview_staff_user(client, example_staff_user, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.get(reverse(groups.groups_overview), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 200


@pytest.mark.django_db
def test_post_group_create_normal_user(client, example_user, testserver_tenant):
    client.force_login(example_user)
    response = client.post(reverse(groups.group_create), data={"name": "Example group"})
    assert response.status_code == 403
    assert Group.objects.filter(name="Example group").count() == 0


@pytest.mark.django_db
def test_post_group_create_super_user(client, example_superuser, testserver_tenant):
    client.force_login(example_superuser)
    response = client.post(reverse(groups.group_create), data={"name": "Example group"})
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 1


@pytest.mark.django_db
def test_post_group_create_staff_user(client, example_staff_user, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.post(reverse(groups.group_create), data={"name": "Example group"})
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 1


@pytest.mark.django_db
def test_post_group_create_false_post_data(client, example_superuser, testserver_tenant):
    client.force_login(example_superuser)
    response = client.post(reverse(groups.group_create), data={"asdf": "Example group"})
    assert response.status_code == 200
    assert Group.objects.filter(name="Example group").count() == 0


@pytest.mark.django_db
def test_get_group_delete_normal_user(client, example_user, example_group, testserver_tenant):
    client.force_login(example_user)
    response = client.get(reverse(groups.group_delete, args=[example_group.pk]), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 403
    assert Group.objects.filter(name="Example group").count() == 1


@pytest.mark.django_db
def test_get_group_delete_staff_user(client, example_staff_user, example_group, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.get(reverse(groups.group_delete, args=[example_group.pk]), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 0


@pytest.mark.django_db
def test_get_group_delete_super_user(client, example_superuser, example_group, testserver_tenant):
    client.force_login(example_superuser)
    response = client.get(reverse(groups.group_delete, args=[example_group.pk]), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 0


@pytest.mark.django_db
def test_get_group_delete_false_get_data(client, example_superuser, example_group, testserver_tenant):
    client.force_login(example_superuser)
    response = client.get(reverse(groups.group_delete, args=[1337]), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 404
    assert Group.objects.filter(name="Example group").count() == 1


@pytest.mark.django_db
def test_post_group_update_normal_user(client, example_user, example_group, testserver_tenant):
    client.force_login(example_user)
    response = client.post(
        reverse(groups.group_update, args=[example_group.pk]), data={"name": "Changed example group"}
    )
    assert response.status_code == 403
    assert Group.objects.filter(name="Example group").count() == 1
    assert Group.objects.filter(name="Changed example group").count() == 0


@pytest.mark.django_db
def test_post_group_update_staff_user(client, example_staff_user, example_group, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.post(
        reverse(groups.group_update, args=[example_group.pk]), data={"name": "Changed example group"}
    )
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 0
    assert Group.objects.filter(name="Changed example group").count() == 1


@pytest.mark.django_db
def test_post_group_update_super_user(client, example_superuser, example_group, testserver_tenant):
    client.force_login(example_superuser)
    response = client.post(
        reverse(groups.group_update, args=[example_group.pk]), data={"name": "Changed example group"}
    )
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 0
    assert Group.objects.filter(name="Changed example group").count() == 1


@pytest.mark.django_db
def test_post_group_update_false_post_data(client, example_superuser, example_group, testserver_tenant):
    client.force_login(example_superuser)
    response = client.post(
        reverse(groups.group_update, args=[example_group.pk]), data={"asdf": "Changed example group"}
    )
    assert response.status_code == 200
    assert Group.objects.filter(name="Example group").count() == 1
    assert Group.objects.filter(name="Changed example group").count() == 0


@pytest.mark.django_db
def test_map_group(example_group: Group) -> None:
    assert map_group(example_group) == {
        "updateUrl": f"/core/group/update/{example_group.id}",
        "deleteUrl": f"/core/group/delete/{example_group.id}",
        "name": "Example group",
    }


@pytest.mark.django_db
def test_get_groups(client: Client, example_superuser: User) -> None:
    for i in range(1, 50):
        Group.objects.create(
            name=f"examplegroup-{str(i)}",
        )
    client.force_login(example_superuser)
    response = client.get(reverse("api_fetch_groups"))
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 49

    response = client.get(f"{reverse('api_fetch_groups')}?filter-search=5")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5
