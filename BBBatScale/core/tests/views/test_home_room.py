import pytest
from core.constants import ROOM_VISIBILITY_PRIVATE, ROOM_VISIBILITY_PUBLIC
from core.forms import HomeRoomForm
from core.views import home_room
from django.urls import reverse


@pytest.mark.django_db
def test_home_room_update_with_get(client, example_moderator_user, gp_test_tenant, example_homeroom):
    client.force_login(example_moderator_user)

    response = client.get(reverse(home_room.home_room_update, args=[example_homeroom.id]))

    assert response.status_code == 200

    assert type(response.context["form"]) is HomeRoomForm


@pytest.mark.django_db
def test_home_room_update_with_post(
    client,
    example_moderator_user,
    gp_test_tenant,
    example_homeroom,
    example_meeting_configuration_template,
    example_scheduling_strategy,
):

    gp_test_tenant.home_room_enabled = True
    gp_test_tenant.home_rooms_scheduling_strategy = example_scheduling_strategy
    gp_test_tenant.home_room_room_configuration = example_meeting_configuration_template
    gp_test_tenant.home_room_teachers_only = False
    gp_test_tenant.save()

    client.force_login(example_moderator_user)

    example_homeroom.visibility = ROOM_VISIBILITY_PUBLIC
    example_homeroom.save()

    form_data = {
        "name": ["room"],
        "default_meeting_configuration": [str(example_meeting_configuration_template.pk)],
        "visibility": ["private"],
        "comment_public": [""],
        "filter_owner": [""],
    }

    response = client.post(
        reverse(home_room.home_room_update, args=[example_homeroom.id]),
        SERVER_NAME=example_moderator_user.tenant.name,
        data=form_data,
    )

    assert response.status_code == 302

    example_homeroom.refresh_from_db()
    assert example_homeroom.visibility == ROOM_VISIBILITY_PRIVATE
