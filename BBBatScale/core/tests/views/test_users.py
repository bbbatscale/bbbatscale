import json
from operator import itemgetter

import pytest
from conftest import RequestFactory
from core.models import User
from core.utils import load_moderator_group
from core.views import users
from core.views.users import map_user
from django.contrib.sites.models import Site
from django.test.client import Client
from django.urls import get_resolver, reverse


@pytest.mark.django_db
@pytest.fixture(scope="function")
def example_user_without_password(testserver_tenant: Site) -> User:
    return User.objects.create_user(
        "example-user-without-password",
        "example-user-without-password@example.org",
        tenant=testserver_tenant,
        is_staff=False,
        is_superuser=False,
        first_name="Example user",
        last_name="Without password",
        display_name="example_user_without_password",
    )


@pytest.mark.django_db
@pytest.fixture(scope="function")
def example_superuser_without_password(testserver_tenant: Site) -> User:
    return User.objects.create_user(
        "example-superuser-without-password",
        "example-superuser-without-password@example.org",
        tenant=testserver_tenant,
        is_staff=False,
        is_superuser=True,
        first_name="Example superuser",
        last_name="Without password",
        display_name="example_superuser_without_password",
    )


@pytest.mark.django_db
def test_users_user_search_json(testserver_tenant: Site, testserver2_tenant: Site) -> None:
    user = User.objects.create(first_name="-", last_name="-", username="-", email="-", tenant=testserver_tenant)

    tenant_users = []

    for i in range(20):
        tenant_users.append(
            User.objects.create(
                first_name="User%02d" % i,
                last_name="Tenant01",
                username="user%02d-tenant01" % i,
                email="email%02d@tenant01.example.org" % i,
                tenant=testserver_tenant,
            )
        )
        User.objects.create(
            first_name="User%02d" % i,
            last_name="Tenant02",
            username="user%02d-tenant02" % i,
            email="email%02d@tenant02.example.org" % i,
            tenant=testserver2_tenant,
        )

    request = RequestFactory(testserver_tenant, user=user).get(reverse(users.user_search_json))
    request.GET._mutable = True
    request.resolver_match = get_resolver().resolve(reverse(users.user_search_json))

    assert json.loads(users.user_search_json(request).content) == {"users": []}

    request.GET["q"] = "u"
    assert json.loads(users.user_search_json(request).content) == {"users": []}

    request.GET["q"] = "us"
    assert sorted(json.loads(users.user_search_json(request).content)["users"], key=itemgetter("id")) == sorted(
        {"users": [{"id": tenant_user.id, "display_name": str(tenant_user)} for tenant_user in tenant_users]}["users"],
        key=itemgetter("id"),
    )

    request.GET["q"] = "email"
    assert sorted(json.loads(users.user_search_json(request).content)["users"], key=itemgetter("id")) == sorted(
        {"users": [{"id": tenant_user.id, "display_name": str(tenant_user)} for tenant_user in tenant_users]}["users"],
        key=itemgetter("id"),
    )

    request.GET["q"] = "TeNaNt01"
    assert sorted(json.loads(users.user_search_json(request).content)["users"], key=itemgetter("id")) == sorted(
        {"users": [{"id": tenant_user.id, "display_name": str(tenant_user)} for tenant_user in tenant_users]}["users"],
        key=itemgetter("id"),
    )

    request.GET["q"] = "tenant02"
    assert json.loads(users.user_search_json(request).content) == {"users": []}

    request.GET["q"] = "uSeR0"
    assert sorted(json.loads(users.user_search_json(request).content)["users"], key=itemgetter("id")) == sorted(
        {"users": [{"id": tenant_user.id, "display_name": str(tenant_user)} for tenant_user in tenant_users[:10]]}[
            "users"
        ],
        key=itemgetter("id"),
    )


@pytest.mark.django_db
def test_update_user(client, testserver_tenant, example_superuser, example_user, example_group):
    moderator_group = load_moderator_group(testserver_tenant)
    client.force_login(example_superuser)
    assert "core.moderate" not in example_user.get_all_permissions()

    response = client.post(
        reverse(users.user_update, args=[example_user.id]),
        data={
            "username": example_user.get_username(),
            "groups": {moderator_group.id, example_group.id},
            "theme": 1,
        },
    )
    assert response.status_code == 302
    example_user = User.objects.get(id=example_user.id)  # Full refresh since permissions have been cached.
    assert moderator_group.id in example_user.groups.values_list("id", flat=True)
    assert example_group.id in example_user.groups.values_list("id", flat=True)
    assert "core.moderate" in example_user.get_all_permissions()


@pytest.mark.django_db
def test_create_user(client, testserver_tenant, example_superuser, example_group):
    moderator_group = load_moderator_group(testserver_tenant)
    client.force_login(example_superuser)
    response = client.post(
        reverse(users.user_create),
        data={"username": "testuser", "password": 123, "groups": {moderator_group.id, example_group.id}, "theme": 1},
    )
    assert response.status_code == 302
    assert User.objects.filter(username="testuser").count() == 1
    assert "core.moderate" in User.objects.get(username="testuser").get_all_permissions()


@pytest.mark.django_db
def test_update_user_delete_moderator_group(client, testserver_tenant, example_superuser, example_user, example_group):
    moderator_group = load_moderator_group(testserver_tenant)
    client.force_login(example_superuser)
    assert "core.moderate" not in User.objects.get(id=example_user.id).get_all_permissions()

    response = client.post(
        reverse(users.user_update, args=[example_user.id]),
        data={"username": example_user.get_username(), "groups": {moderator_group.id}, "theme": 1},
    )
    assert response.status_code == 302
    assert "core.moderate" in User.objects.get(id=example_user.id).get_all_permissions()

    response = client.post(
        reverse(users.user_update, args=[example_user.id]),
        data={"username": example_user.get_username(), "groups": {example_group.id}, "theme": 1},
    )
    assert response.status_code == 302
    assert "core.moderate" not in User.objects.get(id=example_user.id).get_all_permissions()


@pytest.mark.django_db
def test_map_user(testserver_tenant: Site, example_user: User) -> None:
    assert map_user(testserver_tenant, example_user) == {
        "updateUrl": f"/core/user/update/{example_user.id}",
        "deleteUrl": f"/core/user/delete/{example_user.id}",
        "detailsUrl": f"/core/user/details/{example_user.id}",
        "changePassword": f"/core/user/change/password/admin/{example_user.id}",
        "hasUsablePassword": True,
        "username": "example-user",
        "firstName": "Example",
        "lastName": "User",
        "email": "example-user@example.org",
        "isStaff": False,
        "isModerator": False,
    }


@pytest.mark.django_db
def test_get_users(client: Client, example_superuser: User, testserver_tenant: Site) -> None:
    for i in range(1, 50):
        User.objects.create(
            username=f"exampleuser-{str(i)}",
            first_name=f"Example-{str(i)}",
            last_name=f"User-{str(i)}",
            email=f"exampleuser-{str(i)}@example.org",
            password="EXAMPLEPASSWORD",
            tenant=testserver_tenant,
        )
    client.force_login(example_superuser)
    response = client.get(reverse("api_fetch_users"))
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 50

    response = client.get(f"{reverse('api_fetch_users')}?filter-search=5")
    json_response = json.loads(response.content)

    assert response.status_code == 200
    assert json_response["totalCount"] == 5


@pytest.mark.django_db
def test_change_password_with_password(client: Client, example_user: User) -> None:
    client.force_login(example_user)

    response = client.get(reverse(users.user_change_password))
    assert response.status_code == 200

    response = client.post(
        reverse(users.user_change_password),
        data={
            "old_password": "EXAMPLEPASSWORD",
            "new_password1": "new-password",
            "new_password2": "new-password",
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("home")


@pytest.mark.django_db
def test_change_password_without_password(client: Client, example_user_without_password: User) -> None:
    client.force_login(example_user_without_password)

    response = client.get(reverse(users.user_change_password))
    assert response.status_code == 404

    response = client.post(
        reverse(users.user_change_password),
        data={
            "old_password": "non-existing-old-password",
            "new_password1": "new-password",
            "new_password2": "new-password",
        },
    )
    assert response.status_code == 404


@pytest.mark.django_db
def test_admin_change_password_for_user_with_password(
    client: Client, example_superuser: User, example_user: User
) -> None:
    client.force_login(example_superuser)

    response = client.get(reverse(users.user_change_password_admin, args=[example_user.id]))
    assert response.status_code == 200

    response = client.post(
        reverse(users.user_change_password_admin, args=[example_user.id]),
        data={
            "new_password1": "new-password",
            "new_password2": "new-password",
        },
    )
    assert response.status_code == 302
    assert response.url == reverse(users.users_overview)


@pytest.mark.django_db
def test_admin_change_password_for_user_without_password(
    client: Client, example_superuser: User, example_user_without_password: User
) -> None:
    client.force_login(example_superuser)

    response = client.get(reverse(users.user_change_password_admin, args=[example_user_without_password.id]))
    assert response.status_code == 404

    response = client.post(
        reverse(users.user_change_password_admin, args=[example_user_without_password.id]),
        data={
            "new_password1": "new-password",
            "new_password2": "new-password",
        },
    )
    assert response.status_code == 404


@pytest.mark.django_db
def test_admin_without_password_change_password_for_user_with_password(
    client: Client, example_superuser_without_password: User, example_user: User
) -> None:
    client.force_login(example_superuser_without_password)

    response = client.get(reverse(users.user_change_password_admin, args=[example_user.id]))
    assert response.status_code == 200

    response = client.post(
        reverse(users.user_change_password_admin, args=[example_user.id]),
        data={
            "new_password1": "new-password",
            "new_password2": "new-password",
        },
    )
    assert response.status_code == 302
    assert response.url == reverse(users.users_overview)


@pytest.mark.django_db
def test_admin_without_password_change_password_for_user_without_password(
    client: Client, example_superuser_without_password: User, example_user_without_password: User
) -> None:
    client.force_login(example_superuser_without_password)

    response = client.get(reverse(users.user_change_password_admin, args=[example_user_without_password.id]))
    assert response.status_code == 404

    response = client.post(
        reverse(users.user_change_password_admin, args=[example_user_without_password.id]),
        data={
            "new_password1": "new-password",
            "new_password2": "new-password",
        },
    )
    assert response.status_code == 404
