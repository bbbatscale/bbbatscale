import logging
import os
from importlib import reload
from typing import List, Optional

import pytest
from conftest import AsyncRequestFactory, RequestFactory
from core.middleware import (
    AccessLoggingMiddleware,
    CurrentGeneralParameterMiddleware,
    CurrentTenantMiddleware,
    _get_tenant,
)
from core.models import GeneralParameter
from django.contrib.sites.models import Site
from django.core.handlers.asgi import ASGIRequest
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpRequest, HttpResponse
from django.test.client import Client
from django.urls import reverse
from pytest import FixtureRequest
from pytest_django.fixtures import SettingsWrapper
from pytest_mock import MockerFixture

from BBBatScale.settings import base as base_settings


def test_get_tenant_does_not_cache(testserver_tenant: Site) -> None:
    assert _get_tenant(testserver_tenant.domain) is not _get_tenant(testserver_tenant.domain)


def test_current_tenant_parameter_middleware(testserver_tenant: Site) -> None:
    request = HttpRequest()
    request.META["HTTP_HOST"] = testserver_tenant.domain

    assert not hasattr(request, "tenant")

    CurrentTenantMiddleware(lambda _: HttpResponse())(request)

    assert getattr(request, "tenant", None) == testserver_tenant


def test_current_general_parameter_middleware(testserver_tenant: Site) -> None:
    general_parameter = GeneralParameter.objects.load(testserver_tenant)
    request = HttpRequest()
    request.tenant = testserver_tenant

    assert not hasattr(request, "general_parameter")

    CurrentGeneralParameterMiddleware(lambda _: HttpResponse())(request)

    assert getattr(request, "general_parameter", None) == general_parameter


@pytest.mark.parametrize("include_request_query_setting", (None, False, True))
def test_access_logging_middleware(
    mocker: MockerFixture,
    client: Client,
    testserver_tenant: Site,
    settings: SettingsWrapper,
    include_request_query_setting: Optional[bool],
) -> None:
    environ = os.environ.copy()
    if include_request_query_setting is None:
        environ.pop("BBBATSCALE_LOGGING_INCLUDE_REQUEST_QUERY", None)
        expect_query = False
    else:
        environ["BBBATSCALE_LOGGING_INCLUDE_REQUEST_QUERY"] = "true" if include_request_query_setting else "false"
        expect_query = include_request_query_setting
    mocker.patch.dict(os.environ, environ, clear=True)

    reload(base_settings)
    settings.BBBATSCALE_LOGGING_INCLUDE_REQUEST_QUERY = base_settings.BBBATSCALE_LOGGING_INCLUDE_REQUEST_QUERY

    query = "param1=value1&param2=value2&secret=SUPER-SENSITIVE-VALUE"

    handle_spy = mocker.spy(logging.root.handlers[0], "handle")
    client.get(f"{reverse('home')}?{query}", SERVER_NAME=testserver_tenant.name)

    log_records = [
        log_record
        for log_record in (call.args[0] for call in handle_spy.call_args_list)
        if log_record.name == "bbbatscale.request"
    ]
    assert len(log_records) == 1

    log_record = log_records[0]
    assert (
        vars(log_record).items()
        >= {
            "levelno": logging.INFO,
            "request_scheme": "http",
            "request_host": testserver_tenant.name,
            "request_method": "GET",
            "request_path": reverse("home"),
            "response_status_code": 200,
            "http_version": "1.1",
            "remote_address": "127.0.0.1",
        }.items()
    )
    assert isinstance(log_record.correlation_id, str) and log_record.correlation_id != ""

    if expect_query:
        assert log_record.request_uri == f"http://{testserver_tenant.name}/?{query}"
        assert log_record.request_query == query
    else:
        assert log_record.request_uri == f"http://{testserver_tenant.name}/"
        assert "request_query" not in dir(log_record)


@pytest.fixture
def forwarded_for() -> str:
    return " 4.4.4.4,3:3:3::3 , 2.2.2.2 "


@pytest.fixture
def forwarded_wsgi_request(testserver_tenant: Site, forwarded_for: str) -> WSGIRequest:
    return RequestFactory(testserver_tenant, remote_address="1:1::1:1").get("/", HTTP_X_FORWARDED_FOR=forwarded_for)


@pytest.fixture
def forwarded_asgi_request(testserver_tenant: Site, forwarded_for: str) -> ASGIRequest:
    return AsyncRequestFactory(testserver_tenant, remote_address="1:1::1:1").get(
        "/", **{"X-Forwarded-For": forwarded_for}
    )


@pytest.mark.parametrize("forwarded_request", ("forwarded_wsgi_request", "forwarded_asgi_request"))
@pytest.mark.parametrize(
    ("proxy_count", "expected_address", "should_log_critical"),
    [
        (0, "1:1::1:1", False),
        (1, "2.2.2.2", False),
        (2, "3:3:3::3", False),
        (3, "4.4.4.4", False),
        (4, "1:1::1:1", True),
    ],
)
def test_access_logging_middleware__get_remote_address(
    request: FixtureRequest,
    mocker: MockerFixture,
    settings: SettingsWrapper,
    forwarded_request: str,
    proxy_count: int,
    expected_address: str,
    should_log_critical: bool,
):
    settings.BBBATSCALE_HTTP_PROXY_COUNT = proxy_count
    handle_spy = mocker.spy(logging.root.handlers[0], "handle")

    assert AccessLoggingMiddleware._get_remote_address(request.getfixturevalue(forwarded_request)) == expected_address

    log_records: List[logging.LogRecord] = [
        log_record
        for log_record in (call.args[0] for call in handle_spy.call_args_list)
        if log_record.name == "bbbatscale.config"
    ]

    if should_log_critical:
        assert len(log_records) == 1
        assert log_records[0].levelno == logging.CRITICAL

        message = log_records[0].getMessage()
        assert message.startswith(f"The X-Forwarded-For header did not contain at least {proxy_count} address(es).")
        assert " env variable 'BBBATSCALE_HTTP_PROXY_COUNT'" in message
        assert " and can lead to spoofed addresses." in message
    else:
        assert len(log_records) == 0
