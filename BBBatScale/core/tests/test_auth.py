from unittest.mock import patch

import pytest
from conftest import RequestFactory
from core.auth import TenantBasedModelBackend
from django.contrib.auth.backends import ModelBackend


@pytest.mark.parametrize(
    "user_fixture,tenant_fixture,expectation",
    [
        ("example_superuser", "testserver_tenant", True),
        ("example_superuser", "testserver2_tenant", True),
        ("example_staff_user", "testserver_tenant", True),
        ("example_staff_user", "testserver2_tenant", False),
        ("example_user", "testserver_tenant", True),
        ("example_user", "testserver2_tenant", False),
        (None, "testserver_tenant", False),
    ],
)
def test_authenticate(
    request,
    user_fixture,
    tenant_fixture,
    expectation,
):
    user = request.getfixturevalue(user_fixture) if user_fixture else None
    tenant = request.getfixturevalue(tenant_fixture)

    r = RequestFactory(tenant).get("/")

    with patch.object(ModelBackend, "authenticate") as model_backend:
        model_backend.return_value = user
        authenticated_user = TenantBasedModelBackend().authenticate(r, "some_username", "some_pw")

    assert bool(authenticated_user) == expectation
