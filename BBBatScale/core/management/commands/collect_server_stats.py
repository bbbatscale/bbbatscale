# encoding: utf-8
import logging

from core.services import collect_server_stats
from django.core.management.base import BaseCommand
from django.db import transaction
from tendo.singleton import SingleInstance, SingleInstanceException

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Perform health check on all servers."

    @transaction.non_atomic_requests
    def handle(self, *args, **options):
        logger.info("Start collect_server_stats command ")

        try:
            SingleInstance()
            logger.debug("Start collecting server stats")
            collect_server_stats()

        except SingleInstanceException as e:
            logger.error("An error occurred during the collection of server stats. Details:" + str(e))

        logger.info("End collect_server_stats command ")
