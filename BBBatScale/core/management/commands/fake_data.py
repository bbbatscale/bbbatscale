import sys
from argparse import ArgumentParser
from typing import TYPE_CHECKING, Any, Callable, Collection, Optional, Set, TypeVar

from core.constants import ROOM_VISIBILITY_PUBLIC
from core.models import (
    GeneralParameter,
    HomeRoom,
    MeetingConfigurationTemplate,
    PersonalRoom,
    Room,
    SchedulingStrategy,
    User,
)
from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand
from django.db import transaction

if TYPE_CHECKING:
    from faker import Faker

T = TypeVar("T")


def unique_elements(amount: int, factory: Callable[[], T], other_values: Optional[Collection[T]] = None) -> Set[T]:
    elements = set()

    for _ in range(amount):
        element = factory()
        while element in elements or (other_values and element in other_values):
            element = factory()
        elements.add(element)

    return elements


def get_tenant(domain: str, create: bool, drop: bool) -> Optional[Site]:
    tenant = Site.objects.filter(domain=domain).first()
    previous_id = None
    name = domain
    if tenant is not None and drop:
        previous_id = tenant.id
        name = tenant.name

        tenant.room_set.all().delete()
        tenant.schedulingstrategy_set.all().delete()
        tenant.delete()

    if (tenant is None and create) or drop:
        tenant = Site.objects.create(id=previous_id, domain=domain, name=name)

    return tenant


def create_strategies(faker: "Faker", tenant_id: int) -> Collection[SchedulingStrategy]:
    strategies = list()
    for name in unique_elements(4, faker.company):
        strategy = SchedulingStrategy.objects.create(name=name)
        strategy.tenants.add(tenant_id)
        strategies.append(strategy)
    return strategies


def get_or_create_configurations(faker: "Faker") -> Collection[MeetingConfigurationTemplate]:
    configurations = list(MeetingConfigurationTemplate.objects.all()[:4])
    configuration_amount = 4 - len(configurations)
    if configuration_amount > 0:
        for name in unique_elements(
            configuration_amount,
            faker.country,
            list(map(lambda configuration: configuration.name, configurations)),
        ):
            configurations.append(MeetingConfigurationTemplate.objects.create(name=name))
    return configurations


def create_rooms(
    faker: "Faker",
    other_names: Set[str],
    strategies: Collection[SchedulingStrategy],
    configurations: Collection[MeetingConfigurationTemplate],
    tenant_id: int,
) -> Set[str]:
    room_names = unique_elements(512, faker.city, other_names)
    for name in room_names:
        Room.objects.create(
            name=name,
            scheduling_strategy=faker.random_element(strategies),
            default_meeting_configuration=faker.random_element(configurations),
            visibility=ROOM_VISIBILITY_PUBLIC,
        ).tenants.add(tenant_id)
    return room_names


def create_personal_rooms(
    faker: "Faker",
    other_names: Set[str],
    owner: User,
    strategies: Collection[SchedulingStrategy],
    configurations: Collection[MeetingConfigurationTemplate],
    tenant_id: int,
):
    personalroom_names = unique_elements(faker.random_int(min=1, max=4), faker.job, other_names)
    for name in personalroom_names:
        PersonalRoom.objects.create(
            owner=owner,
            name=name,
            scheduling_strategy=faker.random_element(strategies),
            default_meeting_configuration=faker.random_element(configurations),
            visibility=ROOM_VISIBILITY_PUBLIC,
        ).tenants.add(tenant_id)
    return personalroom_names


def create_users(
    faker: "Faker",
    other_room_names: Set[str],
    personalrooms: bool,
    homerooms: bool,
    strategies: Collection[SchedulingStrategy],
    configurations: Collection[MeetingConfigurationTemplate],
    tenant_id: int,
):
    room_names = set()
    for username in unique_elements(16, faker.user_name):
        # Don't use faker.email() since they may actually exist
        user = User.objects.create_user(
            username=username,
            first_name=faker.first_name(),
            last_name=faker.last_name(),
            email=username + "@example.org",
            tenant_id=tenant_id,
        )

        if personalrooms:
            room_names |= create_personal_rooms(
                faker, other_room_names | room_names, user, strategies, configurations, tenant_id
            )

        if homerooms:
            homeroom_name = "home-" + user.email
            room_names.add(homeroom_name)
            HomeRoom.objects.create(
                owner=user,
                name=homeroom_name,
                scheduling_strategy=faker.random_element(strategies),
                default_meeting_configuration=faker.random_element(configurations),
                visibility=ROOM_VISIBILITY_PUBLIC,
            ).tenants.add(tenant_id)
    return room_names


def create_admin(faker: "Faker", tenant_id: int):
    User.objects.create_superuser(
        username="admin",
        password="admin",
        first_name=faker.first_name(),
        last_name=faker.last_name(),
        email="admin@example.org",
        tenant_id=tenant_id,
    )


class Command(BaseCommand):
    help = "Add fake data to a tenant."

    def add_arguments(self, parser: ArgumentParser) -> None:
        parser.add_argument(
            "--create",
            action="store_true",
            help="Create the tenant if it does not exist before proceeding.",
        )
        parser.add_argument(
            "--drop",
            action="store_true",
            help="If the tenant exists, remove the corresponding tenant before proceeding."
            " Implies option --create."
            " WARNING: This will remove all entities"
            " and many2many entities relying on this tenant as well.",
        )
        parser.add_argument(
            "--personalrooms",
            action="store_true",
            help="Create (multiple) PersonalRooms for every user.",
        )
        parser.add_argument(
            "--homerooms",
            action="store_true",
            help="Create HomeRooms for every user.",
        )
        parser.add_argument(
            "--admin",
            action="store_true",
            help="Create a superuser with username 'admin' and password 'admin'.",
        )
        parser.add_argument("tenant")

    def handle(
        self,
        tenant: str,
        *args: Any,
        create: bool,
        drop: bool,
        personalrooms: bool,
        homerooms: bool,
        admin: bool,
        **options: Any,
    ) -> None:
        try:
            from faker import Faker
        except ImportError:
            self.stderr.write("Unable to find faker")
            sys.exit(1)

        with transaction.atomic():
            faker = Faker()

            tenant = get_tenant(tenant, create, drop)
            if tenant is None:
                self.stderr.write("Unable to find tenant:" + str(tenant))
                sys.exit(1)

            strategies = create_strategies(faker, tenant.id)

            configurations = get_or_create_configurations(faker)

            room_names = set()
            room_names |= create_rooms(faker, room_names, strategies, configurations, tenant.id)

            if personalrooms:
                general_parameter = GeneralParameter.objects.load(tenant)
                general_parameter.personal_rooms_enabled = True
                general_parameter.save()

            if homerooms:
                general_parameter = GeneralParameter.objects.load(tenant)
                general_parameter.home_room_enabled = True
                general_parameter.home_room_teachers_only = False
                general_parameter.save()

            room_names |= create_users(
                faker, room_names, personalrooms, homerooms, strategies, configurations, tenant.id
            )

            if admin:
                create_admin(faker, tenant.id)
