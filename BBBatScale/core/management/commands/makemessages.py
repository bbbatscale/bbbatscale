import os
import re
import subprocess
import sys
import traceback
from argparse import ArgumentParser
from typing import Any

from django.core.management import CommandError
from django.core.management.commands import makemessages
from django.core.management.utils import find_command


class TrackPoFiles:
    def __init__(self, revert_unchanged: bool, dry_run: bool, ignore_comments: bool):
        self.revert_unchanged = revert_unchanged
        self.dry_run = dry_run
        self.ignore_comments = ignore_comments
        self.po_files = []
        self.changed_file_diffs = set()

        for root, _, files in os.walk(os.getcwd()):
            for file in files:
                if file.endswith(".po"):
                    self.po_files.append(os.path.join(root, file))

    def __enter__(self) -> "TrackPoFiles":
        self.source_commit = subprocess.run(
            ["git", "rev-parse", "HEAD"], text=True, capture_output=True, check=True
        ).stdout.strip()
        git_toplevel = subprocess.run(
            ["git", "rev-parse", "--show-toplevel"], text=True, capture_output=True, check=True
        ).stdout.strip()
        git_prefix = subprocess.run(
            ["git", "rev-parse", "--show-prefix"], text=True, capture_output=True, check=True
        ).stdout.strip()

        changed_files = set(
            subprocess.run(["git", "diff", "--name-only"], text=True, capture_output=True, check=True)
            .stdout.strip()
            .splitlines()
        )
        changed_files |= set(
            subprocess.run(["git", "diff", "--cached", "--name-only"], text=True, capture_output=True, check=True)
            .stdout.strip()
            .splitlines()
        )
        po_file_paths = map(lambda absolute_path: os.path.relpath(absolute_path, git_toplevel), self.po_files)

        changed_po_files = list(
            map(lambda file_path: os.path.relpath(file_path, git_prefix), changed_files.intersection(po_file_paths))
        )

        if changed_po_files:
            subprocess.run(
                ["git", "commit", "--allow-empty-message", "-m", "", "--", *changed_po_files],
                stdout=subprocess.DEVNULL,
                check=True,
            )

        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> bool:
        for file_path in self.po_files:
            has_changed = self.has_file_changed(file_path)

            if has_changed:
                diff = subprocess.run(
                    ["git", "diff", "--no-prefix", "--", file_path], text=True, capture_output=True
                ).stdout.strip()
                self.changed_file_diffs.add(diff)

            if self.dry_run or (self.revert_unchanged and not has_changed):
                # reset file
                subprocess.run(["git", "checkout", "-q", "-f", "--", file_path])

        subprocess.run(
            ["git", "reset", "--soft", self.source_commit],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            check=True,
        )
        return False

    def has_file_changed(self, file_path: str) -> bool:
        creation_timestamp_pattern = re.compile(r'^[+-]"POT-Creation-Date: \d{4}-\d{2}-\d{2} \d{2}:\d{2}\+\d{4}\\n"$')
        comment_line_pattern = re.compile(r"^[+-]#.*$")

        changed_lines = list(
            filter(
                lambda line: (line.startswith("+") or line.startswith("-"))
                and not (line.startswith("+++") or line.startswith("---")),
                subprocess.run(
                    ["git", "diff", "--unified=0", "--", file_path],
                    text=True,
                    capture_output=True,
                    check=True,
                ).stdout.splitlines(),
            )
        )

        for changed_line in changed_lines:
            if not creation_timestamp_pattern.fullmatch(changed_line) and not (
                self.ignore_comments and comment_line_pattern.fullmatch(changed_line)
            ):
                # found a changed line which does not match the creation_timestamp_pattern or a comment - if ignored
                return True

        # no changes (except the creation timestamp or comments - if ignored) have been found
        return False


class Command(makemessages.Command):
    def add_arguments(self, parser: ArgumentParser) -> None:
        super().add_arguments(parser)
        parser.add_argument(
            "--check",
            action="store_true",
            dest="check_changes",
            help="Exit with a non-zero status if the files would change (ignoring the timestamp inside the files)."
            " This uses git to do the job.",
        )
        parser.add_argument(
            "--dry-run",
            action="store_true",
            help="Just show what changes would be made; don't actually write them (persistently)."
            " This uses git to do the job.",
        )
        parser.add_argument(
            "--revert-unchanged",
            action="store_true",
            help="Revert unchanged messages files. This uses git to do the job.",
        )
        parser.add_argument(
            "--ignore-comments",
            action="store_true",
            help="When checking if a file has changed, ignore comment lines (lines starting with a #)."
            " This only applies in conjunction with at least one of the options --revert-unchanged or --check.",
        )

        domain_action = next(filter(lambda action: action.dest == "domain", parser._actions), None)
        if domain_action is not None:
            mutually_exclusive_group = parser.add_mutually_exclusive_group()
            mutually_exclusive_group._group_actions.append(domain_action)
        else:
            mutually_exclusive_group = parser
        mutually_exclusive_group.add_argument(
            "--bbbatscale",
            action="store_true",
            help="Process both domains (django and djangojs) and ignore the 'static/plugins' folder.",
        )

    def handle_bbbatscale_option(self, bbbatscale: bool, args: tuple, options: dict):
        if bbbatscale:
            ignore_patterns = options.get("ignore_patterns", [])
            ignore_patterns.append("static/plugins")
            ignore_patterns.append("static/vue")
            options["ignore_patterns"] = ignore_patterns

            if options["verbosity"] > 0:
                self.stdout.write("processing djangojs")
            super(Command, self).handle(*args, **dict(options, domain="djangojs"))

            if options["verbosity"] > 0:
                self.stdout.write("processing django")
            super(Command, self).handle(*args, **dict(options, domain="django"))
        else:
            super(Command, self).handle(*args, **options)

    def handle_with_tracking(
        self,
        check_changes: bool,
        dry_run: bool,
        revert_unchanged: bool,
        ignore_comments: bool,
        bbbatscale: bool,
        args: tuple,
        options: dict,
    ):
        with TrackPoFiles(revert_unchanged, dry_run, ignore_comments) as handler:
            try:
                self.handle_bbbatscale_option(bbbatscale, args, options)
            except CommandError as exc:
                traceback.print_tb(exc.__traceback__)
                raise exc

        if handler.changed_file_diffs and (check_changes or dry_run):
            if options["verbosity"] > 0:
                if check_changes:
                    output = self.stderr
                else:
                    output = self.stdout

                output.write("")
                output.write("the following files should be updated:")
                for diff in handler.changed_file_diffs:
                    output.write("")
                    output.write(diff)

            if check_changes:
                sys.exit(1)

    def handle(
        self,
        *args: Any,
        check_changes: bool,
        dry_run: bool,
        revert_unchanged: bool,
        ignore_comments: bool,
        bbbatscale: bool,
        **options: Any,
    ) -> None:
        if revert_unchanged or check_changes or dry_run:
            if find_command("git") is None:
                raise CommandError("Can't find git. Make sure you have installed it.")

            # Check if the cwd is inside a git repository and ignore the output,
            # since it is not important if this is actually inside the working tree or in the git dir.
            if (
                subprocess.run(
                    ["git", "rev-parse", "--is-inside-work-tree"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
                ).returncode
                != 0
            ):
                raise CommandError("The current working directory does not seem to belong to a git-repository.")

            self.handle_with_tracking(
                check_changes, dry_run, revert_unchanged, ignore_comments, bbbatscale, args, options
            )
        else:
            self.handle_bbbatscale_option(bbbatscale, args, options)
