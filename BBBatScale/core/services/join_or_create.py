import dataclasses
import json
import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from functools import cached_property
from typing import Any, Callable, Dict, List, Optional, TypeVar, Union, overload
from uuid import UUID

from core import bigbluebutton
from core.constants import (
    MEETING_STATE_CREATING,
    MEETING_STATE_FINISHED,
    MODERATION_MODE_ALL,
    MODERATION_MODE_MODERATORS,
    ROOM_STATE_CREATING,
)
from core.forms import ConfigureMeetingForm
from core.models import GeneralParameter, Meeting, MeetingConfiguration, Room, User
from core.utils import BigBlueButton, has_tenant_based_perm, snakecase_to_camelcase
from crispy_forms.utils import render_crispy_form
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.models import Site
from django.core import signing
from django.db import transaction
from django.utils.timezone import now

logger = logging.getLogger(__name__)

T = TypeVar("T")
JsonType = Union[int, float, str, List["JsonType"], Dict[str, "JsonType"]]


def _passthrough(obj: T) -> T:
    return obj


def _create_token_dataclass_metadata(
    json_key: str,
    *,
    from_json: Callable[[JsonType], T],
    to_json: Callable[[T], JsonType] = _passthrough,
) -> Dict[str, Any]:
    return {
        "json_key": json_key,
        "from_json": from_json,
        "to_json": to_json,
    }


def _validate_bool(variable_name: str) -> Callable[[JsonType], bool]:
    def validate(value: JsonType) -> bool:
        if not isinstance(value, bool):
            raise TypeError(f"Expected {variable_name} to be of type bool but got {type(value)}")

        return value

    return validate


def _deserialize_room(room_id: JsonType) -> Room:
    # bool subclasses int and must therefor be checked explicitly
    if not isinstance(room_id, int) or isinstance(room_id, bool):
        raise TypeError(f"Expected roomId to be of type int but got {type(room_id)}")

    return Room.objects.get(id=room_id)


def _serialize_room(room: Room) -> int:
    return room.id


def _deserialize_meeting(meeting_id: JsonType) -> Optional[Meeting]:
    if meeting_id is None:
        return None

    if not isinstance(meeting_id, str):
        raise TypeError(f"Expected meetingId to be of type str but got {type(meeting_id)}")

    return Meeting.objects.get(id=UUID(meeting_id))


def _serialize_meeting(meeting: Optional[Meeting]) -> Optional[str]:
    if meeting is None:
        return None

    return str(meeting.id)


def _deserialize_bbb_join_parameters(bbb_join_parameters: JsonType) -> bigbluebutton.JoinParameters:
    if not isinstance(bbb_join_parameters, dict):
        raise TypeError(f"Expected bbbJoinParameters to be of type int but got {type(bbb_join_parameters)}")

    bbb_join_parameters = bbb_join_parameters.copy()
    kwargs = dict()

    for field in dataclasses.fields(bigbluebutton.JoinParameters):
        field_camelcase_name = snakecase_to_camelcase(field.name)
        if field_camelcase_name in bbb_join_parameters:
            value = bbb_join_parameters.pop(field_camelcase_name)
            if not isinstance(value, bool):
                raise TypeError(
                    f"Expected bbbJoinParameters.{field_camelcase_name} to be of type int but got"
                    f" {type(bbb_join_parameters)}"
                )
            kwargs[field.name] = value

    if bbb_join_parameters:
        raise ValueError("Unexpected parameters in bbbJoinParameters: " + ", ".join(bbb_join_parameters.keys()))

    return bigbluebutton.JoinParameters(**kwargs)


def _serialize_bbb_join_parameters(bbb_join_parameters: bigbluebutton.JoinParameters) -> JsonType:
    return dict(
        (snakecase_to_camelcase(field.name), getattr(bbb_join_parameters, field.name))
        for field in dataclasses.fields(bbb_join_parameters)
    )


@dataclass(frozen=True)
class CreateJoinToken:
    room: Room = dataclasses.field(
        metadata=_create_token_dataclass_metadata("roomId", from_json=_deserialize_room, to_json=_serialize_room),
    )
    bbb_join_parameters: bigbluebutton.JoinParameters = dataclasses.field(
        metadata=_create_token_dataclass_metadata(
            "bbbJoinParameters", from_json=_deserialize_bbb_join_parameters, to_json=_serialize_bbb_join_parameters
        )
    )
    skip_meeting_configuration: bool = dataclasses.field(
        default=False,
        metadata=_create_token_dataclass_metadata(
            "skipMeetingConfiguration", from_json=_validate_bool("skipMeetingConfiguration")
        ),
    )
    enforce_create_meeting_permission: bool = dataclasses.field(
        default=False,
        metadata=_create_token_dataclass_metadata(
            "enforceCreateMeetingPermission", from_json=_validate_bool("enforceCreateMeetingPermission")
        ),
    )
    enforce_join_meeting_permission: bool = dataclasses.field(
        default=False,
        metadata=_create_token_dataclass_metadata(
            "enforceJoinMeetingPermission", from_json=_validate_bool("enforceJoinMeetingPermission")
        ),
    )
    enforce_moderate_permission: bool = dataclasses.field(
        default=False,
        metadata=_create_token_dataclass_metadata(
            "enforceModeratePermission", from_json=_validate_bool("enforceModeratePermission")
        ),
    )
    join_as_room: bool = dataclasses.field(
        default=False,
        metadata=_create_token_dataclass_metadata("joinAsRoom", from_json=_validate_bool("joinAsRoom")),
    )
    opt_out_create_meeting: bool = dataclasses.field(
        default=False,
        metadata=_create_token_dataclass_metadata(
            "optOutCreateMeeting", from_json=_validate_bool("optOutCreateMeeting")
        ),
    )
    meeting: Optional[Meeting] = dataclasses.field(
        default=None,
        metadata=_create_token_dataclass_metadata(
            "meetingId", from_json=_deserialize_meeting, to_json=_serialize_meeting
        ),
    )
    is_creator: bool = dataclasses.field(
        default=False,
        metadata=_create_token_dataclass_metadata("isCreator", from_json=_validate_bool("isCreator")),
    )
    access_granted: bool = dataclasses.field(
        default=False,
        metadata=_create_token_dataclass_metadata("accessGranted", from_json=_validate_bool("accessGranted")),
    )

    @classmethod
    def from_init_request(
        cls,
        tenant: Site,
        general_parameter: GeneralParameter,
        room_name: str,
        bbb_join_parameters: bigbluebutton.JoinParameters,
        skip_meeting_configuration: Optional[bool],
        secret: Optional[str],
        enforce_create_meeting_permission: Optional[bool],
        enforce_join_meeting_permission: Optional[bool],
        enforce_moderate_permission: Optional[bool],
        join_as_room: Optional[bool],
    ) -> "CreateJoinToken":
        room = Room.objects_available.init(tenant, general_parameter).get(name=room_name)

        kwargs = {"room": room, "bbb_join_parameters": bbb_join_parameters}

        if skip_meeting_configuration is not None:
            kwargs["skip_meeting_configuration"] = skip_meeting_configuration

        if room.direct_join_secret and room.direct_join_secret == secret:
            if enforce_create_meeting_permission is not None:
                kwargs["enforce_create_meeting_permission"] = enforce_create_meeting_permission
            if enforce_join_meeting_permission is not None:
                kwargs["enforce_join_meeting_permission"] = enforce_join_meeting_permission
            if enforce_moderate_permission is not None:
                kwargs["enforce_moderate_permission"] = enforce_moderate_permission
            if join_as_room is not None:
                kwargs["join_as_room"] = join_as_room

        return cls(**kwargs)

    @classmethod
    def from_json_token(cls, json_token: Dict[str, JsonType]) -> "CreateJoinToken":
        kwargs = dict()
        for field in dataclasses.fields(cls):
            json_key = field.metadata["json_key"]
            from_json = field.metadata["from_json"]
            if json_key in json_token:
                json_data = json_token[json_key]
                py_data = from_json(json_data)
                kwargs[field.name] = py_data

            elif field.default is dataclasses.MISSING and field.default_factory is dataclasses.MISSING:
                raise ValueError(f"The token does not contain the required field '{field.name}'.")

        return cls(**kwargs)

    def serialize(self) -> Dict[str, JsonType]:
        data = dict()

        for field in dataclasses.fields(self):
            json_key = field.metadata["json_key"]
            to_json = field.metadata["to_json"]
            data[json_key] = to_json(getattr(self, field.name))

        return data


class Action(ABC):
    def serialize(self) -> Dict[str, JsonType]:
        return {
            "type": "action",
            "action": self._action_type(),
            "data": self._data(),
        }

    @classmethod
    @abstractmethod
    def _action_type(cls) -> str:
        ...

    @abstractmethod
    def _data(self) -> Dict[str, JsonType]:
        ...


class IntermediateAction(Action, ABC):
    @property
    @abstractmethod
    def token(self) -> CreateJoinToken:
        ...

    def serialize(self) -> Dict[str, JsonType]:
        return {
            **super().serialize(),
            "token": signing.dumps(self.token.serialize()),
        }


class TerminalAction(Action, ABC):
    pass


class ConfigureMeetingAction(IntermediateAction):
    @overload
    def __init__(self, token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> None:
        ...

    @overload
    def __init__(self, token: CreateJoinToken, *, form: ConfigureMeetingForm) -> None:
        ...

    def __init__(
        self,
        token: CreateJoinToken,
        tenant: Optional[Site] = None,
        user: Union[None, AnonymousUser, User] = None,
        *,
        form: Optional[ConfigureMeetingForm] = None,
    ) -> None:
        self._token = token

        if form:
            self._form = form
            self._show_form = not form.is_valid()
        elif tenant and user:
            self._form = ConfigureMeetingForm.create(tenant, GeneralParameter.objects.load(tenant), token.room, user)
            self._show_form = False
        else:
            raise ValueError("Either a form must be submitted or a tenant and a user.")

    @classmethod
    def _action_type(cls) -> str:
        return "CONFIGURE_MEETING"

    def _data(self) -> Dict[str, JsonType]:
        return {
            "form": render_crispy_form(self._form, self._form.helper),
            "showForm": self._show_form,
        }

    @property
    def token(self) -> CreateJoinToken:
        return self._token


class WaitUntilConfiguredAction(IntermediateAction):
    def __init__(self, token: CreateJoinToken, can_opt_in: bool = False) -> None:
        self._token = token
        self._can_opt_in = can_opt_in

    @classmethod
    def _action_type(cls) -> str:
        return "WAIT_UNTIL_CONFIGURED"

    def _data(self) -> Dict[str, JsonType]:
        return {
            "canOptIn": self._can_opt_in,
        }

    @property
    def token(self) -> CreateJoinToken:
        return self._token


class WaitUntilRunningAction(IntermediateAction):
    def __init__(
        self,
        token: CreateJoinToken,
        *,
        should_create_meeting: bool = False,
        started_by_someone_else: Optional[bool] = None,
    ) -> None:
        self._token = token
        self._should_recreate_meeting = should_create_meeting
        self._started_by_someone_else = started_by_someone_else

    @classmethod
    def _action_type(cls) -> str:
        return "WAIT_UNTIL_RUNNING"

    def _data(self) -> Dict[str, JsonType]:
        if self._started_by_someone_else is None:
            return dict()

        return {
            "startedBySomeoneElse": self._started_by_someone_else,
        }

    @property
    def token(self) -> CreateJoinToken:
        return self._token

    @property
    def should_create_meeting(self) -> bool:
        return self._should_recreate_meeting


class PromptLogInAction(IntermediateAction):
    def __init__(self, token: CreateJoinToken) -> None:
        self._token = token

    @classmethod
    def _action_type(cls) -> str:
        return "PROMPT_LOG_IN"

    def _data(self) -> Dict[str, JsonType]:
        return dict()

    @property
    def token(self) -> CreateJoinToken:
        return self._token


class PromptAccessCodeAction(IntermediateAction):
    def __init__(self, token: CreateJoinToken, invalid: bool = False) -> None:
        self._token = token
        self._invalid = invalid

    @classmethod
    def _action_type(cls) -> str:
        return "PROMPT_ACCESS_CODE"

    def _data(self) -> Dict[str, JsonType]:
        return {
            "invalid": self._invalid,
        }

    @property
    def token(self) -> CreateJoinToken:
        return self._token


class PromptJoinNameAction(IntermediateAction):
    def __init__(self, token: CreateJoinToken, join_name: str = "", *, name_is_too_short: bool = False) -> None:
        self._token = token
        self._join_name = join_name
        self._name_is_too_short = name_is_too_short

    @classmethod
    def _action_type(cls) -> str:
        return "PROMPT_JOIN_NAME"

    def _data(self) -> Dict[str, JsonType]:
        return {
            "joinName": self._join_name,
            "nameIsTooShort": self._name_is_too_short,
        }

    @property
    def token(self) -> CreateJoinToken:
        return self._token


class DisplayMeetingEndedAction(TerminalAction):
    @classmethod
    def _action_type(cls) -> str:
        return "DISPLAY_MEETING_HAS_ENDED"

    def _data(self) -> Dict[str, JsonType]:
        return dict()


class RedirectAction(TerminalAction):
    def __init__(
        self, meeting: Meeting, bbb_join_parameters: bigbluebutton.JoinParameters, is_moderator: bool, join_name: str
    ) -> None:
        self._meeting = meeting
        self._bbb_join_parameters = bbb_join_parameters
        self._is_moderator = is_moderator
        self._join_name = join_name

    @cached_property
    def join_url(self) -> str:
        # TODO move into BigBlueButton class
        join_parameter = {
            "meetingID": str(self._meeting.id),
            "password": self._meeting.moderator_pw if self._is_moderator else self._meeting.attendee_pw,
            "fullName": self._join_name,
            "redirect": "true",
        }

        for field in dataclasses.fields(self._bbb_join_parameters):
            join_parameter[f"userdata-bbb_{field.name}"] = json.dumps(getattr(self._bbb_join_parameters, field.name))

        return BigBlueButton(self._meeting.server.dns, self._meeting.server.shared_secret).join(join_parameter)

    @classmethod
    def _action_type(cls) -> str:
        return "REDIRECT"

    def _data(self) -> Dict[str, JsonType]:
        return {
            "joinUrl": self.join_url,
        }


def can_user_create_meeting(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> bool:
    if token.enforce_create_meeting_permission:
        return True

    # TODO: Change to `has_tenant_based_perm`
    if token.room.default_meeting_configuration.everyone_can_start:
        return True

    if user.is_anonymous:
        return False

    if user.is_superuser:
        return True

    if hasattr(token.room, "homeroom"):
        return token.room.homeroom.owner == user

    if hasattr(token.room, "personalroom"):
        return token.room.personalroom.owner == user or token.room.personalroom.has_co_owner(user)

    if user.tenant in token.room.tenants.all():
        if token.room.default_meeting_configuration.authenticated_user_can_start:
            return True

        if has_tenant_based_perm(user, "core.create_meeting", tenant):
            return True

    return False


def can_user_skip_join_preconditions(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> bool:
    if token.enforce_join_meeting_permission:
        return True

    if token.is_creator:
        return True

    if has_tenant_based_perm(user, "core.join_meeting", tenant):
        return True

    return False


def is_user_moderator(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> bool:
    meeting_configuration: Optional[MeetingConfiguration] = getattr(token.meeting, "configuration", None)
    if meeting_configuration is None:
        meta_info = [f"room.id={token.room.id}"]
        if token.meeting is not None:
            meta_info.append(f"meeting.id={token.meeting.id}")
        logger.error(
            f"Retrieved a token without a meeting and/or meeting configuration attached ({', '.join(meta_info)})."
        )
        return False

    if token.enforce_moderate_permission:
        return True

    if token.is_creator:
        return True

    # TODO: Change to `has_tenant_based_perm`
    if user.is_superuser:
        return True

    if meeting_configuration.moderation_mode == MODERATION_MODE_ALL:
        return True

    if meeting_configuration.moderation_mode == MODERATION_MODE_MODERATORS and has_tenant_based_perm(
        user, "core.moderate", tenant
    ):
        return True

    return False


def create_meeting(
    room: Room,
    tenant: Site,
    creator: Union[AnonymousUser, User],
    *,
    configuration: Optional[MeetingConfiguration] = None,
) -> Meeting:
    if configuration is None:
        configuration = room.default_meeting_configuration.instantiate()

    if creator.is_anonymous:
        creator_user = None
        creator_name = ""
    else:
        creator_user = creator
        creator_name = str(creator)

    meeting = Meeting.objects.create(
        room=room,
        room_name=room.name,
        configuration=configuration,
        creator=creator_user,
        creator_name=creator_name,
        state=MEETING_STATE_CREATING,
    )
    meeting.tenants.add(tenant)

    room.state = ROOM_STATE_CREATING
    room.last_running = now()
    room.save(update_fields=["state", "last_running"])

    return meeting


@transaction.atomic
def get_next_action(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    return _choice_token_has_meeting(token, tenant, user)


def _choice_token_has_meeting(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    if token.meeting is None:
        return _choice_room_has_unfinished_meeting(token, tenant, user)
    else:
        token.meeting.refresh_from_db(fields=["state"])

        return _choice_is_meeting_finished(token, tenant, user)


def _choice_room_has_unfinished_meeting(
    token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]
) -> Action:
    unfinished_meeting = token.room.get_unfinished_meeting()
    if unfinished_meeting is None:
        return _choice_user_has_create_permission(token, tenant, user)
    else:
        is_creator = unfinished_meeting.creator is not None and unfinished_meeting.creator == user
        token = dataclasses.replace(token, meeting=unfinished_meeting, is_creator=is_creator)

        return _choice_is_meeting_creating(token, tenant, user)


def _choice_is_meeting_finished(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    if token.meeting.state == MEETING_STATE_FINISHED:
        return DisplayMeetingEndedAction()
    else:
        return _choice_is_meeting_creating(token, tenant, user)


def _choice_user_has_create_permission(
    token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]
) -> Action:
    if can_user_create_meeting(token, tenant, user):
        return _choice_opt_out_create_meeting(token, tenant, user)
    else:
        return WaitUntilConfiguredAction(token)


def _choice_opt_out_create_meeting(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    if token.opt_out_create_meeting:
        return WaitUntilConfiguredAction(token, can_opt_in=True)
    else:
        return _choice_skip_meeting_configuration(token, tenant, user)


def _choice_is_meeting_creating(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    if token.meeting.state == MEETING_STATE_CREATING:
        return WaitUntilRunningAction(token)
    else:
        return _choice_can_skip_join_preconditions(token, tenant, user)


def _choice_can_skip_join_preconditions(
    token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]
) -> Action:
    if can_user_skip_join_preconditions(token, tenant, user) or token.is_creator:
        return _choice_join_as_room(token, tenant, user)
    else:
        return _choice_allow_guests(token, tenant, user)


def _choice_skip_meeting_configuration(
    token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]
) -> Action:
    if token.skip_meeting_configuration:
        meeting = create_meeting(token.room, tenant, user)
        token = dataclasses.replace(token, meeting=meeting, is_creator=True)
        return WaitUntilRunningAction(token, should_create_meeting=True)
    else:
        return ConfigureMeetingAction(token, tenant, user)


def _choice_allow_guests(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    if token.meeting.configuration.allow_guest_entry:
        return _choice_has_access_code(token, tenant, user)
    else:
        if user.is_authenticated:
            return _choice_has_access_code(token, tenant, user)
        else:
            return PromptLogInAction(token)


def _choice_has_access_code(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    if token.meeting.configuration.access_code:
        return _choice_guest_only_access_code(token, tenant, user)
    else:
        return _choice_join_as_room(token, tenant, user)


def _choice_guest_only_access_code(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    if token.meeting.configuration.only_prompt_guests_for_access_code:
        if user.is_authenticated:
            return _choice_join_as_room(token, tenant, user)
        else:
            return _choice_access_granted(token, tenant, user)
    else:
        return _choice_access_granted(token, tenant, user)


def _choice_access_granted(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    if token.access_granted:
        return _choice_join_as_room(token, tenant, user)
    else:
        return PromptAccessCodeAction(token)


def _choice_join_as_room(token: CreateJoinToken, tenant: Site, user: Union[AnonymousUser, User]) -> Action:
    if token.join_as_room:
        is_moderator = is_user_moderator(token, tenant, user)
        return RedirectAction(token.meeting, token.bbb_join_parameters, is_moderator, token.room.name)
    else:
        if user.is_authenticated:
            is_moderator = is_user_moderator(token, tenant, user)
            return RedirectAction(token.meeting, token.bbb_join_parameters, is_moderator, str(user))
        else:
            return PromptJoinNameAction(token)
