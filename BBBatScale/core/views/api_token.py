import logging

from core.decorators import tenant_based_permission_required
from core.forms import ApiTokenForm
from core.models import ApiToken
from core.utils import get_general_parameter, paginate
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_apitoken", elevate_staff=False, raise_exception=True)
def token_overview(request):
    return render(request, "api_token_overview.html")


@login_required
@tenant_based_permission_required("core.add_apitoken", elevate_staff=False, raise_exception=True)
def token_create(request):
    form = ApiTokenForm(request.POST or None, general_parameter=get_general_parameter(request))
    if request.method == "POST" and form.is_valid():
        form.save()
        messages.success(request, _("Token successfully created"))
        return redirect("token_overview")
    return render(request, "api_token_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_apitoken", elevate_staff=False, raise_exception=True)
def token_delete(request, token):
    instance = get_object_or_404(ApiToken, pk=token)
    instance.delete()
    messages.success(request, _("Token successfully deleted"))
    return redirect("token_overview")


@login_required
@tenant_based_permission_required(
    ["core.view_apitoken", "core.change_apitoken"], elevate_staff=False, raise_exception=True
)
def token_update(request, token):
    instance = get_object_or_404(ApiToken, pk=token)
    form = ApiTokenForm(request.POST or None, instance=instance, general_parameter=get_general_parameter(request))
    if request.method == "POST" and form.is_valid():
        form.save()
        messages.success(request, _("Token successfully updated"))
        return redirect("token_overview")
    return render(request, "api_token_create.html", {"form": form})


def map_token(token: ApiToken) -> dict:
    return {
        "updateUrl": reverse("token_update", args=[token.id]),
        "deleteUrl": reverse("token_delete", args=[token.id]),
        "apiUrl": reverse("bbb_api_initial_request", args=[token.slug]),
        "name": token.name,
        "secret": token.secret,
        "tenants": list(token.tenants.all().values_list("name", flat=True)),
        "slug": token.slug,
        "schedulingStrategy": token.scheduling_strategy.name if token.scheduling_strategy else None,
    }


@login_required
@tenant_based_permission_required("core.view_apitoken", elevate_staff=False, raise_exception=True)
def get_tokens(request: HttpRequest) -> HttpResponse:
    query = ApiToken.objects.order_by("name")

    return paginate(query, int(request.GET.get("page", 1)), map_token)
