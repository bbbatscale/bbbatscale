import logging

from core.decorators import tenant_based_permission_required
from core.forms import GeneralParametersForm
from core.models import GeneralParameter
from core.utils import get_general_parameter
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.html import format_html, format_html_join
from django.utils.translation import gettext_lazy as _
from django.views.generic import UpdateView

logger = logging.getLogger(__name__)


class _BaseGeneralParameterUpdateView(UpdateView):
    model = GeneralParameter
    form_class = GeneralParametersForm
    template_name = "settings.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(requesting_user=self.request.user)
        return kwargs

    def form_valid(self, form: GeneralParametersForm) -> HttpResponse:
        messages.success(self.request, _("Settings successfully saved."))
        return super().form_valid(form)

    @classmethod
    def _errors_as_ul(cls, form: GeneralParametersForm):
        if not form.errors:
            return ""

        errors = ((form.fields[field].label, error_list) for field, error_list in form.errors.items())

        return format_html('<ul class="errorlist">{}</ul>', format_html_join("", "<li>{}:{}</li>", errors))

    def form_invalid(self, form: GeneralParametersForm) -> HttpResponse:
        messages.error(
            self.request,
            format_html(
                "{}{}", _("The settings could not be saved. Please revise your input:"), self._errors_as_ul(form)
            ),
        )
        return super().form_invalid(form)


@method_decorator(
    [
        login_required,
        tenant_based_permission_required(
            ["core.view_generalparameter", "core.change_generalparameter"], raise_exception=True
        ),
    ],
    "dispatch",
)
class GeneralParameterUpdateView(_BaseGeneralParameterUpdateView):
    success_url = reverse_lazy("update_general_parameter")

    def get_object(self, queryset=None) -> GeneralParameter:
        return get_general_parameter(self.request)


@method_decorator(
    [
        login_required,
        tenant_based_permission_required(
            ["core.view_generalparameter", "core.change_generalparameter"], elevate_staff=False, raise_exception=True
        ),
    ],
    "dispatch",
)
class AdminGeneralParameterUpdateView(_BaseGeneralParameterUpdateView):
    success_url = reverse_lazy("tenant_overview")

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data["tenant_domain"] = context_data["object"].tenant.domain
        return context_data
