import binascii
import hmac
import json
import logging
from datetime import datetime
from typing import Dict, List

import xmltodict
from core.constants import MEETING_STATE_CREATING, MEETING_STATE_RUNNING, ROOM_STATE_ACTIVE
from core.models import ApiToken, ExternalMeeting, Meeting, Room, Server, User
from core.services import (
    RegistrationDetails,
    ServerStats,
    apitoken_agent_config_selector,
    collect_get_meetings_items,
    create_getrecordings_response_xml,
    delete_recordings,
    end_meeting,
    get_additional_recording_types,
    get_interval,
    meeting_create,
    parse_dict_to_xml,
    register_server,
    select_registration_interval,
    select_stats_interval,
    sync_server_stats,
    update_publish_recordings,
    validate_bbb_api_call,
)
from core.utils import BigBlueButton, RecordingsGRPC, get_general_parameter, get_tenant
from core.views import create_view_access_logging_message
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db.transaction import atomic, non_atomic_requests
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.utils.timezone import now
from django.views.decorators.csrf import csrf_exempt

logger = logging.getLogger(__name__)


@csrf_exempt
def callback_bbb(request):
    logger.info(create_view_access_logging_message(request))

    logger.info("start processing bbb callback request " + request.POST["event"])

    token = request.META["HTTP_AUTHORIZATION"].split(" ")[1]
    event = json.loads(request.POST["event"])[0]["data"]
    domain = request.POST["domain"]
    event_id = event["id"]
    server = Server.objects.get(dns=domain)
    if hmac.compare_digest(token.encode("UTF-8"), server.shared_secret.encode("UTF-8")):
        logger.debug("token validation passed.")
        if event_id in ["meeting-created", "meeting-ended"]:
            meeting_id = event["attributes"]["meeting"]["external-meeting-id"]
            logger.debug("event %s for meeting (id=%s, domain=%s)" % (event_id, meeting_id, domain))
            try:
                meeting = Meeting.objects.filter(id=meeting_id).first()
            except ValidationError:
                logger.debug("ValidationError %s is not a valid UUID" % meeting_id)
                return JsonResponse({"status": "ok"})
            logger.debug("meeting found for meeting %s with querried meeting_id %s" % (meeting, meeting_id))
            if (
                event_id == "meeting-created"
                and "metadata" in event["attributes"]["meeting"]
                and "bbb-origin" in event["attributes"]["meeting"]["metadata"]
                and event["attributes"]["meeting"]["metadata"]["bbb-origin"] == "BBB@Scale"
            ):
                logger.debug("meeting-created webhook processing started for meeting_id: %s" % meeting_id)
                breakout = event["attributes"]["meeting"]["is-breakout"]
                logger.debug("meeting is breakout %s" % str(breakout))
                if breakout:
                    parent_meeting_pk = event["attributes"]["meeting"]["metadata"]["roomsmeetingid"]
                    logger.debug(
                        "bbb breakout room created (domain=%s, parent meeting_pk=%s)" % (domain, parent_meeting_pk)
                    )
                    parent = Meeting.objects.get(pk=int(parent_meeting_pk))
                    meeting = Meeting.objects.create(
                        room=parent.room,
                        room_name=event["attributes"]["meeting"]["name"],
                        is_breakout=breakout,
                        server=parent.server,
                        meeting_configuration=parent.configuration,
                        state=MEETING_STATE_RUNNING,
                        creator_id=parent.creator_id,
                        creator_name=parent.creator_name,
                        bbb_meeting_id=event["attributes"]["meeting"]["internal-meeting-id"],
                        moderator_pw=event["attributes"]["meeting"]["moderator-pass"],
                        attendee_pw=event["attributes"]["meeting"]["viewer-pass"],
                    )
                    meeting.tenants.add(*parent.tenants.all())
                else:
                    logger.debug("meeting is not a breakout meeting (%s)" % meeting)
                    if meeting:
                        logger.debug("bbb meeting-created on (domain=%s, meeting_id=%s)" % (domain, meeting_id))
                        meeting.server = server
                        meeting.state = MEETING_STATE_RUNNING
                        meeting.bbb_meeting_id = event["attributes"]["meeting"]["internal-meeting-id"]
                        meeting.last_health_check = now()
                        meeting.save()
                        if meeting.room:
                            Room.objects.filter(pk=meeting.room.pk).update(state=ROOM_STATE_ACTIVE, last_running=now())
                            logger.debug(
                                "room (%s) for meeting %s updated state to active" % (meeting.room.name, meeting_id)
                            )
                        logger.debug("meeting %s updated (server=%s)" % (meeting_id, server.dns))
            if event_id == "meeting-ended" and meeting:
                logger.debug(
                    "meeting ended (domain=%s, id=%s, name=%s)"
                    % (domain, meeting_id, meeting.room if meeting.room else meeting.room_name)
                )
                end_meeting(meeting)
    logger.info("finished processing bbb callback request")
    return JsonResponse({"status": "ok"})


@csrf_exempt
def recording_callback(request):
    logger.info(create_view_access_logging_message(request))
    gp = get_general_parameter(request)
    data = json.loads(request.body)
    if hmac.compare_digest(data["token"].encode("UTF-8"), settings.RECORDINGS_SECRET.encode("UTF-8")):
        if Meeting.objects.filter(pk=data["rooms_meeting_id"]).update(
            replay_id=data["bbb_meeting_id"],
            replay_url=data.get("bbb_recordings_url", gp.playback_url + data["bbb_meeting_id"]),
        ):
            try:
                meeting: Meeting = Meeting.objects.get(pk=data["rooms_meeting_id"])
                rpc: RecordingsGRPC = RecordingsGRPC(gp.recording_management_url, gp.recording_key, gp.recording_cert)
                recman_recording_types: List[str] = get_additional_recording_types(rpc, data["bbb_meeting_id"])
                meeting.update_available_format_urls(recman_recording_types)
            except Meeting.DoesNotExist:
                logger.error(f'No recording/meeting found for {data["rooms_meeting_id"]}')
                return HttpResponse(status=400)
            except Exception as e:
                logger.error(f"Unexpected error during managing formats of {data['rooms_meeting_id']}: {str(e)}")
            finally:
                del rpc
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=401)


@csrf_exempt
def delete_user_callback(request):
    logger.info(create_view_access_logging_message(request))
    gp = get_general_parameter(request)
    tenant = gp.tenant
    data = json.loads(request.body)
    token = data.get("token", None)
    username = data.get("username", None)
    if not token or not username:
        logger.debug("Request does not contain token or username data")
        return HttpResponse(status=400)
    if hmac.compare_digest(token.encode("UTF-8"), gp.user_secrete.encode("UTF-8")):
        try:
            user = User.objects.get(username=username, tenant=tenant)
            user.delete()
            logger.info(f"User with username '{username}' sucessfully deleted")
            return HttpResponse(status=200)
        except User.DoesNotExist:
            logger.debug(f"User '{username}' not found.")
            return HttpResponse(status=404)
    else:
        return HttpResponse(status=401)


class AgentVerificationException(Exception):
    def __init__(self, resp: HttpResponse, *args: object) -> None:
        super().__init__(*args)
        self.response = resp


def verify_agent(request):  # noqa C901
    if request.method != "POST":
        raise AgentVerificationException(HttpResponseNotAllowed(["POST"]))
    if request.headers.get("Content-Type") != "application/json":
        raise AgentVerificationException(HttpResponse(status=415))
    if request.headers.get("X-Bbbatscale-Experimental-Api") != "iacceptmyfate":
        raise AgentVerificationException(HttpResponseBadRequest())

    # The request authentication scheme is as for webhooks with the added twist
    # that the MAC key is tenant-specific und thus needs to be looked up first
    # using data from the request.
    # The lookup key is the tenant name transmitted in the X-Bbbatscale-Tenant
    # header.
    sig = request.headers.get("X-Request-Signature")
    if not sig:
        raise AgentVerificationException(HttpResponseBadRequest())
    token_slug = request.headers.get("X-Bbbatscale-Tenant")
    if not token_slug:
        raise AgentVerificationException(HttpResponseBadRequest())
    try:
        timestamp, hex_mac = map(
            lambda x: x[1],
            sorted(filter(lambda x: x[0] in ["t", "v1"], map(lambda x: x.split("=", 1), sig.split(",")))),
        )
        timestamp_int = int(timestamp)
        request_mac = binascii.unhexlify(hex_mac)
        communication_token = ApiToken.objects.get(slug=token_slug)
    except binascii.Error as e:
        logger.warning("received mis-encoded message authentication code: %s (X-Request-Signature: %s)", e, sig)
        raise AgentVerificationException(HttpResponseBadRequest())
    except (ValueError, KeyError) as e:
        logger.info("received bogus X-Request-Signature header: %s (value: %s)", e, sig)
        raise AgentVerificationException(HttpResponseBadRequest())
    except ApiToken.DoesNotExist:
        logger.warning("received server registration request for non-existent tenant %s", token_slug)
        raise AgentVerificationException(HttpResponse(status=403))

    mac_key = binascii.unhexlify(communication_token.secret)
    msg = b"%s.%s" % (timestamp.encode("UTF-8"), request.body)
    expect_mac = hmac.digest(mac_key, msg, "SHA512")
    if not hmac.compare_digest(request_mac, expect_mac):
        raise AgentVerificationException(HttpResponse(status=403))

    # Allow for up to an hour of clock drift
    if abs(datetime.now().timestamp() - timestamp_int) > 3600:
        raise AgentVerificationException(HttpResponse(status=403))

    return communication_token


@csrf_exempt
def api_server_registration(request):
    # When reaching this point, request.body was successfully authenticated
    # using the shared secret configured for tenant.
    try:
        communication_token = verify_agent(request)
    except AgentVerificationException as e:
        return e.response

    data = json.loads(request.body)

    server_created = register_server(RegistrationDetails.from_api(communication_token, data))

    interval = get_interval(apitoken_agent_config_selector(communication_token), select_registration_interval)

    return JsonResponse(
        data={"interval": interval.total_seconds() if interval else None, "created": server_created},
        status=200,
    )


@csrf_exempt
def api_sync_servers_stats(request):
    try:
        communication_token = verify_agent(request)
    except AgentVerificationException as e:
        return e.response

    payload: Dict = json.loads(request.body)
    server_sync = ServerStats.from_api(payload)

    server = get_object_or_404(Server, dns=server_sync.dns)

    sync_server_stats(server, server_sync.meeting_stats)
    interval = get_interval(apitoken_agent_config_selector(communication_token), select_stats_interval)

    return JsonResponse(data={"interval": interval.total_seconds() if interval else None}, status=200)


def bbb_initialization_request(request, slug):
    logger.info(create_view_access_logging_message(request, slug))
    try:
        ApiToken.objects.get(slug=slug)
    except ApiToken.DoesNotExist:
        return bbb_xml_response("FAILED", "unknownResource", "Your requested resource is not available.")
    return HttpResponse(
        xmltodict.unparse({"response": {"returncode": "SUCCESS", "version": "2.0"}}), content_type="text/xml"
    )


@csrf_exempt
@non_atomic_requests
def bbb_api_create(request, slug):
    logger.info(create_view_access_logging_message(request, slug))
    body = request.body if request.method == "POST" else None
    # Create Meeting and return XML to moodle
    token = ApiToken.objects.get(slug=slug)
    params = request.GET.copy()
    if not validate_bbb_api_call("create", params, slug):
        return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")
    with atomic(durable=True):
        external_meeting, created = ExternalMeeting.objects.get_or_create(
            external_meeting_id=params["meetingID"], defaults={"name": "{}-{}".format(token.name, params["name"])}
        )
        if external_meeting.meeting_set.filter(state__in=[MEETING_STATE_RUNNING, MEETING_STATE_CREATING]).exists():
            logger.debug(f"Meeting for external meetingID: {params['meetingID']} already exists.")
            return bbb_xml_response(
                "SUCCESS",
                "duplicateWarning",
                "This conference was already in existence and may currently be in progress.",
            )
        meeting = Meeting.objects.create(
            room_name=params["name"],
            attendee_pw=params["attendeePW"],
            moderator_pw=params["moderatorPW"],
            state=MEETING_STATE_CREATING,
            creator_name=token.name,
            external_meeting=external_meeting,
        )
        meeting.tenants.add(get_tenant(request))
    response = meeting_create(request, params, token.scheduling_strategy, meeting, body).text
    logger.debug(f"Creating meeting for {meeting.id}.")
    return HttpResponse(response, content_type="text/xml")


def bbb_api_join(request, slug):
    logger.info(create_view_access_logging_message(request, slug))

    # Create Join URL and return to Moodle
    params = request.GET.copy()
    if validate_bbb_api_call("join", params, slug):
        try:
            external_meeting = ExternalMeeting.objects.get(external_meeting_id=params["meetingID"])
            meeting = external_meeting.meeting_set.filter(state=MEETING_STATE_RUNNING).first()
            if meeting:
                bbb = BigBlueButton(meeting.server.dns, meeting.server.shared_secret)
                params.update({"meetingID": meeting.id})
                return redirect(bbb.join(params))
            logger.debug(f"No running meeting for {params['meetingID']} found.")
            return bbb_xml_response(
                "FAILED",
                "notFound",
                "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?",
            )
        except ExternalMeeting.DoesNotExist:
            pass
        logger.debug(f"No meeting for {params['meetingID']} found.")
        return bbb_xml_response(
            "FAILED",
            "notFound",
            "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?",
        )
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_end(request, slug):
    logger.info(create_view_access_logging_message(request, slug))
    # End Meeting
    params = request.GET.copy()
    if validate_bbb_api_call("end", params, slug):
        try:
            external_meeting = ExternalMeeting.objects.get(external_meeting_id=params["meetingID"])
            meeting = external_meeting.meeting_set.filter(state=MEETING_STATE_RUNNING).first()
            if meeting:
                bbb = BigBlueButton(meeting.server.dns, meeting.server.shared_secret)
                response = bbb.end(meeting.id, meeting.moderator_pw)
                logger.debug(f"Meeting {params['meetingID']} ended with api call.")
                return HttpResponse(response.text, content_type="text/xml")
        except ExternalMeeting.DoesNotExist:
            pass
        logger.debug(f"No running meeting for {params['meetingID']} found.")
        return bbb_xml_response(
            "FAILED",
            "notFound",
            "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?",
        )
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_is_meeting_running(request, slug):
    logger.info(create_view_access_logging_message(request, slug))
    # Is meeting running
    params = request.GET.copy()
    if validate_bbb_api_call("isMeetingRunning", params, slug):
        try:
            external_meeting = ExternalMeeting.objects.get(external_meeting_id=params["meetingID"])
            meeting = external_meeting.meeting_set.filter(state=MEETING_STATE_RUNNING).first()
            if meeting:
                bbb = BigBlueButton(meeting.server.dns, meeting.server.shared_secret)
                response = bbb.is_meeting_running(meeting.id)
                return HttpResponse(response.text, content_type="text/xml")
        except ExternalMeeting.DoesNotExist:
            pass
        logger.debug(f"No ExternalMeeting found for {params['meetingID']}")
        return bbb_xml_meeting_running_false()
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_get_meeting_info(request, slug):
    logger.info(create_view_access_logging_message(request, slug))
    # Get Meeting Infos and return XML

    params = request.GET.copy()
    if validate_bbb_api_call("getMeetingInfo", params, slug):
        try:
            external_meeting = ExternalMeeting.objects.get(external_meeting_id=params["meetingID"])
            meeting = external_meeting.meeting_set.filter(state=MEETING_STATE_RUNNING).first()
            if meeting:
                bbb = BigBlueButton(meeting.server.dns, meeting.server.shared_secret)
                response = bbb.get_meeting_infos(meeting.id)
                return HttpResponse(response.text, content_type="text/xml")
        except ExternalMeeting.DoesNotExist:
            pass
        logger.debug(f"No running meeting for {params['meetingID']} found.")
        return bbb_xml_response("FAILED", "notFound", "We could not find a meeting with that meeting ID")
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_get_meetings(request, slug):
    params = request.GET.copy()

    if not validate_bbb_api_call("getMeetings", params, slug):
        return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")

    running_meetings = Meeting.objects.filter(tenants__in=[get_tenant(request)], state=MEETING_STATE_RUNNING)

    get_meetings_items = collect_get_meetings_items(running_meetings)
    if not get_meetings_items:
        return bbb_xml_response("SUCCESS", "noMeetings", "no meetings were found on this server")

    return bbb_xml_response("SUCCESS", None, None, meetings={"meeting": get_meetings_items})


def bbb_api_get_recordings(request, slug):
    logger.info(create_view_access_logging_message(request, slug))

    params = request.GET.copy()
    if validate_bbb_api_call("getRecordings", params, slug):
        if "meetingID" not in params and "recordID" not in params:
            return bbb_xml_response(
                "FAILED", "missingParameters", "param meetingID or recordID must be included.", recordings=None
            )
        if "recordID" in params:
            meetings = Meeting.objects.filter(replay_id__in=params["recordID"].split(","))
            if len(meetings) >= 1:
                recording_response = create_getrecordings_response_xml(meetings)
                if recording_response:
                    return HttpResponse(parse_dict_to_xml(recording_response), content_type="text/xml")
        elif "meetingID" in params:
            meetings = Meeting.objects.filter(external_meeting__external_meeting_id__in=params["meetingID"].split(","))
            if len(meetings) >= 1:
                recording_response = create_getrecordings_response_xml(meetings)
                if recording_response:
                    return HttpResponse(parse_dict_to_xml(recording_response), content_type="text/xml")
        return bbb_xml_response(
            "SUCCESS", "noRecordings", "There are no recordings for the meeting(s).", recordings=None
        )
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_publish_recordings(request, slug):
    logger.info(create_view_access_logging_message(request, slug))

    params = request.GET.copy()
    if not validate_bbb_api_call("publishRecordings", params, slug):
        return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")
    if "recordID" not in params:
        return bbb_xml_response("FAILED", "missingParamRecordID", "You must specify a recordID.")
    if "publish" not in params:
        return bbb_xml_response("FAILED", "missingParamPublish", "You must specify a publish value true or false.")

    gp = get_general_parameter(request)
    if not gp.recording_management_url or not gp.recording_key or not gp.recording_cert:
        return bbb_xml_response("FAILED", "missingParameters", "recording grpc authentication parameter not set")
    rpc: RecordingsGRPC = RecordingsGRPC(gp.recording_management_url, gp.recording_key, gp.recording_cert)
    try:
        update_publish_recordings(params, rpc, False)
        response = bbb_xml_response("SUCCESS", None, None, published=True if params["publish"] == "true" else False)
    except ObjectDoesNotExist:
        response = bbb_xml_response("FAILED", "notFound", "We could not find recordings")
    except Exception as e:
        response = bbb_xml_response("FAILED", "internalError", str(e))
    finally:
        del rpc
    return response


def bbb_api_update_recordings(request, slug):
    logger.info(create_view_access_logging_message(request, slug))

    params = request.GET.copy()
    if not validate_bbb_api_call("updateRecordings", params, slug):
        return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")
    if "recordID" not in params:
        return bbb_xml_response("FAILED", "missingParamRecordID", "You must specify a recordID.")

    gp = get_general_parameter(request)
    if not gp.recording_management_url or not gp.recording_key or not gp.recording_cert:
        return bbb_xml_response("FAILED", "missingParameters", "recording grpc authentication parameter not set")
    rpc: RecordingsGRPC = RecordingsGRPC(gp.recording_management_url, gp.recording_key, gp.recording_cert)

    try:
        update_publish_recordings(params, rpc, True)
        response = bbb_xml_response("SUCCESS", None, None, updated=True)
    except ObjectDoesNotExist:
        response = bbb_xml_response("FAILED", "notFound", "We could not find recordings")
    except Exception as e:
        response = bbb_xml_response("FAILED", "internalError", str(e))
    finally:
        del rpc
    return response


def bbb_api_delete_recordings(request, slug):
    logger.info(create_view_access_logging_message(request, slug))

    params = request.GET.copy()
    if not validate_bbb_api_call("deleteRecordings", params, slug):
        return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")
    if "recordID" not in params:
        return bbb_xml_response("FAILED", "missingParamRecordID", "You must specify a recordID.")

    gp = get_general_parameter(request)
    if not gp.recording_management_url or not gp.recording_key or not gp.recording_cert:
        return bbb_xml_response("FAILED", "missingParameters", "recording grpc authentication parameter not set")
    rpc: RecordingsGRPC = RecordingsGRPC(gp.recording_management_url, gp.recording_key, gp.recording_cert)
    try:
        delete_recordings(params["recordID"], rpc)
        response = bbb_xml_response("SUCCESS", None, None, deleted=True)
    except ObjectDoesNotExist:
        response = bbb_xml_response("FAILED", "notFound", "We could not find recordings")
    except Exception as e:
        response = bbb_xml_response("FAILED", "internalError", str(e))
    finally:
        del rpc
    return response


def translate_bbb_meta_data(metadata):
    try:
        logger.debug(f"Translation of metadata: {metadata}")
        return {
            "mute_on_start": metadata["muteonstart"] == "True",
            "moderation_mode": metadata["moderationmode"],
            "guest_policy": metadata["guestpolicy"],
            "meetingLayout": metadata["meetinglayout"],
            "allow_guest_entry": metadata["allowguestentry"] == "True",
            "access_code": metadata["accesscode"] if metadata["accesscode"] != "None" else None,
            "only_prompt_guests_for_access_code": metadata["onlypromptguestsforaccesscode"] == "True",
            "disable_cam": metadata["disablecam"] == "True",
            "disable_mic": metadata["disablemic"] == "True",
            "disable_note": metadata["disablenote"] == "True",
            "disable_private_chat": metadata["disableprivatechat"] == "True",
            "disable_public_chat": metadata["disablepublicchat"] == "True",
            "allow_unmuteusers": metadata["allow_unmuteusers"] == "True",
            "allow_ejectcam": metadata["allow_ejectcam"] == "True",
            "allow_learningdashboard": metadata["allow_learningdashboard"] == "True",
            "allow_recording": metadata["allowrecording"] == "True",
            "logoutUrl": metadata["logouturl"],
            "maxParticipants": metadata["maxparticipants"] if metadata["maxparticipants"] != "None" else None,
            "streamingUrl": metadata["streamingurl"] if metadata["streamingurl"] != "None" else None,
        }
    except KeyError as e:
        logger.warning(f"Key error while translating metadata: {str(e)}")
        return {}


def bbb_xml_response(returncode, messageKey, message, **kwargs):
    dict = {
        "response": {
            "returncode": returncode,
            **kwargs,
        }
    }
    if messageKey:
        dict["response"]["messageKey"] = messageKey
    if message:
        dict["response"]["message"] = message
    return HttpResponse(xmltodict.unparse(dict), content_type="text/xml")


def bbb_xml_meeting_running_false(running=False):
    return HttpResponse(
        xmltodict.unparse({"response": {"returncode": "SUCCESS", "running": "false" if not running else "true"}}),
        content_type="text/xml",
    )
