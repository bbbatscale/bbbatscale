from core.decorators import tenant_based_permission_required
from core.forms import GroupForm
from core.utils import paginate
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


@login_required
@tenant_based_permission_required("core.view_group", raise_exception=True)
def groups_overview(request):
    return render(request, "groups_overview.html")


@login_required
@tenant_based_permission_required("core.add_group", raise_exception=True)
def group_create(request):
    form = GroupForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        group = form.save()
        messages.success(request, _("Group {} successfully created.").format(group.name))
        # return to URL
        return redirect("groups_overview")
    return render(request, "group_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_group", raise_exception=True)
def group_delete(request, group):
    instance = get_object_or_404(Group, pk=group)
    instance.delete()
    messages.success(request, _("Group successfully deleted."))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@tenant_based_permission_required(["core.view_group", "core.change_group"], raise_exception=True)
def group_update(request, group):
    instance = get_object_or_404(Group, pk=group)

    form = GroupForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _group = form.save()
        messages.success(request, _("Group {} successfully updated").format(_group.name))
        return redirect("groups_overview")

    return render(request, "group_create.html", {"form": form})


def map_group(group: Group) -> dict:
    return {
        "updateUrl": reverse("group_update", args=[group.id]),
        "deleteUrl": reverse("group_delete", args=[group.id]),
        "name": group.name,
    }


@login_required
@tenant_based_permission_required("core.view_group", raise_exception=True)
def get_groups(request: HttpRequest) -> HttpResponse:
    query = Group.objects.order_by("name")

    search_filter = request.GET.get("filter-search")
    if search_filter:
        query = query.filter(Q(name__icontains=search_filter))

    return paginate(query, int(request.GET.get("page", 1)), map_group)
