import logging
from datetime import date

from core.constants import MEETING_STATE_RUNNING
from core.decorators import tenant_based_permission_required
from core.models import Meeting, Room, User
from core.utils import get_general_parameter, get_tenant
from core.views import create_view_access_logging_message
from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.db.models import Count, Sum
from django.db.models.functions import TruncMonth
from django.shortcuts import render

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_statistics", elevate_staff=True, raise_exception=True)
def statistics(request):
    logger.info(create_view_access_logging_message(request))
    general_parameter = get_general_parameter(request)
    tenant = get_tenant(request)
    if general_parameter.hide_statistics_enable:
        if not request.user.is_superuser:
            raise PermissionDenied

    oldest_meeting_started = date.today() - relativedelta(months=11)
    meetings_per_month = (
        Meeting.objects.filter(
            tenants__in=[tenant],
            started__gte=oldest_meeting_started.replace(day=1),
        )
        .values(month=TruncMonth("started"))
        .annotate(count=Count("pk"))
        .order_by("month")
    )
    meetings_total = Meeting.objects.filter(tenants__in=[tenant]).count()
    recordings_total = Meeting.objects.filter(tenants__in=[tenant]).exclude(replay_id="", replay_url="").count()
    rooms_total = Room.objects.filter(tenants__in=[tenant]).count()
    users_total = User.objects.filter(tenant=tenant).count()
    concurrent_participants = Meeting.objects.filter(tenants__in=[tenant], state=MEETING_STATE_RUNNING).aggregate(
        current_user=Sum("participant_count")
    )

    return render(
        request,
        "statistics.html",
        {
            "concurrent_participants": concurrent_participants["current_user"],
            "meetings_per_month": [(idx + 1, x["count"]) for idx, x in enumerate(meetings_per_month)],
            "month_list": [
                (idx + 1, f'{x["month"].strftime("%b %y")} | {x["count"]}') for idx, x in enumerate(meetings_per_month)
            ],
            "meetings_total": meetings_total,
            "recordings_total": recordings_total,
            "rooms_total": rooms_total,
            "users_total": users_total,
        },
    )
