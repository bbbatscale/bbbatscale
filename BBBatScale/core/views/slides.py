import logging
from typing import Optional

from core.decorators import tenant_based_permission_required
from core.feature_flag import Feature
from core.forms import BatchOwnerChoice, SlidesAdminForm, SlidesForm
from core.models import Slides, User
from core.utils import get_general_parameter, get_tenant, has_tenant_based_perm, has_tenant_based_perms
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@Feature.Media.is_enabled
@login_required
@tenant_based_permission_required("core.view_slides", raise_exception=True)
def slides_overview(request: HttpRequest) -> HttpResponse:
    logger.info(create_view_access_logging_message(request))

    slides_query = Slides.objects.filter(tenant=get_tenant(request))

    return render(
        request,
        "slides_overview.html",
        {
            "slides": slides_query,
            "slides_count": slides_query.count(),
            "show_slides_owner": True,
            "create_url_name": "slides_create",
            "update_url_name": "slides_update",
            "delete_url_name": "slides_delete",
        },
    )


@Feature.Media.is_enabled
@login_required
@tenant_based_permission_required("core.view_own_slides", raise_exception=True)
def owned_slides_overview(request: HttpRequest) -> HttpResponse:
    logger.info(create_view_access_logging_message(request))

    slides_query = Slides.objects.filter(tenant=get_tenant(request), owner=request.user)

    return render(
        request,
        "slides_overview.html",
        {
            "slides": slides_query,
            "slides_count": slides_query.count(),
            "create_url_name": "owned_slides_create",
            "update_url_name": "owned_slides_update",
            "delete_url_name": "owned_slides_delete",
        },
    )


@Feature.Media.is_enabled
@login_required
@tenant_based_permission_required("core.delete_slides", raise_exception=True)
def slides_delete(request: HttpRequest, slides_id: int) -> HttpResponse:
    logger.info(create_view_access_logging_message(request, slides_id))
    redirect_user_id = request.GET.get("redirect_user_id")
    get_object_or_404(Slides, tenant=get_tenant(request), id=slides_id).delete()

    messages.success(request, _("Slides successfully deleted."))
    if redirect_user_id:
        return redirect("user_details", redirect_user_id)
    return redirect("slides_overview")


@Feature.Media.is_enabled
@login_required
@tenant_based_permission_required("core.delete_own_slides", raise_exception=True)
def owned_slides_delete(request: HttpRequest, slides_id: int) -> HttpResponse:
    logger.info(create_view_access_logging_message(request, slides_id))

    get_object_or_404(Slides, tenant=get_tenant(request), id=slides_id, owner=request.user).delete()

    messages.success(request, _("Slides successfully deleted."))
    return redirect("owned_slides_overview")


@Feature.Media.is_enabled
@login_required
def slides_create_update(request: HttpRequest, slides_id: Optional[int] = None) -> HttpResponse:
    tenant = get_tenant(request)
    user = request.user

    if slides_id is not None:
        if not has_tenant_based_perms(user, ["core.view_slides", "core.change_slides"], tenant):
            raise PermissionDenied

        slides = get_object_or_404(Slides, tenant=tenant, id=slides_id)
    else:
        if not has_tenant_based_perm(user, "core.add_slides", tenant):
            raise PermissionDenied

        slides = None

    logger.info(create_view_access_logging_message(request, slides_id))

    form = SlidesAdminForm(
        request.POST or None,
        request.FILES or None,
        slides_max_upload_size=get_general_parameter(request).slides_max_upload_size,
        instance=slides,
        initial={"tenant": tenant, "owner": None} if slides is None else None,
    )
    if request.method == "POST" and form.is_valid():
        slides = form.save()
        messages.success(request, _("Slides {} successfully updated.").format(slides.name))
        return redirect("slides_overview")
    return render(request, "slides_create_update.html", {"form": form})


@Feature.Media.is_enabled
@login_required
def owned_slides_create_update(request: HttpRequest, slides_id: Optional[int] = None) -> HttpResponse:
    tenant = get_tenant(request)
    user = request.user

    if slides_id is not None:
        if not has_tenant_based_perms(user, ["core.view_own_slides", "core.change_own_slides"], tenant):
            raise PermissionDenied

        slides = get_object_or_404(Slides, tenant=tenant, id=slides_id, owner=user)
    else:
        if not has_tenant_based_perm(user, "core.add_own_slides", tenant):
            raise PermissionDenied

        slides = None

    logger.info(create_view_access_logging_message(request, slides_id))

    form = SlidesForm(
        request.POST or None,
        request.FILES or None,
        slides_max_upload_size=get_general_parameter(request).slides_max_upload_size,
        instance=slides,
        initial={"tenant": tenant, "owner": user} if slides is None else None,
    )
    if request.method == "POST" and form.is_valid():
        slides = form.save()
        messages.success(request, _("Slides {} successfully updated.").format(slides.name))
        return redirect("owned_slides_overview")
    return render(request, "slides_create_update.html", {"form": form})


@Feature.Media.is_enabled
@login_required
@tenant_based_permission_required("core.change_slides", raise_exception=True)
def slides_batch_change_owner(request: HttpRequest, owner: User):
    tenant = get_tenant(request)
    instance = get_object_or_404(User, pk=owner)
    form = BatchOwnerChoice(request.POST or None, tenant=tenant)

    if request.method == "POST" and form.is_valid():
        Slides.objects.filter(owner=instance).update(owner=form.cleaned_data["owner"])
        messages.success(request, _("Slides owner successfully changed to {}").format(form.cleaned_data["owner"]))
        return redirect("user_details", owner)

    return render(
        request,
        "batch_owner_change_form.html",
        context={"form": form, "title": _("Transfer all slides to another user form")},
    )
