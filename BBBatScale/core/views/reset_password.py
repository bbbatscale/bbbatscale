from core.forms import PasswordResetForm, SetPasswordForm
from core.utils import get_tenant
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import views as auth_views
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_protect


@csrf_protect
def password_reset(request: HttpRequest) -> HttpResponse:
    def build_password_reset_url(user_id: int, token: str) -> str:
        return request.build_absolute_uri(
            reverse(
                "password_reset_confirm", kwargs={"uidb64": urlsafe_base64_encode(force_bytes(user_id)), "token": token}
            )
        )

    form = PasswordResetForm(request.POST or None, tenant=get_tenant(request))

    if request.method == "POST" and form.is_valid():
        form.save(build_password_reset_url, settings.EMAIL_SENDER_ADDRESS)
        return redirect("home")
    return render(request, "password_reset.html", {"form": form})


class PasswordResetConfirm(auth_views.PasswordResetConfirmView):
    form_class = SetPasswordForm
    template_name = "password_reset_confirm.html"
    success_url = reverse_lazy("login")

    def form_valid(self, form: SetPasswordForm) -> HttpResponse:
        messages.success(self.request, _("Your password has successfully been reset. Please log in."))
        return super().form_valid(form)
