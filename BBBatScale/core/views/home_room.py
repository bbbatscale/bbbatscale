import logging

from core.feature_flag import Feature
from core.forms import HomeRoomForm
from core.models import HomeRoom
from core.utils import get_general_parameter, get_tenant, has_tenant_based_perm
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@Feature.HomeRooms.is_enabled
@login_required
def home_room_update(request: HttpRequest, home_room: int) -> HttpResponse:
    logger.info(create_view_access_logging_message(request, home_room))
    tenant = get_tenant(request)
    general_parameter = get_general_parameter(request)
    instance = get_object_or_404(HomeRoom.objects_available.init(tenant, general_parameter), pk=home_room)

    if instance.owner != request.user and not has_tenant_based_perm(request.user, "core.change_homeroom", tenant):
        logger.warning("{} has tried to update home room of {}".format(request.user, instance.owner))
        raise PermissionDenied

    form = HomeRoomForm(
        request.POST or None,
        tenant=tenant,
        general_parameter=general_parameter,
        requesting_user=request.user,
        instance=instance,
    )
    if request.method == "POST" and form.is_valid():
        form.save()
        messages.success(request, _("Home room was updated successfully."))
        return redirect("home")
    return render(request, "home_rooms_update.html", {"form": form})
