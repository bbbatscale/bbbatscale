from functools import wraps
from logging import Logger
from typing import Any, Callable, Iterable, Type, TypeVar

from django.http import HttpRequest, HttpResponse
from django.views import View

VC = TypeVar("VC", bound=Type[View])


def _create_view_access_logging_message(request: HttpRequest, view_name: str, args: Iterable[Any]) -> str:
    msg = "{} accessed by 'username'= '{}'; ".format(view_name, request.user.username)

    msg += "Parameters="
    for arg in args:
        msg += "{}; ".format(arg)

    if "HTTP_USER_AGENT" in request.META:
        msg += "'HTTP_USER_AGENT'='{}'; ".format(request.META["HTTP_USER_AGENT"])
    else:
        msg += "'HTTP_USER_AGENT'='{}'; ".format("None")

    if "HTTP_HOST" in request.META:
        msg += "'HTTP_HOST'='{})".format(request.META["HTTP_HOST"])
    else:
        msg += "'HTTP_HOST'='{})".format("None")

    return msg


def create_view_access_logging_message(request: HttpRequest, *args):
    return _create_view_access_logging_message(request, request.resolver_match.view_name + "-view", args)


def view_access_logging_decorator(logger: Logger) -> Callable[[VC], VC]:
    def decorator(view_class: VC) -> VC:
        dispatch = view_class.dispatch

        @wraps(dispatch)
        def dispatch_wrapper(self: View, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
            logger.info(_create_view_access_logging_message(request, view_class.__name__, [*args, *kwargs.values()]))
            return dispatch(self, request, *args, **kwargs)

        view_class.dispatch = dispatch_wrapper

        return view_class

    return decorator
