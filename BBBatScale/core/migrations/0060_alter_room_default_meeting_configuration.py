# Generated by Django 3.2.19 on 2023-08-22 12:58

import core.models
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0059_alter_meeting_options"),
    ]

    operations = [
        migrations.AlterField(
            model_name="room",
            name="default_meeting_configuration",
            field=models.ForeignKey(
                default=core.models.get_default_meeting_configuration_template_id,
                on_delete=django.db.models.deletion.SET_DEFAULT,
                to="core.meetingconfigurationtemplate",
                verbose_name="Default meeting configuration",
            ),
        ),
    ]
