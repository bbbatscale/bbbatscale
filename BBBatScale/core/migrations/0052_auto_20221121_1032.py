# Generated by Django 3.2.13 on 2022-11-21 10:32

import django.db.models.deletion
from django.apps.registry import Apps
from django.db import migrations, models
from django.db.backends.base.schema import BaseDatabaseSchemaEditor
from django.db.models import Case, CharField, Count, IntegerField, OuterRef, Value, When
from django.db.models.functions import Cast, Concat, Extract, LPad, Mod


def forwards_func_ensure_one_to_one(apps: Apps, schema_editor: BaseDatabaseSchemaEditor) -> None:
    MeetingConfiguration = apps.get_model("core", "MeetingConfiguration")
    Meeting = apps.get_model("core", "Meeting")
    db_alias = schema_editor.connection.alias

    multiple_used_configurations = (
        MeetingConfiguration.objects.using(db_alias)
        .annotate(
            usage_count=Meeting.objects.using(db_alias)
            .filter(configuration=OuterRef("pk"))
            .values("configuration")
            .annotate(count=Count("pk"))
            .values("count")
        )
        .filter(usage_count__gte=2)
        .distinct()
    )

    # Applying the foreignkey constraints immediately, since otherwise the later
    # update of the MeetingConfiguration would fail.
    for constraint in schema_editor._constraint_names(MeetingConfiguration, foreign_key=True):
        schema_editor.execute(f'SET CONSTRAINTS "{constraint}" IMMEDIATE;')

    for configuration in multiple_used_configurations:
        for meeting in configuration.meeting_set.order_by("started")[1:]:
            configuration.pk = None
            configuration.id = None
            configuration._state.adding = True
            configuration.save(using=db_alias)

            meeting.configuration = configuration
            meeting.save(using=db_alias, update_fields=["configuration"])


def reverse_func_set_name(apps: Apps, schema_editor: BaseDatabaseSchemaEditor) -> None:
    MeetingConfiguration = apps.get_model("core", "MeetingConfiguration")
    Meeting = apps.get_model("core", "Meeting")
    db_alias = schema_editor.connection.alias

    microseconds = LPad(
        Cast(Mod(Cast(Extract("started", "microsecond"), IntegerField()), Value(1000)), CharField()), 3, Value("0")
    )
    latest_meeting = (
        Meeting.objects.using(db_alias)
        .filter(configuration_id=OuterRef("id"), room__isnull=False)
        .order_by("-started")[:1]
    )
    MeetingConfiguration.objects.using(db_alias).annotate(
        room_name=latest_meeting.values("room_name"),
        started=latest_meeting.values("started"),
    ).update(
        name=Case(
            When(
                room_name__isnull=False,
                started__isnull=False,
                then=Concat("room_name", Value("-config-"), microseconds),
            ),
            default=Value(""),
        )
    )


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0051_auto_20221019_1310"),
    ]

    operations = [
        migrations.AlterField(
            model_name="meetingconfiguration",
            name="name",
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.RunPython(forwards_func_ensure_one_to_one, reverse_func_set_name),
        migrations.RemoveField(
            model_name="meetingconfiguration",
            name="name",
        ),
        migrations.AlterField(
            model_name="meeting",
            name="configuration",
            field=models.OneToOneField(
                blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to="core.meetingconfiguration"
            ),
        ),
    ]
