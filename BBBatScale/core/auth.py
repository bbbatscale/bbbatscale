from typing import Any, Optional

from core.utils import get_tenant
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import AbstractUser
from django.http import HttpRequest


class TenantBasedModelBackend(ModelBackend):
    def authenticate(
        self, request: HttpRequest, username: Optional[str] = None, password: Optional[str] = None, **kwargs: Any
    ) -> Optional[AbstractUser]:
        user: Optional[AbstractUser] = super().authenticate(request, username, password, **kwargs)
        if user and (user.is_superuser or (getattr(user, "tenant", None) == get_tenant(request))):
            return user
        return None
