{% load i18n %}
{% blocktranslate %}Dear {{ display_name }},{% endblocktranslate %}
{% autoescape off %}
{% blocktranslate %}You're receiving this email because you requested a password reset for your user account at {{ domain }}.{% endblocktranslate %}

{% translate "Please go to the following page and choose a new password:" %}
{{ reset_url }}
{% translate "This link is valid for 15 minutes after creation." %}
{% endautoescape %}
