import itertools
import logging
from typing import Callable, Optional, Tuple

from channels.db import database_sync_to_async
from channels.http import AsgiRequest
from channels.middleware import BaseMiddleware
from core.exceptions import BaseHttpException
from core.models import GeneralParameter
from core.utils import get_tenant
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.handlers.exception import response_for_exception
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpRequest, HttpResponse
from django.http.request import split_domain_port
from django.utils.encoding import iri_to_uri
from django.utils.functional import LazyObject


class TenantLazyObject(LazyObject):
    def __init__(self, host: str) -> None:
        super().__init__()
        # Assign to __dict__ to avoid infinite __setattr__ loops.
        self.__dict__["_host"] = host

    def _setup(self) -> None:
        self._wrapped = _get_tenant(self.__dict__["_host"])


def _get_tenant(host: str) -> Site:
    try:
        # First attempt to look up the site by host with or without port.
        return Site.objects.get(domain__iexact=host)
    except Site.DoesNotExist:
        # Fallback to looking up site after stripping port from the host.
        domain, port = split_domain_port(host)
        return Site.objects.get(domain__iexact=domain)


class CurrentTenantASGIMiddleware(BaseMiddleware):
    def populate_scope(self, scope: dict) -> None:
        host_headers = [value for key, value in scope["headers"] if key == b"host"]

        if len(host_headers) == 0:
            raise ValueError("The required host HTTP-Header is not present.")
        elif len(host_headers) != 1:
            raise ValueError("The host HTTP-Header is specified more than once.")
        elif "tenant" not in scope:
            scope["tenant"] = TenantLazyObject(host_headers[0].decode("ISO-8859-1"))

    async def resolve_scope(self, scope: dict) -> None:
        await database_sync_to_async(scope["tenant"]._setup)()


class CurrentTenantMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        request.tenant = _get_tenant(request.get_host())
        return self.get_response(request)


class CurrentGeneralParameterMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        assert hasattr(request, "tenant"), (
            "The general parameter middleware requires the tenant middleware "
            "to be installed. Edit your MIDDLEWARE setting to insert "
            "'core.middleware.CurrentTenantMiddleware' before "
            "'core.middleware.CurrentGeneralParameterMiddleware'."
        )
        request.general_parameter = GeneralParameter.objects.load(get_tenant(request))
        return self.get_response(request)


class CatchHttpExceptionMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        return self.get_response(request)

    @staticmethod
    def process_exception(request: HttpRequest, exception: Exception) -> HttpResponse:
        if isinstance(exception, BaseHttpException):
            return exception.response(request)

        return response_for_exception(request, exception)


class AccessLoggingMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]) -> None:
        self.get_response = get_response
        self.logger = logging.getLogger("bbbatscale.request")

    @staticmethod
    def _get_uri_and_query(request: HttpRequest) -> Tuple[str, Optional[str]]:
        if settings.BBBATSCALE_LOGGING_INCLUDE_REQUEST_QUERY:
            query = iri_to_uri(request.META.get("QUERY_STRING", ""))
            uri = request.build_absolute_uri()
        else:
            query = None
            uri = request.build_absolute_uri(f"//{request.path}")

        return uri, query

    @staticmethod
    def _get_http_version(request: HttpRequest) -> Optional[str]:
        http_version = None

        if isinstance(request, WSGIRequest):
            if "SERVER_PROTOCOL" in request.environ:
                http_version = request.environ.get("SERVER_PROTOCOL", None)
                http_version = http_version.removeprefix("HTTP/") if http_version else None
        elif isinstance(request, AsgiRequest):
            http_version = request.scope.get("http_version", None)

        return http_version or None

    @staticmethod
    def _get_remote_address(request: HttpRequest) -> Optional[str]:
        proxy_count = settings.BBBATSCALE_HTTP_PROXY_COUNT
        remote_address = None

        if proxy_count > 0:
            x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR", None)
            if x_forwarded_for:
                remote_address = next(
                    itertools.islice(reversed(x_forwarded_for.split(",")), proxy_count - 1, None), None
                )
                if remote_address:
                    remote_address = remote_address.strip()
                else:
                    logging.getLogger("bbbatscale.config").critical(
                        "The X-Forwarded-For header did not contain at least %d address(es)."
                        " This is most likely caused by a misconfiguration of the env variable"
                        " 'BBBATSCALE_HTTP_PROXY_COUNT' and can lead to spoofed addresses."
                        " See also https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-For"
                        "#security_and_privacy_concerns.",
                        proxy_count,
                    )

        return remote_address or request.META.get("REMOTE_ADDR", None)

    def __call__(self, request: HttpRequest) -> HttpResponse:
        response = self.get_response(request)

        http_version = self._get_http_version(request)
        remote_address = self._get_remote_address(request)
        uri, query = self._get_uri_and_query(request)

        extra = {
            "request_scheme": request.scheme,
            "request_host": request.get_host(),
            "request_method": request.method,
            "request_path": request.path,
            "request_uri": uri,
            "response_status_code": response.status_code,
            "http_version": http_version,
            "remote_address": remote_address,
        }

        if query is not None:
            extra["request_query"] = query

        self.logger.info(
            '%(remote_address)s - "%(method)s %(uri)s%(http_version)s" %(status_code)d',
            {
                "remote_address": remote_address or "N/A",
                "method": request.method,
                "uri": uri,
                "http_version": f" HTTP/{http_version}" if http_version else "",
                "status_code": response.status_code,
            },
            extra=extra,
        )

        return response
