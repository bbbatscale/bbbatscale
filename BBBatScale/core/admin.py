from core.models import Meeting
from django.apps import apps
from django.contrib import admin


class MeetingAdmin(admin.ModelAdmin):
    readonly_fields = ["configuration"]


admin.site.register(Meeting, MeetingAdmin)

for model in apps.get_app_config("core").get_models():
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass
