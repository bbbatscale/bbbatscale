import dataclasses
from dataclasses import dataclass

from django.utils.translation import gettext_lazy as _


@dataclass(frozen=True)
class JoinParameters:
    auto_join_audio: bool = dataclasses.field(
        default=True,
        metadata={
            "name": _("Auto join audio"),
            "help_text": _("If enabled, the process of joining the audio will automatically be started."),
        },
    )
    listen_only_mode: bool = dataclasses.field(
        default=True,
        metadata={
            "name": _("Listen only mode"),
            "help_text": _(
                "If enabled, joining the audio part of the meeting without a microphone will be possible, otherwise"
                " listen-only mode will be disabled. Disabling this will make it possible, in conjunction with skipping"
                ' the "echo test" prompt, to directly join a meeting without selecting further options.'
            ),
        },
    )
    skip_check_audio: bool = dataclasses.field(
        default=False,
        metadata={
            "name": _("Skip check audio"),
            "help_text": _('If enabled, the "echo test" prompt will not be shown when sharing audio.'),
        },
    )
    skip_check_audio_on_first_join: bool = dataclasses.field(
        default=False,
        metadata={
            "name": _("Skip check audio on first join"),
            "help_text": _(
                'If enabled, the "echo test" prompt will not be shown when sharing audio for the first time in the'
                ' meeting. If audio sharing will be stopped, next time trying to share the audio the "echo test" prompt'
                " will be displayed, allowing for configuration changes to be made prior to sharing audio again."
            ),
        },
    )
    auto_share_webcam: bool = dataclasses.field(
        default=False,
        metadata={
            "name": _("Auto share webcam"),
            "help_text": _("If enabled, the process of sharing the webcam (if any) will automatically be started."),
        },
    )
    record_video: bool = dataclasses.field(
        default=True,
        metadata={"name": _("Record video"), "help_text": _("If enabled, the video stream will be recorded.")},
    )
    skip_video_preview: bool = dataclasses.field(
        default=False,
        metadata={
            "name": _("Skip video preview"),
            "help_text": _("If enabled, a preview of the webcam before sharing it will not be shown."),
        },
    )
    skip_video_preview_on_first_join: bool = dataclasses.field(
        default=False,
        metadata={
            "name": _("Skip video preview on first join"),
            "help_text": _(
                "If enabled, a preview of the webcam before sharing it will not be shown when sharing for the first"
                " time in the meeting. If video sharing will be stopped, next time trying to share the webcam the video"
                " preview will be displayed, allowing for configuration changes to be made prior to sharing."
            ),
        },
    )
    mirror_own_webcam: bool = dataclasses.field(
        default=False,
        metadata={
            "name": _("Mirror own webcam"),
            "help_text": _(
                "If enabled, a mirrored version of the webcam will be shown. Does not affect the incoming video streams"
                " for other users."
            ),
        },
    )
    force_restore_presentation_on_new_events: bool = dataclasses.field(
        default=False,
        metadata={
            "name": _("Force restore presentation on new events"),
            "help_text": _(
                "If enabled, the presentation area will be forcefully restored if it is minimized (viewers only) on the"
                " following actions: changing presentation/slide/zoom, publishing polls, or adding annotations."
            ),
        },
    )
    auto_swap_layout: bool = dataclasses.field(
        default=False,
        metadata={
            "name": _("Auto swap layout"),
            "help_text": _("If enabled, the presentation area will be minimized when joining a meeting."),
        },
    )
    show_participants_on_login: bool = dataclasses.field(
        default=True,
        metadata={
            "name": _("Show participants on first join"),
            "help_text": _("If enabled, the participants panel will be displayed when joining a meeting."),
        },
    )
    show_public_chat_on_login: bool = dataclasses.field(
        default=True,
        metadata={
            "name": _("Show public chat on first join"),
            "help_text": _(
                "If enabled, the chat panel will be visible when joining a meeting. The participants panel must also be"
                " shown to work."
            ),
        },
    )
