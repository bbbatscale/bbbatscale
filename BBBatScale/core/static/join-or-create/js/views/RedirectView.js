const { inject, computed } = Vue;
const { Translation } = VueI18n;
import RedirectViewComponent from "../components/RedirectViewComponent.js";

export default {
    components: {
        Translation,
        RedirectViewComponent
    },
    setup() {
        const roomName = inject("roomName");
        const data = inject("data");

        return {
            roomName,
            joinUrl: computed(() => data.value.joinUrl)
        };
    },
    template: `
        <RedirectViewComponent :redirectUrl="joinUrl">
            <template #header>
                <Translation keypath="redirect.header.join" tag="span">
                    <template #name>
                        <strong>{{ roomName }}</strong>
                    </template>
                </Translation>
            </template>

            <template #manuelRedirectLinkText>
                <span v-t="'redirect.manuelRedirectLinkText.join'" />
            </template>
        </RedirectViewComponent>
    `
};
