const { ref, inject, onMounted, onBeforeUnmount } = Vue;
const { Translation } = VueI18n;
import ViewContainerComponent from "../components/ViewContainerComponent.js";
import EitherOrComponent from "../components/EitherOrComponent.js";
import CollapsableCardComponent from "../components/CollapsableCardComponent.js";
import ConfigureMeetingFormComponent from "../components/ConfigureMeetingFormComponent.js";

export default {
    components: {
        Translation,
        ViewContainerComponent,
        EitherOrComponent,
        CollapsableCardComponent,
        ConfigureMeetingFormComponent
    },
    setup() {
        /**
         * @type {fetchJoinOrCreate}
         */
        const fetchJoinOrCreate = inject("fetchJoinOrCreate");
        const roomName = inject("roomName");
        const data = inject("data");
        const configureMeetingFormCollapsable = ref(null);
        const configureMeetingForm = ref(null);

        let isMounted = false;

        function showFormIfNeeded() {
            if (isMounted && data.value.showForm) {
                configureMeetingFormCollapsable.value.show();
            }
        }

        function submit() {
            fetchJoinOrCreate("/api/join-create/create-meeting", {
                requestData: configureMeetingForm.value.formData,
                successCallback: showFormIfNeeded
            });
        }

        function joinWaitingRoom() {
            fetchJoinOrCreate("/api/join-create/opt-out-create-meeting");
        }

        onMounted(() => {
            isMounted = true;
            showFormIfNeeded();
        });

        onBeforeUnmount(() => {
            isMounted = false;
        });

        return {
            roomName,
            configureMeetingFormCollapsable,
            configureMeetingForm,
            submit,
            joinWaitingRoom
        };
    },
    template: `
        <ViewContainerComponent>
            <template #header>
                <Translation keypath="meeting.configure.header" tag="span">
                    <template #name>
                        <strong>{{ roomName }}</strong>
                    </template>
                </Translation>
            </template>

            <EitherOrComponent>
                <template #either>
                    <Translation keypath="meeting.configure.body" tag="p">
                        <template #name>
                            <strong>{{ roomName }}</strong>
                        </template>
                    </Translation>

                    <CollapsableCardComponent ref="configureMeetingFormCollapsable">
                        <template #header>
                            <span v-t="'meeting.configure.dropdownHeader'" />
                        </template>

                        <ConfigureMeetingFormComponent ref="configureMeetingForm" />
                    </CollapsableCardComponent>

                    <button class="btn btn-primary" @click="submit" v-t="'meeting.configure.start'" />
                </template>

                <template #or>
                    <p v-t="'meeting.configure.or.body'" />

                    <button type="button" class="btn btn-secondary" @click="joinWaitingRoom" v-t="'meeting.configure.or.button'" />
                </template>
            </EitherOrComponent>
        </ViewContainerComponent>
    `
};
