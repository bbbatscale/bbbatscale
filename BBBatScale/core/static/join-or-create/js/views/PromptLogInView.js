import RedirectViewComponent from "../components/RedirectViewComponent.js";

export default {
    components: {
        RedirectViewComponent
    },
    setup() {
        const loginUrl = new URL("/login", location);
        loginUrl.searchParams.set("next", location.pathname + location.search + location.hash);

        return {
            loginUrl: loginUrl.toString()
        };
    },
    template: `
        <RedirectViewComponent :redirectUrl="loginUrl">
            <template #header>
                <span v-t="'redirect.header.login'" />
            </template>

            <template #manuelRedirectLinkText>
                <span v-t="'redirect.manuelRedirectLinkText.login'" />
            </template>
        </RedirectViewComponent>
    `
};
