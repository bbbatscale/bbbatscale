const { inject, ref, computed } = Vue;
const { Translation } = VueI18n;
import ViewContainerComponent from "../components/ViewContainerComponent.js";
import AwaitNextActionComponent from "../components/AwaitNextActionComponent.js";
import EitherOrComponent from "../components/EitherOrComponent.js";

export default {
    components: {
        Translation,
        ViewContainerComponent,
        AwaitNextActionComponent,
        EitherOrComponent
    },
    setup() {
        /**
         * @type {fetchJoinOrCreate}
         */
        const fetchJoinOrCreate = inject("fetchJoinOrCreate");
        const roomName = inject("roomName");
        const data = inject("data");
        const awaitNextAction = ref(null);

        function createMeeting() {
            awaitNextAction.value?.cancel();
            fetchJoinOrCreate("/api/join-create/opt-in-create-meeting");
        }

        return {
            roomName,
            awaitNextAction,
            canOptIn: computed(() => data.value.canOptIn),
            createMeeting
        };
    },
    template: `
        <ViewContainerComponent>
            <template #header>
                <Translation keypath="wait.untilConfigured.header" tag="span">
                    <template #name>
                        <strong>{{ roomName }}</strong>
                    </template>
                </Translation>
            </template>

            <EitherOrComponent v-if="canOptIn">
                <template #either>
                    <AwaitNextActionComponent ref="awaitNextAction">
                        <span v-t="'wait.untilConfigured.body'" />
                    </AwaitNextActionComponent>
                </template>

                <template #or>
                    <div class="d-flex justify-content-center align-items-center">
                        <button type="button" class="btn btn-secondary" @click="createMeeting" v-t="'wait.untilConfigured.orCreate'" />
                    </div>
                </template>
            </EitherOrComponent>

            <AwaitNextActionComponent ref="awaitNextAction" v-else>
                <span v-t="'wait.untilConfigured.bodyWithLogInHint'" />
            </AwaitNextActionComponent>
        </ViewContainerComponent>
    `
};
