const { inject } = Vue;
const { Translation } = VueI18n;
import ViewContainerComponent from "../components/ViewContainerComponent.js";

export default {
    components: {
        Translation,
        ViewContainerComponent
    },
    setup() {
        const roomName = inject("roomName");

        return {
            roomName
        };
    },
    template: `
        <ViewContainerComponent card-type="secondary">
            <template #header>
                <Translation keypath="meeting.ended.header" tag="span">
                    <template #name>
                        <strong>{{ roomName }}</strong>
                    </template>
                </Translation>
            </template>

            <Translation keypath="meeting.ended.body" tag="p" class="card-text">
                <template #name>
                    <strong>{{ roomName }}</strong>
                </template>
            </Translation>
        </ViewContainerComponent>
    `
};
