const { ref, inject, computed } = Vue;
const { Translation } = VueI18n;
import ViewContainerComponent from "../components/ViewContainerComponent.js";

export default {
    components: {
        Translation,
        ViewContainerComponent
    },
    setup() {
        /**
         * @type {fetchJoinOrCreate}
         */
        const fetchJoinOrCreate = inject("fetchJoinOrCreate");
        const roomName = inject("roomName");
        const data = inject("data");

        const inputElement = ref(null);
        const accessCode = ref("");
        const showAccessCode = ref(false);

        function submit() {
            fetchJoinOrCreate("/api/join-create/validate-access-code", {
                requestData: { accessCode: accessCode.value },
                successCallback() {
                    accessCode.value = "";
                }
            });
        }

        function toggleAccessCodeAndResetFocus() {
            inputElement.value?.focus();
            showAccessCode.value = !showAccessCode.value;
        }

        return {
            roomName,
            inputElement,
            accessCode,
            showAccessCode,
            invalid: computed(() => data.value.invalid),
            submit,
            toggleAccessCodeAndResetFocus
        };
    },
    directives: {
        focus: {
            mounted: (el) => el.focus()
        }
    },
    template: `
        <ViewContainerComponent>
            <template #header>
                <Translation keypath="join.accessCode.header" tag="span">
                    <template #name>
                        <strong>{{ roomName }}</strong>
                    </template>
                </Translation>
            </template>

            <div class="input-group" :class="{ 'is-invalid': invalid }">
                <input @keyup.enter="submit" v-focus ref="inputElement" v-model="accessCode" id="access-code"
                       :type="showAccessCode ? 'text' : 'password'" class="form-control"
                       :class="{ 'is-invalid': invalid }" :placeholder="$t('join.accessCode.placeholder')">
                <div class="input-group-append">
                    <!-- prevent keydown.enter since this would result in a keyup.enter event inside the input
                         and therefore submit the access code rather than only make it visible -->
                    <button @click="toggleAccessCodeAndResetFocus" @keydown.enter.prevent
                            @keyup.enter="toggleAccessCodeAndResetFocus" class="btn btn-secondary"
                            :title="showAccessCode ? $t('join.accessCode.toggleVisibility.hide') : $t('join.accessCode.toggleVisibility.show')">
                        <i class="fas" :class="[showAccessCode ? 'fa-eye-slash' : 'fa-eye']" />
                    </button>
                    <button @click="submit" class="btn btn-primary" :title="$t('common.button.submit')">
                        <i class="fas fa-arrow-right" />
                    </button>
                </div>
            </div>

            <div class="invalid-feedback" v-t="'join.accessCode.invalid'" />
            <div class="form-text text-muted" v-t="'join.accessCode.help'" />
        </ViewContainerComponent>
    `
};
