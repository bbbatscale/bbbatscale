const { Translation } = VueI18n;
const { inject, ref } = Vue;
import ViewContainerComponent from "../components/ViewContainerComponent.js";
import { ErrorResponse } from "../Errors.js";

const ERROR_MESSAGES = new Map([
    ["DOES_NOT_EXIST", ["error.header.roomDoesNotExist", "error.body.roomDoesNotExist"]]
]);

export default {
    components: {
        Translation,
        ViewContainerComponent
    },
    setup() {
        const roomName = inject("roomName");
        const error = inject("error");

        const headerKey = ref("error.header.unexpectedError");
        const bodyKey = ref("error.body.unexpectedError");

        if (error.value instanceof ErrorResponse) {
            ([headerKey.value, bodyKey.value] = ERROR_MESSAGES.get(error.value.errorType) ?? [headerKey.value, bodyKey.value]);
        }

        return {
            roomName,
            headerKey,
            bodyKey
        };
    },
    template: `
        <ViewContainerComponent card-type="warning">
            <template #header>
                <Translation :keypath="headerKey" tag="span">
                    <template #name>
                        <strong>{{ roomName }}</strong>
                    </template>
                </Translation>
            </template>

            <p class="card-text" v-t="bodyKey" />
        </ViewContainerComponent>
    `
};
