const { ref, onMounted, onBeforeUnmount, watch, inject, nextTick, computed } = Vue;


export default {
    setup(props, context) {
        const form = ref(null);
        const data = inject("data");

        let isMounted = false;

        function initForm() {
            initSlidesInput(document.getElementById("id_slides"), document.getElementById("id_initial_slides"));

            MeetingTimeManager(
                document.getElementById("id_meeting_duration_time_0"),
                document.getElementById("id_meeting_duration_time_1"),
                document.getElementById("id_meeting_duration_time_2")
            );
        }

        onMounted(() => {
            isMounted = true;
            initForm();
        });

        watch(() => data.value.form, () => {
            if (isMounted) {
                nextTick(initForm);
            }
        });

        onBeforeUnmount(() => {
            isMounted = false;
        });

        context.expose({
            get formData() {
                return form.value ? new FormData(form.value) : new FormData();
            }
        });

        return {
            form,
            formHtml: computed(() => data.value.form)
        };
    },
    template: `
        <form ref="form" v-html="formHtml"></form>
    `
};
