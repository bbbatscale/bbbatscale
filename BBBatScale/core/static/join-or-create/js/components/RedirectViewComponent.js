const { onBeforeMount } = Vue;
const { Translation } = VueI18n;
import ViewContainerComponent from "../components/ViewContainerComponent.js";

export default {
    props: {
        redirectUrl: {
            type: String,
            required: true
        }
    },
    components: {
        Translation,
        ViewContainerComponent
    },
    setup({ redirectUrl }) {
        onBeforeMount(() => {
            location.href = redirectUrl;
        });

        return {
            redirectUrl
        };
    },
    template: `
        <ViewContainerComponent card-type="primary">
            <template #header>
                <slot name="header" />
            </template>

            <Translation keypath="redirect.body" tag="p" class="card-text">
                <template #redirect>
                    <a :href="redirectUrl">
                        <slot name="manuelRedirectLinkText" />
                    </a>
                </template>
            </Translation>
        </ViewContainerComponent>
    `
};
