export default {
    template: `
        <div class="px-3">
            <slot name="either" />
        </div>

        <div class="separator my-4" v-t="'eitherOr.or'" />

        <div class="px-3">
            <slot name="or" />
        </div>
    `
};
