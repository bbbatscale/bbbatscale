export default {
    template: `
        <div class="container">
            <slot />
        </div>
    `
};
