import ContainerComponent from "./ContainerComponent.js";
import ViewComponent from "./ViewComponent.js";

export default {
    props: ["cardType"],
    components: {
        ContainerComponent,
        ViewComponent
    },
    template: `
        <ContainerComponent>
            <ViewComponent :card-type="cardType">
                <template #header>
                    <slot name="header" />
                </template>

                <slot />
            </ViewComponent>
        </ContainerComponent>
    `
};
