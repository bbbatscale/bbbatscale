export default {
    template: `
        <div class="d-flex justify-content-center align-items-center">
            <div class="spinner-border" role="status">
                <span class="sr-only" v-t="'loading.srIndication'" />
            </div>
        </div>
    `
};
