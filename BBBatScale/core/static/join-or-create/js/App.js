import { ErrorResponse } from "./Errors.js";

const { ref, readonly, provide, watch } = Vue;
import DisplayMeetingHasEndedView from "./views/DisplayMeetingHasEndedView.js";
import ConfigureMeetingView from "./views/ConfigureMeetingView.js";
import WaitUntilConfiguredView from "./views/WaitUntilConfiguredView.js";
import WaitUntilRunningView from "./views/WaitUntilRunningView.js";
import PromptLogInView from "./views/PromptLogInView.js";
import PromptAccessCodeView from "./views/PromptAccessCodeView.js";
import PromptJoinNameView from "./views/PromptJoinNameView.js";
import RedirectView from "./views/RedirectView.js";
import ErrorView from "./views/ErrorView.js";
import LoadingComponent from "./components/LoadingComponent.js";


/**
 * @typedef {"CONFIGURE_MEETING" | "WAIT_UNTIL_CONFIGURED" | "WAIT_UNTIL_RUNNING" | "PROMPT_LOG_IN" | "PROMPT_ACCESS_CODE" | "PROMPT_JOIN_NAME"} IntermediateAction
 */

/**
 * @typedef {"DISPLAY_MEETING_HAS_ENDED" | "REDIRECT"} TerminalAction
 */

/**
 * @typedef {"ERROR" | IntermediateAction | TerminalAction} State
 */

/**
 * @typedef {Object} InitData
 * @property {string} roomName
 * @property {boolean} [skipMeetingConfiguration]
 * @property {string} [secret]
 * @property {boolean} [enforceCreateMeetingPermission]
 * @property {boolean} [enforceJoinMeetingPermission]
 * @property {boolean} [enforceModeratePermission]
 * @property {boolean} [joinAsRoom]
 *
 * @property {boolean} [bbbAutoJoinAudio]
 * @property {boolean} [bbbListenOnlyMode]
 * @property {boolean} [bbbSkipCheckAudio]
 * @property {boolean} [bbbSkipCheckAudioOnFirstJoin]
 * @property {boolean} [bbbAutoShareWebcam]
 * @property {boolean} [bbbRecordVideo]
 * @property {boolean} [bbbSkipVideoPreview]
 * @property {boolean} [bbbSkipVideoPreviewOnFirstJoin]
 * @property {boolean} [bbbMirrorOwnWebcam]
 * @property {boolean} [bbbForceRestorePresentationOnNewEvents]
 * @property {boolean} [bbbAutoSwapLayout]
 * @property {boolean} [bbbShowParticipantsOnLogin]
 * @property {boolean} [bbbShowPublicChatOnLogin]
 */

/**
 * @typedef {string} Token
 */

/**
 * @typedef {Object} IntermediateResponseData
 * @property {Token} token
 * @property {IntermediateAction} action
 * @property {Object} data
 */

/**
 * @typedef {Object} TerminalResponseData
 * @property {TerminalAction} action
 * @property {Object} data
 */

/**
 * @typedef {IntermediateResponseData | TerminalResponseData} ResponseData
 */

/**
 * @typedef {Object} FetchJoinOrCreateOptions
 * @property {Object | FormData} [requestData={}]
 * @property {boolean} [showLoading=true]
 * @property {(() => void) | null} [successCallback=null]
 * @property {AbortSignal} [signal=null]
 */

/**
 * @callback fetchJoinOrCreate
 * @param {string} url
 * @param {FetchJoinOrCreateOptions} [options={}]
 * @return {void}
 */

/**
 * @typedef {Object} InitDataMapping
 * @property {string} searchParamKey
 * @property {"string" | "boolean"} type
 * @property {string | boolean | undefined} [defaultValue=undefined]
 */

/**
 * @type {Map<keyof InitData, InitDataMapping>}
 */
const INIT_DATA_MAPPINGS = new Map([
    ["roomName", { searchParamKey: "room", type: "string", defaultValue: "" }],
    ["skipMeetingConfiguration", { searchParamKey: "skipMeetingConfiguration", type: "boolean" }],
    ["secret", { searchParamKey: "secret", type: "string" }],
    ["enforceCreateMeetingPermission", { searchParamKey: "enforceCreateMeetingPermission", type: "boolean" }],
    ["enforceJoinMeetingPermission", { searchParamKey: "enforceJoinMeetingPermission", type: "boolean" }],
    ["enforceModeratePermission", { searchParamKey: "enforceModeratePermission", type: "boolean" }],
    ["joinAsRoom", { searchParamKey: "joinAsRoom", type: "boolean" }],

    // BigBlueButton join parameters
    ["bbbAutoJoinAudio", { searchParamKey: "bbbAutoJoinAudio", type: "boolean" }],
    ["bbbListenOnlyMode", { searchParamKey: "bbbListenOnlyMode", type: "boolean" }],
    ["bbbSkipCheckAudio", { searchParamKey: "bbbSkipCheckAudio", type: "boolean" }],
    ["bbbSkipCheckAudioOnFirstJoin", { searchParamKey: "bbbSkipCheckAudioOnFirstJoin", type: "boolean" }],
    ["bbbAutoShareWebcam", { searchParamKey: "bbbAutoShareWebcam", type: "boolean" }],
    ["bbbRecordVideo", { searchParamKey: "bbbRecordVideo", type: "boolean" }],
    ["bbbSkipVideoPreview", { searchParamKey: "bbbSkipVideoPreview", type: "boolean" }],
    ["bbbSkipVideoPreviewOnFirstJoin", { searchParamKey: "bbbSkipVideoPreviewOnFirstJoin", type: "boolean" }],
    ["bbbMirrorOwnWebcam", { searchParamKey: "bbbMirrorOwnWebcam", type: "boolean" }],
    ["bbbForceRestorePresentationOnNewEvents", {
        searchParamKey: "bbbForceRestorePresentationOnNewEvents",
        type: "boolean"
    }],
    ["bbbAutoSwapLayout", { searchParamKey: "bbbAutoSwapLayout", type: "boolean" }],
    ["bbbShowParticipantsOnLogin", { searchParamKey: "bbbShowParticipantsOnLogin", type: "boolean" }],
    ["bbbShowPublicChatOnLogin", { searchParamKey: "bbbShowPublicChatOnLogin", type: "boolean" }]
]);

/**
 * @type {Map<IntermediateAction, (data: Record<string, unknown>) => boolean>}
 */
const INTERMEDIATE_ACTIONS = new Map([
    ["CONFIGURE_MEETING", (data) => {
        return (
            Object.keys(data).length === 2
            && typeof data.form === "string"
            && typeof data.showForm === "boolean"
        );
    }],
    ["WAIT_UNTIL_CONFIGURED", (data) => {
        return (
            Object.keys(data).length === 1
            && typeof data.canOptIn === "boolean"
        );
    }],
    ["WAIT_UNTIL_RUNNING", (data) => {
        return (
            Object.keys(data).length === 0 || (
                Object.keys(data).length === 1
                && typeof data.startedBySomeoneElse === "boolean"
            )
        );
    }],
    ["PROMPT_LOG_IN", (data) => {
        return Object.keys(data).length === 0;
    }],
    ["PROMPT_ACCESS_CODE", (data) => {
        return (
            Object.keys(data).length === 1
            && typeof data.invalid === "boolean"
        );
    }],
    ["PROMPT_JOIN_NAME", (data) => {
        return (
            Object.keys(data).length === 2
            && typeof data.joinName === "string"
            && typeof data.nameIsTooShort === "boolean"
        );
    }]
]);
/**
 * @type {Map<TerminalAction, (data: Record<string, unknown>) => boolean>}
 */
const TERMINAL_ACTIONS = new Map([
    ["DISPLAY_MEETING_HAS_ENDED", (data) => {
        return Object.keys(data).length === 0;
    }],
    ["REDIRECT", (data) => {
        return (
            Object.keys(data).length === 1
            && typeof data.joinUrl === "string"
        );
    }]
]);

/**
 * @return {InitData}
 */
function parseUrl() {
    const searchParams = new URL(location).searchParams;
    /**
     * @type {InitData}
     */
    const initData = {};

    INIT_DATA_MAPPINGS.forEach(({ searchParamKey, type, defaultValue }, initDataKey) => {
        const rawValue = searchParams.get(searchParamKey);

        if (type === "string") {
            initData[initDataKey] = rawValue ?? defaultValue;
            return;
        } else {
            try {
                const _value = JSON.parse(rawValue);
                if (typeof _value === "boolean") {
                    initData[initDataKey] = _value;
                    return;
                }
            } catch (ignore) {
            }
        }

        if (typeof defaultValue !== "undefined") {
            initData[initDataKey] = defaultValue;
        }
    });

    return initData;
}

/**
 * @param {InitData} initData
 * @return {void}
 */
function tidyUpUrl(initData) {
    const url = new URL(location);
    url.search = "";

    INIT_DATA_MAPPINGS.forEach(({ searchParamKey, type, defaultValue }, initDataKey) => {
        if (typeof initData[initDataKey] !== type) {
            return;
        }

        if (type === "string") {
            url.searchParams.set(searchParamKey, initData[initDataKey]);
        } else {
            url.searchParams.set(searchParamKey, JSON.stringify(initData[initDataKey]));
        }
    });

    history.replaceState({}, "", url);
}

/**
 * @param {unknown} value
 * @return {value is Record<string, unknown>}
 */
function isObject(value) {
    return value !== null && typeof value === "object";
}

/**
 * @param {Response} response
 * @return {Record<string, unknown>}
 */
async function validateResponse(response) {
    const responseData = await response.json();

    if (typeof responseData.type !== "string") {
        throw new Error("Malformed response");
    }

    if (responseData.type === "error") {
        throw new ErrorResponse(response.status, responseData);
    }

    if (responseData.type === "action") {
        if (INTERMEDIATE_ACTIONS.has(responseData.action)) {
            if (
                typeof responseData.token === "string"
                && isObject(responseData.data)
                && INTERMEDIATE_ACTIONS.get(responseData.action)(responseData.data)
            ) {
                return responseData;
            }
            throw new Error(`Malformed response: action '${responseData.action}' is malformed`);
        }

        if (
            TERMINAL_ACTIONS.has(responseData.action)) {
            if (
                isObject(responseData.data)
                && TERMINAL_ACTIONS.get(responseData.action)(responseData.data)
            ) {
                return responseData;
            }
            throw new Error(`Malformed response: action '${responseData.action}' is malformed`);
        }

        throw new Error(`Malformed response: unknown action '${responseData.action}'`);
    }

    throw new Error(`Malformed response: unknown type '${responseData.type}'`);
}

export default {
    components: {
        DisplayMeetingHasEndedView,
        ConfigureMeetingView,
        WaitUntilConfiguredView,
        WaitUntilRunningView,
        PromptLogInView,
        PromptAccessCodeView,
        PromptJoinNameView,
        RedirectView,
        ErrorView,
        LoadingComponent
    },
    setup() {
        const initData = parseUrl();
        const state = ref(null);
        const token = ref(sessionStorage.getItem("token"));
        const data = ref(null);
        const error = ref(null);

        /**
         * @type {fetchJoinOrCreate}
         */
        const fetchJoinOrCreate = async (url, {
            requestData = {},
            showLoading = true,
            successCallback = null,
            signal = null
        } = {}) => {
            let loadingTimeoutId = null;
            if (showLoading) {
                loadingTimeoutId = setTimeout(() => {
                    state.value = null;
                    data.value = null;
                    error.value = null;
                }, 250);
            }

            let preparedRequestData;
            let additionalHeaders = {};
            if (requestData instanceof FormData) {
                preparedRequestData = requestData;
                preparedRequestData.set("__token", token.value);
            } else {
                preparedRequestData = JSON.stringify({ ...requestData, token: token.value ?? undefined });
                additionalHeaders = { "Content-Type": "application/json" };
            }

            try {
                const response = await fetch(url, {
                    method: "POST",
                    headers: additionalHeaders,
                    body: preparedRequestData,
                    signal
                });

                const responseData = await validateResponse(response);

                clearTimeout(loadingTimeoutId);
                state.value = responseData.action;
                token.value = responseData.token ?? null;
                data.value = responseData.data;
                error.value = null;

                if (successCallback) {
                    successCallback();
                }
            } catch (e) {
                clearTimeout(loadingTimeoutId);

                if (e instanceof DOMException && e.name === "AbortError") {
                    return;
                }

                state.value = "ERROR";
                data.value = null;
                error.value = e;
                console.error(e);
            }
        };

        watch(token, (value) => {
            if (value !== null) {
                sessionStorage.setItem("token", value);
            } else {
                sessionStorage.removeItem("token");
            }
        });

        tidyUpUrl(initData);

        fetchJoinOrCreate("/api/join-create/init", { requestData: { ...initData, token: token ?? undefined } });

        provide("fetchJoinOrCreate", fetchJoinOrCreate);
        provide("roomName", initData.roomName);
        provide("data", readonly(data));
        provide("error", readonly(error));

        return {
            state
        };
    },
    template: `
        <DisplayMeetingHasEndedView v-if="state === 'DISPLAY_MEETING_HAS_ENDED'" />
        <ConfigureMeetingView v-else-if="state === 'CONFIGURE_MEETING'" />
        <WaitUntilConfiguredView v-else-if="state === 'WAIT_UNTIL_CONFIGURED'" />
        <WaitUntilRunningView v-else-if="state === 'WAIT_UNTIL_RUNNING'" />
        <PromptLogInView v-else-if="state === 'PROMPT_LOG_IN'" />
        <PromptAccessCodeView v-else-if="state === 'PROMPT_ACCESS_CODE'" />
        <PromptJoinNameView v-else-if="state === 'PROMPT_JOIN_NAME'" />
        <RedirectView v-else-if="state === 'REDIRECT'" />
        <ErrorView v-else-if="state === 'ERROR'" />
        <LoadingComponent v-else />
    `
};
