const { createI18n } = VueI18n;
import en from "./translations/en.js";
import de from "./translations/de.js";

export const initI18n = (locale) => createI18n({
    legacy: false,
    locale,
    fallbackLocale: "en",
    messages: {
        en,
        de
    }
});
