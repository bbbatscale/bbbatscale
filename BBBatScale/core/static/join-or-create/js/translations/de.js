export default {
    eitherOr: {
        or: "oder"
    },
    loading: {
        srIndication: "laden..."
    },
    error: {
        header: {
            unexpectedError: "Etwas ist schiefgelaufen!",
            roomDoesNotExist: "Der Raum {name} existiert nicht!"
        },
        body: {
            unexpectedError: "Entschuldigen Sie, etwas ist schiefgelaufen. Bitte versuchen Sie es später erneut. Wenn das Problem weiterhin bestehen bleibt, schließen Sie bitte diesen Tab und öffnen einen neuen.",
            roomDoesNotExist: "Der Raum den Sie gesucht haben existiert nicht."
        }
    },
    meeting: {
        configure: {
            header: "Meeting {name} konfigurieren",
            body: "Das Meeting {name} starten. Die Einstellungen für dieses Meeting können nach Bedarf angepasst werden.",
            dropdownHeader: "Meeting konfigurieren",
            start: "Starten",
            or: {
                body: "Wenn Sie das Meeting nicht erstellen und auf eine andere Person warten wollen, die dieses startet, können Sie dem Warteraum beitreten.",
                button: "Warteraum betreten"
            }
        },
        ended: {
            header: "Das Meeting {name} wurde beendet",
            body: "Das Meeting {name} wurde in der Zwischenzeit beendet. Zum Erstellen/Beitreten eines neuen Meetings können Sie diese Seite neu laden."
        }
    },
    wait: {
        untilConfigured: {
            header: "Bitte warten Sie bis {name} von jemand anderem gestartet wurde",
            body: "Bitte warten Sie bis jemand anderes das Meeting gestartet hat.",
            bodyWithLogInHint: "@:wait.untilConfigured.body Wenn Sie versuchen das Meeting selbst zu starten, vergewissern Sie sich bitte, dass Sie angemeldet sind.",
            orCreate: "Meeting erstellen"
        },
        untilRunning: {
            startedBySomeoneElseInfo: "Das Meeting wurde durch jemand anderen gestartet.",
            header: "Das Meeting {name} startet",
            body: "Das Meeting startet. Sie werden weitergeleitet, sobald es bereit ist."
        }
    },
    join: {
        accessCode: {
            header: "Um dem Meeting {name} beizutreten wird ein Zugangscode benötigt",
            label: "Zugangscode",
            toggleVisibility: {
                show: "Zugangscode anzeigen",
                hide: "Zugangscode verbergen"
            },
            invalid: "Der übermittelte Zugangscode ist ungültig. Bitte versuchen Sie es erneut.",
            help: "Bitte geben Sie den Zugangscode ein, um dem Meeting beizutreten."
        },
        joinName: {
            header: "Einen Namen setzen welcher benutzt wird, um dem Meeting {name} beizutreten",
            placeholder: "Name",
            invalid: "Der Name ist zu kurz. Bitte geben Sie einen längeren ein.",
            help: "Bitte geben Sie Ihren Namen ein, um dem Meeting beizutreten."
        }
    },
    redirect: {
        header: {
            login: "Zum Login weiterleiten...",
            join: "Zu {name} weiterleiten..."
        },
        body: "Sie sollten in Kürze weitergeleitet werden. Wenn Sie nicht weitergeleitet werden, klicken Sie bitte den folgenden Link: {redirect}",
        manuelRedirectLinkText: {
            login: "einloggen",
            join: "Meeting beitreten"
        }
    },
    common: {
        button: {
            submit: "Absenden",
            close: "Schließen"
        }
    }
};
