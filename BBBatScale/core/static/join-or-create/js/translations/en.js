export default {
    eitherOr: {
        or: "or"
    },
    loading: {
        srIndication: "loading..."
    },
    error: {
        header: {
            unexpectedError: "Something went wrong!",
            roomDoesNotExist: "The room {name} does not exist!"
        },
        body: {
            unexpectedError: "Sorry, something went wrong. Please try again later. If the problem persists, please close this tab and open a new one.",
            roomDoesNotExist: "The room you were looking for does not exist."
        }
    },
    meeting: {
        configure: {
            header: "Configure the meeting {name}",
            body: "Start the meeting {name}. The settings for this meeting can be adjusted as needed.",
            dropdownHeader: "Configure meeting",
            start: "Start",
            or: {
                body: "If you do not want to create the meeting and wait for another person to start it, you can join the waiting room.",
                button: "Join waiting room"
            }
        },
        ended: {
            header: "The meeting {name} has been ended",
            body: "The meeting {name} has been ended in the meantime. To create/join a new meeting you can reload this page."
        }
    },
    wait: {
        untilConfigured: {
            header: "Please wait until {name} has been started by someone else",
            body: "Please wait until someone has started the meeting.",
            bodyWithLogInHint: "@:wait.untilConfigured.body If you are trying to start the meeting yourself, please make sure you are logged in.",
            orCreate: "Create meeting"
        },
        untilRunning: {
            startedBySomeoneElseInfo: "The meeting has been started by someone else.",
            header: "The meeting {name} is starting",
            body: "The meeting is starting. You will be redirected as soon as it is ready."
        }
    },
    join: {
        accessCode: {
            header: "To join the meeting {name} an access code is required",
            placeholder: "Access code",
            toggleVisibility: {
                show: "Show access code",
                hide: "Hide access code"
            },
            invalid: "The supplied access code is invalid. Please try again.",
            help: "Please enter the access code to join the meeting."
        },
        joinName: {
            header: "Set a name which will be used to join the meeting {name}",
            placeholder: "Name",
            invalid: "The name is too short. Please enter a longer one.",
            help: "Please enter your name to join the meeting."
        }
    },
    redirect: {
        header: {
            login: "Redirecting to login...",
            join: "Redirecting to {name}..."
        },
        body: "You should be redirected shortly. If you don't get redirected, please click the following link: {redirect}",
        manuelRedirectLinkText: {
            login: "log in",
            join: "join meeting"
        }
    },
    common: {
        button: {
            submit: "Submit",
            close: "Close"
        }
    }
};
