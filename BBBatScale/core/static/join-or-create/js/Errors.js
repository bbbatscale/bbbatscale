/**
 * @typedef {Object} ErrorResponseData
 * @property {"error"} type
 * @property {string} error
 * @property {string} reason
 */

export class ErrorResponse extends Error {
    /**
     * @param {number} status
     * @param {ErrorResponseData} response
     */
    constructor(status, response) {
        super(`${response.error}[status=${status}]: ${response.reason}`);
        this.status = status;
        this.errorType = response.error;
        this.reason = response.reason;
    }
}
