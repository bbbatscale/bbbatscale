from typing import Union

from core.models import Room
from core.utils import join_or_create_url
from django import template

register = template.Library()


@register.simple_tag(name="join_or_create_url")
def do_join_or_create_url(room: Union[Room, str]) -> str:
    return join_or_create_url(room)
