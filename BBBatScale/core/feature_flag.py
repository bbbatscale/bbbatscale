from abc import ABC, abstractmethod
from functools import wraps
from typing import TYPE_CHECKING, Any, Callable, Dict, Iterator, Tuple, Type

from django.conf import settings
from django.http import Http404, HttpRequest, HttpResponse
from django.utils.functional import SimpleLazyObject

if TYPE_CHECKING:
    from core.models import GeneralParameter
    from support_chat.models import SupportChatParameter


class FeatureNamespace(type):
    def __new__(mcs, name: str, bases: Tuple[type], attrs: Dict[str, Any], **kwargs: Any) -> Type["FeatureNamespace"]:
        super_new = super().__new__

        new_class = super_new(mcs, name, bases, attrs)
        new_class.__features = frozenset(
            attr for attr in attrs.values() if isinstance(attr, type) and issubclass(attr, FeatureFlag)
        )

        return new_class

    def __iter__(cls) -> Iterator[Type["FeatureFlag"]]:
        return iter(cls.__features)


class FeatureFlag(ABC):
    def __init__(self) -> None:
        self.is_enabled = self._is_enabled

    @classmethod
    @abstractmethod
    def from_request(cls, request: HttpRequest) -> "FeatureFlag":
        ...

    @abstractmethod
    def _is_enabled(self) -> bool:
        ...

    @classmethod
    def is_enabled(
        cls, view: Callable[[HttpRequest, Any, ...], HttpResponse]
    ) -> Callable[[HttpRequest, Any, Any], HttpResponse]:
        @wraps(view)
        def wrapper(request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
            if not cls.from_request(request).is_enabled():
                raise Http404

            return view(request, *args, **kwargs)

        return wrapper


class Feature(metaclass=FeatureNamespace):
    def __new__(cls, *args, **kwargs):
        raise NotImplementedError

    class HomeRooms(FeatureFlag):
        def __init__(self, general_parameter: "GeneralParameter") -> None:
            super().__init__()
            self._general_parameter = general_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.HomeRooms":
            from core.utils import get_general_parameter

            return cls(get_general_parameter(request))

        def _is_enabled(self) -> bool:
            return self._general_parameter.home_room_enabled

    class Jitsi(FeatureFlag):
        def __init__(self, general_parameter: "GeneralParameter") -> None:
            super().__init__()
            self._general_parameter = general_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.Jitsi":
            from core.utils import get_general_parameter

            return cls(get_general_parameter(request))

        def _is_enabled(self) -> bool:
            return self._general_parameter.jitsi_enable

    class Media(FeatureFlag):
        def __init__(self, general_parameter: "GeneralParameter") -> None:
            super().__init__()
            self._general_parameter = general_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.Media":
            from core.utils import get_general_parameter

            return cls(get_general_parameter(request))

        def _is_enabled(self) -> bool:
            return settings.BBBATSCALE_MEDIA_ENABLED and self._general_parameter.media_enabled

    class Occupancy(FeatureFlag):
        def __init__(self, general_parameter: "GeneralParameter") -> None:
            super().__init__()
            self._general_parameter = general_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.Occupancy":
            from core.utils import get_general_parameter

            return cls(get_general_parameter(request))

        def _is_enabled(self) -> bool:
            return self._general_parameter.enable_occupancy

    class PersonalRooms(FeatureFlag):
        def __init__(self, general_parameter: "GeneralParameter") -> None:
            super().__init__()
            self._general_parameter = general_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.PersonalRooms":
            from core.utils import get_general_parameter

            return cls(get_general_parameter(request))

        def _is_enabled(self) -> bool:
            return self._general_parameter.personal_rooms_enabled

    class Recordings(FeatureFlag):
        def __init__(self, general_parameter: "GeneralParameter") -> None:
            super().__init__()
            self._general_parameter = general_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.Recordings":
            from core.utils import get_general_parameter

            return cls(get_general_parameter(request))

        def _is_enabled(self) -> bool:
            return self._general_parameter.enable_recordings

    class Support(FeatureFlag):
        def __init__(self, support_chat_parameter: "SupportChatParameter") -> None:
            super().__init__()
            self._support_chat_parameter = support_chat_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.Support":
            from support_chat.utils import get_support_chat_parameter

            return cls(get_support_chat_parameter(request))

        def _is_enabled(self) -> bool:
            return self._support_chat_parameter.enabled

    class Webhooks(FeatureFlag):
        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.Webhooks":
            return cls()

        def _is_enabled(self) -> bool:
            return settings.WEBHOOKS_ENABLED

    class AdminUnmute(FeatureFlag):
        def __init__(self, general_parameter: "GeneralParameter") -> None:
            super().__init__()
            self._general_parameter = general_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.AdminUnmute":
            from core.utils import get_general_parameter

            return cls(get_general_parameter(request))

        def _is_enabled(self) -> bool:
            return self._general_parameter.enable_adminunmute

    class MeetingDialIn(FeatureFlag):
        def __init__(self, general_parameter: "GeneralParameter") -> None:
            super().__init__()
            self._general_parameter = general_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.MeetingDialIn":
            from core.utils import get_general_parameter

            return cls(get_general_parameter(request))

        def _is_enabled(self) -> bool:
            return self._general_parameter.enable_meeting_dial_in

    class MeetingRTMPStreaming(FeatureFlag):
        def __init__(self, general_parameter: "GeneralParameter") -> None:
            super().__init__()
            self._general_parameter = general_parameter

        @classmethod
        def from_request(cls, request: HttpRequest) -> "Feature.MeetingRTMPStreaming":
            from core.utils import get_general_parameter

            return cls(get_general_parameter(request))

        def _is_enabled(self) -> bool:
            return self._general_parameter.enable_meeting_rtmp_streaming


def feature_flag_context_preprocessor(request: HttpRequest) -> dict:
    context_features = dict()

    for feature in Feature:
        context_features[feature.__name__] = {"is_enabled": SimpleLazyObject(feature.from_request(request).is_enabled)}

    return {Feature.__name__: context_features}
