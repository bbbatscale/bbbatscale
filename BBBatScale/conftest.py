import itertools
from datetime import datetime, timezone
from json import JSONEncoder
from typing import TYPE_CHECKING, Any, AsyncIterator, Dict, Generic, Iterable, Iterator, List, Optional, TypeVar
from uuid import UUID

import pytest
from channels.db import database_sync_to_async
from core import constants
from core.constants import MEETING_STATE_RUNNING
from core.middleware import CurrentGeneralParameterMiddleware
from core.models import (
    ApiToken,
    ExternalMeeting,
    GeneralParameter,
    HomeRoom,
    Meeting,
    MeetingConfiguration,
    MeetingConfigurationTemplate,
    PersonalRoom,
    Room,
    SchedulingStrategy,
    Server,
    Slides,
    Theme,
    User,
)
from core.utils import load_moderator_group
from django.contrib.auth.models import AnonymousUser, Group
from django.contrib.sites.models import Site
from django.core import mail
from django.core.handlers.asgi import ASGIRequest
from django.core.handlers.wsgi import WSGIRequest
from django.core.mail import EmailMessage
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.test.client import MULTIPART_CONTENT
from django.test.client import AsyncRequestFactory as DjangoAsyncRequestFactory
from django.test.client import RequestFactory as DjangoRequestFactory
from faker import Faker
from oidc_authentication.models import AuthParameter
from support_chat.middleware import CurrentSupportChatParameterMiddleware
from support_chat.models import SupportChatParameter

Request = TypeVar("Request", WSGIRequest, ASGIRequest)


class _RequestFactoryMixin(Generic[Request]):
    def __init__(
        self,
        tenant: Site,
        *,
        json_encoder: JSONEncoder = DjangoJSONEncoder,
        general_parameter: Optional[GeneralParameter] = None,
        support_chat_parameter: Optional[SupportChatParameter] = None,
        user: Optional[User] = None,
        remote_address: str = "127.0.0.1",
        **defaults: Any,
    ):
        self.__check_defaults(defaults)
        super().__init__(json_encoder=json_encoder, **defaults)

        self.tenant = tenant
        self.general_parameter = general_parameter
        self.support_chat_parameter = support_chat_parameter
        self.user = user
        self.remote_address = remote_address

    @classmethod
    def __check_defaults(cls, defaults: Dict[str, Any]) -> None:
        pass

    def request(self, **request: Any) -> Request:
        _request = super().request(**request)

        _request.tenant = self.tenant

        if self.general_parameter is not None:
            _request.general_parameter = self.general_parameter
        else:
            CurrentGeneralParameterMiddleware(lambda _: HttpResponse())(_request)

        if self.support_chat_parameter is not None:
            _request.support_chat_parameter = self.support_chat_parameter
        else:
            CurrentSupportChatParameterMiddleware(lambda _: HttpResponse())(_request)

        if self.user is not None:
            _request.user = self.user
        else:
            _request.user = AnonymousUser()

        return _request

    if TYPE_CHECKING:  # noqa: C901

        def get(self, path: str, data: Optional[Any] = None, secure: bool = False, **extra: Any) -> Request:
            ...

        def post(
            self,
            path: str,
            data: Optional[Any] = None,
            content_type: str = MULTIPART_CONTENT,
            secure: bool = False,
            **extra: Any,
        ) -> Request:
            ...

        def head(self, path: str, data: Optional[Any] = None, secure: bool = False, **extra: Any) -> Request:
            ...

        def trace(self, path: str, secure: bool = False, **extra: Any) -> Request:
            ...

        def options(
            self,
            path: str,
            data: Any = "",
            content_type: str = "application/octet-stream",
            secure: bool = False,
            **extra: Any,
        ) -> Request:
            ...

        def put(
            self,
            path: str,
            data: Any = "",
            content_type: str = "application/octet-stream",
            secure: bool = False,
            **extra: Any,
        ) -> Request:
            ...

        def patch(
            self,
            path: str,
            data: Any = "",
            content_type: str = "application/octet-stream",
            secure: bool = False,
            **extra: Any,
        ) -> Request:
            ...

        def delete(
            self,
            path: str,
            data: Any = "",
            content_type: str = "application/octet-stream",
            secure: bool = False,
            **extra: Any,
        ) -> Request:
            ...

        def generic(
            self,
            method: str,
            path: str,
            data: Any = "",
            content_type: str = "application/octet-stream",
            secure: bool = False,
            **extra: Any,
        ) -> Request:
            ...


class RequestFactory(_RequestFactoryMixin[WSGIRequest], DjangoRequestFactory):
    def _base_environ(self, **request: Any) -> Dict[str, Any]:
        if "SERVER_NAME" in self.defaults or "SERVER_NAME" in request:
            raise ValueError("The 'SERVER_NAME' is not allowed to be passed as a request parameter.")

        return super()._base_environ(**request, REMOTE_ADDR=self.remote_address, SERVER_NAME=self.tenant.domain)


class AsyncRequestFactory(_RequestFactoryMixin[ASGIRequest], DjangoAsyncRequestFactory):
    @classmethod
    def __check_defaults(cls, defaults: Dict[str, Any]) -> None:
        if "server" in defaults:
            raise ValueError("The 'server' is not allowed to be passed as a default parameter.")

    def _base_scope(self, **request: Any) -> Dict[str, Any]:
        _, port = request.pop("server", (None, "80"))
        request["server"] = (self.tenant.domain, port)

        return super()._base_scope(**request, client=[self.remote_address, 0])

    def request(self, **request) -> ASGIRequest:
        headers = [(header, value) for header, value in request["headers"] if header != b"host"]
        headers.insert(0, (b"host", self.tenant.domain))
        return super().request(**request)


class DictAlsoCheckingType(dict):
    """
    A normal dictionary except that __eq__ also checks that all types match.
    This is useful to ensure that `assert dict1 == dict2` works more like JS's === operator.
    Only the left hand side of the assertion must be made into an object of this class to work.
    Only the right hand side would not work.

    Examples:
        >>> dict(key=1) == dict(key=1)
        True
        >>> dict(key=True) == dict(key=1)
        True

        >>> DictAlsoCheckingType(key=1) == dict(key=1)
        True
        >>> DictAlsoCheckingType(key=True) == dict(key=1)
        False

        >>> DictAlsoCheckingType(dict(key=1)) == dict(key=1)
        True
        >>> DictAlsoCheckingType(dict(key=True)) == dict(key=1)
        False
    """

    def __eq__(self, other: Any) -> bool:
        equal = super().__eq__(other)
        if not equal:
            return False

        for key, value in self.items():
            if type(value) != type(other[key]):  # noqa: E721
                return False

        return True


def product_kwargs(**kwargs: Iterable[Any]) -> Iterator[Dict[str, Any]]:
    """
    Like `itertools.product` but for kwargs.
    """
    inflated_kwargs = (tuple((key, value) for value in values) for key, values in kwargs.items())
    product = itertools.product(*inflated_kwargs)
    return (dict(entries) for entries in product)


@pytest.fixture(scope="function")
def testserver_tenant(db) -> Site:
    return Site.objects.create(name="testserver", domain="testserver")


@pytest.fixture(scope="function")
def testserver_tenant_auth_parameter(db, testserver_tenant) -> AuthParameter:
    return AuthParameter.objects.create(
        tenant=testserver_tenant,
        client_id="example",
        client_secret="example-secret",
        authorization_endpoint="auth-example.org",
        token_endpoint="token-example.org",
        user_endpoint="user-example.org",
        jwks_endpoint="jwks-example.org",
        end_session_endpoint="end-session-example.org",
    )


@pytest.fixture(scope="function")
def testserver2_tenant(db) -> Site:
    return Site.objects.create(domain="testserver2", name="testserver2")


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def async_example_tenant(db) -> AsyncIterator[Site]:
    tenant = await database_sync_to_async(Site.objects.create)(name="example.org", domain="example.org")
    yield tenant
    await database_sync_to_async(tenant.delete)()


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def async_other_example_tenant(db) -> AsyncIterator[Site]:
    tenant = await database_sync_to_async(Site.objects.create)(name="other.example.org", domain="other.example.org")
    yield tenant
    await database_sync_to_async(tenant.delete)()


@pytest.fixture(scope="function")
def gp_test_tenant(
    db, testserver_tenant, example_scheduling_strategy, example_meeting_configuration_template
) -> GeneralParameter:
    return GeneralParameter.objects.update_or_create(
        tenant=testserver_tenant,
        defaults={
            "default_theme_id": 1,
            "home_room_enabled": True,
            "home_room_scheduling_strategy": example_scheduling_strategy,
            "home_room_room_configuration": example_meeting_configuration_template,
            "recording_key": "asdf",
            "recording_cert": "asdf",
            "recording_management_url": "asdf",
            "user_secrete": "asdf",
        },
    )[0]


@pytest.mark.django_db
@pytest.fixture(scope="function")
def gp_test_tenant_media_upload_disabled(
    testserver_tenant: Site,
    example_scheduling_strategy: SchedulingStrategy,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> GeneralParameter:
    return GeneralParameter.objects.create(
        tenant=testserver_tenant,
        default_theme_id=1,
        home_room_enabled=True,
        home_room_scheduling_strategy=example_scheduling_strategy,
        home_room_room_configuration=example_meeting_configuration_template,
        recording_key="asdf",
        recording_cert="asdf",
        recording_management_url="asdf",
        media_enabled=False,
    )


@pytest.mark.django_db
@pytest.fixture(scope="function")
def gp_test_tenant_media_upload_enabled(
    testserver_tenant: Site,
    example_scheduling_strategy: SchedulingStrategy,
    example_meeting_configuration_template: MeetingConfigurationTemplate,
) -> GeneralParameter:
    return GeneralParameter.objects.create(
        tenant=testserver_tenant,
        default_theme_id=1,
        home_room_enabled=True,
        home_room_scheduling_strategy=example_scheduling_strategy,
        home_room_room_configuration=example_meeting_configuration_template,
        recording_key="asdf",
        recording_cert="asdf",
        recording_management_url="asdf",
        media_enabled=True,
    )


@pytest.fixture(scope="function")
def example_scheduling_strategy(db, testserver_tenant) -> SchedulingStrategy:
    scheduling_strategy = SchedulingStrategy.objects.create(name="example")
    scheduling_strategy.tenants.set([testserver_tenant])
    return scheduling_strategy


@pytest.mark.django_db
@pytest.fixture(scope="function")
def example_api_token(testserver_tenant: Site, example_scheduling_strategy: SchedulingStrategy) -> ApiToken:
    api_token = ApiToken.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        secret="0123456789ABCDEF",
        name="api-token-name",
        slug="api-token-slug",
    )
    api_token.tenants.add(testserver_tenant)
    return api_token


@pytest.fixture(scope="function")
def example_meeting_configuration_template(db, testserver_tenant) -> MeetingConfigurationTemplate:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(name="Example Config")
    meeting_configuration_template.tenants.set([testserver_tenant])
    return meeting_configuration_template


@pytest.fixture(scope="function")
def example_superuser(db, testserver_tenant) -> User:
    return User.objects.create_user(
        "example-admin", "example-admin@example.org", "EXAMPLEPASSWORD", tenant=testserver_tenant, is_superuser=True
    )


@pytest.fixture(scope="function")
def example_staff_user(db, testserver_tenant) -> User:
    return User.objects.create_user(
        "example-staff", "example-staff@example.org", "EXAMPLEPASSWORD", tenant=testserver_tenant, is_staff=True
    )


@pytest.fixture(scope="function")
def example_moderator_user(db, testserver_tenant, moderator_group) -> User:
    example_moderator_user = User.objects.create_user(
        "example-moderator", "example-moderator@example.org", "EXAMPLEPASSWORD", tenant=testserver_tenant
    )
    example_moderator_user.groups.add(moderator_group)
    example_moderator_user.save()
    return example_moderator_user


@pytest.fixture(scope="function")
def example_user(db, testserver_tenant) -> User:
    return User.objects.create_user(
        "example-user",
        "example-user@example.org",
        "EXAMPLEPASSWORD",
        tenant=testserver_tenant,
        is_staff=False,
        is_superuser=False,
        first_name="Example",
        last_name="User",
        display_name="example_user",
    )


@pytest.fixture
def anonymous_user() -> AnonymousUser:
    return AnonymousUser()


@pytest.fixture(scope="function")
def example_server(db, example_scheduling_strategy) -> Server:
    return Server.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        dns="example.org",
        shared_secret="123456789",
        state=constants.SERVER_STATE_UP,
    )


@pytest.fixture(scope="function")
def example_room(
    db, example_scheduling_strategy, example_server, testserver_tenant, example_meeting_configuration_template
) -> Room:
    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="room",
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])
    return room


@pytest.fixture(scope="function")
def example_personal_room(
    db, example_superuser, example_scheduling_strategy, testserver_tenant, example_meeting_configuration_template
) -> PersonalRoom:
    p_room = PersonalRoom.objects.create(
        name="Example PersonalRoom",
        owner=example_superuser,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    p_room.tenants.set([testserver_tenant])
    return p_room


@pytest.fixture(scope="function")
def example_homeroom(db, example_moderator_user) -> HomeRoom:
    return example_moderator_user.homeroom


@pytest.fixture(scope="function")
def example_meeting(example_room, example_server, example_meeting_configuration):
    Faker.seed(1337)
    return Meeting.objects.create(
        room=example_room,
        room_name=example_room.name,
        server=example_server,
        id=UUID(Faker().uuid4()),
        state=MEETING_STATE_RUNNING,
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        configuration=example_meeting_configuration,
    )


@pytest.fixture(scope="function")
@pytest.mark.djang_db
def external_meeting(example_server: Server) -> Meeting:
    external_meeting = ExternalMeeting.objects.create(
        name="external-meeting", external_meeting_id="external-meeting-id"
    )

    return Meeting.objects.create(
        room_name="external-meeting-room-name",
        external_meeting=external_meeting,
        server=example_server,
        id=UUID(Faker().uuid4()),
        state=MEETING_STATE_RUNNING,
    )


@pytest.fixture(scope="function")
def example_home_room_meeting(example_homeroom, example_server, example_meeting_configuration):
    Faker.seed(1337)
    return Meeting.objects.create(
        room=example_homeroom,
        room_name=example_homeroom.name,
        server=example_server,
        id=Faker().uuid4(),
        state=MEETING_STATE_RUNNING,
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        configuration=example_meeting_configuration,
    )


@pytest.fixture
def current_timestamp():
    return datetime(2022, 1, 2, 3, 4, 5, tzinfo=timezone.utc)


@pytest.fixture(scope="function")
def example_group(db) -> Group:
    return Group.objects.create(name="Example group")


@pytest.mark.django_db
@pytest.fixture
def example_slides(testserver_tenant: Site) -> Slides:
    return Slides.objects.create(name="example_slides", tenant=testserver_tenant)


@pytest.mark.django_db
@pytest.fixture
def example_slides_with_owner(testserver_tenant: Site, example_staff_user: User) -> Slides:
    return Slides.objects.create(name="example_slides_with_owner", tenant=testserver_tenant, owner=example_staff_user)


@pytest.fixture
def example_theme() -> Theme:
    return Theme.objects.create(
        name="example_theme",
    )


@pytest.mark.django_db
@pytest.fixture
def moderator_group(testserver_tenant: Site) -> Group:
    return load_moderator_group(testserver_tenant)


@pytest.fixture(scope="function")
def example_meeting_configuration(db) -> MeetingConfiguration:
    return MeetingConfiguration.objects.create()


@pytest.fixture
def outbox() -> List[EmailMessage]:
    mail.outbox.clear()
    return mail.outbox
