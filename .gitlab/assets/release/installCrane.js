"use strict";

const fetch = import("node-fetch").then((module) => module.default);
const tar = require("tar");
const semver = require("semver");

async function getRelease(versionRange) {
    async function findRelease(url) {
        const response = await (await fetch)(url);
        const releases = await response.json();

        const release = releases.find(release => semver.satisfies(release.name, versionRange));
        if (release) {
            return release;
        }

        const nextLink = /<([^>]+)>; rel="next"/g.exec(response.headers.get("link") ?? "")?.[1];
        if (nextLink) {
            return findRelease(nextLink);
        }

        return undefined;
    }

    const release = await findRelease("https://api.github.com/repos/google/go-containerregistry/releases");
    if (!release) {
        throw new Error(`Unable to find release matching the version range: ${versionRange}`);
    }

    return release;
}

function getAssetUrl(release) {
    const asset = release.assets.find(
        (asset) => asset.name === "go-containerregistry_Linux_x86_64.tar.gz"
    );

    if (!asset) {
        throw new Error("Unable to find linux-x86-64 release asset");
    }

    return asset.browser_download_url;
}

async function installCrane() {
    if (process.argv.length > 3) {
        throw new Error(
            "There should be at most only one argument, the version to install" +
            " or if not present default to the latest version." +
            " The version must be supplied as semver range."
        );
    }

    const release = await getRelease(process.argv[2] ?? "*");
    const assetUrl = getAssetUrl(release);

    console.log(`Installing crane version ${semver.clean(release.name)}.`)

    const downloadResponse = await (await fetch)(assetUrl);
    downloadResponse.body.pipe(tar.extract({ cwd: "/usr/local/bin" }, ["crane"]));
}

installCrane().catch((error) => {
    console.error(error);
    process.exit(1);
});
